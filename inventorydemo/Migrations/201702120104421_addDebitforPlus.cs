namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDebitforPlus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderAccountsforSubAccountPurchases", "DebitforPlus", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountPurchases", "DebitforPlus", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderAccountsforSubAccountSales", "DebitforPlus", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountSales", "DebitforPlus", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderOtherTotalAccountSales", "DebitforPlus");
            DropColumn("dbo.OrderAccountsforSubAccountSales", "DebitforPlus");
            DropColumn("dbo.OrderOtherTotalAccountPurchases", "DebitforPlus");
            DropColumn("dbo.OrderAccountsforSubAccountPurchases", "DebitforPlus");
        }
    }
}
