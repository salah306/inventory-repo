namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordertypetoOrderName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrdersNames", "Ordertype", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrdersNames", "Ordertype");
        }
    }
}
