namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billsp2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubAccountMovements", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.SubAccountMovements", "SalesBillId", "dbo.SalesBills");
            DropIndex("dbo.SubAccountMovements", new[] { "SalesBillId" });
            DropIndex("dbo.SubAccountMovements", new[] { "PurchasesBillId" });
            DropColumn("dbo.SubAccountMovements", "SalesBillId");
            DropColumn("dbo.SubAccountMovements", "PurchasesBillId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAccountMovements", "PurchasesBillId", c => c.Int());
            AddColumn("dbo.SubAccountMovements", "SalesBillId", c => c.Int());
            CreateIndex("dbo.SubAccountMovements", "PurchasesBillId");
            CreateIndex("dbo.SubAccountMovements", "SalesBillId");
            AddForeignKey("dbo.SubAccountMovements", "SalesBillId", "dbo.SalesBills", "SalesBillId");
            AddForeignKey("dbo.SubAccountMovements", "PurchasesBillId", "dbo.PurchasesBills", "PurchasesBillId");
        }
    }
}
