namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountsCatogeryTypesAcccountsCreated3 : DbMigration
    {
        public override void Up()
        {
            //DropColumn("dbo.AccountsCatogeryTypesAcccounts", "Id");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
           // DropPrimaryKey("dbo.AccountsCatogeryTypesAcccounts");
           // AddColumn("dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId", c => c.Int(nullable: false, identity: true));
           // AddPrimaryKey("dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId");
            //AddForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId", cascadeDelete: true);
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountsCatogeryTypesAcccounts", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropPrimaryKey("dbo.AccountsCatogeryTypesAcccounts");
            DropColumn("dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId");
            AddPrimaryKey("dbo.AccountsCatogeryTypesAcccounts", "Id");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts", "Id", cascadeDelete: true);
        }
    }
}
