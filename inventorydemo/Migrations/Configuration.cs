﻿namespace Inventory.Migrations
{
    using Models;
    using Inventory.DataLayer;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {


            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "مخازن" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "عملاء" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "موردين" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "خزنة" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "بنك" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "موظفين" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "اخري" });


            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "عدد طبيعي" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "عدد عشري" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "بريد الكتروني" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "هاتف محمول" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "هاتف ارضي" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "ملاحظات" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "عنوان" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "صور - مستندات" });


            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "عدد صحيح" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "عدد عشري" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "نص" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "تاريخ" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "تاريخ - اشعار" });


            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "عدد صحيح" });
            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "عدد عشري" });

            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "صنف" , show = false , description = "" , Value = ""  , Required = true , orderNum = 1 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "كود", show = false, description = "", Value = "", Required = true, orderNum = 2 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "سعر", show = false, description = "", Value = "", Required = true, orderNum = 3 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "لون", show = true, description = "", Value = "red", Required = false});
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "نص", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "عدد صحيح", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "عدد عشري", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "مدة صلاحية", show = true, description = "باليوم", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "باركود", show = true, description = "باركود اضافي", Value = "", Required = false });

            // AccountsCatogeryDataTypes
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "الشركة",
                name = "Company",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع تغير البيانات" , name = "CanEdit"},

                                }
            });

         
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "الفروع",
                name = "Branch",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع تغير البيانات" , name = "CanEdit"},

                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "دليل الحسابات",
                name = "AccountsSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                     new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد حسابات المستوي الاول",
                name = "BalanceSheetSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                     new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد حسابات المستوي الثاني",
                name = "BalanceSheetTypesSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                     new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد حسابات الاستاذ",
                name = "AccountCategoriesSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                     new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد القوائم المالية",
                name = "FstatementSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد الاذون",
                name = "OrdersSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "اعداد المواد والسلع",
                name = "ItemsSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "لوحة المتابعة",
                name = "Dashboard",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "دفتر اليومية",
                name = "GeneralJournal",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاستخدام" , name = "CanUse"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "الاستاذ المساعد",
                name = "SubGl",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاستخدام" , name = "CanUse"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "الاستاذ العام",
                name = "Gl",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "ميزان المراجعة",
                name = "TrialBalance",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "القوائم المالية",
                name = "fStatements",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "الاذون والفواتير",
                name = "Orders",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع التعديل" , name = "CanEdit"},
                                    new settingRole() {roleName = "يستطيع الحذف" , name = "CanDelete"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "كارتة صنف",
                name = "InventoryCards",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "الاطلاع علي الكمية فقط" , name = "CanViewQty"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"}
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "مخازن",
                name = "Inventories",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "نحويلات داخلية",
                name = "InventoriesTransfer",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "يستطيع الاضافة" , name = "CanAdd"},
                                    new settingRole() {roleName = "يستطيع الاطلاع" , name = "CanView"},
                                }
            });

            context.SaveChanges();

            context.AccountOrderProperties.AddOrUpdate(co => co.AccountOrderPropertiesName, new AccountOrderProperties { AccountOrderPropertiesName = "مدين" });
            context.AccountOrderProperties.AddOrUpdate(co => co.AccountOrderPropertiesName, new AccountOrderProperties { AccountOrderPropertiesName = "دائن" });
            context.SaveChanges();

            var companymanger = new CompanyUserManager();

            List<CompanyRole> rolesCompany = new List<CompanyRole>();

            rolesCompany.Add(companymanger.AddCompanyRole("Company Admin"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Create Company"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Company"));
       


            rolesCompany.Add(companymanger.AddCompanyRole("Branch Admin"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Create Branch"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Branch"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can View Branch"));

            rolesCompany.Add(companymanger.AddCompanyRole("Can Create Accounts"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Accounts"));

            rolesCompany.Add(companymanger.AddCompanyRole("Can Create Financial Statements"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Financial statements"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can View Financial statements"));


            rolesCompany.Add(companymanger.AddCompanyRole("Can Invite User"));
            rolesCompany.Add(companymanger.AddCompanyRole("Can Lock User"));

   


        }
    }
}



