namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordernum2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "orderNum", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValuesforItemGroups", "orderNum");
        }
    }
}
