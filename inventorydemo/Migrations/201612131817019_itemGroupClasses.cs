namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountsforItemGroups",
                c => new
                    {
                        AccountsforItemGroupId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        ItemGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountsforItemGroupId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.ItemGroupId);
            
            CreateTable(
                "dbo.ItemGroups",
                c => new
                    {
                        ItemGroupId = c.Int(nullable: false, identity: true),
                        ItemGroupName = c.String(),
                        UnitId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ItemGroupId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: false)
                .Index(t => t.UnitId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PropertiesforItemGroups",
                c => new
                    {
                        PropertiesforItemGroupId = c.Int(nullable: false, identity: true),
                        PropertiesforItemGroupName = c.String(),
                        TypeId = c.Int(nullable: false),
                        Type_TypeforItemGroupId = c.Int(),
                        ItemGroup_ItemGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.PropertiesforItemGroupId)
                .ForeignKey("dbo.TypeforItemGroups", t => t.Type_TypeforItemGroupId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroup_ItemGroupId)
                .Index(t => t.Type_TypeforItemGroupId)
                .Index(t => t.ItemGroup_ItemGroupId);
            
            CreateTable(
                "dbo.PropertyValuesforItemGroups",
                c => new
                    {
                        PropertyValuesforItemGroupId = c.Int(nullable: false, identity: true),
                        PropertyValuesforItemGroupName = c.String(),
                        PropertyValuesforItemGroupValue = c.String(),
                        PropertiesforItemGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PropertyValuesforItemGroupId)
                .ForeignKey("dbo.PropertiesforItemGroups", t => t.PropertiesforItemGroupId, cascadeDelete: false)
                .Index(t => t.PropertiesforItemGroupId);
            
            CreateTable(
                "dbo.TypeforItemGroups",
                c => new
                    {
                        TypeforItemGroupId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                        show = c.Boolean(nullable: false),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.TypeforItemGroupId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        UnitId = c.Int(nullable: false, identity: true),
                        UnitName = c.String(),
                        UnitTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UnitId)
                .ForeignKey("dbo.UnitTypes", t => t.UnitTypeId, cascadeDelete: false)
                .Index(t => t.UnitTypeId);
            
            CreateTable(
                "dbo.UnitTypes",
                c => new
                    {
                        UnitTypeId = c.Int(nullable: false, identity: true),
                        UnitTypeName = c.String(),
                    })
                .PrimaryKey(t => t.UnitTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ItemGroups", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Units", "UnitTypeId", "dbo.UnitTypes");
            DropForeignKey("dbo.PropertiesforItemGroups", "ItemGroup_ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.PropertiesforItemGroups", "Type_TypeforItemGroupId", "dbo.TypeforItemGroups");
            DropForeignKey("dbo.PropertyValuesforItemGroups", "PropertiesforItemGroupId", "dbo.PropertiesforItemGroups");
            DropForeignKey("dbo.ItemGroups", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.AccountsforItemGroups", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.AccountsforItemGroups", "AccountId", "dbo.Accounts");
            DropIndex("dbo.Units", new[] { "UnitTypeId" });
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "PropertiesforItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "ItemGroup_ItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "Type_TypeforItemGroupId" });
            DropIndex("dbo.ItemGroups", new[] { "CompanyId" });
            DropIndex("dbo.ItemGroups", new[] { "UnitId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "ItemGroupId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "AccountId" });
            DropTable("dbo.UnitTypes");
            DropTable("dbo.Units");
            DropTable("dbo.TypeforItemGroups");
            DropTable("dbo.PropertyValuesforItemGroups");
            DropTable("dbo.PropertiesforItemGroups");
            DropTable("dbo.ItemGroups");
            DropTable("dbo.AccountsforItemGroups");
        }
    }
}
