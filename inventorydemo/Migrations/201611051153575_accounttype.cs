namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accounttype : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Accounts", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.Accounts", new[] { "AccountTypeId" });
            RenameColumn(table: "dbo.Accounts", name: "AccountTypeId", newName: "AccountType_AccountTypeId");
            AddColumn("dbo.AccountCategories", "AccountTypeId", c => c.Int());
            AlterColumn("dbo.Accounts", "AccountType_AccountTypeId", c => c.Int());
            CreateIndex("dbo.AccountCategories", "AccountTypeId");
            CreateIndex("dbo.Accounts", "AccountType_AccountTypeId");
            AddForeignKey("dbo.AccountCategories", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
            AddForeignKey("dbo.Accounts", "AccountType_AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "AccountType_AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountCategories", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.Accounts", new[] { "AccountType_AccountTypeId" });
            DropIndex("dbo.AccountCategories", new[] { "AccountTypeId" });
            AlterColumn("dbo.Accounts", "AccountType_AccountTypeId", c => c.Int(nullable: false));
            DropColumn("dbo.AccountCategories", "AccountTypeId");
            RenameColumn(table: "dbo.Accounts", name: "AccountType_AccountTypeId", newName: "AccountTypeId");
            CreateIndex("dbo.Accounts", "AccountTypeId");
            AddForeignKey("dbo.Accounts", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId", cascadeDelete: true);
        }
    }
}
