namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customAccounts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountCategories",
                c => new
                    {
                        AccountCategoryId = c.Int(nullable: false, identity: true),
                        AccountCategoryName = c.String(nullable: false, maxLength: 200),
                        AccountCategoryCode = c.String(),
                        BalanceSheetTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountCategoryId)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.BalanceSheetTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => new { t.AccountCategoryName, t.BalanceSheetTypeId }, unique: true, name: "AK_AccountCategory_AccountCategoryName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountCategoryProperties",
                c => new
                    {
                        AccountCategoryPropertiesId = c.Int(nullable: false, identity: true),
                        AccountCategoryPropertiesName = c.String(nullable: false, maxLength: 80),
                        AccountCategoryPropertiesValue = c.String(),
                        AccountCategoryId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        DataTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountCategoryPropertiesId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AccountsDataTypes", t => t.DataTypeId, cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.DataTypeId);
            
            CreateTable(
                "dbo.AccountCategoryPropertiesValues",
                c => new
                    {
                        AccountCategoryPropertiesValueId = c.Int(nullable: false, identity: true),
                        AccountCategoryPropertiesValueName = c.String(maxLength: 80),
                        AccountCategoryPropertiesId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountCategoryPropertiesValueId)
                .ForeignKey("dbo.AccountCategoryProperties", t => t.AccountCategoryPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountCategoryPropertiesId)
                .Index(t => t.AccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        AccountName = c.String(nullable: false, maxLength: 200),
                        AccountAliasName = c.String(maxLength: 200),
                        AccountCode = c.String(),
                        TotalDebit = c.Decimal(precision: 18, scale: 2),
                        TotalCrdit = c.Decimal(precision: 18, scale: 2),
                        AccountCategoryId = c.Int(nullable: false),
                        AccountTypeId = c.Int(nullable: false),
                        CustomAccountCategoryId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountName, name: "AK_Accounts_AccountName")
                .Index(t => t.AccountCategoryId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountMovements",
                c => new
                    {
                        AccountMovementId = c.Int(nullable: false, identity: true),
                        AccountMovementCode = c.String(),
                        AccountMovementDate = c.DateTime(nullable: false),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Crdit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountMovementNote = c.String(maxLength: 200),
                        AccountId = c.Int(nullable: false),
                        AccountOrderId = c.Int(nullable: false),
                        AccountorderHasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        Branch_BranchId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountMovementId)
                .ForeignKey("dbo.AccountOrders", t => t.AccountOrderId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.Branch_BranchId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountId)
                .Index(t => t.AccountOrderId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.Branch_BranchId);
            
            CreateTable(
                "dbo.AccountOrders",
                c => new
                    {
                        AccountOrderId = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        OrderNote = c.String(),
                        HasDone = c.Boolean(),
                        AccountOrderPropertiesId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountOrderCode = c.String(),
                        BranchId = c.Int(),
                        OrderNo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountOrderId)
                .ForeignKey("dbo.AccountOrderProperties", t => t.AccountOrderPropertiesId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountOrderPropertiesId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId);
            
            CreateTable(
                "dbo.AccountOrderProperties",
                c => new
                    {
                        AccountOrderPropertiesId = c.Int(nullable: false, identity: true),
                        AccountOrderPropertiesName = c.String(nullable: false, maxLength: 200),
                        AccountOrderPropertiesCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountOrderPropertiesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Occupation = c.String(),
                        Phone = c.String(),
                        About = c.String(),
                        ImageUrl = c.String(),
                        JoinDate = c.DateTime(nullable: false),
                        Level = c.Byte(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ApplicationUserComapnies",
                c => new
                    {
                        ApplicationUserComapnyId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(),
                        ApplicationUserId = c.String(maxLength: 128),
                        UserAcceptedPolicy = c.Boolean(),
                        Locked = c.Boolean(),
                    })
                .PrimaryKey(t => t.ApplicationUserComapnyId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId)
                .Index(t => t.ApplicationUserId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false, maxLength: 200),
                        CompanyCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyName, unique: true, name: "AK_Company_CompanyName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        BranchId = c.Int(nullable: false, identity: true),
                        BranchName = c.String(nullable: false, maxLength: 200),
                        BranchCode = c.String(),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => new { t.BranchName, t.CompanyId }, unique: true, name: "AK_Branch_BranchName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BalanceSheets",
                c => new
                    {
                        BalanceSheetId = c.Int(nullable: false, identity: true),
                        BalanceSheetName = c.String(nullable: false, maxLength: 200),
                        BalanceSheetCode = c.String(),
                        BranchId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BalanceSheetId)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => new { t.BalanceSheetName, t.BranchId }, unique: true, name: "AK_BalanceSheet_BalanceSheetName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BalanceSheetTypes",
                c => new
                    {
                        BalanceSheetTypeId = c.Int(nullable: false, identity: true),
                        BalanceSheetTypeName = c.String(),
                        BalanceSheetTypeCode = c.String(),
                        BalanceSheetId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BalanceSheetTypeId)
                .ForeignKey("dbo.BalanceSheets", t => t.BalanceSheetId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BalanceSheetId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Warehouses",
                c => new
                    {
                        WarehouseId = c.Int(nullable: false, identity: true),
                        WarehouseName = c.String(nullable: false, maxLength: 200),
                        BranchId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.WarehouseId)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.WarehouseName, unique: true, name: "AK_WarehouseName_WarehouseName")
                .Index(t => t.BranchId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        InventoryId = c.Int(nullable: false, identity: true),
                        InventoryInput = c.Decimal(precision: 18, scale: 2),
                        InventoryOutput = c.Decimal(precision: 18, scale: 2),
                        InventoryExpiryDate = c.DateTime(),
                        InventoryNotes = c.String(maxLength: 200),
                        ItemId = c.Int(nullable: false),
                        WarehouseId = c.Int(nullable: false),
                        AccountOrderId = c.Int(nullable: false),
                        InventoryOrderId = c.Int(nullable: false),
                        InventoryInvoiceId = c.Int(nullable: false),
                        AccountOrderhasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryId)
                .ForeignKey("dbo.AccountOrders", t => t.AccountOrderId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.InventoryInvoices", t => t.InventoryInvoiceId, cascadeDelete: false)
                .ForeignKey("dbo.InventoryOrders", t => t.InventoryOrderId, cascadeDelete: false)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseId, cascadeDelete: false)
                .Index(t => t.ItemId)
                .Index(t => t.WarehouseId)
                .Index(t => t.AccountOrderId)
                .Index(t => t.InventoryOrderId)
                .Index(t => t.InventoryInvoiceId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoryInvoices",
                c => new
                    {
                        InventoryInvoiceId = c.Int(nullable: false, identity: true),
                        InventoryInvoiceDate = c.DateTime(nullable: false),
                        InventoryInvoiceCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryInvoiceId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoryOrders",
                c => new
                    {
                        InventoryOrderId = c.Int(nullable: false, identity: true),
                        InventoryOrderDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryOrderId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        ItemName = c.String(nullable: false, maxLength: 150),
                        HasExpiryDate = c.Boolean(nullable: false),
                        Availability = c.Boolean(nullable: false),
                        ItemCategoryId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemCategories", t => t.ItemCategoryId, cascadeDelete: false)
                .Index(t => t.ItemName, unique: true, name: "AK_Item_ItemName")
                .Index(t => t.ItemCategoryId)
                .Index(t => t.CompanyId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.ItemCategories",
                c => new
                    {
                        ItemCategoryId = c.Int(nullable: false, identity: true),
                        ItemCategoryName = c.String(nullable: false, maxLength: 80),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.ItemCategoryName, unique: true, name: "AK_ItemCategory_ItemCategoryName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.ItemCategoryProperties",
                c => new
                    {
                        ItemCategoryPropertiesId = c.Int(nullable: false, identity: true),
                        ItemCategoryPropertiesName = c.String(nullable: false, maxLength: 150),
                        ItemCategoryId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryPropertiesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemCategories", t => t.ItemCategoryId, cascadeDelete: false)
                .Index(t => t.ItemCategoryPropertiesName, name: "AK_ItemCategoryProperties_ItemCategoryPropertiesName")
                .Index(t => t.ItemCategoryId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.ItemCategoryPropertiesValues",
                c => new
                    {
                        ItemCategoryPropertiesValuesId = c.Int(nullable: false, identity: true),
                        ItemCategoryPropertiesValuesName = c.String(maxLength: 150),
                        ItemCategoryPropertiesId = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryPropertiesValuesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: false)
                .ForeignKey("dbo.ItemCategoryProperties", t => t.ItemCategoryPropertiesId, cascadeDelete: false)
                .Index(t => t.ItemCategoryPropertiesValuesName, name: "AK_ItemCategoryPropertiesValues_ItemCategoryPropertiesValuesName")
                .Index(t => t.ItemCategoryPropertiesId)
                .Index(t => t.ItemId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AccountTypes",
                c => new
                    {
                        AccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(nullable: false, maxLength: 200),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeName, unique: true, name: "AK_AccountType_AccountTypeName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountsDataTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ActivityLogId = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Activity = c.String(),
                        PageUrl = c.String(),
                        ActivityDte = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ActivityLogId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AppUserCompanyRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                        CompanyRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.ApplicationUserComapnyId)
                .Index(t => t.CompanyRoleId);
            
            CreateTable(
                "dbo.CompanyRoles",
                c => new
                    {
                        CompanyRoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.CompanyRoleId)
                .Index(t => t.Name, unique: true, name: "AK_CompanyRole_Name");
            
            CreateTable(
                "dbo.Vocabularies",
                c => new
                    {
                        VocabularyId = c.Int(nullable: false, identity: true),
                        Lang = c.String(),
                        Name = c.String(nullable: false, maxLength: 200),
                        CompanyRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VocabularyId)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.Name, unique: true, name: "AK_Vocabulary_Name")
                .Index(t => t.CompanyRoleId);
            
            CreateTable(
                "dbo.CustomAccountCategories",
                c => new
                    {
                        CustomAccountCategoryId = c.Int(nullable: false, identity: true),
                        AccountCategoryName = c.String(),
                        AccountCategoryCode = c.String(),
                        BalanceSheetTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BalanceSheetType_CustomBalanceSheetTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomAccountCategoryId)
                .ForeignKey("dbo.CustomBalanceSheetTypes", t => t.BalanceSheetType_CustomBalanceSheetTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BalanceSheetType_CustomBalanceSheetTypeId);
            
            CreateTable(
                "dbo.CustomAccounts",
                c => new
                    {
                        CustomAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        CustomAccountCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CustomAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.CustomAccountCategories", t => t.CustomAccountCategoryId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.CustomAccountCategoryId);
            
            CreateTable(
                "dbo.CustomBalanceSheetTypes",
                c => new
                    {
                        CustomBalanceSheetTypeId = c.Int(nullable: false, identity: true),
                        BalanceSheetTypeName = c.String(),
                        BalanceSheetTypeCode = c.String(),
                        BalanceSheetId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BalanceSheet_CustomBalanceSheetId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomBalanceSheetTypeId)
                .ForeignKey("dbo.CustomBalanceSheets", t => t.BalanceSheet_CustomBalanceSheetId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BalanceSheet_CustomBalanceSheetId);
            
            CreateTable(
                "dbo.CustomBalanceSheets",
                c => new
                    {
                        CustomBalanceSheetId = c.Int(nullable: false, identity: true),
                        BalanceSheetName = c.String(),
                        BalanceSheetCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CustomBalanceSheetId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentId = c.Int(nullable: false, identity: true),
                        DocumentName = c.String(),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(nullable: false),
                        AccountMovementId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.DocumentId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountMovements", t => t.AccountMovementId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountMovementId)
                .Index(t => t.AccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Documents", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Documents", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Documents", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Documents", "AccountMovementId", "dbo.AccountMovements");
            DropForeignKey("dbo.Documents", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomAccountCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheets", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "BalanceSheet_CustomBalanceSheetId", "dbo.CustomBalanceSheets");
            DropForeignKey("dbo.CustomAccountCategories", "BalanceSheetType_CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes");
            DropForeignKey("dbo.CustomAccounts", "CustomAccountCategoryId", "dbo.CustomAccountCategories");
            DropForeignKey("dbo.CustomAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AppUserCompanyRoles", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.Vocabularies", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.AppUserCompanyRoles", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropForeignKey("dbo.ActivityLogs", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategoryProperties", "DataTypeId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountCategoryProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Accounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Accounts", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountMovements", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOrders", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOrderProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Companies", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "WarehouseId", "dbo.Warehouses");
            DropForeignKey("dbo.Items", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "ItemCategoryPropertiesId", "dbo.ItemCategoryProperties");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemCategoryProperties", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.ItemCategoryProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Items", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Items", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Inventories", "InventoryOrderId", "dbo.InventoryOrders");
            DropForeignKey("dbo.InventoryOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "InventoryInvoiceId", "dbo.InventoryInvoices");
            DropForeignKey("dbo.InventoryInvoices", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "AccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.Warehouses", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Warehouses", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Branches", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Branches", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.BalanceSheets", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BalanceSheets", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.BalanceSheetTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BalanceSheetTypes", "BalanceSheetId", "dbo.BalanceSheets");
            DropForeignKey("dbo.AccountCategories", "BalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.AccountMovements", "Branch_BranchId", "dbo.Branches");
            DropForeignKey("dbo.ApplicationUserComapnies", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ApplicationUserComapnies", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOrders", "AccountOrderPropertiesId", "dbo.AccountOrderProperties");
            DropForeignKey("dbo.AccountMovements", "AccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountCategoryPropertiesId", "dbo.AccountCategoryProperties");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Documents", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Documents", new[] { "AccountId" });
            DropIndex("dbo.Documents", new[] { "AccountMovementId" });
            DropIndex("dbo.Documents", new[] { "BranchId" });
            DropIndex("dbo.Documents", new[] { "CompanyId" });
            DropIndex("dbo.CustomBalanceSheets", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "BalanceSheet_CustomBalanceSheetId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomAccounts", new[] { "CustomAccountCategoryId" });
            DropIndex("dbo.CustomAccounts", new[] { "AccountId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "BalanceSheetType_CustomBalanceSheetTypeId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Vocabularies", new[] { "CompanyRoleId" });
            DropIndex("dbo.Vocabularies", "AK_Vocabulary_Name");
            DropIndex("dbo.CompanyRoles", "AK_CompanyRole_Name");
            DropIndex("dbo.AppUserCompanyRoles", new[] { "CompanyRoleId" });
            DropIndex("dbo.AppUserCompanyRoles", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.ActivityLogs", new[] { "UserId" });
            DropIndex("dbo.AccountTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountTypes", "AK_AccountType_AccountTypeName");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "ItemId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "ItemCategoryPropertiesId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", "AK_ItemCategoryPropertiesValues_ItemCategoryPropertiesValuesName");
            DropIndex("dbo.ItemCategoryProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategoryProperties", new[] { "ItemCategoryId" });
            DropIndex("dbo.ItemCategoryProperties", "AK_ItemCategoryProperties_ItemCategoryPropertiesName");
            DropIndex("dbo.ItemCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategories", "AK_ItemCategory_ItemCategoryName");
            DropIndex("dbo.Items", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Items", new[] { "CompanyId" });
            DropIndex("dbo.Items", new[] { "ItemCategoryId" });
            DropIndex("dbo.Items", "AK_Item_ItemName");
            DropIndex("dbo.InventoryOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryInvoices", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Inventories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Inventories", new[] { "InventoryInvoiceId" });
            DropIndex("dbo.Inventories", new[] { "InventoryOrderId" });
            DropIndex("dbo.Inventories", new[] { "AccountOrderId" });
            DropIndex("dbo.Inventories", new[] { "WarehouseId" });
            DropIndex("dbo.Inventories", new[] { "ItemId" });
            DropIndex("dbo.Warehouses", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Warehouses", new[] { "BranchId" });
            DropIndex("dbo.Warehouses", "AK_WarehouseName_WarehouseName");
            DropIndex("dbo.BalanceSheetTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BalanceSheetTypes", new[] { "BalanceSheetId" });
            DropIndex("dbo.BalanceSheets", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BalanceSheets", "AK_BalanceSheet_BalanceSheetName");
            DropIndex("dbo.Branches", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Branches", "AK_Branch_BranchName");
            DropIndex("dbo.Companies", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Companies", "AK_Company_CompanyName");
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ApplicationUserId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "CompanyId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AccountOrderProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOrders", new[] { "BranchId" });
            DropIndex("dbo.AccountOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOrders", new[] { "AccountOrderPropertiesId" });
            DropIndex("dbo.AccountMovements", new[] { "Branch_BranchId" });
            DropIndex("dbo.AccountMovements", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountMovements", new[] { "AccountOrderId" });
            DropIndex("dbo.AccountMovements", new[] { "AccountId" });
            DropIndex("dbo.Accounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Accounts", new[] { "AccountTypeId" });
            DropIndex("dbo.Accounts", new[] { "AccountCategoryId" });
            DropIndex("dbo.Accounts", "AK_Accounts_AccountName");
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountCategoryPropertiesId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "DataTypeId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountCategoryId" });
            DropIndex("dbo.AccountCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategories", "AK_AccountCategory_AccountCategoryName");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Documents");
            DropTable("dbo.CustomBalanceSheets");
            DropTable("dbo.CustomBalanceSheetTypes");
            DropTable("dbo.CustomAccounts");
            DropTable("dbo.CustomAccountCategories");
            DropTable("dbo.Vocabularies");
            DropTable("dbo.CompanyRoles");
            DropTable("dbo.AppUserCompanyRoles");
            DropTable("dbo.ActivityLogs");
            DropTable("dbo.AccountsDataTypes");
            DropTable("dbo.AccountTypes");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.ItemCategoryPropertiesValues");
            DropTable("dbo.ItemCategoryProperties");
            DropTable("dbo.ItemCategories");
            DropTable("dbo.Items");
            DropTable("dbo.InventoryOrders");
            DropTable("dbo.InventoryInvoices");
            DropTable("dbo.Inventories");
            DropTable("dbo.Warehouses");
            DropTable("dbo.BalanceSheetTypes");
            DropTable("dbo.BalanceSheets");
            DropTable("dbo.Branches");
            DropTable("dbo.Companies");
            DropTable("dbo.ApplicationUserComapnies");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AccountOrderProperties");
            DropTable("dbo.AccountOrders");
            DropTable("dbo.AccountMovements");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountCategoryPropertiesValues");
            DropTable("dbo.AccountCategoryProperties");
            DropTable("dbo.AccountCategories");
        }
    }
}
