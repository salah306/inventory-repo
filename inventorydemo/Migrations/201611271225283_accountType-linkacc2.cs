namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountTypelinkacc2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomAccountCategories", "IsLinked", c => c.Boolean());
            AddColumn("dbo.CustomAccountCategories", "LinkedAccountId", c => c.Int());
            AddColumn("dbo.CustomAccountCategories", "LinkedAccountName", c => c.String());
            AddColumn("dbo.CustomBalanceSheetTypes", "IsLinked", c => c.Boolean());
            AddColumn("dbo.CustomBalanceSheetTypes", "LinkedAccountId", c => c.Int());
            AddColumn("dbo.CustomBalanceSheetTypes", "LinkedAccountName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomBalanceSheetTypes", "LinkedAccountName");
            DropColumn("dbo.CustomBalanceSheetTypes", "LinkedAccountId");
            DropColumn("dbo.CustomBalanceSheetTypes", "IsLinked");
            DropColumn("dbo.CustomAccountCategories", "LinkedAccountName");
            DropColumn("dbo.CustomAccountCategories", "LinkedAccountId");
            DropColumn("dbo.CustomAccountCategories", "IsLinked");
        }
    }
}
