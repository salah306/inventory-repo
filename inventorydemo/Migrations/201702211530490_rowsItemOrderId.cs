namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rowsItemOrderId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ItemsRows", "orderid", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemsRows", "orderid");
        }
    }
}
