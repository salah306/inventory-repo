namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateOrdersGroups : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderGroupPayMethods", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderGroupPayMethods", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderGroupTypes", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderGroupTypeSaleAndPurchese", "OrderGroupTypeId", "dbo.OrderGroupTypes");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderProperties", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderProperties", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrdersNames", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrdersNames", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderTotalAccounts", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrderTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderTotalAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "AccountId" });
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderGroupPayMethods", new[] { "AccountId" });
            DropIndex("dbo.OrderGroupPayMethods", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderGroupTypes", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderGroupTypeSaleAndPurchese", new[] { "OrderGroupTypeId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderProperties", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderProperties", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrdersNames", new[] { "OrderGroupId" });
            DropIndex("dbo.OrdersNames", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderTotalAccounts", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrderTotalAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderTotalAccounts", new[] { "CurrentWorkerId" });
            RenameColumn(table: "dbo.OrderMainAccounts", name: "Account_AccountId", newName: "InventoryAccount_AccountId");
            RenameIndex(table: "dbo.OrderMainAccounts", name: "IX_Account_AccountId", newName: "IX_InventoryAccount_AccountId");
            CreateTable(
                "dbo.OrderAccountsforSubAccountPurchases",
                c => new
                    {
                        OrderAccountsforSubAccountPurchasesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountPurchasesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.PurchasesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        PurchasesId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.PurchasesId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccountPurchases",
                c => new
                    {
                        OrderOtherTotalAccountPurchasesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountPurchasesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.PurchasesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderPropertiesPurchases",
                c => new
                    {
                        OrderPropertiesPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.String(),
                        PurchasesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPurchasesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId, cascadeDelete: false)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.PurchasesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrdersTypePurchases",
                c => new
                    {
                        OrdersTypePurchasesId = c.Int(nullable: false, identity: true),
                        sale = c.String(nullable: false),
                        returnd = c.String(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrdersTypePurchasesId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.PurchasesId);
            
            CreateTable(
                "dbo.OrderGroupPayMethodPurchases",
                c => new
                    {
                        OrderGroupPayMethodPurchasesId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodName = c.String(nullable: false),
                        AccountCategoryId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodPurchasesId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.PurchasesId);
            
            CreateTable(
                "dbo.OrderAccountsforSubAccountSales",
                c => new
                    {
                        OrderAccountsforSubAccountSalesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountSalesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SalesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        SalesId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.SalesId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccountSales",
                c => new
                    {
                        OrderOtherTotalAccountSalesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountSalesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SalesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderPropertiesSales",
                c => new
                    {
                        OrderPropertiesSalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.String(),
                        SalesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesSalesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId, cascadeDelete: false)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.SalesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrdersTypeSales",
                c => new
                    {
                        OrdersTypeSalesId = c.Int(nullable: false, identity: true),
                        sale = c.String(nullable: false),
                        returnd = c.String(nullable: false),
                        SalesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrdersTypeSalesId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.SalesId);
            
            CreateTable(
                "dbo.OrderGroupPayMethodSales",
                c => new
                    {
                        OrderGroupPayMethodSalesId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodName = c.String(nullable: false),
                        AccountCategoryId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodSalesId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.SalesId);
            
            AddColumn("dbo.OrderGroups", "SalesId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderGroups", "PurchasesId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "InventoryAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "Sales_SalesId", c => c.Int());
            CreateIndex("dbo.OrderMainAccounts", "Sales_SalesId");
            CreateIndex("dbo.OrderGroups", "SalesId");
            CreateIndex("dbo.OrderGroups", "PurchasesId");
            AddForeignKey("dbo.OrderGroups", "PurchasesId", "dbo.Purchases", "PurchasesId", cascadeDelete: false);
            AddForeignKey("dbo.OrderGroups", "SalesId", "dbo.Sales", "SalesId", cascadeDelete: false);
            AddForeignKey("dbo.OrderMainAccounts", "Sales_SalesId", "dbo.Sales", "SalesId");
            DropColumn("dbo.OrderMainAccounts", "AccountId");
            DropTable("dbo.OrderAccountsforSubAccounts");
            DropTable("dbo.OrderGroupPayMethods");
            DropTable("dbo.OrderGroupTypes");
            DropTable("dbo.OrderGroupTypeSaleAndPurchese");
            DropTable("dbo.OrderOtherTotalAccounts");
            DropTable("dbo.OrderProperties");
            DropTable("dbo.OrdersNames");
            DropTable("dbo.OrderTotalAccounts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OrderTotalAccounts",
                c => new
                    {
                        OrderTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountCategoryId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderTotalAccountId);
            
            CreateTable(
                "dbo.OrdersNames",
                c => new
                    {
                        OrdersNameId = c.Int(nullable: false, identity: true),
                        OrderName = c.String(),
                        OrderGroupId = c.Int(nullable: false),
                        IsDebit = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Ordertype = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrdersNameId);
            
            CreateTable(
                "dbo.OrderProperties",
                c => new
                    {
                        OrderPropertiesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.String(),
                        OrderGroupId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccounts",
                c => new
                    {
                        OrderOtherTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountId);
            
            CreateTable(
                "dbo.OrderGroupTypeSaleAndPurchese",
                c => new
                    {
                        OrderGroupTypeSaleAndPurcheseId = c.Int(nullable: false, identity: true),
                        CaseDebit = c.String(nullable: false),
                        CaseCrdit = c.String(nullable: false),
                        OrderGroupTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupTypeSaleAndPurcheseId);
            
            CreateTable(
                "dbo.OrderGroupTypes",
                c => new
                    {
                        OrderGroupTypeId = c.Int(nullable: false, identity: true),
                        OrderGroupTypeName = c.String(),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupTypeId);
            
            CreateTable(
                "dbo.OrderGroupPayMethods",
                c => new
                    {
                        OrderGroupPayMethodId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodName = c.String(nullable: false),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodId);
            
            CreateTable(
                "dbo.OrderAccountsforSubAccounts",
                c => new
                    {
                        OrderAccountsforSubAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountId);
            
            AddColumn("dbo.OrderMainAccounts", "AccountId", c => c.Int(nullable: false));
            DropForeignKey("dbo.OrderGroupPayMethodSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderGroupPayMethodSales", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrdersTypeSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderPropertiesSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderPropertiesSales", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "Sales_SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderGroups", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderGroups", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrdersTypePurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderPropertiesPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderPropertiesPurchases", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "SalesId" });
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrdersTypeSales", new[] { "SalesId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "SalesId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "SalesId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "AccountId" });
            DropIndex("dbo.OrderGroups", new[] { "PurchasesId" });
            DropIndex("dbo.OrderGroups", new[] { "SalesId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "Sales_SalesId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "SalesId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "AccountId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrdersTypePurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "AccountId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "AccountId" });
            DropColumn("dbo.OrderMainAccounts", "Sales_SalesId");
            DropColumn("dbo.OrderMainAccounts", "InventoryAccountId");
            DropColumn("dbo.OrderGroups", "PurchasesId");
            DropColumn("dbo.OrderGroups", "SalesId");
            DropTable("dbo.OrderGroupPayMethodSales");
            DropTable("dbo.OrdersTypeSales");
            DropTable("dbo.OrderPropertiesSales");
            DropTable("dbo.OrderOtherTotalAccountSales");
            DropTable("dbo.Sales");
            DropTable("dbo.OrderAccountsforSubAccountSales");
            DropTable("dbo.OrderGroupPayMethodPurchases");
            DropTable("dbo.OrdersTypePurchases");
            DropTable("dbo.OrderPropertiesPurchases");
            DropTable("dbo.OrderOtherTotalAccountPurchases");
            DropTable("dbo.Purchases");
            DropTable("dbo.OrderAccountsforSubAccountPurchases");
            RenameIndex(table: "dbo.OrderMainAccounts", name: "IX_InventoryAccount_AccountId", newName: "IX_Account_AccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "InventoryAccount_AccountId", newName: "Account_AccountId");
            CreateIndex("dbo.OrderTotalAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderTotalAccounts", "OrderGroupId");
            CreateIndex("dbo.OrderTotalAccounts", "AccountCategoryId");
            CreateIndex("dbo.OrdersNames", "CurrentWorkerId");
            CreateIndex("dbo.OrdersNames", "OrderGroupId");
            CreateIndex("dbo.OrderProperties", "CurrentWorkerId");
            CreateIndex("dbo.OrderProperties", "OrderPropertyTypeId");
            CreateIndex("dbo.OrderProperties", "OrderGroupId");
            CreateIndex("dbo.OrderOtherTotalAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderOtherTotalAccounts", "OrderGroupId");
            CreateIndex("dbo.OrderOtherTotalAccounts", "AccountId");
            CreateIndex("dbo.OrderGroupTypeSaleAndPurchese", "OrderGroupTypeId");
            CreateIndex("dbo.OrderGroupTypes", "OrderGroupId");
            CreateIndex("dbo.OrderGroupPayMethods", "OrderGroupId");
            CreateIndex("dbo.OrderGroupPayMethods", "AccountId");
            CreateIndex("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderAccountsforSubAccounts", "OrderGroupId");
            CreateIndex("dbo.OrderAccountsforSubAccounts", "AccountId");
            AddForeignKey("dbo.OrderTotalAccounts", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderTotalAccounts", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId", cascadeDelete: false);
            AddForeignKey("dbo.OrdersNames", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrdersNames", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderProperties", "OrderPropertyTypeId", "dbo.OrderPropertyTypes", "OrderPropertyTypeId", cascadeDelete: false);
            AddForeignKey("dbo.OrderProperties", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderProperties", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderOtherTotalAccounts", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderOtherTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderOtherTotalAccounts", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.OrderGroupTypeSaleAndPurchese", "OrderGroupTypeId", "dbo.OrderGroupTypes", "OrderGroupTypeId", cascadeDelete: false);
            AddForeignKey("dbo.OrderGroupTypes", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderGroupPayMethods", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderGroupPayMethods", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.OrderAccountsforSubAccounts", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
            AddForeignKey("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderAccountsforSubAccounts", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
        }
    }
}
