namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingsRole : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ApplicationUserComapnies", "ApplicationUserId", "dbo.AspNetUsers");
            CreateTable(
                "dbo.settingPropertiesforUsers",
                c => new
                    {
                        settingPropertiesforUserId = c.Int(nullable: false, identity: true),
                        settingPropertiesId = c.Int(nullable: false),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingPropertiesforUserId)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingProperties", t => t.settingPropertiesId, cascadeDelete: false)
                .Index(t => t.settingPropertiesId)
                .Index(t => t.ApplicationUserComapnyId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingRolesForUsers",
                c => new
                    {
                        settingRolesForUserId = c.Int(nullable: false, identity: true),
                        roleName = c.Int(nullable: false),
                        active = c.Boolean(nullable: false),
                        RefrenceType = c.String(),
                        RefrenceId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        settingPropertiesforUser_settingPropertiesforUserId = c.Int(),
                    })
                .PrimaryKey(t => t.settingRolesForUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingPropertiesforUsers", t => t.settingPropertiesforUser_settingPropertiesforUserId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.settingPropertiesforUser_settingPropertiesforUserId);
            
            CreateTable(
                "dbo.RolepropertiesforUsers",
                c => new
                    {
                        RolepropertiesforUserId = c.Int(nullable: false, identity: true),
                        settingRoleId = c.Int(nullable: false),
                        isTrue = c.Boolean(nullable: false),
                        typeRefrenceId = c.Int(nullable: false),
                        typeRefrence = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        settingRolesForUser_settingRolesForUserId = c.Int(),
                    })
                .PrimaryKey(t => t.RolepropertiesforUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingRoles", t => t.settingRoleId, cascadeDelete: false)
                .ForeignKey("dbo.settingRolesForUsers", t => t.settingRolesForUser_settingRolesForUserId)
                .Index(t => t.settingRoleId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.settingRolesForUser_settingRolesForUserId);
            
            CreateTable(
                "dbo.settingRoles",
                c => new
                    {
                        settingRoleId = c.Int(nullable: false, identity: true),
                        roleName = c.String(),
                        settingPropertiesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingRoleId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingProperties", t => t.settingPropertiesId, cascadeDelete: false)
                .Index(t => t.settingPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingProperties",
                c => new
                    {
                        settingPropertiesId = c.Int(nullable: false, identity: true),
                        propertyName = c.String(),
                        name = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingPropertiesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.ApplicationUserComapnies", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.ApplicationUserComapnies", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.ApplicationUserComapnies", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.ApplicationUserComapnies", "CurrentWorkerId");
            CreateIndex("dbo.ApplicationUserComapnies", "ApplicationUser_Id");
            AddForeignKey("dbo.ApplicationUserComapnies", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ApplicationUserComapnies", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationUserComapnies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingPropertiesforUsers", "settingPropertiesId", "dbo.settingProperties");
            DropForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUser_settingPropertiesforUserId", "dbo.settingPropertiesforUsers");
            DropForeignKey("dbo.RolepropertiesforUsers", "settingRolesForUser_settingRolesForUserId", "dbo.settingRolesForUsers");
            DropForeignKey("dbo.RolepropertiesforUsers", "settingRoleId", "dbo.settingRoles");
            DropForeignKey("dbo.settingRoles", "settingPropertiesId", "dbo.settingProperties");
            DropForeignKey("dbo.settingProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingRoles", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RolepropertiesforUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingRolesForUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingPropertiesforUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingPropertiesforUsers", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropForeignKey("dbo.ApplicationUserComapnies", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.settingProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingRoles", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingRoles", new[] { "settingPropertiesId" });
            DropIndex("dbo.RolepropertiesforUsers", new[] { "settingRolesForUser_settingRolesForUserId" });
            DropIndex("dbo.RolepropertiesforUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.RolepropertiesforUsers", new[] { "settingRoleId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "settingPropertiesforUser_settingPropertiesforUserId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "settingPropertiesId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "CurrentWorkerId" });
            DropColumn("dbo.ApplicationUserComapnies", "ApplicationUser_Id");
            DropColumn("dbo.ApplicationUserComapnies", "CurrentWorkerId");
            DropColumn("dbo.ApplicationUserComapnies", "RowVersion");
            DropTable("dbo.settingProperties");
            DropTable("dbo.settingRoles");
            DropTable("dbo.RolepropertiesforUsers");
            DropTable("dbo.settingRolesForUsers");
            DropTable("dbo.settingPropertiesforUsers");
            AddForeignKey("dbo.ApplicationUserComapnies", "ApplicationUserId", "dbo.AspNetUsers", "Id");
        }
    }
}
