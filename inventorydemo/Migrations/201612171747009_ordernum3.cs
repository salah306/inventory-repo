namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordernum3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertiesforItemGroups", "orderNum", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertiesforItemGroups", "orderNum");
        }
    }
}
