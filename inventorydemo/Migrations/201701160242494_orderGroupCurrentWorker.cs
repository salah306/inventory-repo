namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderGroupCurrentWorker : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderAccountsforSubAccounts", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderMainAccounts", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderMainAccounts", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrdersNames", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrdersNames", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderOtherTotalAccounts", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderOtherTotalAccounts", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderProperties", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderProperties", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderPropertyTypes", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderPropertyTypes", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderTotalAccounts", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderTotalAccounts", "CurrentWorkerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderGroups", "CurrentWorkerId");
            CreateIndex("dbo.OrderMainAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderProperties", "CurrentWorkerId");
            CreateIndex("dbo.OrderPropertyTypes", "CurrentWorkerId");
            CreateIndex("dbo.OrdersNames", "CurrentWorkerId");
            CreateIndex("dbo.OrderOtherTotalAccounts", "CurrentWorkerId");
            CreateIndex("dbo.OrderTotalAccounts", "CurrentWorkerId");
            AddForeignKey("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderMainAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderProperties", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderPropertyTypes", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrdersNames", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderOtherTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrdersNames", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderPropertyTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderMainAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.OrderTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrdersNames", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertyTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "CurrentWorkerId" });
            DropColumn("dbo.OrderTotalAccounts", "CurrentWorkerId");
            DropColumn("dbo.OrderTotalAccounts", "RowVersion");
            DropColumn("dbo.OrderPropertyTypes", "CurrentWorkerId");
            DropColumn("dbo.OrderPropertyTypes", "RowVersion");
            DropColumn("dbo.OrderProperties", "CurrentWorkerId");
            DropColumn("dbo.OrderProperties", "RowVersion");
            DropColumn("dbo.OrderOtherTotalAccounts", "CurrentWorkerId");
            DropColumn("dbo.OrderOtherTotalAccounts", "RowVersion");
            DropColumn("dbo.OrdersNames", "CurrentWorkerId");
            DropColumn("dbo.OrdersNames", "RowVersion");
            DropColumn("dbo.OrderMainAccounts", "CurrentWorkerId");
            DropColumn("dbo.OrderMainAccounts", "RowVersion");
            DropColumn("dbo.OrderGroups", "CurrentWorkerId");
            DropColumn("dbo.OrderGroups", "RowVersion");
            DropColumn("dbo.OrderAccountsforSubAccounts", "CurrentWorkerId");
            DropColumn("dbo.OrderAccountsforSubAccounts", "RowVersion");
        }
    }
}
