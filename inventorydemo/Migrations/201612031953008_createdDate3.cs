namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdDate3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubAccounts",
                c => new
                    {
                        SubAccountId = c.Int(nullable: false, identity: true),
                        SubAccountName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubAccounts", "AccountId", "dbo.Accounts");
            DropIndex("dbo.SubAccounts", new[] { "AccountId" });
            DropTable("dbo.SubAccounts");
        }
    }
}
