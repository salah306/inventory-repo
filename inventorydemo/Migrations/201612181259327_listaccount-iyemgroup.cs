namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class listaccountiyemgroup : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomSubAccountsOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomSubOrderAccounts", "AliesAccountCol_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomSubOrderAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomSubOrderAccounts", "CustomSubAccountsOrder_CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId", "dbo.CustomSubOrderAccounts");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1", "dbo.CustomSubOrderAccounts");
            DropForeignKey("dbo.SubAccountOrders", "CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders");
            DropForeignKey("dbo.CustomSubAccountsOrders", "TotalAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountsSubAccountMovements", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountsSubAccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountsSubAccountMovements", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropIndex("dbo.SubAccountOrders", new[] { "CustomSubAccountsOrderId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomAccount_AccountId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomSubOrderAccount_CustomSubOrderAccountId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomSubOrderAccount_CustomSubOrderAccountId1" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "TotalAccount_AccountId" });
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "AliesAccountCol_AccountId" });
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "CustomSubAccountsOrder_CustomSubAccountsOrderId" });
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "AccountId" });
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "SubAccountMovementId" });
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "CurrentWorkerId" });
            DropTable("dbo.CustomSubAccountsOrders");
            DropTable("dbo.CustomSubOrderAccounts");
            DropTable("dbo.AccountsSubAccountMovements");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AccountsSubAccountMovements",
                c => new
                    {
                        AccountsSubAccountMovementId = c.Int(nullable: false, identity: true),
                        SubAccountMovementDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        IsRow = c.Boolean(nullable: false),
                        IsPercent = c.Boolean(nullable: false),
                        SubAccountMovementId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountsSubAccountMovementId);
            
            CreateTable(
                "dbo.CustomSubOrderAccounts",
                c => new
                    {
                        CustomSubOrderAccountId = c.Int(nullable: false, identity: true),
                        AliesAccountColName = c.String(),
                        AliesAccountColId = c.Int(nullable: false),
                        CustomSubAccountsOrderId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AliesAccountCol_AccountId = c.Int(),
                        CustomSubAccountsOrder_CustomSubAccountsOrderId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomSubOrderAccountId);
            
            CreateTable(
                "dbo.CustomSubAccountsOrders",
                c => new
                    {
                        CustomSubAccountsOrderId = c.Int(nullable: false, identity: true),
                        OrderTitle = c.String(),
                        CustomAccountName = c.String(),
                        CustomAccountId = c.Int(nullable: false),
                        IsPricePerUnit = c.Boolean(nullable: false),
                        AddQuantity = c.Boolean(nullable: false),
                        IsDebit = c.Boolean(nullable: false),
                        IsPercent = c.Boolean(nullable: false),
                        TotalName = c.String(),
                        TotalAccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CustomSubOrderAccountsId = c.Int(nullable: false),
                        CustomAccount_AccountId = c.Int(),
                        CustomSubOrderAccount_CustomSubOrderAccountId = c.Int(),
                        CustomSubOrderAccount_CustomSubOrderAccountId1 = c.Int(),
                        TotalAccount_AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomSubAccountsOrderId);
            
            CreateIndex("dbo.AccountsSubAccountMovements", "CurrentWorkerId");
            CreateIndex("dbo.AccountsSubAccountMovements", "SubAccountMovementId");
            CreateIndex("dbo.AccountsSubAccountMovements", "AccountId");
            CreateIndex("dbo.CustomSubOrderAccounts", "CustomSubAccountsOrder_CustomSubAccountsOrderId");
            CreateIndex("dbo.CustomSubOrderAccounts", "AliesAccountCol_AccountId");
            CreateIndex("dbo.CustomSubOrderAccounts", "CurrentWorkerId");
            CreateIndex("dbo.CustomSubAccountsOrders", "TotalAccount_AccountId");
            CreateIndex("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1");
            CreateIndex("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId");
            CreateIndex("dbo.CustomSubAccountsOrders", "CustomAccount_AccountId");
            CreateIndex("dbo.CustomSubAccountsOrders", "CurrentWorkerId");
            CreateIndex("dbo.SubAccountOrders", "CustomSubAccountsOrderId");
            AddForeignKey("dbo.AccountsSubAccountMovements", "SubAccountMovementId", "dbo.SubAccountMovements", "SubAccountMovementId", cascadeDelete: true);
            AddForeignKey("dbo.AccountsSubAccountMovements", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AccountsSubAccountMovements", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: true);
            AddForeignKey("dbo.CustomSubAccountsOrders", "TotalAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.SubAccountOrders", "CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders", "CustomSubAccountsOrderId", cascadeDelete: true);
            AddForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1", "dbo.CustomSubOrderAccounts", "CustomSubOrderAccountId");
            AddForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId", "dbo.CustomSubOrderAccounts", "CustomSubOrderAccountId");
            AddForeignKey("dbo.CustomSubOrderAccounts", "CustomSubAccountsOrder_CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders", "CustomSubAccountsOrderId");
            AddForeignKey("dbo.CustomSubOrderAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.CustomSubOrderAccounts", "AliesAccountCol_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.CustomSubAccountsOrders", "CustomAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.CustomSubAccountsOrders", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
    }
}
