namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountsCatogeryTypesAcccountsCreated : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountsCatogeryTypesAcccounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ApplicationUserComapnies", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.Companies", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.Branches", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Branches", "CreatedDate");
            DropColumn("dbo.Companies", "CreatedDate");
            DropColumn("dbo.ApplicationUserComapnies", "CreatedDate");
            DropTable("dbo.AccountsCatogeryTypesAcccounts");
        }
    }
}
