namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateOrdersGroups2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderMainAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderMainAccounts", "Sales_SalesId", "dbo.Sales");
            DropIndex("dbo.OrderMainAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "Sales_SalesId" });
            RenameColumn(table: "dbo.OrderMainAccounts", name: "Sales_SalesId", newName: "SalesId");
            AlterColumn("dbo.OrderMainAccounts", "SalesId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderMainAccounts", "SalesId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesId", "dbo.Sales", "SalesId", cascadeDelete: false);
            DropColumn("dbo.OrderMainAccounts", "OrderGroupId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderMainAccounts", "OrderGroupId", c => c.Int(nullable: false));
            DropForeignKey("dbo.OrderMainAccounts", "SalesId", "dbo.Sales");
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesId" });
            AlterColumn("dbo.OrderMainAccounts", "SalesId", c => c.Int());
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesId", newName: "Sales_SalesId");
            CreateIndex("dbo.OrderMainAccounts", "Sales_SalesId");
            CreateIndex("dbo.OrderMainAccounts", "OrderGroupId");
            AddForeignKey("dbo.OrderMainAccounts", "Sales_SalesId", "dbo.Sales", "SalesId");
            AddForeignKey("dbo.OrderMainAccounts", "OrderGroupId", "dbo.OrderGroups", "OrderGroupId", cascadeDelete: false);
        }
    }
}
