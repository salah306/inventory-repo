namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountDataType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "AccountsDataTypesId", c => c.Int());
            CreateIndex("dbo.Accounts", "AccountsDataTypesId");
            AddForeignKey("dbo.Accounts", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.Accounts", new[] { "AccountsDataTypesId" });
            DropColumn("dbo.Accounts", "AccountsDataTypesId");
        }
    }
}
