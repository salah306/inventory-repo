﻿using Inventory.dl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.dl.ModelsConfiguration
{
    public class AccountTypeConfiguration : EntityTypeConfiguration<AccountType>
    {
        public AccountTypeConfiguration()
        {
            Property(accty => accty.AccountTypeName)
                .HasMaxLength(200)
                .IsRequired()
                .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_AccountType_AccountTypeName") { IsUnique = true }));
        }
    }
}