﻿using Inventory.dl.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.dl.ModelsConfiguration
{
    public class AccountOrderPropertiesConfiguration  : EntityTypeConfiguration<AccountOrderProperties>
    {
        public AccountOrderPropertiesConfiguration()
        {
            Property(a => a.AccountOrderPropertiesName)
                .HasMaxLength(200)
                .IsRequired();

        }
    }
}