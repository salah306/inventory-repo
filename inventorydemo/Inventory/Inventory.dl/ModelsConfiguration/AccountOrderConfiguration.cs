﻿using Inventory.dl.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.dl.ModelsConfiguration
{
    public class AccountOrderConfiguration : EntityTypeConfiguration<AccountOrder>
    {
        public AccountOrderConfiguration()
        {
            Property(a => a.BranchId)
              .IsOptional();

            Property(a => a.AccountOrderPropertiesId).IsOptional();

           

        }
    }
}