﻿using Inventory.dl.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.dl.ModelsConfiguration
{
    public class AccountCategoryPropertiesValuesConfiguration : EntityTypeConfiguration<AccountCategoryPropertiesValue>
    {
        public AccountCategoryPropertiesValuesConfiguration()
        {
            Property(a => a.AccountCategoryPropertiesValueName)
                .HasMaxLength(80)
                 .IsOptional();

        }
    }
}