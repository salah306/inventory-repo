﻿using Inventory.dl.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.dl.ModelsConfiguration
{
    public class BranchConfiguration : EntityTypeConfiguration<Branch>
    {
        public BranchConfiguration()
        {
            Property(co => co.BranchName)
               .HasMaxLength(200)
               .IsRequired()
               .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_Branch_BranchName" , 1) { IsUnique = true }));
            Property(co => co.CompanyId)
                .IsRequired()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_Branch_BranchName", 2) { IsUnique = true }));
        }
    }

    public class BalanceSheetConfiguration : EntityTypeConfiguration<BalanceSheet>
    {
        public BalanceSheetConfiguration()
        {
            Property(co => co.BalanceSheetName)
               .HasMaxLength(200)
               .IsRequired()
               .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_BalanceSheet_BalanceSheetName", 1) { IsUnique = true }));
            Property(co => co.CompanyId)
                .IsRequired()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_BalanceSheet_BalanceSheetName", 2) { IsUnique = true }));
            Property(co => co.AccountTypeId)
                .IsOptional();
        }
    }

    public class BalanceSheetTypeConfiguration : EntityTypeConfiguration<BalanceSheetType>
    {
        public BalanceSheetTypeConfiguration()
        {
            Property(co => co.BalanceSheetTypeName)
               .HasMaxLength(200)
               .IsRequired()
               .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_BalanceSheetType_BalanceSheetName", 1) { IsUnique = true }));
            Property(co => co.BalanceSheetId)
                .IsRequired()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_BalanceSheetType_BalanceSheetName", 2) { IsUnique = true }));
            Property(co => co.AccountTypeId)
                .IsOptional();
        }
    }


}