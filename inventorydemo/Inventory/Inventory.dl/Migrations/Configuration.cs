namespace Inventory.dl.Migrations
{
    using Inventory.dl.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Inventory.dl.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            context.SalessOrdersConfigurationAccountTypes.AddOrUpdate(co => co.AccountTypeName, new SalessOrdersConfigurationAccountType { AccountTypeName = "����" });
            context.SalessOrdersConfigurationAccountTypes.AddOrUpdate(co => co.AccountTypeName, new SalessOrdersConfigurationAccountType { AccountTypeName = "����" });

            context.PurchasesOrdersConfigurationAccountTypes.AddOrUpdate(co => co.AccountTypeName, new PurchasesOrdersConfigurationAccountType { AccountTypeName = "����" });
            context.PurchasesOrdersConfigurationAccountTypes.AddOrUpdate(co => co.AccountTypeName, new PurchasesOrdersConfigurationAccountType { AccountTypeName = "����" });

            context.BsMainType.AddOrUpdate(b => b.Name, new BsMainType { Name = "����", NickName = "assets", orderNum = 1 });
            context.BsMainType.AddOrUpdate(b => b.Name, new BsMainType { Name = "����", NickName = "liabilities", orderNum = 2 });
            context.BsMainType.AddOrUpdate(b => b.Name, new BsMainType { Name = "�������", NickName = "expenses", orderNum = 3 });
            context.BsMainType.AddOrUpdate(b => b.Name, new BsMainType { Name = "�������", NickName = "revenues", orderNum = 4 });
            context.SaveChanges();
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "�����", aliasName = "inventory" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "�����", aliasName = "cust" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "������", aliasName = "pur" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����", aliasName = "cash" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����", aliasName = "bank" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����� ���", aliasName = "notepay" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����� ���", aliasName = "notereceive" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "������", aliasName = "employee" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����", aliasName = "other" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����� �������", aliasName = "goods-in-transit" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "������", aliasName = "sales" });
            context.AccountsDataTypes.AddOrUpdate(co => co.TypeName, new AccountsDataTypes { TypeName = "����� ��������", aliasName = "cost-sales" });

            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "��� �����" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "��� ����" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "���� ��������" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "���� �����" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "���� ����" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "�������" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "�����" });
            context.AccountsCatogeryTypesAcccounts.AddOrUpdate(co => co.TypeName, new AccountsCatogeryTypesAcccounts { TypeName = "��� - �������" });


            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "��� ����" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "��� ����" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "��" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "�����" });
            context.OrderPropertyTypes.AddOrUpdate(co => co.OrderPropertyTypeName, new OrderPropertyType { OrderPropertyTypeName = "����� - �����" });


            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "��� ����" });
            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "��� ����" });

            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "���", show = false, description = "", Value = "", Required = true, orderNum = 1 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "���", show = false, description = "", Value = "", Required = true, orderNum = 2 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "���", show = false, description = "", Value = "", Required = true, orderNum = 3 });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "���", show = true, description = "", Value = "red", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "��", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "��� ����", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "��� ����", show = true, description = "", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "��� ������", show = true, description = "������", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "������", show = true, description = "������ �����", Value = "", Required = false });
            context.TypeforItemGroups.AddOrUpdate(co => co.TypeName, new TypeforItemGroup { TypeName = "�� �����", show = false, description = "", Value = "", Required = true, orderNum = 4 });


            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "��� ����" });
            context.UnitTypes.AddOrUpdate(co => co.UnitTypeName, new UnitType { UnitTypeName = "��� ����" });
            // AccountsCatogeryDataTypes
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������",
                name = "Company",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ���� ��������" , name = "CanEdit"},

                                }
            });


            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������",
                name = "Branch",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ���� ��������" , name = "CanEdit"},

                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "���� ��������",
                name = "AccountsSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                     new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������ ������� �����",
                name = "BalanceSheetSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                     new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������ ������� ������",
                name = "BalanceSheetTypesSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                     new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������ �������",
                name = "AccountCategoriesSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                     new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������� �������",
                name = "FstatementSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������",
                name = "OrdersSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ������ ������",
                name = "ItemsSetup",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "���� ��������",
                name = "Dashboard",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "���� �������",
                name = "GeneralJournal",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ���������" , name = "CanUse"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������� �������",
                name = "SubGl",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ���������" , name = "CanUse"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������� �����",
                name = "Gl",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ��������",
                name = "TrialBalance",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������� �������",
                name = "fStatements",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������ ���������",
                name = "Orders",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ �����" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ���",
                name = "InventoryCards",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������� ��� ������ ���" , name = "CanViewQty"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"}
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "�����",
                name = "Inventories",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������� ������",
                name = "InventoriesTransfer",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ �������" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������",
                name = "cash",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "������",
                name = "bank",
                settingRole = new List<settingRole>()
                                {
                                    new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "�������",
                name = "cust",
                settingRole = new List<settingRole>()
                                {
                                   new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });

            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "��������",
                name = "pur",
                settingRole = new List<settingRole>()
                                {
                                   new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ���",
                name = "notepay",
                settingRole = new List<settingRole>()
                                {
                                   new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });
            context.settingProperties.AddOrUpdate(a => a.name, new settingProperties
            {
                propertyName = "����� ���",
                name = "notereceive",
                settingRole = new List<settingRole>()
                                {
                                   new settingRole() {roleName = "������ ����� ����" , name = "CanAdd"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanEdit"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanConfirm"},
                                    new settingRole() {roleName = "������ ����� ������" , name = "CanDelete"},
                                    new settingRole() {roleName = "������ ����� �����" , name = "CanAddProperty"},
                                    new settingRole() {roleName = "������ ����� ����� ������" , name = "CanAddPropertyValue"},
                                    new settingRole() {roleName = "������ �������" , name = "CanView"},
                                }
            });


            context.SaveChanges();

            context.AccountOrderProperties.AddOrUpdate(co => co.AccountOrderPropertiesName, new AccountOrderProperties { AccountOrderPropertiesName = "����" });
            context.AccountOrderProperties.AddOrUpdate(co => co.AccountOrderPropertiesName, new AccountOrderProperties { AccountOrderPropertiesName = "����" });
            context.SaveChanges();



            //List<CompanyRole> rolesCompany = new List<CompanyRole>();

            //rolesCompany.Add(companymanger.AddCompanyRole("Company Admin"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Create Company"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Company"));



            //rolesCompany.Add(companymanger.AddCompanyRole("Branch Admin"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Create Branch"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Branch"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can View Branch"));

            //rolesCompany.Add(companymanger.AddCompanyRole("Can Create Accounts"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Accounts"));

            //rolesCompany.Add(companymanger.AddCompanyRole("Can Create Financial Statements"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Delete Financial statements"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can View Financial statements"));


            //rolesCompany.Add(companymanger.AddCompanyRole("Can Invite User"));
            //rolesCompany.Add(companymanger.AddCompanyRole("Can Lock User"));




        }
    }
}
