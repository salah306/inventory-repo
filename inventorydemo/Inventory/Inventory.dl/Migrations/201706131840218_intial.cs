namespace Inventory.dl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class intial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountCategories",
                c => new
                    {
                        AccountCategoryId = c.Int(nullable: false, identity: true),
                        AccountCategoryName = c.String(),
                        Code = c.String(),
                        CodeABC = c.String(),
                        BalanceSheetTypeId = c.Int(nullable: false),
                        AccountTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        AccountsDataTypesId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountCategoryId)
                .ForeignKey("dbo.AccountsDataTypes", t => t.AccountsDataTypesId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.BalanceSheetTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BalanceSheetTypeId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountsDataTypesId);
            
            CreateTable(
                "dbo.AccountsDataTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                        aliasName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountCategoryProperties",
                c => new
                    {
                        AccountCategoryPropertiesId = c.Int(nullable: false, identity: true),
                        AccountCategoryPropertiesName = c.String(nullable: false, maxLength: 80),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountsTypesAcccountsId = c.Int(nullable: false),
                        AccountsDataTypesId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountCategoryPropertiesId)
                .ForeignKey("dbo.AccountsCatogeryTypesAcccounts", t => t.AccountsTypesAcccountsId, cascadeDelete: false)
                .ForeignKey("dbo.AccountsDataTypes", t => t.AccountsDataTypesId, cascadeDelete: false)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountsTypesAcccountsId)
                .Index(t => t.AccountsDataTypesId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.AccountCategoryPropertiesValues",
                c => new
                    {
                        AccountCategoryPropertiesValueId = c.Int(nullable: false, identity: true),
                        AccountCategoryPropertiesValueName = c.String(maxLength: 80),
                        AccountCategoryPropertiesId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountCategoryPropertiesValueId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountCategoryProperties", t => t.AccountCategoryPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountCategoryPropertiesId)
                .Index(t => t.AccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        AccountName = c.String(nullable: false, maxLength: 200),
                        AccountAliasName = c.String(maxLength: 200),
                        Code = c.String(),
                        CodeABC = c.String(),
                        AccountCategoryId = c.Int(nullable: false),
                        CustomAccountCategoryId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        Activated = c.Boolean(nullable: false),
                        CompanyId = c.Int(),
                        isCompany = c.Boolean(nullable: false),
                        AccountType_AccountTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.AccountTypes", t => t.AccountType_AccountTypeId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => new { t.AccountName, t.BranchId }, name: "AK_Accounts_AccountName")
                .Index(t => t.AccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.AccountType_AccountTypeId);
            
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        BranchId = c.Int(nullable: false, identity: true),
                        BranchName = c.String(nullable: false, maxLength: 200),
                        BranchCode = c.String(),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => new { t.BranchName, t.CompanyId }, unique: true, name: "AK_Branch_BranchName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountMovements",
                c => new
                    {
                        AccountMovementId = c.Int(nullable: false, identity: true),
                        AccountMovementCode = c.String(),
                        AccountMovementDate = c.DateTime(nullable: false),
                        Debit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Crdit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountMovementNote = c.String(maxLength: 200),
                        typeName = c.String(),
                        AccountId = c.Int(nullable: false),
                        AccountOrderId = c.Int(nullable: false),
                        AccountorderHasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        HasCanceled = c.Boolean(),
                        SubAccountOrderId = c.Int(),
                        Branch_BranchId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountMovementId)
                .ForeignKey("dbo.AccountOrders", t => t.AccountOrderId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SubAccountOrders", t => t.SubAccountOrderId)
                .ForeignKey("dbo.Branches", t => t.Branch_BranchId)
                .Index(t => t.AccountId)
                .Index(t => t.AccountOrderId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.SubAccountOrderId)
                .Index(t => t.Branch_BranchId);
            
            CreateTable(
                "dbo.AccountOrders",
                c => new
                    {
                        AccountOrderId = c.Int(nullable: false, identity: true),
                        OrderDate = c.DateTime(nullable: false),
                        OrderNote = c.String(),
                        HasDone = c.Boolean(),
                        AccountOrderPropertiesId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountOrderCode = c.String(),
                        BranchId = c.Int(),
                        OrderMoveRequestId = c.Int(),
                        CompanyId = c.Int(),
                        OrderNo = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        OrderTypeRefrence = c.String(),
                        OrderIdRefrence = c.Int(),
                        IsSales = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AccountOrderId)
                .ForeignKey("dbo.AccountOrderProperties", t => t.AccountOrderPropertiesId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderMoveRequests", t => t.OrderMoveRequestId)
                .Index(t => t.AccountOrderPropertiesId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.OrderMoveRequestId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.AccountOrderProperties",
                c => new
                    {
                        AccountOrderPropertiesId = c.Int(nullable: false, identity: true),
                        AccountOrderPropertiesName = c.String(nullable: false, maxLength: 200),
                        AccountOrderPropertiesCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountOrderPropertiesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(maxLength: 50),
                        LastName = c.String(maxLength: 50),
                        Occupation = c.String(),
                        Phone = c.String(maxLength: 50),
                        About = c.String(),
                        ImageUrl = c.String(),
                        MaxCompany = c.Int(),
                        JoinDate = c.DateTime(nullable: false),
                        Level = c.Byte(nullable: false),
                        Gender = c.String(maxLength: 50),
                        BirthDate = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.ApplicationUserComapnies",
                c => new
                    {
                        ApplicationUserComapnyId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(),
                        ApplicationUserId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        BalanceSheetId = c.Int(),
                        FinancialListId = c.Int(),
                        ItemGroupId = c.Int(),
                        UserAcceptedPolicy = c.Boolean(),
                        Locked = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CreatedDate = c.DateTime(nullable: false),
                        ApplicationUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ApplicationUserComapnyId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId)
                .ForeignKey("dbo.BalanceSheets", t => t.BalanceSheetId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.FinancialLists", t => t.FinancialListId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id)
                .Index(t => t.CompanyId)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.BranchId)
                .Index(t => t.BalanceSheetId)
                .Index(t => t.FinancialListId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.ApplicationUser_Id);
            
            CreateTable(
                "dbo.BalanceSheets",
                c => new
                    {
                        BalanceSheetId = c.Int(nullable: false, identity: true),
                        BalanceSheetName = c.String(nullable: false, maxLength: 200),
                        Code = c.String(),
                        CodeABC = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountTypeId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        BsMainTypeId = c.Int(),
                        Branch_BranchId = c.Int(),
                    })
                .PrimaryKey(t => t.BalanceSheetId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.BsMainTypes", t => t.BsMainTypeId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Branches", t => t.Branch_BranchId)
                .Index(t => new { t.BalanceSheetName, t.CompanyId }, unique: true, name: "AK_BalanceSheet_BalanceSheetName")
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.BsMainTypeId)
                .Index(t => t.Branch_BranchId);
            
            CreateTable(
                "dbo.AccountTypes",
                c => new
                    {
                        AccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(nullable: false, maxLength: 200),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeName, unique: true, name: "AK_AccountType_AccountTypeName")
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BsMainTypes",
                c => new
                    {
                        BsMainTypeId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NickName = c.String(),
                        orderNum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BsMainTypeId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false, maxLength: 200),
                        CompanyCode = c.String(),
                        InventoryValuationMethodId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        MaxBranch = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                        startDate = c.DateTime(),
                        endDate = c.DateTime(),
                        closed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.InventoryValuationMethods", t => t.InventoryValuationMethodId)
                .Index(t => t.CompanyName, unique: true, name: "AK_Company_CompanyName")
                .Index(t => t.InventoryValuationMethodId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoryValuationMethods",
                c => new
                    {
                        InventoryValuationMethodId = c.Int(nullable: false, identity: true),
                        InventoryValuationMethodName = c.String(),
                    })
                .PrimaryKey(t => t.InventoryValuationMethodId);
            
            CreateTable(
                "dbo.ItemGroups",
                c => new
                    {
                        ItemGroupId = c.Int(nullable: false, identity: true),
                        ItemGroupName = c.String(nullable: false, maxLength: 100),
                        UnitId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemGroupId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: false)
                .Index(t => new { t.ItemGroupName, t.CompanyId }, unique: true, name: "IX_ItemGroupName")
                .Index(t => t.UnitId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountsforItemGroups",
                c => new
                    {
                        AccountsforItemGroupId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        ItemGroupId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountsforItemGroupId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PropertiesforItemGroups",
                c => new
                    {
                        PropertiesforItemGroupId = c.Int(nullable: false, identity: true),
                        PropertiesforItemGroupName = c.String(),
                        TypeforItemGroupId = c.Int(nullable: false),
                        Show = c.Boolean(nullable: false),
                        ItemGroupId = c.Int(nullable: false),
                        orderNum = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PropertiesforItemGroupId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .ForeignKey("dbo.TypeforItemGroups", t => t.TypeforItemGroupId, cascadeDelete: false)
                .Index(t => t.TypeforItemGroupId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PropertyValuesforItemGroups",
                c => new
                    {
                        PropertyValuesforItemGroupId = c.Int(nullable: false, identity: true),
                        PropertyValuesforItemGroupName = c.String(),
                        PropertyValuesforItemGroupValue = c.String(),
                        PropertiesforItemGroupId = c.Int(nullable: false),
                        orderNum = c.Int(),
                        uniqueId = c.String(),
                        isNew = c.Boolean(nullable: false),
                        Realted = c.String(nullable: false),
                        parintType = c.String(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PropertyValuesforItemGroupId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.PropertiesforItemGroups", t => t.PropertiesforItemGroupId, cascadeDelete: false)
                .Index(t => t.PropertiesforItemGroupId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.TempItempropertyValues",
                c => new
                    {
                        TempItempropertyValuesId = c.Int(nullable: false, identity: true),
                        TempItemId = c.Int(),
                        PropertyValuesforItemGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.TempItempropertyValuesId)
                .ForeignKey("dbo.PropertyValuesforItemGroups", t => t.PropertyValuesforItemGroupId)
                .ForeignKey("dbo.TempItems", t => t.TempItemId)
                .Index(t => t.TempItemId)
                .Index(t => t.PropertyValuesforItemGroupId);
            
            CreateTable(
                "dbo.TempItems",
                c => new
                    {
                        TempItemId = c.Int(nullable: false, identity: true),
                        code = c.String(nullable: false),
                        name = c.String(nullable: false),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        priceMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        priceMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpireDate = c.DateTime(),
                        ItemGroupId = c.Int(nullable: false),
                        UnitId = c.Int(nullable: false),
                        reorderPoint = c.String(),
                        CompanyId = c.Int(nullable: false),
                        Realted = c.String(nullable: false),
                        ExpireinDayes = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.TempItemId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: false)
                .Index(t => t.ItemGroupId)
                .Index(t => t.UnitId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.SubAccounts",
                c => new
                    {
                        SubAccountId = c.Int(nullable: false, identity: true),
                        SubAccountName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        Activated = c.Boolean(nullable: false),
                        Code = c.String(),
                        ItemInfoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.TempItems", t => t.ItemInfoId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.ItemInfoId);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        UnitId = c.Int(nullable: false, identity: true),
                        UnitName = c.String(nullable: false, maxLength: 100),
                        UnitTypeId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UnitId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.UnitTypes", t => t.UnitTypeId, cascadeDelete: false)
                .Index(t => new { t.UnitName, t.CompanyId }, unique: true, name: "IX_UnitName")
                .Index(t => t.UnitTypeId);
            
            CreateTable(
                "dbo.UnitTypes",
                c => new
                    {
                        UnitTypeId = c.Int(nullable: false, identity: true),
                        UnitTypeName = c.String(),
                    })
                .PrimaryKey(t => t.UnitTypeId);
            
            CreateTable(
                "dbo.TypeforItemGroups",
                c => new
                    {
                        TypeforItemGroupId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                        Value = c.String(),
                        Required = c.Boolean(nullable: false),
                        show = c.Boolean(nullable: false),
                        description = c.String(),
                        orderNum = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.TypeforItemGroupId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.FinancialLists",
                c => new
                    {
                        FinancialListId = c.Int(nullable: false, identity: true),
                        FinancialListName = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.FinancialListId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.CustomBalanceSheets",
                c => new
                    {
                        CustomBalanceSheetId = c.Int(nullable: false, identity: true),
                        BalanceSheetName = c.String(),
                        BalanceSheetCode = c.String(),
                        FinancialListId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountTypeId = c.Int(),
                        IsLinked = c.Boolean(),
                        LinkedAccountId = c.Int(),
                        LinkedAccountName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomBalanceSheetId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.FinancialLists", t => t.FinancialListId, cascadeDelete: false)
                .Index(t => t.FinancialListId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.CustomBalanceSheetTypes",
                c => new
                    {
                        CustomBalanceSheetTypeId = c.Int(nullable: false, identity: true),
                        BalanceSheetTypeName = c.String(),
                        BalanceSheetTypeCode = c.String(),
                        CustomBalanceSheetId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        IsLinked = c.Boolean(),
                        LinkedAccountId = c.Int(),
                        LinkedAccountName = c.String(),
                        AccountTypeId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomBalanceSheetTypeId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.CustomBalanceSheets", t => t.CustomBalanceSheetId, cascadeDelete: false)
                .Index(t => t.CustomBalanceSheetId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.CustomAccountCategories",
                c => new
                    {
                        CustomAccountCategoryId = c.Int(nullable: false, identity: true),
                        AccountCategoryName = c.String(),
                        AccountCategoryCode = c.String(),
                        CustomBalanceSheetTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        IsLinked = c.Boolean(),
                        LinkedAccountId = c.Int(),
                        LinkedAccountName = c.String(),
                        AccountTypeId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomAccountCategoryId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.CustomBalanceSheetTypes", t => t.CustomBalanceSheetTypeId, cascadeDelete: false)
                .Index(t => t.CustomBalanceSheetTypeId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.CustomAccounts",
                c => new
                    {
                        CustomAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        CustomAccountCategoryId = c.Int(nullable: false),
                        AccountName = c.String(),
                        AccountCatogeryName = c.String(),
                        CurrentWorkerId = c.String(),
                        AccountTypeId = c.Int(),
                        Code = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.CustomAccountCategories", t => t.CustomAccountCategoryId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.CustomAccountCategoryId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.OrderMoveRequests",
                c => new
                    {
                        OrderMoveRequestId = c.Int(nullable: false, identity: true),
                        OrderNo = c.Int(nullable: false),
                        mainAccountId = c.Int(nullable: false),
                        tilteAccountId = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        typeName = c.String(),
                        isDone = c.Boolean(nullable: false),
                        isToCancel = c.Boolean(nullable: false),
                        attchedRequest = c.Boolean(nullable: false),
                        orderRequestId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        AccountsDataTypesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        note = c.String(),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.OrderMoveRequestId)
                .ForeignKey("dbo.AccountsDataTypes", t => t.AccountsDataTypesId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.mainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderRequests", t => t.orderRequestId)
                .ForeignKey("dbo.Accounts", t => t.tilteAccountId, cascadeDelete: false)
                .Index(t => t.mainAccountId)
                .Index(t => t.tilteAccountId)
                .Index(t => t.orderRequestId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountsDataTypesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderRequests",
                c => new
                    {
                        OrderRequestId = c.Int(nullable: false, identity: true),
                        requestNo = c.Int(nullable: false),
                        mainAccountId = c.Int(nullable: false),
                        tilteAccountId = c.Int(nullable: false),
                        requestDate = c.DateTime(nullable: false),
                        dueDate = c.DateTime(nullable: false),
                        typeName = c.String(),
                        isDone = c.Boolean(nullable: false),
                        isToCancel = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        AccountsDataTypesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        refrenceRequesterOrderType = c.String(),
                        refrenceRequesterOType = c.String(),
                        refrenceRequesterNo = c.Int(),
                    })
                .PrimaryKey(t => t.OrderRequestId)
                .ForeignKey("dbo.AccountsDataTypes", t => t.AccountsDataTypesId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.mainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.tilteAccountId, cascadeDelete: false)
                .Index(t => t.mainAccountId)
                .Index(t => t.tilteAccountId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountsDataTypesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderRequestItems",
                c => new
                    {
                        OrderRequestItemsId = c.Int(nullable: false, identity: true),
                        note = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderRequestId = c.Int(nullable: false),
                        refrenceType = c.String(),
                        refrenceTypeId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderRequestItemsId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderRequests", t => t.OrderRequestId, cascadeDelete: false)
                .Index(t => t.OrderRequestId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SubAccountOrders",
                c => new
                    {
                        SubAccountOrderId = c.Int(nullable: false, identity: true),
                        OrderNote = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CustomSubAccountsOrderId = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        isDone = c.Boolean(),
                    })
                .PrimaryKey(t => t.SubAccountOrderId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BranchProperties",
                c => new
                    {
                        BranchPropertiesId = c.Int(nullable: false, identity: true),
                        BranchId = c.Int(nullable: false),
                        propertyName = c.String(),
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BranchPropertiesId)
                .ForeignKey("dbo.AccountsCatogeryTypesAcccounts", t => t.AccountsCatogeryTypesAcccountsId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountsCatogeryTypesAcccountsId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AccountsCatogeryTypesAcccounts",
                c => new
                    {
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.AccountsCatogeryTypesAcccountsId);
            
            CreateTable(
                "dbo.BranchPropertyValues",
                c => new
                    {
                        BranchPropertyValuesId = c.Int(nullable: false, identity: true),
                        BranchPropertiesId = c.Int(nullable: false),
                        propertyValue = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BranchPropertyValuesId)
                .ForeignKey("dbo.BranchProperties", t => t.BranchPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BranchPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BalanceSheetTypes",
                c => new
                    {
                        BalanceSheetTypeId = c.Int(nullable: false, identity: true),
                        BalanceSheetTypeName = c.String(),
                        Code = c.String(),
                        CodeABC = c.String(),
                        BalanceSheetId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AccountTypeId = c.Int(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BalanceSheetTypeId)
                .ForeignKey("dbo.AccountTypes", t => t.AccountTypeId)
                .ForeignKey("dbo.BalanceSheets", t => t.BalanceSheetId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BalanceSheetId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.AccountOes",
                c => new
                    {
                        AccountOId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        amountType = c.String(),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        name = c.String(),
                        ItemsRequestO_ItemsRequestOId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountOId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemsRequestOes", t => t.ItemsRequestO_ItemsRequestOId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.ItemsRequestO_ItemsRequestOId);
            
            CreateTable(
                "dbo.OrderOes",
                c => new
                    {
                        OrderOId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        SubjectAccountId = c.Int(nullable: false),
                        InventoryRequestsId = c.Int(nullable: false),
                        OrderTitleName = c.String(),
                        OrderConfigName = c.String(),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        recivedDate = c.DateTime(nullable: false),
                        userId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userNameThird = c.String(),
                        userThirdId = c.String(maxLength: 128),
                        cancelById = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        PurchaseOrderId = c.Int(),
                        requestOrderAttched = c.Boolean(nullable: false),
                        RefrenceRequestId = c.Int(),
                        RefrenceRequestName = c.String(),
                        RefrenceInventoryOrderId = c.Int(),
                        RefrenceInventoryOrderName = c.String(),
                        RefrenceQueryId = c.Int(),
                        RefrenceQueryName = c.String(),
                        RefrenceBillId = c.Int(),
                        RefrenceBillName = c.String(),
                        RefrenceOrderConfigId = c.Int(),
                        orderType = c.String(),
                        type = c.String(),
                    })
                .PrimaryKey(t => t.OrderOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.AspNetUsers", t => t.cancelById)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.InventoryRequests", t => t.InventoryRequestsId, cascadeDelete: false)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.RefrenceOrderConfigId)
                .ForeignKey("dbo.Accounts", t => t.SubjectAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferToOId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.userId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.SubjectAccountId)
                .Index(t => t.InventoryRequestsId)
                .Index(t => t.userId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.cancelById)
                .Index(t => t.TransferToOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.PurchaseOrderId)
                .Index(t => t.RefrenceOrderConfigId);
            
            CreateTable(
                "dbo.InventoryRequests",
                c => new
                    {
                        InventoryRequestId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        RecivedDate = c.DateTime(nullable: false),
                        userFirstId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userThirdId = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        cancelDate = c.DateTime(),
                        userCancelId = c.String(maxLength: 128),
                        orderONo = c.Int(),
                        OType = c.String(),
                        OrderType = c.String(),
                    })
                .PrimaryKey(t => t.InventoryRequestId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferToId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.userCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.userFirstId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.userFirstId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.TransferToId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.userCancelId);
            
            CreateTable(
                "dbo.InventoryItemsRequests",
                c => new
                    {
                        InventoryItemsRequestId = c.Int(nullable: false, identity: true),
                        InventoryRequestId = c.Int(nullable: false),
                        SubAccountId = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Realted = c.String(),
                    })
                .PrimaryKey(t => t.InventoryItemsRequestId)
                .ForeignKey("dbo.InventoryRequests", t => t.InventoryRequestId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.InventoryRequestId)
                .Index(t => t.SubAccountId);
            
            CreateTable(
                "dbo.ItemsRequestOes",
                c => new
                    {
                        ItemsRequestOId = c.Int(nullable: false, identity: true),
                        SubAccountId = c.Int(nullable: false),
                        itemId = c.Int(nullable: false),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        costPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        realted = c.String(),
                        itemUnitType = c.String(),
                        itemUnitName = c.String(),
                        maxQ = c.Decimal(precision: 18, scale: 2),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ItemsRequestOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.SubAccountId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PayOes",
                c => new
                    {
                        PayOId = c.Int(nullable: false, identity: true),
                        PayId = c.Int(nullable: false),
                        PayName = c.String(),
                        AccountId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        dueDate = c.DateTime(nullable: false),
                        note = c.String(),
                        type = c.String(),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PayOId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        PurchaseOrderId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        RecivedDate = c.DateTime(nullable: false),
                        userFirstId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userThirdId = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        cancelDate = c.DateTime(),
                        userCancelId = c.String(maxLength: 128),
                        orderONo = c.Int(),
                        OType = c.String(),
                        OrderType = c.String(),
                    })
                .PrimaryKey(t => t.PurchaseOrderId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferToId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.userCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.userFirstId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.userFirstId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.TransferToId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.userCancelId);
            
            CreateTable(
                "dbo.ItemsRequests",
                c => new
                    {
                        ItemsRequestId = c.Int(nullable: false, identity: true),
                        PurchaseOrderId = c.Int(nullable: false),
                        SubAccountId = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Realted = c.String(),
                    })
                .PrimaryKey(t => t.ItemsRequestId)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.PurchaseOrderId)
                .Index(t => t.SubAccountId);
            
            CreateTable(
                "dbo.PurchaseOrdersConfigurations",
                c => new
                    {
                        PurchaseOrdersConfigurationId = c.Int(nullable: false, identity: true),
                        PurchaseOrderName = c.String(nullable: false),
                        PurchaseName = c.String(nullable: false),
                        PurchaseReturnName = c.String(nullable: false),
                        IsActiveOrder = c.Boolean(nullable: false),
                        OnlyInventory = c.Boolean(nullable: false),
                        DirectGlTransaction = c.Boolean(nullable: false),
                        TitleAccountNickName = c.String(),
                        TitleAccountCategoryId = c.Int(nullable: false),
                        TitleBalanceSheetTypeId = c.Int(),
                        DefaultPayAccountCategoryId = c.Int(),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PurchaseOrdersConfigurationId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AccountCategories", t => t.DefaultPayAccountCategoryId)
                .ForeignKey("dbo.AccountCategories", t => t.TitleAccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.TitleBalanceSheetTypeId)
                .Index(t => t.TitleAccountCategoryId)
                .Index(t => t.TitleBalanceSheetTypeId)
                .Index(t => t.DefaultPayAccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationPayMethods",
                c => new
                    {
                        PurchasesOrdersConfigurationPayMethodId = c.Int(nullable: false, identity: true),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        PayName = c.String(),
                        AccountCategoryId = c.Int(nullable: false),
                        DefaultChildAccountId = c.Int(),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationPayMethodId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.DefaultChildAccountId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.DefaultChildAccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTableAccounts",
                c => new
                    {
                        PurchasesOrdersConfigurationTableAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        CurrentWorkerTableAccountId = c.String(maxLength: 128),
                        itemId = c.Int(),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        itemcostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        realted = c.String(),
                        ItemGroupId = c.Int(),
                        isInventory = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        serialNo = c.String(),
                        expireDate = c.DateTime(),
                        attchedRequest = c.Boolean(nullable: false),
                        isPercent = c.Boolean(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        firstItemNote = c.String(),
                        SecondItemNote = c.String(),
                        thirdItemNote = c.String(),
                        firstItemNoteId = c.Int(),
                        SecondItemNoteId = c.Int(),
                        thirdItemNoteId = c.Int(),
                        firstItemAccountNoteId = c.Int(),
                        secondItemAccountNoteId = c.Int(),
                        thirdItemAccountNoteId = c.Int(),
                        firstItemNoteDate = c.DateTime(),
                        secondItemNoteDate = c.DateTime(),
                        thirdItemNoteDate = c.DateTime(),
                        firstReferenceId = c.Int(),
                        firstReferenceType = c.Int(),
                        secondReferenceId = c.Int(),
                        secondReferenceType = c.Int(),
                        thirdReferenceId = c.Int(),
                        thirdReferenceType = c.Int(),
                        cancelDate = c.DateTime(),
                        doneFirst = c.Boolean(nullable: false),
                        doneSceond = c.Boolean(nullable: false),
                        doneThird = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        CurrentWorkerCancelId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.firstItemAccountNoteId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.secondItemAccountNoteId)
                .ForeignKey("dbo.Accounts", t => t.thirdItemAccountNoteId)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerTableAccountId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.firstItemAccountNoteId)
                .Index(t => t.secondItemAccountNoteId)
                .Index(t => t.thirdItemAccountNoteId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.CurrentWorkerCancelId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationAccountTypes",
                c => new
                    {
                        PurchasesOrdersConfigurationAccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationAccountTypeId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTerms",
                c => new
                    {
                        PurchasesOrdersConfigurationTermId = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTermId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTotalAccounts",
                c => new
                    {
                        PurchasesOrdersConfigurationTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTotalAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.TermOes",
                c => new
                    {
                        TermOId = c.Int(nullable: false, identity: true),
                        termName = c.String(nullable: false),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.TermOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.TotalAccountOes",
                c => new
                    {
                        TotalAccountOId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        type = c.String(),
                        amountType = c.String(),
                        name = c.String(),
                        OrderO_OrderOId = c.Int(),
                    })
                .PrimaryKey(t => t.TotalAccountOId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderOes", t => t.OrderO_OrderOId)
                .Index(t => t.AccountId)
                .Index(t => t.OrderO_OrderOId);
            
            CreateTable(
                "dbo.ActivityLogs",
                c => new
                    {
                        ActivityLogId = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Activity = c.String(),
                        PageUrl = c.String(),
                        ActivityDte = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ActivityLogId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AppUserCompanyRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                        CompanyRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.ApplicationUserComapnyId)
                .Index(t => t.CompanyRoleId);
            
            CreateTable(
                "dbo.CompanyRoles",
                c => new
                    {
                        CompanyRoleId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.CompanyRoleId)
                .Index(t => t.Name, unique: true, name: "AK_CompanyRole_Name");
            
            CreateTable(
                "dbo.BillAccountItems",
                c => new
                    {
                        BillAccountItemsId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                        uniqueId = c.String(),
                    })
                .PrimaryKey(t => t.BillAccountItemsId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.AccountId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
            CreateTable(
                "dbo.PurchasesBills",
                c => new
                    {
                        PurchasesBillId = c.Int(nullable: false, identity: true),
                        PurchasesBillName = c.String(),
                        type = c.String(),
                        BillNo = c.Int(nullable: false),
                        OrdersTypePurchasesType = c.String(),
                        OrdersTypePurchasesId = c.Int(nullable: false),
                        BillDate = c.DateTime(nullable: false),
                        BillMainAccountId = c.Int(nullable: false),
                        BillTitleAccountId = c.Int(nullable: false),
                        BillTotalAccountId = c.Int(nullable: false),
                        BillPaymethodId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PurchasesBillId)
                .ForeignKey("dbo.Accounts", t => t.BillMainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderGroupPayMethodPurchases", t => t.BillPaymethodId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTitleAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTotalAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrdersTypePurchases", t => t.OrdersTypePurchasesId, cascadeDelete: false)
                .Index(t => t.OrdersTypePurchasesId)
                .Index(t => t.BillMainAccountId)
                .Index(t => t.BillTitleAccountId)
                .Index(t => t.BillTotalAccountId)
                .Index(t => t.BillPaymethodId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.BillOtherAccountBills",
                c => new
                    {
                        BillOtherAccountBillId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                    })
                .PrimaryKey(t => t.BillOtherAccountBillId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.AccountId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
            CreateTable(
                "dbo.SalesBills",
                c => new
                    {
                        SalesBillId = c.Int(nullable: false, identity: true),
                        SalesBillName = c.String(),
                        type = c.String(),
                        BillNo = c.Int(nullable: false),
                        OrdersTypeSalesType = c.String(),
                        OrdersTypeSalesId = c.Int(nullable: false),
                        BillDate = c.DateTime(nullable: false),
                        BillMainAccountId = c.Int(nullable: false),
                        BillTitleAccountId = c.Int(nullable: false),
                        BillTotalAccountId = c.Int(nullable: false),
                        BillPaymethodId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SalesBillId)
                .ForeignKey("dbo.Accounts", t => t.BillMainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderGroupPayMethodSales", t => t.BillPaymethodId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTitleAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTotalAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrdersTypeSales", t => t.OrdersTypeSalesId, cascadeDelete: false)
                .Index(t => t.OrdersTypeSalesId)
                .Index(t => t.BillMainAccountId)
                .Index(t => t.BillTitleAccountId)
                .Index(t => t.BillTotalAccountId)
                .Index(t => t.BillPaymethodId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.BillPaymentProperties",
                c => new
                    {
                        BillPaymentPropertiesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesId = c.Int(),
                        OrderPropertiesPayPurchasesId = c.Int(),
                        PurchasesBillId = c.Int(),
                        SalesBillId = c.Int(),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesId)
                .ForeignKey("dbo.OrderPropertiesPayPurchases", t => t.OrderPropertiesPayPurchasesId)
                .ForeignKey("dbo.OrderPropertiesPaySales", t => t.OrderPropertiesPaySalesId)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.OrderPropertiesPaySalesId)
                .Index(t => t.OrderPropertiesPayPurchasesId)
                .Index(t => t.PurchasesBillId)
                .Index(t => t.SalesBillId);
            
            CreateTable(
                "dbo.OrderPropertiesPayPurchases",
                c => new
                    {
                        OrderPropertiesPayPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPayPurchasesName = c.String(),
                        OrderGroupPayMethodPurchasesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        ToPrint = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPayPurchasesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderGroupPayMethodPurchases", t => t.OrderGroupPayMethodPurchasesId, cascadeDelete: false)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId, cascadeDelete: false)
                .Index(t => t.OrderGroupPayMethodPurchasesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderGroupPayMethodPurchases",
                c => new
                    {
                        OrderGroupPayMethodPurchasesId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodPurchasesName = c.String(nullable: false),
                        AccountCategoryId = c.Int(nullable: false),
                        AccountId = c.Int(),
                        PurchasesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodPurchasesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.AccountId)
                .Index(t => t.PurchasesId);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        PurchasesId = c.Int(nullable: false, identity: true),
                        PurchasesName = c.String(),
                        IsCompany = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderAccountsforSubAccountPurchases",
                c => new
                    {
                        OrderAccountsforSubAccountPurchasesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        IsNewOrder = c.Boolean(nullable: false),
                        CostEffect = c.Boolean(nullable: false),
                        DebitforPlus = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountPurchasesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.PurchasesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccountPurchases",
                c => new
                    {
                        OrderOtherTotalAccountPurchasesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        IsNewOrder = c.Boolean(nullable: false),
                        CostEffect = c.Boolean(nullable: false),
                        DebitforPlus = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountPurchasesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.PurchasesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrdersTypePurchases",
                c => new
                    {
                        OrdersTypePurchasesId = c.Int(nullable: false, identity: true),
                        sale = c.String(nullable: false),
                        returnd = c.String(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrdersTypePurchasesId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .Index(t => t.PurchasesId);
            
            CreateTable(
                "dbo.OrderPropertyTypes",
                c => new
                    {
                        OrderPropertyTypeId = c.Int(nullable: false, identity: true),
                        OrderPropertyTypeName = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertyTypeId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderPropertiesPaySales",
                c => new
                    {
                        OrderPropertiesPaySalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesName = c.String(),
                        OrderGroupPayMethodSalesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        ToPrint = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPaySalesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderGroupPayMethodSales", t => t.OrderGroupPayMethodSalesId, cascadeDelete: false)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId, cascadeDelete: false)
                .Index(t => t.OrderGroupPayMethodSalesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderGroupPayMethodSales",
                c => new
                    {
                        OrderGroupPayMethodSalesId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodName = c.String(nullable: false),
                        AccountCategoryId = c.Int(nullable: false),
                        AccountId = c.Int(),
                        SalesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodSalesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.AccountId)
                .Index(t => t.SalesId);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        SalesId = c.Int(nullable: false, identity: true),
                        SalesName = c.String(),
                        IsCompany = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalesId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderAccountsforSubAccountSales",
                c => new
                    {
                        OrderAccountsforSubAccountSalesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        IsNewOrder = c.Boolean(nullable: false),
                        CostEffect = c.Boolean(nullable: false),
                        DebitforPlus = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountSalesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SalesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderMainAccounts",
                c => new
                    {
                        OrderMainAccountId = c.Int(nullable: false, identity: true),
                        InventoryAccountId = c.Int(nullable: false),
                        SalesCostAccountId = c.Int(nullable: false),
                        SalesAccountId = c.Int(nullable: false),
                        SalesReturnAccountId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderMainAccountId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.InventoryAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesCostAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesReturnAccountId, cascadeDelete: false)
                .Index(t => t.InventoryAccountId)
                .Index(t => t.SalesCostAccountId)
                .Index(t => t.SalesAccountId)
                .Index(t => t.SalesReturnAccountId)
                .Index(t => t.SalesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccountSales",
                c => new
                    {
                        OrderOtherTotalAccountSalesId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                        IsNewOrder = c.Boolean(nullable: false),
                        CostEffect = c.Boolean(nullable: false),
                        DebitforPlus = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountSalesId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SalesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrdersTypeSales",
                c => new
                    {
                        OrdersTypeSalesId = c.Int(nullable: false, identity: true),
                        sale = c.String(nullable: false),
                        returnd = c.String(nullable: false),
                        SalesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrdersTypeSalesId)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.SalesId);
            
            CreateTable(
                "dbo.BillSubAccountItems",
                c => new
                    {
                        BillSubAccountItemsId = c.Int(nullable: false, identity: true),
                        SubAccountId = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                        uniqueId = c.String(),
                    })
                .PrimaryKey(t => t.BillSubAccountItemsId)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.SubAccountId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
            CreateTable(
                "dbo.ItemsRows",
                c => new
                    {
                        itemsRowId = c.Int(nullable: false, identity: true),
                        itemPropertyValueId = c.Int(nullable: false),
                        itemPropertyValueName = c.String(),
                        itemPropertyId = c.Int(nullable: false),
                        value = c.String(),
                        type = c.String(),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                        orderid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.itemsRowId)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
            CreateTable(
                "dbo.billMainAccountPurSts",
                c => new
                    {
                        billMainAccountPurStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        value = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.billMainAccountPurStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.billMainAccountSalesSts",
                c => new
                    {
                        billMainAccountSalesStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        value = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.billMainAccountSalesStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BillPaymentPropertiesSales",
                c => new
                    {
                        BillPaymentPropertiesSalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesId = c.Int(nullable: false),
                        SalesBillId = c.Int(nullable: false),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesSalesId)
                .ForeignKey("dbo.OrderPropertiesPaySales", t => t.OrderPropertiesPaySalesId, cascadeDelete: false)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId, cascadeDelete: false)
                .Index(t => t.OrderPropertiesPaySalesId)
                .Index(t => t.SalesBillId);
            
            CreateTable(
                "dbo.branchGroupSts",
                c => new
                    {
                        branchGroupStrId = c.Int(nullable: false, identity: true),
                        id = c.String(),
                        name = c.String(),
                        companyName = c.String(),
                        fullName = c.String(),
                        isCompany = c.Boolean(nullable: false),
                        companyId = c.Int(nullable: false),
                        realName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                        oId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.branchGroupStrId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.CompanyProperties",
                c => new
                    {
                        CompanyPropertiesId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        propertyName = c.String(),
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CompanyPropertiesId)
                .ForeignKey("dbo.AccountsCatogeryTypesAcccounts", t => t.AccountsCatogeryTypesAcccountsId, cascadeDelete: false)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.AccountsCatogeryTypesAcccountsId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.CompanyPropertyValues",
                c => new
                    {
                        CompanyPropertyValuesId = c.Int(nullable: false, identity: true),
                        CompanyPropertiesId = c.Int(nullable: false),
                        propertyValue = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CompanyPropertyValuesId)
                .ForeignKey("dbo.CompanyProperties", t => t.CompanyPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentId = c.Int(nullable: false, identity: true),
                        DocumentName = c.String(),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(nullable: false),
                        AccountMovementId = c.Int(nullable: false),
                        AccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.DocumentId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountMovements", t => t.AccountMovementId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountMovementId)
                .Index(t => t.AccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoriesConfigurtions",
                c => new
                    {
                        InventoriesConfigurtionId = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        SalesCostAccountId = c.Int(nullable: false),
                        SalesAccountId = c.Int(nullable: false),
                        SalesReturnId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoriesConfigurtionId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.InventoryId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesCostAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesReturnId, cascadeDelete: false)
                .Index(t => t.InventoryId)
                .Index(t => t.SalesCostAccountId)
                .Index(t => t.SalesAccountId)
                .Index(t => t.SalesReturnId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.InventoryItems",
                c => new
                    {
                        InventoryItemsId = c.Int(nullable: false, identity: true),
                        SubAccountFromId = c.Int(nullable: false),
                        SubAccountToId = c.Int(nullable: false),
                        SubAccountTransitId = c.Int(nullable: false),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        costPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        ItemGroupId = c.Int(nullable: false),
                        InventoryTransferEntityId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoryItemsId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.InventoryTransferEntities", t => t.InventoryTransferEntityId, cascadeDelete: false)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountFromId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountToId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountTransitId, cascadeDelete: false)
                .Index(t => t.SubAccountFromId)
                .Index(t => t.SubAccountToId)
                .Index(t => t.SubAccountTransitId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.InventoryTransferEntityId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoryTransferEntities",
                c => new
                    {
                        InventoryTransferEntityId = c.Int(nullable: false, identity: true),
                        OrderNo = c.Int(nullable: false),
                        OrderNoIn = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        OrderDateSecond = c.DateTime(nullable: false),
                        MainAccountId = c.Int(nullable: false),
                        TitleAccountId = c.Int(nullable: false),
                        Cancel = c.Boolean(nullable: false),
                        CancelDate = c.DateTime(),
                        DoneFirst = c.Boolean(nullable: false),
                        DoneSceond = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        CurrentWorkerCancelId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoryTransferEntityId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.Accounts", t => t.MainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TitleAccountId, cascadeDelete: false)
                .Index(t => t.MainAccountId)
                .Index(t => t.TitleAccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.CurrentWorkerCancelId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.inventorySts",
                c => new
                    {
                        inventoryStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.inventoryStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderGroups",
                c => new
                    {
                        OrderGroupId = c.Int(nullable: false, identity: true),
                        OrderGroupName = c.String(),
                        IsCompany = c.Boolean(nullable: false),
                        CompanyBranchId = c.Int(nullable: false),
                        SalesId = c.Int(nullable: false),
                        PurchasesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderGroupId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Purchases", t => t.PurchasesId, cascadeDelete: false)
                .ForeignKey("dbo.Sales", t => t.SalesId, cascadeDelete: false)
                .Index(t => t.SalesId)
                .Index(t => t.PurchasesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.RoleCollections",
                c => new
                    {
                        RoleCollectionId = c.Int(nullable: false, identity: true),
                        RoleCollectionName = c.String(),
                        CompanyRoleId = c.Int(nullable: false),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoleCollectionId)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.CompanyRoleId)
                .Index(t => t.ApplicationUserComapnyId);
            
            CreateTable(
                "dbo.RolepropertiesforUsers",
                c => new
                    {
                        RolepropertiesforUserId = c.Int(nullable: false, identity: true),
                        settingRoleId = c.Int(nullable: false),
                        isTrue = c.Boolean(nullable: false),
                        typeRefrenceId = c.Int(nullable: false),
                        typeRefrence = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.RolepropertiesforUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingRoles", t => t.settingRoleId, cascadeDelete: false)
                .Index(t => t.settingRoleId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingRoles",
                c => new
                    {
                        settingRoleId = c.Int(nullable: false, identity: true),
                        roleName = c.String(),
                        name = c.String(),
                        settingPropertiesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingRoleId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingProperties", t => t.settingPropertiesId, cascadeDelete: false)
                .Index(t => t.settingPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingProperties",
                c => new
                    {
                        settingPropertiesId = c.Int(nullable: false, identity: true),
                        propertyName = c.String(),
                        name = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingPropertiesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SalesOrdersConfigurations",
                c => new
                    {
                        SalesOrdersConfigurationId = c.Int(nullable: false, identity: true),
                        SalesOrderName = c.String(nullable: false),
                        SalesName = c.String(nullable: false),
                        SalesReturnName = c.String(nullable: false),
                        IsActiveOrder = c.Boolean(nullable: false),
                        OnlyInventory = c.Boolean(nullable: false),
                        DirectGlTransaction = c.Boolean(nullable: false),
                        TitleAccountNickName = c.String(),
                        TitleAccountCategoryId = c.Int(nullable: false),
                        TitleBalanceSheetTypeId = c.Int(),
                        DefaultPayAccountCategoryId = c.Int(),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.SalesOrdersConfigurationId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AccountCategories", t => t.DefaultPayAccountCategoryId)
                .ForeignKey("dbo.AccountCategories", t => t.TitleAccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.TitleBalanceSheetTypeId)
                .Index(t => t.TitleAccountCategoryId)
                .Index(t => t.TitleBalanceSheetTypeId)
                .Index(t => t.DefaultPayAccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationPayMethods",
                c => new
                    {
                        SalessOrdersConfigurationPayMethodId = c.Int(nullable: false, identity: true),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        PayName = c.String(),
                        AccountCategoryId = c.Int(nullable: false),
                        DefaultChildAccountId = c.Int(),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationPayMethodId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.DefaultChildAccountId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.DefaultChildAccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTableAccounts",
                c => new
                    {
                        SalessOrdersConfigurationTableAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        CurrentWorkerTableAccountId = c.String(maxLength: 128),
                        itemId = c.Int(),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        itemcostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        realted = c.String(),
                        ItemGroupId = c.Int(),
                        isInventory = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        serialNo = c.String(),
                        expireDate = c.DateTime(),
                        attchedRequest = c.Boolean(nullable: false),
                        isPercent = c.Boolean(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        firstItemNote = c.String(),
                        SecondItemNote = c.String(),
                        thirdItemNote = c.String(),
                        firstItemNoteId = c.Int(),
                        SecondItemNoteId = c.Int(),
                        thirdItemNoteId = c.Int(),
                        firstItemAccountNoteId = c.Int(),
                        secondItemAccountNoteId = c.Int(),
                        thirdItemAccountNoteId = c.Int(),
                        firstItemNoteDate = c.DateTime(),
                        secondItemNoteDate = c.DateTime(),
                        thirdItemNoteDate = c.DateTime(),
                        firstReferenceId = c.Int(),
                        firstReferenceType = c.Int(),
                        secondReferenceId = c.Int(),
                        secondReferenceType = c.Int(),
                        thirdReferenceId = c.Int(),
                        thirdReferenceType = c.Int(),
                        cancelDate = c.DateTime(),
                        doneFirst = c.Boolean(nullable: false),
                        doneSceond = c.Boolean(nullable: false),
                        doneThird = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        CurrentWorkerCancelId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.SalessOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.firstItemAccountNoteId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.secondItemAccountNoteId)
                .ForeignKey("dbo.Accounts", t => t.thirdItemAccountNoteId)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerTableAccountId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.firstItemAccountNoteId)
                .Index(t => t.secondItemAccountNoteId)
                .Index(t => t.thirdItemAccountNoteId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.CurrentWorkerCancelId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationAccountTypes",
                c => new
                    {
                        SalessOrdersConfigurationAccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationAccountTypeId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTerms",
                c => new
                    {
                        SalessOrdersConfigurationTermId = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTermId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTotalAccounts",
                c => new
                    {
                        SalessOrdersConfigurationTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTotalAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.SalessOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.selectedOrderGroupNameSts",
                c => new
                    {
                        selectedOrderGroupNameStId = c.Int(nullable: false, identity: true),
                        orderGroupId = c.Int(nullable: false),
                        orderGroupName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.selectedOrderGroupNameStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingPropertiesforUsers",
                c => new
                    {
                        settingPropertiesforUserId = c.Int(nullable: false, identity: true),
                        settingPropertiesId = c.Int(nullable: false),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingPropertiesforUserId)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingProperties", t => t.settingPropertiesId, cascadeDelete: false)
                .Index(t => t.settingPropertiesId)
                .Index(t => t.ApplicationUserComapnyId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.settingRolesForUsers",
                c => new
                    {
                        settingRolesForUserId = c.Int(nullable: false, identity: true),
                        settingPropertiesforUserId = c.Int(nullable: false),
                        roleName = c.String(),
                        active = c.Boolean(nullable: false),
                        RefrenceType = c.String(),
                        RefrenceId = c.Int(),
                        settingRoleId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.settingRolesForUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.settingPropertiesforUsers", t => t.settingPropertiesforUserId, cascadeDelete: false)
                .ForeignKey("dbo.settingRoles", t => t.settingRoleId, cascadeDelete: false)
                .Index(t => t.settingPropertiesforUserId)
                .Index(t => t.settingRoleId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SubAccountMovements",
                c => new
                    {
                        SubAccountMovementId = c.Int(nullable: false, identity: true),
                        SubAccountMovementDate = c.DateTime(nullable: false),
                        SubAccountId = c.Int(nullable: false),
                        AccountorderHasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        OrderId = c.Int(nullable: false),
                        OrderNote = c.String(),
                        SubAccountOrderId = c.Int(nullable: false),
                        QuantityIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QuantityOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemPricein = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemPriceOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemTotalPriceIn = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemTotalPriceOut = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemStockQuantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemStockPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ItemTotalStockPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubAccountMovementId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccountOrders", t => t.SubAccountOrderId, cascadeDelete: false)
                .Index(t => t.SubAccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.SubAccountOrderId);
            
            CreateTable(
                "dbo.Vocabularies",
                c => new
                    {
                        VocabularyId = c.Int(nullable: false, identity: true),
                        Lang = c.String(),
                        Name = c.String(nullable: false, maxLength: 200),
                        CompanyRoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VocabularyId)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.Name, unique: true, name: "AK_Vocabulary_Name")
                .Index(t => t.CompanyRoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vocabularies", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.SubAccountMovements", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.SubAccountMovements", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.SubAccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingRolesForUsers", "settingRoleId", "dbo.settingRoles");
            DropForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUserId", "dbo.settingPropertiesforUsers");
            DropForeignKey("dbo.settingRolesForUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingPropertiesforUsers", "settingPropertiesId", "dbo.settingProperties");
            DropForeignKey("dbo.settingPropertiesforUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingPropertiesforUsers", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropForeignKey("dbo.selectedOrderGroupNameSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "AccountTypeId", "dbo.SalessOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesOrdersConfigurations", "TitleBalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.SalesOrdersConfigurations", "TitleAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.SalessOrdersConfigurationTerms", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTerms", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "thirdItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "secondItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "firstItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerTableAccountId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.SalessOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "DefaultChildAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.SalesOrdersConfigurations", "DefaultPayAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.SalesOrdersConfigurations", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalesOrdersConfigurations", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SalesOrdersConfigurations", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.RolepropertiesforUsers", "settingRoleId", "dbo.settingRoles");
            DropForeignKey("dbo.settingRoles", "settingPropertiesId", "dbo.settingProperties");
            DropForeignKey("dbo.settingProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.settingRoles", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RolepropertiesforUsers", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.RoleCollections", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.RoleCollections", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropForeignKey("dbo.OrderGroups", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderGroups", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.inventorySts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryItems", "SubAccountTransitId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItems", "SubAccountToId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItems", "SubAccountFromId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItems", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.InventoryTransferEntities", "TitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryTransferEntities", "MainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryItems", "InventoryTransferEntityId", "dbo.InventoryTransferEntities");
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryTransferEntities", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoryTransferEntities", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.InventoryItems", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesReturnId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesCostAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "InventoryId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoriesConfigurtions", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoriesConfigurtions", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Documents", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Documents", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Documents", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Documents", "AccountMovementId", "dbo.AccountMovements");
            DropForeignKey("dbo.Documents", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CompanyProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CompanyPropertyValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CompanyPropertyValues", "CompanyPropertiesId", "dbo.CompanyProperties");
            DropForeignKey("dbo.CompanyProperties", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.CompanyProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropForeignKey("dbo.branchGroupSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BillPaymentPropertiesSales", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillPaymentPropertiesSales", "OrderPropertiesPaySalesId", "dbo.OrderPropertiesPaySales");
            DropForeignKey("dbo.billMainAccountSalesSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.billMainAccountPurSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesBills", "OrdersTypePurchasesId", "dbo.OrdersTypePurchases");
            DropForeignKey("dbo.PurchasesBills", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesBills", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchasesBills", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.PurchasesBills", "BillTotalAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesBills", "BillTitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesBills", "BillPaymethodId", "dbo.OrderGroupPayMethodPurchases");
            DropForeignKey("dbo.SalesBills", "OrdersTypeSalesId", "dbo.OrdersTypeSales");
            DropForeignKey("dbo.ItemsRows", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.ItemsRows", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.SalesBills", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalesBills", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SalesBills", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.SalesBills", "BillTotalAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesBills", "BillTitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.BillSubAccountItems", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.BillSubAccountItems", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillSubAccountItems", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.SalesBills", "BillPaymethodId", "dbo.OrderGroupPayMethodSales");
            DropForeignKey("dbo.BillPaymentProperties", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillPaymentProperties", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillPaymentProperties", "OrderPropertiesPaySalesId", "dbo.OrderPropertiesPaySales");
            DropForeignKey("dbo.OrderPropertiesPaySales", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderGroupPayMethodSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrdersTypeSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesReturnAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesCostAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderMainAccounts", "InventoryAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "SalesId", "dbo.Sales");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccountSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Sales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Sales", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Sales", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderPropertiesPaySales", "OrderGroupPayMethodSalesId", "dbo.OrderGroupPayMethodSales");
            DropForeignKey("dbo.OrderGroupPayMethodSales", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrderGroupPayMethodSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderPropertiesPaySales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertyTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrdersTypePurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOtherTotalAccountPurchases", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderAccountsforSubAccountPurchases", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Purchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Purchases", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "OrderGroupPayMethodPurchasesId", "dbo.OrderGroupPayMethodPurchases");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BillOtherAccountBills", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.SalesBills", "BillMainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.BillAccountItems", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillOtherAccountBills", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillOtherAccountBills", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesBills", "BillMainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.BillAccountItems", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillAccountItems", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AppUserCompanyRoles", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.AppUserCompanyRoles", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropForeignKey("dbo.ActivityLogs", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.OrderOes", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "userId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "TransferToOId", "dbo.Accounts");
            DropForeignKey("dbo.TotalAccountOes", "OrderO_OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.TotalAccountOes", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.TermOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.TermOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TermOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.TermOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderOes", "SubjectAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderOes", "RefrenceOrderConfigId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "AccountTypeId", "dbo.PurchasesOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "TitleBalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "TitleAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTerms", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTerms", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "thirdItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "secondItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "firstItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerTableAccountId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.PurchasesOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "DefaultChildAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "DefaultPayAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderOes", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userFirstId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "TransferToId", "dbo.Accounts");
            DropForeignKey("dbo.ItemsRequests", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.ItemsRequests", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrders", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.PayOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.PayOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PayOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.PayOes", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.ItemsRequestOes", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.ItemsRequestOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.ItemsRequestOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemsRequestOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ItemsRequestOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "ItemsRequestO_ItemsRequestOId", "dbo.ItemsRequestOes");
            DropForeignKey("dbo.OrderOes", "InventoryRequestsId", "dbo.InventoryRequests");
            DropForeignKey("dbo.InventoryRequests", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userFirstId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "TransferToId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryItemsRequests", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItemsRequests", "InventoryRequestId", "dbo.InventoryRequests");
            DropForeignKey("dbo.InventoryRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoryRequests", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderOes", "cancelById", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.AccountOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategories", "BalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.BalanceSheetTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BalanceSheetTypes", "BalanceSheetId", "dbo.BalanceSheets");
            DropForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountCategories", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountCategories", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountCategoryProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategoryProperties", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountCategoryPropertiesId", "dbo.AccountCategoryProperties");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Accounts", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Accounts", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Branches", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Branches", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.BranchProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BranchPropertyValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BranchPropertyValues", "BranchPropertiesId", "dbo.BranchProperties");
            DropForeignKey("dbo.BranchProperties", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.BranchProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropForeignKey("dbo.BalanceSheets", "Branch_BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountMovements", "Branch_BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountMovements", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.SubAccountOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountMovements", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountMovements", "AccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.AccountOrders", "OrderMoveRequestId", "dbo.OrderMoveRequests");
            DropForeignKey("dbo.OrderMoveRequests", "tilteAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMoveRequests", "orderRequestId", "dbo.OrderRequests");
            DropForeignKey("dbo.OrderRequests", "tilteAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderRequests", "mainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderRequestItems", "OrderRequestId", "dbo.OrderRequests");
            DropForeignKey("dbo.OrderRequestItems", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderRequests", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderRequests", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderRequests", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.OrderMoveRequests", "mainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMoveRequests", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderMoveRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderMoveRequests", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderMoveRequests", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOrders", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.AccountOrders", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOrderProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ApplicationUserComapnies", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheets", "FinancialListId", "dbo.FinancialLists");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId", "dbo.CustomBalanceSheets");
            DropForeignKey("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes");
            DropForeignKey("dbo.CustomAccounts", "CustomAccountCategoryId", "dbo.CustomAccountCategories");
            DropForeignKey("dbo.CustomAccounts", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.CustomAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomAccountCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomAccountCategories", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.CustomBalanceSheets", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheets", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.FinancialLists", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FinancialLists", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ApplicationUserComapnies", "FinancialListId", "dbo.FinancialLists");
            DropForeignKey("dbo.ApplicationUserComapnies", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ApplicationUserComapnies", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ApplicationUserComapnies", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ApplicationUserComapnies", "BalanceSheetId", "dbo.BalanceSheets");
            DropForeignKey("dbo.BalanceSheets", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ItemGroups", "UnitId", "dbo.Units");
            DropForeignKey("dbo.PropertiesforItemGroups", "TypeforItemGroupId", "dbo.TypeforItemGroups");
            DropForeignKey("dbo.TypeforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PropertyValuesforItemGroups", "PropertiesforItemGroupId", "dbo.PropertiesforItemGroups");
            DropForeignKey("dbo.TempItems", "UnitId", "dbo.Units");
            DropForeignKey("dbo.Units", "UnitTypeId", "dbo.UnitTypes");
            DropForeignKey("dbo.Units", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems");
            DropForeignKey("dbo.SubAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.TempItempropertyValues", "TempItemId", "dbo.TempItems");
            DropForeignKey("dbo.TempItems", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.TempItems", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.TempItempropertyValues", "PropertyValuesforItemGroupId", "dbo.PropertyValuesforItemGroups");
            DropForeignKey("dbo.PropertyValuesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PropertiesforItemGroups", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.PropertiesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemGroups", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ApplicationUserComapnies", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.AccountsforItemGroups", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.AccountsforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountsforItemGroups", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Companies", "InventoryValuationMethodId", "dbo.InventoryValuationMethods");
            DropForeignKey("dbo.Companies", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BalanceSheets", "BsMainTypeId", "dbo.BsMainTypes");
            DropForeignKey("dbo.BalanceSheets", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.AccountTypes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Accounts", "AccountType_AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.ApplicationUserComapnies", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOrders", "AccountOrderPropertiesId", "dbo.AccountOrderProperties");
            DropForeignKey("dbo.Accounts", "AccountCategoryId", "dbo.AccountCategories");
            DropIndex("dbo.Vocabularies", new[] { "CompanyRoleId" });
            DropIndex("dbo.Vocabularies", "AK_Vocabulary_Name");
            DropIndex("dbo.SubAccountMovements", new[] { "SubAccountOrderId" });
            DropIndex("dbo.SubAccountMovements", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SubAccountMovements", new[] { "SubAccountId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "settingRoleId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "settingPropertiesforUserId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.settingPropertiesforUsers", new[] { "settingPropertiesId" });
            DropIndex("dbo.selectedOrderGroupNameSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationTerms", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTerms", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerCancelId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "thirdItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "secondItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "firstItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "ItemGroupId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerTableAccountId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "DefaultChildAccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "AccountCategoryId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "CompanyId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "BranchId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "DefaultPayAccountCategoryId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "TitleBalanceSheetTypeId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "TitleAccountCategoryId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.settingProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingRoles", new[] { "CurrentWorkerId" });
            DropIndex("dbo.settingRoles", new[] { "settingPropertiesId" });
            DropIndex("dbo.RolepropertiesforUsers", new[] { "CurrentWorkerId" });
            DropIndex("dbo.RolepropertiesforUsers", new[] { "settingRoleId" });
            DropIndex("dbo.RoleCollections", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.RoleCollections", new[] { "CompanyRoleId" });
            DropIndex("dbo.OrderGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderGroups", new[] { "PurchasesId" });
            DropIndex("dbo.OrderGroups", new[] { "SalesId" });
            DropIndex("dbo.inventorySts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CompanyId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "BranchId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerCancelId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "TitleAccountId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "MainAccountId" });
            DropIndex("dbo.InventoryItems", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryItems", new[] { "InventoryTransferEntityId" });
            DropIndex("dbo.InventoryItems", new[] { "ItemGroupId" });
            DropIndex("dbo.InventoryItems", new[] { "SubAccountTransitId" });
            DropIndex("dbo.InventoryItems", new[] { "SubAccountToId" });
            DropIndex("dbo.InventoryItems", new[] { "SubAccountFromId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "CompanyId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "BranchId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesReturnId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesAccountId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesCostAccountId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "InventoryId" });
            DropIndex("dbo.Documents", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Documents", new[] { "AccountId" });
            DropIndex("dbo.Documents", new[] { "AccountMovementId" });
            DropIndex("dbo.Documents", new[] { "BranchId" });
            DropIndex("dbo.Documents", new[] { "CompanyId" });
            DropIndex("dbo.CompanyPropertyValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CompanyPropertyValues", new[] { "CompanyPropertiesId" });
            DropIndex("dbo.CompanyProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CompanyProperties", new[] { "AccountsCatogeryTypesAcccountsId" });
            DropIndex("dbo.CompanyProperties", new[] { "CompanyId" });
            DropIndex("dbo.branchGroupSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BillPaymentPropertiesSales", new[] { "SalesBillId" });
            DropIndex("dbo.BillPaymentPropertiesSales", new[] { "OrderPropertiesPaySalesId" });
            DropIndex("dbo.billMainAccountSalesSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.billMainAccountPurSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemsRows", new[] { "PurchasesBillId" });
            DropIndex("dbo.ItemsRows", new[] { "SalesBillId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "SalesBillId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "SubAccountId" });
            DropIndex("dbo.OrdersTypeSales", new[] { "SalesId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "SalesId" });
            DropIndex("dbo.OrderOtherTotalAccountSales", new[] { "AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesReturnAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesCostAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "InventoryAccountId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "SalesId" });
            DropIndex("dbo.OrderAccountsforSubAccountSales", new[] { "AccountId" });
            DropIndex("dbo.Sales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Sales", new[] { "BranchId" });
            DropIndex("dbo.Sales", new[] { "CompanyId" });
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "SalesId" });
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "AccountId" });
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "OrderGroupPayMethodSalesId" });
            DropIndex("dbo.OrderPropertyTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrdersTypePurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderOtherTotalAccountPurchases", new[] { "AccountId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderAccountsforSubAccountPurchases", new[] { "AccountId" });
            DropIndex("dbo.Purchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Purchases", new[] { "BranchId" });
            DropIndex("dbo.Purchases", new[] { "CompanyId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "AccountId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "OrderGroupPayMethodPurchasesId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "SalesBillId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "OrderPropertiesPayPurchasesId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "OrderPropertiesPaySalesId" });
            DropIndex("dbo.SalesBills", new[] { "CompanyId" });
            DropIndex("dbo.SalesBills", new[] { "BranchId" });
            DropIndex("dbo.SalesBills", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalesBills", new[] { "BillPaymethodId" });
            DropIndex("dbo.SalesBills", new[] { "BillTotalAccountId" });
            DropIndex("dbo.SalesBills", new[] { "BillTitleAccountId" });
            DropIndex("dbo.SalesBills", new[] { "BillMainAccountId" });
            DropIndex("dbo.SalesBills", new[] { "OrdersTypeSalesId" });
            DropIndex("dbo.BillOtherAccountBills", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillOtherAccountBills", new[] { "SalesBillId" });
            DropIndex("dbo.BillOtherAccountBills", new[] { "AccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "CompanyId" });
            DropIndex("dbo.PurchasesBills", new[] { "BranchId" });
            DropIndex("dbo.PurchasesBills", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillPaymethodId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillTotalAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillTitleAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillMainAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "OrdersTypePurchasesId" });
            DropIndex("dbo.BillAccountItems", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillAccountItems", new[] { "SalesBillId" });
            DropIndex("dbo.BillAccountItems", new[] { "AccountId" });
            DropIndex("dbo.CompanyRoles", "AK_CompanyRole_Name");
            DropIndex("dbo.AppUserCompanyRoles", new[] { "CompanyRoleId" });
            DropIndex("dbo.AppUserCompanyRoles", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.ActivityLogs", new[] { "UserId" });
            DropIndex("dbo.TotalAccountOes", new[] { "OrderO_OrderOId" });
            DropIndex("dbo.TotalAccountOes", new[] { "AccountId" });
            DropIndex("dbo.TermOes", new[] { "CompanyId" });
            DropIndex("dbo.TermOes", new[] { "BranchId" });
            DropIndex("dbo.TermOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.TermOes", new[] { "OrderOId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTerms", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTerms", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerCancelId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "thirdItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "secondItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "firstItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "ItemGroupId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerTableAccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "AccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "DefaultChildAccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "AccountCategoryId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "CompanyId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "BranchId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "DefaultPayAccountCategoryId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "TitleBalanceSheetTypeId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "TitleAccountCategoryId" });
            DropIndex("dbo.ItemsRequests", new[] { "SubAccountId" });
            DropIndex("dbo.ItemsRequests", new[] { "PurchaseOrderId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userCancelId" });
            DropIndex("dbo.PurchaseOrders", new[] { "CompanyId" });
            DropIndex("dbo.PurchaseOrders", new[] { "BranchId" });
            DropIndex("dbo.PurchaseOrders", new[] { "TransferToId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userThirdId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userSecondId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userFirstId" });
            DropIndex("dbo.PayOes", new[] { "CompanyId" });
            DropIndex("dbo.PayOes", new[] { "BranchId" });
            DropIndex("dbo.PayOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PayOes", new[] { "OrderOId" });
            DropIndex("dbo.PayOes", new[] { "AccountId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "CompanyId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "BranchId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "OrderOId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "SubAccountId" });
            DropIndex("dbo.InventoryItemsRequests", new[] { "SubAccountId" });
            DropIndex("dbo.InventoryItemsRequests", new[] { "InventoryRequestId" });
            DropIndex("dbo.InventoryRequests", new[] { "userCancelId" });
            DropIndex("dbo.InventoryRequests", new[] { "CompanyId" });
            DropIndex("dbo.InventoryRequests", new[] { "BranchId" });
            DropIndex("dbo.InventoryRequests", new[] { "TransferToId" });
            DropIndex("dbo.InventoryRequests", new[] { "userThirdId" });
            DropIndex("dbo.InventoryRequests", new[] { "userSecondId" });
            DropIndex("dbo.InventoryRequests", new[] { "userFirstId" });
            DropIndex("dbo.OrderOes", new[] { "RefrenceOrderConfigId" });
            DropIndex("dbo.OrderOes", new[] { "PurchaseOrderId" });
            DropIndex("dbo.OrderOes", new[] { "CompanyId" });
            DropIndex("dbo.OrderOes", new[] { "BranchId" });
            DropIndex("dbo.OrderOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOes", new[] { "TransferToOId" });
            DropIndex("dbo.OrderOes", new[] { "cancelById" });
            DropIndex("dbo.OrderOes", new[] { "userThirdId" });
            DropIndex("dbo.OrderOes", new[] { "userSecondId" });
            DropIndex("dbo.OrderOes", new[] { "userId" });
            DropIndex("dbo.OrderOes", new[] { "InventoryRequestsId" });
            DropIndex("dbo.OrderOes", new[] { "SubjectAccountId" });
            DropIndex("dbo.AccountOes", new[] { "ItemsRequestO_ItemsRequestOId" });
            DropIndex("dbo.AccountOes", new[] { "CompanyId" });
            DropIndex("dbo.AccountOes", new[] { "BranchId" });
            DropIndex("dbo.AccountOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOes", new[] { "OrderOId" });
            DropIndex("dbo.AccountOes", new[] { "AccountId" });
            DropIndex("dbo.BalanceSheetTypes", new[] { "AccountTypeId" });
            DropIndex("dbo.BalanceSheetTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BalanceSheetTypes", new[] { "BalanceSheetId" });
            DropIndex("dbo.BranchPropertyValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BranchPropertyValues", new[] { "BranchPropertiesId" });
            DropIndex("dbo.BranchProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BranchProperties", new[] { "AccountsCatogeryTypesAcccountsId" });
            DropIndex("dbo.BranchProperties", new[] { "BranchId" });
            DropIndex("dbo.SubAccountOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderRequestItems", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderRequestItems", new[] { "OrderRequestId" });
            DropIndex("dbo.OrderRequests", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderRequests", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.OrderRequests", new[] { "BranchId" });
            DropIndex("dbo.OrderRequests", new[] { "CompanyId" });
            DropIndex("dbo.OrderRequests", new[] { "tilteAccountId" });
            DropIndex("dbo.OrderRequests", new[] { "mainAccountId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "BranchId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "CompanyId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "orderRequestId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "tilteAccountId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "mainAccountId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.CustomAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomAccounts", new[] { "CustomAccountCategoryId" });
            DropIndex("dbo.CustomAccounts", new[] { "AccountId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "CustomBalanceSheetTypeId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "CustomBalanceSheetId" });
            DropIndex("dbo.CustomBalanceSheets", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomBalanceSheets", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomBalanceSheets", new[] { "FinancialListId" });
            DropIndex("dbo.FinancialLists", new[] { "CompanyId" });
            DropIndex("dbo.FinancialLists", new[] { "BranchId" });
            DropIndex("dbo.FinancialLists", new[] { "CurrentWorkerId" });
            DropIndex("dbo.TypeforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Units", new[] { "UnitTypeId" });
            DropIndex("dbo.Units", "IX_UnitName");
            DropIndex("dbo.SubAccounts", new[] { "ItemInfoId" });
            DropIndex("dbo.SubAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SubAccounts", new[] { "AccountId" });
            DropIndex("dbo.TempItems", new[] { "CompanyId" });
            DropIndex("dbo.TempItems", new[] { "UnitId" });
            DropIndex("dbo.TempItems", new[] { "ItemGroupId" });
            DropIndex("dbo.TempItempropertyValues", new[] { "PropertyValuesforItemGroupId" });
            DropIndex("dbo.TempItempropertyValues", new[] { "TempItemId" });
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "PropertiesforItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "ItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "TypeforItemGroupId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "ItemGroupId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "AccountId" });
            DropIndex("dbo.ItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemGroups", new[] { "UnitId" });
            DropIndex("dbo.ItemGroups", "IX_ItemGroupName");
            DropIndex("dbo.Companies", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Companies", new[] { "InventoryValuationMethodId" });
            DropIndex("dbo.Companies", "AK_Company_CompanyName");
            DropIndex("dbo.AccountTypes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountTypes", "AK_AccountType_AccountTypeName");
            DropIndex("dbo.BalanceSheets", new[] { "Branch_BranchId" });
            DropIndex("dbo.BalanceSheets", new[] { "BsMainTypeId" });
            DropIndex("dbo.BalanceSheets", new[] { "AccountTypeId" });
            DropIndex("dbo.BalanceSheets", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BalanceSheets", "AK_BalanceSheet_BalanceSheetName");
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ItemGroupId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "FinancialListId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "BalanceSheetId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "BranchId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ApplicationUserId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "CompanyId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AccountOrderProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOrders", new[] { "CompanyId" });
            DropIndex("dbo.AccountOrders", new[] { "OrderMoveRequestId" });
            DropIndex("dbo.AccountOrders", new[] { "BranchId" });
            DropIndex("dbo.AccountOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOrders", new[] { "AccountOrderPropertiesId" });
            DropIndex("dbo.AccountMovements", new[] { "Branch_BranchId" });
            DropIndex("dbo.AccountMovements", new[] { "SubAccountOrderId" });
            DropIndex("dbo.AccountMovements", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountMovements", new[] { "AccountOrderId" });
            DropIndex("dbo.AccountMovements", new[] { "AccountId" });
            DropIndex("dbo.Branches", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Branches", "AK_Branch_BranchName");
            DropIndex("dbo.Accounts", new[] { "AccountType_AccountTypeId" });
            DropIndex("dbo.Accounts", new[] { "CompanyId" });
            DropIndex("dbo.Accounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Accounts", new[] { "AccountCategoryId" });
            DropIndex("dbo.Accounts", "AK_Accounts_AccountName");
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountCategoryPropertiesId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "CompanyId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsTypesAcccountsId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategories", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.AccountCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountCategories", new[] { "AccountTypeId" });
            DropIndex("dbo.AccountCategories", new[] { "BalanceSheetTypeId" });
            DropTable("dbo.Vocabularies");
            DropTable("dbo.SubAccountMovements");
            DropTable("dbo.settingRolesForUsers");
            DropTable("dbo.settingPropertiesforUsers");
            DropTable("dbo.selectedOrderGroupNameSts");
            DropTable("dbo.SalessOrdersConfigurationTotalAccounts");
            DropTable("dbo.SalessOrdersConfigurationTerms");
            DropTable("dbo.SalessOrdersConfigurationAccountTypes");
            DropTable("dbo.SalessOrdersConfigurationTableAccounts");
            DropTable("dbo.SalessOrdersConfigurationPayMethods");
            DropTable("dbo.SalesOrdersConfigurations");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.settingProperties");
            DropTable("dbo.settingRoles");
            DropTable("dbo.RolepropertiesforUsers");
            DropTable("dbo.RoleCollections");
            DropTable("dbo.OrderGroups");
            DropTable("dbo.inventorySts");
            DropTable("dbo.InventoryTransferEntities");
            DropTable("dbo.InventoryItems");
            DropTable("dbo.InventoriesConfigurtions");
            DropTable("dbo.Documents");
            DropTable("dbo.CompanyPropertyValues");
            DropTable("dbo.CompanyProperties");
            DropTable("dbo.branchGroupSts");
            DropTable("dbo.BillPaymentPropertiesSales");
            DropTable("dbo.billMainAccountSalesSts");
            DropTable("dbo.billMainAccountPurSts");
            DropTable("dbo.ItemsRows");
            DropTable("dbo.BillSubAccountItems");
            DropTable("dbo.OrdersTypeSales");
            DropTable("dbo.OrderOtherTotalAccountSales");
            DropTable("dbo.OrderMainAccounts");
            DropTable("dbo.OrderAccountsforSubAccountSales");
            DropTable("dbo.Sales");
            DropTable("dbo.OrderGroupPayMethodSales");
            DropTable("dbo.OrderPropertiesPaySales");
            DropTable("dbo.OrderPropertyTypes");
            DropTable("dbo.OrdersTypePurchases");
            DropTable("dbo.OrderOtherTotalAccountPurchases");
            DropTable("dbo.OrderAccountsforSubAccountPurchases");
            DropTable("dbo.Purchases");
            DropTable("dbo.OrderGroupPayMethodPurchases");
            DropTable("dbo.OrderPropertiesPayPurchases");
            DropTable("dbo.BillPaymentProperties");
            DropTable("dbo.SalesBills");
            DropTable("dbo.BillOtherAccountBills");
            DropTable("dbo.PurchasesBills");
            DropTable("dbo.BillAccountItems");
            DropTable("dbo.CompanyRoles");
            DropTable("dbo.AppUserCompanyRoles");
            DropTable("dbo.ActivityLogs");
            DropTable("dbo.TotalAccountOes");
            DropTable("dbo.TermOes");
            DropTable("dbo.PurchasesOrdersConfigurationTotalAccounts");
            DropTable("dbo.PurchasesOrdersConfigurationTerms");
            DropTable("dbo.PurchasesOrdersConfigurationAccountTypes");
            DropTable("dbo.PurchasesOrdersConfigurationTableAccounts");
            DropTable("dbo.PurchasesOrdersConfigurationPayMethods");
            DropTable("dbo.PurchaseOrdersConfigurations");
            DropTable("dbo.ItemsRequests");
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.PayOes");
            DropTable("dbo.ItemsRequestOes");
            DropTable("dbo.InventoryItemsRequests");
            DropTable("dbo.InventoryRequests");
            DropTable("dbo.OrderOes");
            DropTable("dbo.AccountOes");
            DropTable("dbo.BalanceSheetTypes");
            DropTable("dbo.BranchPropertyValues");
            DropTable("dbo.AccountsCatogeryTypesAcccounts");
            DropTable("dbo.BranchProperties");
            DropTable("dbo.SubAccountOrders");
            DropTable("dbo.OrderRequestItems");
            DropTable("dbo.OrderRequests");
            DropTable("dbo.OrderMoveRequests");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.CustomAccounts");
            DropTable("dbo.CustomAccountCategories");
            DropTable("dbo.CustomBalanceSheetTypes");
            DropTable("dbo.CustomBalanceSheets");
            DropTable("dbo.FinancialLists");
            DropTable("dbo.TypeforItemGroups");
            DropTable("dbo.UnitTypes");
            DropTable("dbo.Units");
            DropTable("dbo.SubAccounts");
            DropTable("dbo.TempItems");
            DropTable("dbo.TempItempropertyValues");
            DropTable("dbo.PropertyValuesforItemGroups");
            DropTable("dbo.PropertiesforItemGroups");
            DropTable("dbo.AccountsforItemGroups");
            DropTable("dbo.ItemGroups");
            DropTable("dbo.InventoryValuationMethods");
            DropTable("dbo.Companies");
            DropTable("dbo.BsMainTypes");
            DropTable("dbo.AccountTypes");
            DropTable("dbo.BalanceSheets");
            DropTable("dbo.ApplicationUserComapnies");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AccountOrderProperties");
            DropTable("dbo.AccountOrders");
            DropTable("dbo.AccountMovements");
            DropTable("dbo.Branches");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountCategoryPropertiesValues");
            DropTable("dbo.AccountCategoryProperties");
            DropTable("dbo.AccountsDataTypes");
            DropTable("dbo.AccountCategories");
        }
    }
}
