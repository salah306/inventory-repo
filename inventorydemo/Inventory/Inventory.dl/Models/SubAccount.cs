﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.dl.Models
{
    public partial class SubAccount
    {
        public Int32 SubAccountId { get; set; }
        public string SubAccountName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }

        public virtual IEnumerable<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }
        public bool Activated { get; set; }
        public string Code { get; set; }
        public virtual IEnumerable<SubAccountMovement> SubAccountMovements { get; set; }


        public Int32 ItemInfoId { get; set; }
        [ForeignKey("ItemInfoId")]
        public virtual TempItem ItemInfo { get; set; }
    }

    public partial class SubAccount
    {
        public decimal GetStockQTy()
        {

            return this.SubAccountMovements.Any() ? this.SubAccountMovements.FirstOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemStockQuantity : 0;
        }
        public decimal GetStockPrice()
        {
        
            return this.SubAccountMovements.Any() ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemTotalStockPrice : 0;
        }
        public decimal GetCostPrice()
        {

            return this.GetStockQTy() > 0 ? this.GetStockPrice() / this.GetStockQTy() : 0;
        }

        public decimal TotalDebit()
        {
            return this.SubAccountMovements.Any() ? this.SubAccountMovements.Sum(a => a.ItemPricein * a.QuantityIn) : 0;

        }

        public decimal TotalCrdit()
        {
            return this.SubAccountMovements.Any() ? this.SubAccountMovements.Sum(a => a.ItemPriceOut * a.QuantityOut) : 0;

        }

    }
}
