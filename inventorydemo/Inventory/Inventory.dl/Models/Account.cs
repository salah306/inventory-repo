﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class Account
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public  string AccountAliasName { get; set; }
        public string Code { get; set; }
        public string CodeABC { get; set; }
       
        public Int32 AccountCategoryId { get; set; }
        [ForeignKey("AccountCategoryId")]
        public virtual AccountCategory AccountCategory { get; set; }
       

        public virtual IEnumerable<AccountMovement> AccountsMovements { get; set; }
        public Int32? CustomAccountCategoryId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]

        public virtual ApplicationUser CurrentWorker { get; set; }

        public decimal? AccountLimitDebit { get; set; }
        public decimal? AccountLimitCrdit { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public virtual IEnumerable<SubAccount> SubAccounts { get; set; }
        public bool Activated { get; set; }
        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool isCompany { get; set; }
        public virtual IEnumerable<AccountsforItemGroup> AccountsforItemGroups { get; set; }
        public virtual IEnumerable<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }


    }
    public partial class Account
    {
        public decimal GetTotalDebit()
        {
            return this.AccountsMovements.Any() ? this.AccountsMovements.Sum(a => a.Debit) : 0;

        }
        public decimal GetTotalCrdit()
        {
            return this.AccountsMovements.Any() ? this.AccountsMovements.Sum(a => a.Crdit): 0;
        }

        public decimal GetBalance()
        {
            return this.GetTotalDebit() - this.GetTotalCrdit();
        }

   

    }


}