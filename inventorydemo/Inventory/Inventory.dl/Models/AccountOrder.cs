﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class AccountOrder
    {
       
        public Int32 AccountOrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderNote { get; set; }
        public bool? HasDone { get; set; }
        public Int32? AccountOrderPropertiesId { get; set; }
        [ForeignKey("AccountOrderPropertiesId")]
        public virtual AccountOrderProperties AccountOrderProperties { get; set; }
        public virtual IEnumerable<AccountMovement> AccountMovements { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }

        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string AccountOrderCode { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]

        public Branch Branch { get; set; }
        public Int32? OrderMoveRequestId { get; set; }
        [ForeignKey("OrderMoveRequestId")]
        public virtual OrderMoveRequest OrderMoveRequest { get; set; }
        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]

        public Company Company { get; set; }
        public Int32 OrderNo { get; set; }
        public bool IsCompany { get; set; }
        public string OrderTypeRefrence { get; set; }
        public Int32? OrderIdRefrence { get; set; }
        public bool IsSales { get; set; }


    }
    public partial class AccountOrder
    {
        public decimal GetTotalDebit()
        {
            return this.AccountMovements.Any() ? this.AccountMovements.Sum(a => a.Debit) : 0;
        }

        public decimal GetTotalCrdit()
        {
            return this.AccountMovements.Any() ? this.AccountMovements.Sum(a => a.Crdit) : 0;
        }
        public decimal GetTotalAmount()
        {
            return this.AccountMovements.Any() ? this.GetTotalDebit() - this.GetTotalCrdit(): 0;
        }
    }
    public class SalesBill
    {
        public Int32 SalesBillId { get; set; }
        public string SalesBillName { get; set; }
        public string type { get; set; }
        public Int32 BillNo { get; set; }
        public string OrdersTypeSalesType { get; set; }
        public Int32 OrdersTypeSalesId { get; set; }
        [ForeignKey("OrdersTypeSalesId")]
        public virtual OrdersTypeSales OrdersTypeSales { get; set; }
        public DateTime BillDate { get; set; }
        public Int32 BillMainAccountId { get; set; }
        [ForeignKey("BillMainAccountId")]
        public virtual Account BillMainAccount { get; set; }
        public Int32 BillTitleAccountId { get; set; }
        [ForeignKey("BillTitleAccountId")]
        public virtual Account BillTitleAccount { get; set; }
        
        public Int32 BillTotalAccountId { get; set; }
        [ForeignKey("BillTotalAccountId")]
        public virtual Account BillTotalAccount { get; set; }
        public Int32 BillPaymethodId { get; set; }
        [ForeignKey("BillPaymethodId")]
        public virtual OrderGroupPayMethodSales BillPaymethod { get; set; }
        public virtual List<BillPaymentProperties> BillPaymentProperties { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual List<BillOtherAccountBill> BillOtherAccountBills { get; set; }
        public virtual List<BillAccountItems> BillAccountItems { get; set; }

        public virtual List<BillSubAccountItems> BillSubAccountItems { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        public virtual List<ItemsRow> itemsRows { get; set; }
    }
    public class ItemsRow
    {
        public Int32 itemsRowId { get; set; }
        public Int32 itemPropertyValueId { get; set; }
        public string itemPropertyValueName { get; set; }
        public Int32 itemPropertyId { get; set; }
        public string value { get; set; }
        public string type { get; set; }
        public Int32? SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]
        public virtual SalesBill SalesBill { get; set; }
        public Int32? PurchasesBillId { get; set; }
        [ForeignKey("PurchasesBillId")]
        public virtual PurchasesBill PurchasesBill { get; set; }
        public int orderid { get; set; }
    }
    public class BillAccountItems
    {
        public Int32 BillAccountItemsId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public decimal value { get; set; }
        public bool IsDebit { get; set; }
        public Int32? SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]
        public SalesBill SalesBill { get; set; }
        public Int32? PurchasesBillId { get; set; }
        [ForeignKey("PurchasesBillId")]
        public PurchasesBill PurchasesBill { get; set; }
        public string uniqueId { get; set; }

    }

    public class BillOtherAccountBill
    {
        public Int32 BillOtherAccountBillId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public decimal value { get; set; }
        public bool IsDebit { get; set; }
        public Int32? SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]
        public SalesBill SalesBill { get; set; }
        public Int32? PurchasesBillId { get; set; }
        [ForeignKey("PurchasesBillId")]
        public PurchasesBill PurchasesBill { get; set; }

    }

    public class BillAccountItemsVm
    { 
        public Int32 AccountId { get; set; }
        public decimal value { get; set; }
        public bool IsDebit { get; set; }
        public bool PlusType { get; set; }

    }
    public class BillSubAccountItems
    {
        public Int32 BillSubAccountItemsId { get; set; }
        public Int32 SubAccountId { get; set; }
        public virtual SubAccount SubAccount { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public Int32? SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]
        public SalesBill SalesBill { get; set; }
        public Int32? PurchasesBillId { get; set; }
        [ForeignKey("PurchasesBillId")]
        public PurchasesBill PurchasesBill { get; set; }
        public string uniqueId { get; set; }
    }

    public class BillSubAccountItemsVm
    {
        
        public Int32 SubAccountId { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
     

    }

    public class PurchasesBill
    {
        public Int32 PurchasesBillId { get; set; }
        public string PurchasesBillName { get; set; }
        public string type { get; set; }
        public Int32 BillNo { get; set; }
        public string OrdersTypePurchasesType { get; set; }
        public Int32 OrdersTypePurchasesId { get; set; }
        [ForeignKey("OrdersTypePurchasesId")]
        public virtual OrdersTypePurchases OrdersTypePurchases { get; set; }
        public DateTime BillDate { get; set; }
        public Int32 BillMainAccountId { get; set; }
        [ForeignKey("BillMainAccountId")]
        public virtual Account BillMainAccount { get; set; }
        public Int32 BillTitleAccountId { get; set; }
        [ForeignKey("BillTitleAccountId")]
        public virtual Account BillTitleAccount { get; set; }
      
        public Int32 BillTotalAccountId { get; set; }
        [ForeignKey("BillTotalAccountId")]
        public virtual Account BillTotalAccount { get; set; }
        public Int32 BillPaymethodId { get; set; }
        [ForeignKey("BillPaymethodId")]
        public virtual OrderGroupPayMethodPurchases BillPaymethod { get; set; }
        public virtual List<BillPaymentProperties> BillPaymentProperties { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public bool IsCompany { get; set; }
        public virtual List<ItemsRow> itemsRows { get; set; }
        public virtual List<BillOtherAccountBill> BillOtherAccountBills { get; set; }
        public virtual List<BillAccountItems> BillAccountItems { get; set; }

        public virtual List<BillSubAccountItems> BillSubAccountItems { get; set; }
    }
    public class BillPaymentPropertiesSales
    {
        public Int32 BillPaymentPropertiesSalesId { get; set; }
        public Int32 OrderPropertiesPaySalesId { get; set; }
        [ForeignKey("OrderPropertiesPaySalesId")]
        public virtual OrderPropertiesPaySales OrderPropertiesPaySales { get; set; }
        public Int32 SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]

        public virtual SalesBill SalesBill { get; set; }
        public string value { get; set; }

    }
    public class BillPaymentProperties
    {
        public Int32 BillPaymentPropertiesId { get; set; }
        public Int32? OrderPropertiesPaySalesId { get; set; }
        [ForeignKey("OrderPropertiesPaySalesId")]
        public virtual OrderPropertiesPaySales OrderPropertiesPaySales { get; set; }
        public Int32? OrderPropertiesPayPurchasesId { get; set; }
        [ForeignKey("OrderPropertiesPayPurchasesId")]
        public virtual OrderPropertiesPayPurchases OrderPropertiesPayPurchases { get; set; }
        public Int32? PurchasesBillId { get; set; }
        [ForeignKey("PurchasesBillId")]
        public virtual PurchasesBill PurchasesBill { get; set; }
        public Int32? SalesBillId { get; set; }
        [ForeignKey("SalesBillId")]
        public virtual SalesBill SalesBill { get; set; }
        public string value { get; set; }

    }

    public class OrderGroup
    {
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public bool IsCompany { get; set; }
        public Int32 CompanyBranchId { get; set; }
        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        public Int32 PurchasesId { get; set; }

        public virtual Purchases Purchases { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

       

    }
    public class Purchases
    {
        public virtual Int32 PurchasesId { get; set; }
        public string PurchasesName { get; set; }
        public bool IsCompany { get; set; }
        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public Int32? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public virtual List<OrdersTypePurchases> ordersType { get; set; }
        public virtual List<OrderGroupPayMethodPurchases> payMethod { get; set; }
        public virtual List<OrderAccountsforSubAccountPurchases> orderAccountsforSubAccounts { get; set; }
        public virtual List<OrderOtherTotalAccountPurchases> orderOtherTotalAccounts { get; set; }
       
    }

    public class Sales
    {
        public Int32 SalesId { get; set; }
        public string SalesName { get; set; }
        public bool IsCompany { get; set; }
        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public Int32? BranchId { get; set; }
        public virtual Branch Branch { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public virtual List<OrdersTypeSales> ordersType { get; set; }
        public virtual List<OrderGroupPayMethodSales> payMethod { get; set; }
        public virtual List<OrderMainAccount> orderMainAccounts { get; set; }
        public  virtual List<OrderAccountsforSubAccountSales> orderAccountsforSubAccounts { get; set; }
        public virtual List<OrderOtherTotalAccountSales> orderOtherTotalAccounts { get; set; }
    }

    public class OrdersTypeSales
    {
        public Int32 OrdersTypeSalesId { get; set; }
        [Required]
        public string sale { get; set; }
        [Required]
        public string returnd { get; set; }
        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }

    }
    public class OrdersTypePurchases
    {
        public Int32 OrdersTypePurchasesId { get; set; }
        [Required]
        public string sale { get; set; }
        [Required]
        public string returnd { get; set; }
        public Int32 PurchasesId { get; set; }
        public virtual Purchases Purchases { get; set; }

    }
    //111
    public class OrderGroupPayMethodSales
    {
        public Int32 OrderGroupPayMethodSalesId { get; set; }
        [Required]
        public string OrderGroupPayMethodName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? AccountId { get; set; }
        public virtual Account Account { get; set; }
        public virtual List<OrderPropertiesPaySales> OrderPropertiesPaySales { get; set; }
        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }
    }
    public class OrderGroupPayMethodPurchases
    {
        public Int32 OrderGroupPayMethodPurchasesId { get; set; }
        [Required]
        public string OrderGroupPayMethodPurchasesName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 PurchasesId { get; set; }
        public virtual Purchases Purchases { get; set; }
        public virtual List<OrderPropertiesPayPurchases> OrderPropertiesPayPurchases { get; set; }
    }




    public class OrderMainAccount
    {
        public Int32 OrderMainAccountId { get; set; }
        public Int32 InventoryAccountId { get; set; }
        [ForeignKey("InventoryAccountId")]

        public virtual Account InventoryAccount { get; set; }

        public Int32 SalesCostAccountId { get; set; }
        [ForeignKey("SalesCostAccountId")]
        public virtual Account SalesCostAccount { get; set; }

        public Int32 SalesAccountId { get; set; }
        [ForeignKey("SalesAccountId")]
        public virtual Account SalesAccount { get; set; }
        public Int32 SalesReturnAccountId { get; set; }
        [ForeignKey("SalesReturnAccountId")]
        public virtual Account SalesReturnAccount { get; set; }

        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }

    public class OrderPropertiesPaySales
    {
        public Int32 OrderPropertiesPaySalesId { get; set; }
        public string OrderPropertiesPaySalesName { get; set; }
       
        public Int32 OrderGroupPayMethodSalesId { get; set; }
        public virtual OrderGroupPayMethodSales OrderGroupPayMethodSales { get; set; }
        public bool IsRequired { get; set; }
        public bool ToPrint { get; set; }

        public Int32 OrderPropertyTypeId { get; set; }
        public virtual OrderPropertyType OrderPropertyType { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }
    public class OrderPropertiesPayPurchases
    {
        public Int32 OrderPropertiesPayPurchasesId { get; set; }
        public string OrderPropertiesPayPurchasesName { get; set; }
        public Int32 OrderGroupPayMethodPurchasesId { get; set; }
        public virtual OrderGroupPayMethodPurchases OrderGroupPayMethodPurchases { get; set; }
        public bool IsRequired { get; set; }
        public bool ToPrint { get; set; }
        public Int32 OrderPropertyTypeId { get; set; }
        public virtual OrderPropertyType OrderPropertyType { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }


    public class OrderPropertyType
    {
        public Int32 OrderPropertyTypeId { get; set; }
        public string OrderPropertyTypeName { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }
    public class OrderPropertyTypeVm
    {
        public Int32 OrderPropertyTypeId { get; set; }
        public string OrderPropertyTypeName { get; set; }
       
    }

    public class OrderAccountsforSubAccountSales
    {
        public Int32 OrderAccountsforSubAccountSalesId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }

    public class OrderAccountsforSubAccountPurchases
    {
        public Int32 OrderAccountsforSubAccountPurchasesId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 PurchasesId { get; set; }
        public virtual Purchases Purchases { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }

    public class OrderOtherTotalAccountSales
    {
        public Int32 OrderOtherTotalAccountSalesId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 SalesId { get; set; }
        public virtual Sales Sales { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }
    public class OrderOtherTotalAccountPurchases
    {
        public Int32 OrderOtherTotalAccountPurchasesId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 PurchasesId { get; set; }
        public virtual Purchases Purchases { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }
        public bool DebitforPlus { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }
   

    public class OrderAccountsforSubAccountpurchases
    {

        public Int32 OrderAccountsforSubAccountId { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }

        public string typeWhenAdd { get; set; }
    }

   

    public class OrderMoveRequest
    {
        public Int32 OrderMoveRequestId { get; set; }
        public Int32 OrderNo { get; set; }
        public Int32 mainAccountId { get; set; }
        [ForeignKey("mainAccountId")]
        public virtual Account mainAccount { get; set; }
        public Int32 tilteAccountId { get; set; }
        [ForeignKey("tilteAccountId")]
        public virtual Account tilteAccount { get; set; }
        public DateTime OrderDate { get; set; }
        public string typeName { get; set; }
        public bool isDone { get; set; }
        public bool isToCancel { get; set; }
        public bool attchedRequest { get; set; }
        public Int32? orderRequestId { get; set; }
        public OrderRequest orderRequest { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        public Int32 AccountsDataTypesId { get; set; }
        [ForeignKey("AccountsDataTypesId")]
        public virtual AccountsDataTypes AccountsDataTypes { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string note { get; set; }
        public decimal amount { get; set; }
    }
    public class OrderRequest
    {
        public Int32 OrderRequestId { get; set; }
        public Int32 requestNo { get; set; }
        public Int32 mainAccountId { get; set; }
        [ForeignKey("mainAccountId")]
        public virtual Account mainAccount { get; set; }
        public Int32 tilteAccountId { get; set; }
        [ForeignKey("tilteAccountId")]
        public virtual Account tilteAccount { get; set; }
        public DateTime requestDate { get; set; }
        public DateTime dueDate { get; set; }
        public string typeName { get; set; }
        public virtual List<OrderRequestItems> items { get; set; }
        public bool isDone { get; set; }
        public bool isToCancel { get; set; }
        public bool isQty { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        public Int32 AccountsDataTypesId { get; set; }
        [ForeignKey("AccountsDataTypesId")]
        public virtual AccountsDataTypes AccountsDataTypes { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string refrenceRequesterOrderType { get; set; }
        public string refrenceRequesterOType { get; set; }
        public Int32? refrenceRequesterNo { get; set; }
    }

    public class OrderRequestItems
    {
        public Int32 OrderRequestItemsId { get; set; }
        public string note { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public Int32 OrderRequestId { get; set; }
        public virtual OrderRequest OrderReques { get; set; }
        public string refrenceType { get; set; }
        public Int32? refrenceTypeId { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class getOrderRequest
    {
        public Int32 requestNo { get; set; }
        public Int32 CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public string typeName { get; set; }
        public string RequestTypeName { get; set; }
    }

    public class OrderRequestMoveVm
    {
        public Int32 orderMoveRequestId { get; set; }
        public Int32 OrderNo { get; set; }
        public accountforRequestVm mainAccount { get; set; }
        public accountforRequestVm tilteAccount { get; set; }
        public DateTime OrderDate { get; set; }
        public string typeName { get; set; }
        public bool isNewRequest { get; set; }
        public bool isToCancel { get; set; }
        public bool isDone { get; set; }
        public bool attchedRequest { get; set; }
        public Int32 requestNo { get; set; }
        public OrderRequesVm orderRequest { get; set; }
        public string note { get; set; }
        public decimal amount { get; set; }
        public Int32 CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public string userName { get; set; }
        public DateTime DoneDate { get; set; }
        public string RequestTypeName { get; set; }
    }
    public class OrderRequesVm
    {
        public Int32 orderRequestId { get; set; }
        public Int32 requestNo { get; set; }
        public accountforRequestVm mainAccount { get; set; }
        public accountforRequestVm tilteAccount { get; set; }
        public DateTime requestDate { get; set; }
        public DateTime dueDate { get; set; }
        public string typeName { get; set; }
        public List<OrderRequestitemsVm> items { get; set; }
        public bool isDone { get; set; }
        public bool isToCancel { get; set; }
        public bool isQty { get; set; }
        public bool isNewRequest { get; set; }
        public Int32 CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public string userName { get; set; }
        public string RequestTypeName { get; set; }

    }
    public class OrderRequestitemsVm
    {
        public Int32 OrderRequestitemsId { get; set; }
        public string note { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal amount { get; set; }
        public string refrenceType { get; set; }
        public Int32 refrenceTypeId { get; set; }
    }

    public class accountforRequestVm
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public string Code { get; set; }
    }

}