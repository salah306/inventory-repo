﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class AccountMovement
    {
        public Int32 AccountMovementId { get; set; }
        public string AccountMovementCode { get; set; }
        public DateTime AccountMovementDate { get; set; }
        public decimal Debit { get; set; }
        public decimal Crdit { get; set; }
        public string AccountMovementNote { get; set; }
        public string typeName { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Accounts { get; set; }
        public Int32 AccountOrderId { get; set; }
        [ForeignKey("AccountOrderId")]
        public virtual AccountOrder AccountOrder { get; set; }
        public bool? AccountorderHasDone { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public bool? HasCanceled { get; set; }
        public Int32? SubAccountOrderId { get; set; }
        [ForeignKey("SubAccountOrderId")]

        public virtual SubAccountOrder SubAccountOrder { get; set; }


    }

    public class SubAccountMovement
    {
        public Int32 SubAccountMovementId { get; set; }
        public DateTime SubAccountMovementDate { get; set; }
        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public bool? AccountorderHasDone { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32 OrderId { get; set; }
        public string OrderNote { get; set; }
        public Int32 SubAccountOrderId { get; set; }
        [ForeignKey("SubAccountOrderId")]
        public virtual SubAccountOrder SubAccountOrder{ get; set; }
        public decimal QuantityIn { get; set; }
        public decimal QuantityOut { get; set; }
        public decimal ItemPricein { get; set; }
        public decimal ItemPriceOut { get; set; }
        public decimal ItemTotalPriceIn { get; set; }
        public decimal ItemTotalPriceOut { get; set; }
        public decimal ItemStockQuantity { get; set; }
        public decimal ItemStockPrice { get; set; }
        public decimal ItemTotalStockPrice { get; set; }
        public bool IsDebit { get; set; }
    }

    public partial class SubAccountOrder
    {
        public Int32 SubAccountOrderId { get; set; }
        public string OrderNote { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }
        public virtual IEnumerable<SubAccountMovement> SubAccountMovements { get; set; }
        public virtual IEnumerable<AccountMovement> AccountMovements { get; set; }
        public Int32 CustomSubAccountsOrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public bool? isDone { get; set; }
    }

    public partial class SubAccountOrder
    {
        public decimal GetTotalQtyIn()
        {
           return this.SubAccountMovements.Any() ? this.SubAccountMovements.Sum(a => a.QuantityIn) : 0;
        }

        public decimal GetTotalQtyOut()
        {
            return this.SubAccountMovements.Any() ? this.SubAccountMovements.Sum(a => a.QuantityOut) : 0;
        }
        public decimal GetTotalAmount()
        {
            return this.SubAccountMovements.Any() ? this.SubAccountMovements.Sum(a => (a.ItemPricein - a.ItemPriceOut) * (a.QuantityIn - a.QuantityOut)) : 0;
        }
        
    }
}