﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class ItemGroup
    {
        //الفئة : مراوح
        public Int32 ItemGroupId { get; set; }
        [Required(ErrorMessage ="يجب ادخال اسم للفئة")]
        [Display(Name ="اسم الفئة")]
        [StringLength(100)]
        [Index("IX_ItemGroupName", 1, IsUnique = true)]
        public string ItemGroupName { get; set; }
        public Int32 UnitId { get; set; }
        public virtual Unit Unit { get; set; }
        public virtual List<AccountsforItemGroup> Accounts{ get; set; }
        public virtual List<ApplicationUserComapny> ApplicationUserComapny { get; set; }
      
        [Index("IX_ItemGroupName", 2, IsUnique = true)]
        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        //الصنف , الكود , السعر
        public virtual List<PropertiesforItemGroup> Properties{ get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public virtual IEnumerable<TempItem> Items { get; set; }
    }
    public partial class TempItem
    {
        public Int32 TempItemId { get; set; }
        [Required]
        public string code { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public decimal price { get; set; }
        public decimal priceMin { get; set; }
        public decimal priceMax { get; set; }
        public DateTime? ExpireDate { get; set; }
        public Int32 ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public Int32 UnitId { get; set; }
        public string reorderPoint { get; set; }
        public virtual Unit Unit { get; set; }
        public Int32 CompanyId { get; set; }
        [Required]
        public string Realted { get; set; }
        public string ExpireinDayes { get; set; }
        public virtual Company Company { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual List<SubAccount> SubAccounts { get; set; }
        public virtual List<TempItempropertyValues> Properties { get; set; }

    }
    public partial class TempItem
    {

        public decimal GetTotalQty()
        {
            return this.SubAccounts.Any() ? this.SubAccounts.Sum(a => a.GetStockQTy()) : 0;
        }
        public decimal GetAVGCostPrice()
        {
            return this.SubAccounts.Any() ? this.SubAccounts.Average(a => a.GetCostPrice()) : 0;

        }
        public decimal GetTotalStockPrice()
        {
            return this.SubAccounts.Any() ? this.SubAccounts.Average(a => a.GetStockPrice()) : 0;

        }

    }
  public class TempItemVm
    {
        public string code { get; set; }
        public string name { get; set; }
        public decimal price { get; set; }
        public decimal avQty { get; set; }
        public decimal costPrice { get; set; }
        public decimal priceMin { get; set; }
        public decimal priceMax { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string ItemGroupName { get; set; }
        public string UnitName { get; set; }
        public string UnitType { get; set; }
        public string reorderPoint { get; set; }
        public string Realted { get; set; }
        public string ExpireinDayes { get; set; }
        public Int32 subAccountId { get; set; }
        public tempItemInventoryVm ItemInventory { get; set; }

    }

    public class tempItemInventoryVm
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
        public string branch { get; set; }
        //public List<TempItemVm> items { get; set; }
    }
    public class PropertiesforItemGroup
    {
        //الصنف , الكود , السعر
        public Int32 PropertiesforItemGroupId { get; set; }

        public string PropertiesforItemGroupName { get; set; }
        public Int32 TypeforItemGroupId { get; set; }
        public bool Show { get; set; }
        public virtual TypeforItemGroup Type { get; set; }
        public Int32 ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public virtual List<PropertyValuesforItemGroup> PropertyValuesforItemGroup { get; set; }
        public int? orderNum { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    public class PropertyValuesforItemGroup
    {
        //مروحة توشيبا , مروحة عمود
        public Int32 PropertyValuesforItemGroupId { get; set; }
        public string PropertyValuesforItemGroupName { get; set; }
        public string PropertyValuesforItemGroupValue { get; set; }
        public Int32 PropertiesforItemGroupId { get; set; }
        public virtual PropertiesforItemGroup PropertiesforItemGroup { get; set; }
        //public virtual List<subAccountNpropertyValues> subAccountNpropertyValues { get; set; }
        public virtual List<TempItempropertyValues> Properties { get; set; }
        public int? orderNum { get; set; }
        public string uniqueId { get; set; }
        public bool isNew { get; set; }
        [Required]
        public string Realted { get; set; }
        [Required]
        public string parintType { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    //public class subAccountNpropertyValues
    //{
    //    public Int32 subAccountNpropertyValuesId { get; set; }
    //    public Int32 PropertyValuesforItemGroupId { get; set; }
    //    public virtual PropertyValuesforItemGroup PropertyValuesforItemGroup { get; set; }
    //    public Int32? SubAccountId { get; set; }
    //    public virtual SubAccount SubAccount { get; set; }
    //}

    public class TempItempropertyValues
    {
        public Int32 TempItempropertyValuesId { get; set; }
        public Int32? TempItemId { get; set; }
        [ForeignKey("TempItemId")]
        public virtual TempItem TempItem { get; set; }
        public Int32? PropertyValuesforItemGroupId { get; set; }
        [ForeignKey("PropertyValuesforItemGroupId")]
        public virtual PropertyValuesforItemGroup PropertyValuesforItemGroup { get; set; }
    }

    public class TypeforItemGroup
    {
        public Int32 TypeforItemGroupId { get; set; }
        public string TypeName { get; set; }
        public string Value { get; set; }
        public bool Required { get; set; }
        public bool show { get; set; }
        public string description { get; set; }
        public int? orderNum { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    public class AccountsforItemGroup
    {
        public Int32 AccountsforItemGroupId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    public class Unit
    {
        public Int32 UnitId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم الوحدة")]
        [Display(Name = "اسم الوحدة")]
        [StringLength(100)]
        [Index("IX_UnitName", 1, IsUnique = true)]

        public string UnitName { get; set; }
        public Int32 UnitTypeId { get; set; }
        public virtual UnitType UnitType { get; set; }
        [Index("IX_UnitName", 2, IsUnique = true)]

        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }
    public class UnitType
    {
        public Int32 UnitTypeId { get; set; }
        public string UnitTypeName { get; set; }
    }
    public class GetitemsGroupNames
    {
        public Int32 itemGroupId { get; set; }
        public string itemGroupName { get; set; }
    }

    public class GetInventoryValuationMethodVm
    {
       
        public Int32 InventoryValuationMethodId { get; set; }
        public string InventoryValuationMethodName { get; set; }
        public string isSelected { get; set; }
        public Int32 companyId { get; set; }
    }
    public class GetitemsGroupUnit
    {
        public Int32 itemGroupId { get; set; }
        public Int32 unitId { get; set; }
    }

    public class NamesNUnit
    {
        public List<GetitemsGroupNames> Names { get; set; }
        public List<UnitVM> Units { get; set; }
        public List<TypeforItemGroupVM> propertyTypes { get; set; }
    }

    public class ItemGroupVM
    {
        public Int32 ItemGroupId { get; set; }
        public Int32 CompanyId { get; set; }

        public string ItemGroupName { get; set; }

        public virtual UnitVM Unit { get; set; }
        public virtual List<AccountsforItemGroupVM> Accounts { get; set; }
        //الصنف , الكود , السعر
        public virtual List<PropertiesforItemGroupVM> Properties { get; set; }
    }
    public class UnitVM
    {
        public Int32 UnitId { get; set; }
        public string UnitName { get; set; }
        public Int32 UnitTypeId { get; set; }
       
    }
    /// <summary>
    /// Starting of ItemGroupVM
    /// </summary>

    public class AccountsforItemGroupVM
    {
        public Int32 AccountId { get; set; }
        public string accountName { get; set; }
        public string code { get; set; }
        public Int32 ItemGroupId { get;set; }
    }

    public class delFromitemGroup
    {
        public Int32 AccountId { get; set; }
        public string TypeName { get; set; }
        public Int32 ItemGroupId { get; set; }
        public virtual List<PropertyValuesforItemGroupVM> propertyValues { get; set; }

    }
    public class PropertiesforItemGroupVM
    {
        public Int32 propertyId { get; set; }

        public string propertyName { get; set; }
        public bool Show { get; set; }
        public virtual TypeforItemGroupVM Type { get; set; }

        public virtual List<PropertyValuesforItemGroupVM> propertyValues { get; set; }
    }

    public class TypeforItemGroupVM
    {
        public Int32 typeId { get; set; }
        public string typeName { get; set; }
        public bool show { get; set; }
        public string description { get; set; }
        public string value { get; set; }
        public Int32 propertyId { get; set; }

    }

    public class PropertyValuesforItemGroupVM
    {
        public Int32 propertyValueId { get; set; }
        public string propertyValueName { get; set; }
        public Int32 propertyId { get; set; }
        public string typeName { get; set; }
        public string value { get; set; }
        public string uniqueId { get; set; }
        public bool isNew { get; set; }
    }


    public class TypeforITemInputVm
    {
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public bool show { get; set; }
        public string description { get; set; }
        public string value { get; set; }
        public int propertyId { get; set; }
    }

    public class ItemInputVm
    {
        public int propertyId { get; set; }
        public string propertyName { get; set; }
        public bool show { get; set; }
        public TypeforITemInputVm type { get; set; }
        public string propertyValues { get; set; }
        public int itemGroupId { get; set; }
        public string uniqueId { get; set; }
        public bool isNew { get; set; }
        public int companyId { get; set; }
    }

  


}