﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.dl.Models
{
    public class AccountO
    {
        public Int32 AccountOId { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public string amountType { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string name { get; set; }
    }
    public class ItemsRequestO
    {
        public Int32 ItemsRequestOId { get; set; }
        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public int itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal costPrice { get; set; }
        public string note { get; set; }
        public string realted { get; set; }
        public virtual List<AccountO> accounts { get; set; }
        public string itemUnitType { get; set; }
        public string itemUnitName { get; set; }
        public decimal? maxQ { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public class PayO
    {
        public Int32 PayOId { get; set; }
        public Int32 PayId { get; set; }
        public string PayName { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public DateTime dueDate { get; set; }
        public string note { get; set; }
        public string type { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

    }

    public class TotalAccountO
    {
        public Int32 TotalAccountOId { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
        public string name { get; set; }
    }

    public class TermO
    {
        public Int32 TermOId { get; set; }
        [Required(ErrorMessage = "يجب ادخال نص للشروط والاحكام")]
        public string termName { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

    }

    public class OrderO
    {
        public Int32 OrderOId { get; set; }
        public Int32 orderNo { get; set; }
        public Int32 SubjectAccountId { get; set; }
        [ForeignKey("SubjectAccountId")]
        public virtual Account SubjectAccount { get; set; }
        public Int32 InventoryRequestsId { get; set; }
        [ForeignKey("InventoryRequestsId")]
        public virtual InventoryRequest InventoryRequests { get; set; }
        public string OrderTitleName { get; set; }
        public string OrderConfigName { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public DateTime recivedDate { get; set; }
        public string userId { get; set; }
        [ForeignKey("userId")]
        public virtual ApplicationUser user { get; set; }
        public string userSecondId { get; set; }
        [ForeignKey("userSecondId")]
        public virtual ApplicationUser userSecond { get; set; }
        public string userNameThird { get; set; }
        public string userThirdId { get; set; }
        [ForeignKey("userThirdId")]
        public virtual ApplicationUser userThird { get; set; }
        public string cancelById { get; set; }
        [ForeignKey("cancelById")]
        public virtual ApplicationUser cancelBy { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        public Int32 TransferToOId { get; set; }
        [ForeignKey("TransferToOId")]
        public virtual Account TransferToO { get; set; }
        public virtual List<ItemsRequestO> itemsRequest { get; set; }
        public virtual List<PayO> pay { get; set; }
        public virtual List<TotalAccountO> totalAccount { get; set; }
        public virtual List<TermO> terms { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public Int32? PurchaseOrderId { get; set; }
        [ForeignKey("PurchaseOrderId")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public bool requestOrderAttched { get; set; }
        public Int32? RefrenceRequestId { get; set; }
        public string RefrenceRequestName { get; set; }
        public Int32? RefrenceInventoryOrderId { get; set; }
        public string RefrenceInventoryOrderName { get; set; }
        public Int32? RefrenceQueryId { get; set; }
        public string RefrenceQueryName { get; set; }
        public Int32? RefrenceBillId { get; set; }
        public string RefrenceBillName { get; set; }
        public Int32? RefrenceOrderConfigId { get; set; }
        [ForeignKey("RefrenceOrderConfigId")]
        public PurchaseOrdersConfiguration RefrenceOrderConfig { get; set; }

        public string orderType { get; set; }
        public string type { get; set; }

    }
}
