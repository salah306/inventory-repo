﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class BalanceSheetType
    {
        public Int32 BalanceSheetTypeId { get; set; }
        public string BalanceSheetTypeName { get; set; }
        public string Code { get; set; }
        public string CodeABC { get; set; }
        public Int32 BalanceSheetId { get; set; }
        [ForeignKey("BalanceSheetId")]
        public virtual BalanceSheet BalanceSheet { get; set; }
        public virtual IEnumerable<AccountCategory> AccountCategories { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
       
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual AccountType AccountType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

    }

    public partial class BalanceSheetType
    {
        public decimal GetTotalDebit()
        {
            return this.AccountCategories.Sum(a => a.GetTotalDebit());

        }
        public decimal GetTotalCrdit()
        {
            return this.AccountCategories.Sum(a => a.GetTotalCrdit());
        }
        public decimal GetBalance()
        {
            return this.AccountCategories.Sum(a => a.GetBalance());
        }
    }
}