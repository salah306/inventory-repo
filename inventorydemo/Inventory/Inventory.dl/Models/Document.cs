﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class Document
    {
        public Int32 DocumentId { get; set; }
        public string DocumentName { get; set; }

        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public Int32 BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        public Int32 AccountMovementId { get; set; }

        public virtual AccountMovement AccountMovement { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }


    }
}