﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class AccountCategoryPropertiesValue
    {
        public Int32 AccountCategoryPropertiesValueId { get; set; }
        public string AccountCategoryPropertiesValueName { get; set; }
        public Int32 AccountCategoryPropertiesId { get; set; }
        public virtual AccountCategoryProperties AccountCategoryProperties { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }
}