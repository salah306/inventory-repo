﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.dl.Models
{
    public class InventoryRequest
    {
        public Int32 InventoryRequestId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public DateTime RecivedDate { get; set; }
        public string userFirstId { get; set; }
        [ForeignKey("userFirstId")]
        public virtual ApplicationUser userFirst { get; set; }
        public string userSecondId { get; set; }
        [ForeignKey("userSecondId")]
        public virtual ApplicationUser userSecond { get; set; }
        public string userThirdId { get; set; }
        [ForeignKey("userThirdId")]
        public virtual ApplicationUser userThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        public Int32 TransferToId { get; set; }
        [ForeignKey("TransferToId")]
        public virtual Account TransferTo { get; set; }
        [Display(Name = "الاصناف")]
        public virtual List<InventoryItemsRequest> itemsRequest { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public DateTime? cancelDate { get; set; }
        public string userCancelId { get; set; }
        [ForeignKey("userCancelId")]
        public virtual ApplicationUser userCancel { get; set; }
        public Int32? orderONo { get; set; }
        public string OType { get; set; }
        public string OrderType { get; set; }

    }

   
}
