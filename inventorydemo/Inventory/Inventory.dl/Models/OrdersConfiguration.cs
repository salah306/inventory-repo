﻿using Inventory.dl.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace Inventory.dl.Models
{


    #region SalesOrdersConfiguration
    public class SalesOrdersConfiguration
    {
        public Int32 SalesOrdersConfigurationId { get; set; }
        [Required]
        public string SalesOrderName { get; set; }
        [Required]
        public string SalesName { get; set; }
        [Required]
        public string SalesReturnName { get; set; }
        public bool IsActiveOrder { get; set; }
        public bool OnlyInventory { get; set; }
        public bool DirectGlTransaction { get; set; }
        public string TitleAccountNickName { get; set; }
        public Int32 TitleAccountCategoryId { get; set; }
        [ForeignKey("TitleAccountCategoryId")]
        public virtual AccountCategory TitleAccountCategory { get; set; }
        public Int32? TitleBalanceSheetTypeId { get; set; }
        [ForeignKey("TitleBalanceSheetTypeId")]
        public virtual BalanceSheetType TitleBalanceSheetType { get; set; }

        public Int32? DefaultPayAccountCategoryId { get; set; }
        [ForeignKey("DefaultPayAccountCategoryId")]
        public virtual AccountCategory DefaultPayAccountCategory { get; set; }

        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]
        public virtual List<SalessOrdersConfigurationPayMethod> PayMethod { get; set; }
        public virtual List<SalessOrdersConfigurationTerm> Terms { get; set; }
        public virtual List<SalessOrdersConfigurationTableAccount> TableAccounts { get; set; }
        public virtual List<SalessOrdersConfigurationTotalAccount> TotalAccounts { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

    }

    public class SalessOrdersConfigurationPayMethod
    {
        public Int32 SalessOrdersConfigurationPayMethodId { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public string PayName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        [ForeignKey("AccountCategoryId")]
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? DefaultChildAccountId { get; set; }
        [ForeignKey("DefaultChildAccountId")]
        public virtual Account DefaultChildAccount { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

    }
    public class SalessOrdersConfigurationTerm
    {
        public Int32 SalessOrdersConfigurationTermId { get; set; }
        public string Note { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class SalessOrdersConfigurationTableAccount
    {
        public Int32 SalessOrdersConfigurationTableAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual SalessOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public string CurrentWorkerTableAccountId { get; set; }
        [ForeignKey("CurrentWorkerTableAccountId")]
        public virtual ApplicationUser CurrentWorkerTableAccount { get; set; }
        #region This part is for requestes and inventories and others uses in table header
        public Int32? itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal itemcostPrice { get; set; }
        public string realted { get; set; }
        public Int32? ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public bool isInventory { get; set; }
        public bool isQty { get; set; }
        public string serialNo { get; set; }
        public DateTime? expireDate { get; set; }
        public bool attchedRequest { get; set; }
        public bool isPercent { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsCanceled { get; set; }
        public string firstItemNote { get; set; }
        public string SecondItemNote { get; set; }
        public string thirdItemNote { get; set; }
        public Int32? firstItemNoteId { get; set; }
        public Int32? SecondItemNoteId { get; set; }
        public Int32? thirdItemNoteId { get; set; }
        public Int32? firstItemAccountNoteId { get; set; }
        [ForeignKey("firstItemAccountNoteId")]
        public virtual Account firstItemAccountNote { get; set; }
        public Int32? secondItemAccountNoteId { get; set; }
        [ForeignKey("secondItemAccountNoteId")]
        public virtual Account secondItemAccountNote { get; set; }
        public Int32? thirdItemAccountNoteId { get; set; }
        [ForeignKey("thirdItemAccountNoteId")]
        public virtual Account thirdItemAccountNote { get; set; }
        public DateTime? firstItemNoteDate { get; set; }
        public DateTime? secondItemNoteDate { get; set; }
        public DateTime? thirdItemNoteDate { get; set; }
        public Int32? firstReferenceId { get; set; }
        public Int32? firstReferenceType { get; set; }
        public Int32? secondReferenceId { get; set; }
        public Int32? secondReferenceType { get; set; }
        public Int32? thirdReferenceId { get; set; }
        public Int32? thirdReferenceType { get; set; }
        public DateTime? cancelDate { get; set; }
        public bool doneFirst { get; set; }
        public bool doneSceond { get; set; }
        public bool doneThird { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerSecondId { get; set; }
        [ForeignKey("CurrentWorkerSecondId")]
        public virtual ApplicationUser CurrentWorkerSecond { get; set; }
        public string CurrentWorkerCancelId { get; set; }
        [ForeignKey("CurrentWorkerCancelId")]
        public virtual ApplicationUser CurrentWorkerCancel { get; set; }
        #endregion

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }                 

    public class SalessOrdersConfigurationTotalAccount
    {
        public Int32 SalessOrdersConfigurationTotalAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual SalessOrdersConfigurationAccountType AccountType{ get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    public class SalessOrdersConfigurationAccountType
    {
        public Int32 SalessOrdersConfigurationAccountTypeId { get; set; }
        public string AccountTypeName { get; set; }

    }


  

    #endregion

    #region PurchaseOrdersConfiguration
    public class PurchaseOrdersConfiguration
    {
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [Required]
        public string PurchaseOrderName { get; set; }
        [Required]
        public string PurchaseName { get; set; }
        [Required]
        public string PurchaseReturnName { get; set; }
        public bool IsActiveOrder { get; set; }
        public bool OnlyInventory { get; set; }
        public bool DirectGlTransaction { get; set; }
        public string TitleAccountNickName { get; set; }
        public Int32 TitleAccountCategoryId { get; set; }
        [ForeignKey("TitleAccountCategoryId")]
        public virtual AccountCategory TitleAccountCategory { get; set; }
        public Int32? TitleBalanceSheetTypeId { get; set; }
        [ForeignKey("TitleBalanceSheetTypeId")]
        public virtual BalanceSheetType TitleBalanceSheetType { get; set; }

        public Int32? DefaultPayAccountCategoryId { get; set; }
        [ForeignKey("DefaultPayAccountCategoryId")]
        public virtual AccountCategory DefaultPayAccountCategory { get; set; }
        public virtual List<PurchasesOrdersConfigurationPayMethod> PayMethod { get; set; }
        public virtual List<PurchasesOrdersConfigurationTerm> Terms { get; set; }
        public virtual List<PurchasesOrdersConfigurationTableAccount> TableAccounts { get; set; }
        public virtual List<PurchasesOrdersConfigurationTotalAccount> TotalAccounts { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }



    }
    public class PurchasesOrdersConfigurationPayMethod
    {
        public Int32 PurchasesOrdersConfigurationPayMethodId { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public string PayName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        [ForeignKey("AccountCategoryId")]
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? DefaultChildAccountId { get; set; }
        [ForeignKey("DefaultChildAccountId")]
        public virtual Account DefaultChildAccount { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

    }
    public class PurchasesOrdersConfigurationTerm
    {
        public Int32 PurchasesOrdersConfigurationTermId { get; set; }
        public string Note { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class PurchasesOrdersConfigurationTableAccount
    {
        public Int32 PurchasesOrdersConfigurationTableAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual PurchasesOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public string CurrentWorkerTableAccountId { get; set; }
        [ForeignKey("CurrentWorkerTableAccountId")]
        public virtual ApplicationUser CurrentWorkerTableAccount { get; set; }
        #region This part is for requestes and inventories and others uses in table header
        public Int32? itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal itemcostPrice { get; set; }
        public string realted { get; set; }
        public Int32? ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public bool isInventory { get; set; }
        public bool isQty { get; set; }
        public string serialNo { get; set; }
        public DateTime? expireDate { get; set; }
        public bool attchedRequest { get; set; }
        public bool isPercent { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsCanceled { get; set; }
        public string firstItemNote { get; set; }
        public string SecondItemNote { get; set; }
        public string thirdItemNote { get; set; }
        public Int32? firstItemNoteId { get; set; }
        public Int32? SecondItemNoteId { get; set; }
        public Int32? thirdItemNoteId { get; set; }
        public Int32? firstItemAccountNoteId { get; set; }
        [ForeignKey("firstItemAccountNoteId")]
        public virtual Account firstItemAccountNote { get; set; }
        public Int32? secondItemAccountNoteId { get; set; }
        [ForeignKey("secondItemAccountNoteId")]
        public virtual Account secondItemAccountNote { get; set; }
        public Int32? thirdItemAccountNoteId { get; set; }
        [ForeignKey("thirdItemAccountNoteId")]
        public virtual Account thirdItemAccountNote { get; set; }
        public DateTime? firstItemNoteDate { get; set; }
        public DateTime? secondItemNoteDate { get; set; }
        public DateTime? thirdItemNoteDate { get; set; }
        public Int32? firstReferenceId { get; set; }
        public Int32? firstReferenceType { get; set; }
        public Int32? secondReferenceId { get; set; }
        public Int32? secondReferenceType { get; set; }
        public Int32? thirdReferenceId { get; set; }
        public Int32? thirdReferenceType { get; set; }
        public DateTime? cancelDate { get; set; }
        public bool doneFirst { get; set; }
        public bool doneSceond { get; set; }
        public bool doneThird { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerSecondId { get; set; }
        [ForeignKey("CurrentWorkerSecondId")]
        public virtual ApplicationUser CurrentWorkerSecond { get; set; }
        public string CurrentWorkerCancelId { get; set; }
        [ForeignKey("CurrentWorkerCancelId")]
        public virtual ApplicationUser CurrentWorkerCancel { get; set; }
        #endregion

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
    public class PurchasesOrdersConfigurationTotalAccount
    {
        public Int32 PurchasesOrdersConfigurationTotalAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual PurchasesOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class PurchasesOrdersConfigurationAccountType
    {
        public Int32 PurchasesOrdersConfigurationAccountTypeId { get; set; }
        public string AccountTypeName { get; set; }

    }

  



    #endregion


    #region Inventory

    public class InventoriesConfigurtion
    {
        public Int32 InventoriesConfigurtionId { get; set; }
        public Int32 InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        public virtual Account Inventory { get; set; }
        public Int32 SalesCostAccountId { get; set; }
        [ForeignKey("SalesCostAccountId")]
        public virtual Account SalesCostAccount { get; set; }

        public Int32 SalesAccountId { get; set; }
        [ForeignKey("SalesAccountId")]
        public virtual Account SalesAccount { get; set; }
     
        public Int32 SalesReturnId { get; set; }
        [ForeignKey("SalesReturnId")]
        public virtual Account SalesReturn { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }


    }

  
    #endregion

   


    

}