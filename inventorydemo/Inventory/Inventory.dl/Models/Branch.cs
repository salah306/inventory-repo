﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class Branch
    {
        public Int32 BranchId { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }

        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public virtual IEnumerable<Account> Accounts { get; set; }
        public virtual IEnumerable<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public virtual IEnumerable<BranchProperties> BranchProperties { get; set; }

    }
    public partial class Branch
    {
        public decimal GetTotalDebit()
        {
            return this.Accounts.Sum(a => a.GetTotalDebit());

        }
        public decimal GetTotalCrdit()
        {
            return this.Accounts.Sum(a => a.GetTotalCrdit());
        }
        public decimal GetBalance()
        {
            return this.GetTotalDebit() - this.GetTotalCrdit();
        }
    }

    public class BranchProperties
    {
        public Int32 BranchPropertiesId { get; set; }
        public Int32 BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        public string propertyName { get; set; }
        public Int32 AccountsCatogeryTypesAcccountsId { get; set; }
        public AccountsCatogeryTypesAcccounts AccountsCatogeryTypesAcccounts { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual IEnumerable<BranchPropertyValues> BranchPropertyValues { get; set; }
    }
    public class BranchPropertyValues
    {
        public Int32 BranchPropertyValuesId { get; set; }
        public Int32 BranchPropertiesId { get; set; }
        public virtual BranchProperties BranchProperties { get; set; }
        public string propertyValue { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

}