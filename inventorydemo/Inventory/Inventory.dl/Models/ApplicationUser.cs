﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Inventory.dl.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public string Occupation { get; set; }
        [StringLength(50)]
        public string Phone { get; set; }
        public string About { get; set; }
        public string ImageUrl { get; set; }
        public int? MaxCompany { get; set; }
        public DateTime JoinDate { get; set; }
        public byte Level { get; set; }
        public virtual List<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        [StringLength(50)]
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string FullName { get { return string.Format("{0} {1}", this.FirstName, this.LastName); } }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        //can view , can update , can delete , can create
        public ApplicationRole(string name) : base(name) { }
        public virtual IEnumerable<SubRole> Companies { get; set; }

    }

    public class SubRole
    {

        public string SubRoleId { get; set; }
        //شركة - فرع - ميزان مراجعة etc..
        public string name { get; set; }
        public Guid RoleId { get; set; }
        public virtual ApplicationRole Role { get; set; }
        public IEnumerable<BelongList> list { get; set; }

    }

    public class BelongList
    {
        public int BelongListId { get; set; }
        public Int32 EntityId { get; set; }
        public bool IsBelonged { get; set; }
    }
}