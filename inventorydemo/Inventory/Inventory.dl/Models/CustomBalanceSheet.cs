﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{

    public class FinancialList
    {
        public Int32 FinancialListId { get; set; }
        public string FinancialListName { get; set; }
        public virtual IEnumerable<CustomBalanceSheet> CustomBalanceSheets { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public virtual List<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }

        public string CurrentWorkerId { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }

        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

    }
    public class CustomBalanceSheet
    {
        public Int32 CustomBalanceSheetId { get; set; }
        public string BalanceSheetName { get; set; }
        public string BalanceSheetCode { get; set; }
        public Int32 FinancialListId { get; set; }
        public virtual FinancialList FinancialList { get; set; }
        public virtual IEnumerable<CustomBalanceSheetType> CustomBalanceSheetTypes { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public Int32? AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        public bool? IsLinked { get; set; }
        public Int32? LinkedAccountId { get; set; }
        public string LinkedAccountName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

    }

    public class CustomBalanceSheetType
    {
        public Int32 CustomBalanceSheetTypeId { get; set; }
        public string BalanceSheetTypeName { get; set; }
        public string BalanceSheetTypeCode { get; set; }

        public Int32 CustomBalanceSheetId { get; set; }
        public virtual CustomBalanceSheet CustomBalanceSheet { get; set; }
        public virtual IEnumerable<CustomAccountCategory> CustomAccountCategories { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        public bool? IsLinked { get; set; }
        public Int32? LinkedAccountId { get; set; }
        public string LinkedAccountName { get; set; }
        public Int32? AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

    }

    public class CustomAccountCategory
    {
        public Int32 CustomAccountCategoryId { get; set; }
        public string AccountCategoryName { get; set; }
        public string AccountCategoryCode { get; set; }
        public Int32 CustomBalanceSheetTypeId { get; set; }
        public virtual CustomBalanceSheetType CustomBalanceSheetTypes { get; set; }
        public virtual IEnumerable<CustomAccount> CustomAccount { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        public bool? IsLinked { get; set; }
        public Int32? LinkedAccountId { get; set; }
        public string LinkedAccountName { get; set; }
        public Int32? AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
    }


    public class CustomAccount
    {
        public Int32 CustomAccountId { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account { get; set; }
        public Int32 CustomAccountCategoryId { get; set; }
        public virtual CustomAccountCategory CustomAccountCategory { get; set; }
        public string AccountName { get; set; }
        public string AccountCatogeryName  { get; set; }
        public string CurrentWorkerId { get; set; }
        public Int32? AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        public Int32 Code { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

    }


    public class saveGetCusomAccounts
    {
        public Int32 CustomAccountCategoryId { get; set; }
        public string CustomAccountCatogeryName { get; set; }
        public Int32 BranchId { get; set; }
        public bool isCompany { get; set; }
        public List<AccountToSave> AccountsToSave { get; set; }

    }

    public class AccountToSave
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
    }


    public class getCusomAccounts
    {
        public Int32 ListId { get; set; }
        public Int32 BranchId { get; set; }
    }

    public class FListes
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ParentId { get; set; }
        public string ParentName { get; set; }
        public int Level { get; set; }
        public string forLevel { get; set; }
        public string AddTitle { get; set; }
        public string Addlink { get; set; }
        public string LevelName { get; set; }
        public decimal? amount { get; set; }
        public decimal? PreviousAmount { get; set; }
        public bool Show { get; set; }
        public int childern { get; set; }
        public string accountType { get; set; }
        public bool? IsLinked { get; set; }
        public Int32? LinkedAccountId { get; set; }
        public string LinkedAccountName { get; set; }
        public Int32 Code { get; set; }
        public Int32 companyId { get; set; }
        public bool IsCompany { get; set; }

    }

    public class Alllists
    {
        public string ListId { get; set; }
        public string ListName { get; set; }
        public decimal? amount { get; set; }
        public decimal? Pamount { get; set; }
        public DateTime pstartDate { get; set; }
        public DateTime pendDate { get; set; }
        public bool? forWidget { get; set; }
        public List<FListes> Listes { get; set; }
    }


}