﻿using Inventory.dl.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.dl.Models
{
    public class InventoryItemsRequest
    {
       
        public Int32 InventoryItemsRequestId { get; set; }
        public Int32 InventoryRequestId { get; set; }
        public virtual InventoryRequest InventoryRequest { get; set; }
        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal LastCostPrice { get; set; }
        public decimal CurrentCostPrice { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string Realted { get; set; }
    }
}
