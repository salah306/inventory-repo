﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class AccountType
    {
        public Int32 AccountTypeId { get; set; }
        public string AccountTypeName { get; set; }
        public virtual List<Account> Accounts { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }
}