﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class AccountCategory
    {
        public Int32 AccountCategoryId { get; set; }
        public string AccountCategoryName { get; set; }
        public string Code { get; set; }
        public string CodeABC { get; set; }
        public Int32 BalanceSheetTypeId { get; set; }
        [ForeignKey("BalanceSheetTypeId")]
        public virtual BalanceSheetType BalanceSheetType { get; set; }
        public virtual IEnumerable<Account> Accounts { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual AccountType AccountType { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

     
     
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public Int32? AccountsDataTypesId { get; set; }
        [ForeignKey("AccountsDataTypesId")]

        public virtual AccountsDataTypes AccountsDataTypes { get; set; }
       
    }

    public partial class AccountCategory
    {
        public decimal GetTotalDebit()
        {
            return  this.Accounts.Sum(a => a.GetTotalDebit());

        }
        public decimal GetTotalCrdit()
        {
            return this.Accounts.Sum(a => a.GetTotalCrdit());
        }
        public decimal GetBalance()
        {
            return this.Accounts.Sum(a => a.GetBalance());
        }
    }
}