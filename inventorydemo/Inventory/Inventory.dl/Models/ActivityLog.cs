﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public class ActivityLog
    {
        public Int32 ActivityLogId { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public string Activity { get; set; }
        public string PageUrl { get; set; }
        public DateTime ActivityDte { get; set; }

    }
}