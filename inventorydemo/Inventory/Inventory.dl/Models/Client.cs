﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.dl.Models
{
    public class Client
    {
        [Key]
        [MaxLength(32)]
        public string ClientId { get; set; }
        [MaxLength(80)]
        [Required]
        public string Base64Secret { get; set; }
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }
    }
}
