﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.dl.Models
{
    public partial class BalanceSheet
    {
        public Int32 BalanceSheetId { get; set; }
        public string BalanceSheetName { get; set; }
        public string Code { get; set; }
        public string CodeABC { get; set; }
        public virtual IEnumerable<BalanceSheetType> BalanceSheetTypes { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public virtual IEnumerable<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        public string CurrentWorkerId { get; set; }
        public Int32 AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public Int32? BsMainTypeId { get; set; }
        [ForeignKey("BsMainTypeId")]
        public virtual BsMainType BsMainType { get; set; }
    }

    public partial class BalanceSheet
    {
        public decimal GetTotalDebit()
        {
            return this.BalanceSheetTypes.Sum(a => a.GetTotalDebit());

        }
        public decimal GetTotalCrdit()
        {
            return this.BalanceSheetTypes.Sum(a => a.GetTotalCrdit());
        }
        public decimal GetBalance()
        {
            return this.BalanceSheetTypes.Sum(a => a.GetBalance());
        }
    }

    public class BsMainType
    {
        public Int32 BsMainTypeId { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public virtual IEnumerable<BalanceSheet> BalanceSheets { get; set; }
        public int orderNum { get; set; }
    }

    public class GetFinancialList
    {
        public Int32 Id { get; set; }
        public string AccountName { get; set; }
        public int AccountLevel { get; set; }

    }

    public class BranchTree
    {
        public string id { get; set; }
        public string text { get; set; }
        public IEnumerable<BalanceSheetTree> children { get; set; }
        

    }
    public class BalanceSheetTree
    {
        public string id { get; set; }
        public string text { get; set; }
        public IEnumerable<BalanceSheetTypeTree> children { get; set; }

       
    }
    public class BalanceSheetTypeTree
    {
        public string id { get; set; }
        public string text { get; set; }
        public IEnumerable<AccountCategoryTree> children { get; set; }

    }

    public class AccountCategoryTree
    {
        public string id { get; set; }
        public string text { get; set; }
        public IEnumerable<AccountTree> children { get; set; }


    }

    public class AccountTree
    {
        public string id { get; set; }
        public string text { get; set; }


    }

    

    public class accountsForItemGroup
    {
       
        public Int32 TypeId { get; set; }
        public string TypeName { get; set; }
        public bool Value { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public Int32  code { get; set; }
    }
    public class getAccountList
    {
        public Int32 Id { get; set; }
        public bool Iscompany { get; set; }
    }
    public class AccountsTable
    {
        public string Id { get; set; }
        public string AccountName { get; set; }
        public string AccountLevel { get; set; }
        public string AccountType { get; set; }
        public string ParentId { get; set; }
        public string TitleForAdd { get; set; }
        public Int32 Code { get; set; }
        public string fullCode { get; set; }
        public Int32? linkedAccountId { get; set; }
        public string linkedAccountName { get; set; }
        public bool isCompany { get; set; }
        public Int32 AccountsDataTypesId  { get; set; }
        public bool Activated { get; set; }
        public string AccountsDataTypesName { get;  set; }
    }

    public class PostAccountsTable
    {
        public AccountsTable old { get; set; }
        public AccountsTable edited { get; set; }

    }




    public class AccountTableTree
    {
        public List<List<AccountsTable>> AcountTable { get; set; }
        public List<BranchTree> AccountTree { get; set; }
    }





}