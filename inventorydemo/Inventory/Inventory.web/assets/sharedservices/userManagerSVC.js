﻿ "use strict";

    angular.module('inventoryModule').factory('userManagerSVC', ['$http', '$q', function ($http, API) {

        var svc = {

            getAllUsers: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/getAllUsers', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            getUserRole: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/getUserRole', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
              
            },
            getitemsInfoVM: function (data) {

                var deferred = $q.defer();
                $http.post('/api/items/getitemInfo', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            saveGroupVM: function (data) {

                var deferred = $q.defer();
                $http.post('/api/items/save', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },
            getItemsbyGroupId: function (groupId) {
                var deferred = $q.defer();

                $http.get('/api/items/getItemsbyGroupId/' + groupId)
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },
            loaditemsInfoVM: function (data) {

                var deferred = $q.defer();

                $http.post('/api/items/getUnits', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);


                return deferred.promise;
            },
            getuers: function () {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/getuers')
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            getusersand: function () {
 
                var deferred = $q.defer();
                 $http.post('/api/easystoreusers/getusersand')
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },
            setUserRole: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/setUserRole', it)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            branchGroupSt: function (data) {
                var deferred = $q.defer();
                $http.post('/api/AppStorge/branchGroupSt', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            getCompanyByName: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/getCompanyByName', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },
            addUserToCompany: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/addUserToCompany', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },
            inviteUser: function (data) {
                var deferred = $q.defer();
                $http.post('/api/easystoreusers/inviteUser', data)
                    .success(deferred.resolve)
                    .error(deferred.reject);
                return deferred.promise;
            },

            getUserSettings: function (data) {
                var deferred = $q.defer();
                $http.get('/api/easystoreusers/getUserSettings/' + data)
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },

        };
        return svc;

    }]);