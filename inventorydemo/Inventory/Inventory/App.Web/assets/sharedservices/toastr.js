﻿'use strict';

angular.module('inventoryModule').factory('toastr', ['$rootScope','$timeout', function ($rootScope, $timeout) {

    var error = function (message) {
        return toastr.error(message);
    };

    var info = function (message) {
        return toastr.info(message);
    };

    var success = function (message) {
        return toastr.success(message, { timeOut: 6000 })
    };

    var warning = function (message) {
        return toastr.warning(message, { timeOut: 6000 })
    };

    return {
        success: success,
        warning: warning,
        info: info,
        error: error,
    };

}]);



