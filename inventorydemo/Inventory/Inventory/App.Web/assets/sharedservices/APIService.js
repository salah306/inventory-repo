﻿"use strict"

angular.module('inventoryModule').factory('API', ['$http', '$q', 'notifySvc', 'userModel', function ($http, $q, notifySvc, userModel) {
   
   var urlBase = "https://www.mag-consulting.net.eg/ServiceTracker/";

    function getData(data) {
        var NullData = {};
        var deferred = $q.defer();
        var tokenKey = userModel.getUserInfo().accessToken;

        var req = {
            method: 'GET',
            url: urlBase + data.url,
            headers: {
                Authorization: 'Bearer ' + tokenKey
            }
        };

        $http(req).then(function (response) {
            deferred.resolve(response);
        }, function (response) {
            if (response.data.modelState) {
                notifySvc.error(response.data.message, response.data.modelState[""].toString());
            } else {
                notifySvc.error(response.data.message, response.data.message);
            }
        });


        return deferred.promise;
    }


    // Post Call to the api with credentials
    function postData(data) {
        var deferred = $q.defer();
        var NullData = {};
        var tokenKey = userModel.getUserInfo().accessToken;

        var req = {
            method: 'POST',
            url: urlBase + data.url,
            data: data.details,
            headers: {
                Authorization: 'Bearer ' + tokenKey
            }
        };
        $http(req).then(function (response) {
            deferred.resolve(response.data);
        }).catch(function (response) {
            if (response.data.modelState) {
                notifySvc.error(response.data.message, response.data.modelState[""].toString());
            } else {
                notifySvc.error(response.data.message, response.data.message);
            }            
        });

        return deferred.promise;
    }

    // PUT Call to the api with credentials
    function putData(data) {
        var deferred = $q.defer();
        var NullData = {};
        var tokenKey = userModel.getUserInfo().accessToken;

        var req = {
            method: 'Put',
            url: urlBase + data.url,
            data: data.details,
            headers: {
                Authorization: 'Bearer ' + tokenKey
            }
        };
        $http(req).then(function (response) {
            deferred.resolve(response.data);
        }).catch(function (response) {
            if (response.data.modelState) {
                notifySvc.error(response.data.message, response.data.modelState[""].toString());
            } else {
                notifySvc.error(response.data.message, response.data.message);
            }
        });

        return deferred.promise;
    }

    function getDataSecure(data) {
        var NullData = {};
        var deferred = $q.defer();
        var tokenKey = userModel.getUserInfo().accessToken;

        var req = {
            method: 'GET',
            url: urlBase + data.url,
            headers: {
                Authorization: 'Bearer ' + tokenKey
            }
        };


        $http(req).then(function (response) {
            deferred.resolve(response);
        }, function (response) {
            if (response.data.modelState) {
                notifySvc.error(response.data.message, response.data.modelState[""].toString());
            } else {
                notifySvc.error(response.data.message, response.data.message);
            }
        });


        return deferred.promise;
    }

    return {
        getData: getData,
        getDataSecure: getDataSecure,
        postData: postData,
        putData: putData

    };
}]);