﻿
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("inventoryTransferControlle", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];
       
        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                        $scope.order = {
                            "inventoryTransferId": 0,
                            "orderNo": 0,
                            "orderDate": "",
                            "orderDateSecond": "",
                            "userNameFirst": "",
                            "userNameSecond": "",
                            "isDoneFirst": false,
                            "isDoneSecond": false,
                            "cancel": false,
                            "isNewOrder": true,
                            "transferFrom": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "transferTo": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "transferItems": [
                              {
                                  "itemId": 0,
                                  "itemName": "",
                                  "itemCode": "",
                                  "qty": 0,
                                  "price": 0,
                                  "note": "",
                                  "noteSecond": null,
                                  "realted": "",
                                  "itemGroupName": "",
                                  "maxQ": 0
                              }
                            ],
                            "companyId": 0,
                            "isCompany": true,
                            "cancelDate": "",
                            "userNameCancel": ""
                        };
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getInventory();
                    }
                }

            }
        });

      

    


        //$scope.inventory = [{ inventoryId: 1, inventoryName: 'مخزن مواد وسلع' }, { inventoryId: 2, inventoryName: 'مخزن 2' }, { inventoryId: 3, inventoryName: 'مخزن 3' }, { inventoryId: 4, inventoryName: 'مخزن 4' }]
        $scope.inventoryTo = [];
        $scope.order = {
            "inventoryTransferId": 0,
            "orderNo": 0,
            "orderDate": "",
            "orderDateSecond": "",
            "userNameFirst": "",
            "userNameSecond": "",
            "isDoneFirst": false,
            "isDoneSecond": false,
            "cancel": false,
            "isNewOrder": true,
            "transferFrom": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "transferTo": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "transferItems": [
              {
                  "itemId": 0,
                  "itemName": "",
                  "itemCode": "",
                  "qty": 0,
                  "price": 0,
                  "note": "",
                  "noteSecond": null,
                  "realted": "",
                  "itemGroupName": "",
                  "maxQ": 0
              }
            ],
            "companyId": 0,
            "isCompany": true,
            "cancelDate": "",
            "userNameCancel": ""
        }
       
      
        $scope.$watch('order.transferFrom.inventoryId', function (newVal, oldVal) {

            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.inventory, { inventoryId: newVal }, true));
                $scope.order.transferFrom = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };
                if (findinventory.length) {
                    $localStorage.invrntorytransferfrom = $scope.order.transferFrom;
                    $scope.getInventoryTo();
                }
            }
        });
        $scope.$watch('order.transferTo.inventoryId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.inventoryTo, { inventoryId: newVal }, true));
                $scope.order.transferTo = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };
                if (findinventory.length) {
                    $localStorage.invrntorytransferto = $scope.order.transferTo;
                }
            }
        });
     
        $scope.getInventory = function () {
        
            AccountOrderspersistenceService.action.getInventory(JSON.stringify({ companyId: bId, isComapny: $scope.branchGroub.isCompany })).then(
              function (result) {
                  $scope.inventory = result.data;
                  if ($localStorage.invrntorytransferfrom) {
                      var accs = $localStorage.invrntorytransferfrom;
                      var findacc = angular.copy($filter('filter')($scope.inventory, { inventoryId: accs.inventoryId }, true));
                      if (findacc.length) {
                         $scope.order.transferFrom.inventoryId = $localStorage.invrntorytransferfrom.inventoryId
                      }
                  };
              },
              function (error) {
                  if (error.data) {
                      if (error.data.message) {
                          toastr.error(error.data.message);
                      }
                    
                  } else {
                      toastr.error("لا يمكن اتمام العملية الان");
                  }

              });
        };

        $scope.items = [];
        $scope.getInventoryTo = function () {
            if ($scope.order.transferFrom.inventoryName) {
                AccountOrderspersistenceService.action.getInventoryTo(JSON.stringify({ inventoryId: $scope.order.transferFrom.inventoryId, inventoryName: $scope.order.transferFrom.inventoryName, companyId: bId, isComapny: $scope.branchGroub.isCompany })).then(
                    function (result) {
                        $scope.inventoryTo = result.data.inventoryTransferTo;
                        $scope.items = result.data.items;
                        if ($localStorage.invrntorytransferto) {
                            var accs = $localStorage.invrntorytransferto;
                            var findacc = angular.copy($filter('filter')($scope.inventoryTo, { inventoryId: accs.inventoryId }, true));
                            if (findacc.length) {
                                $scope.order.transferTo.inventoryId = $localStorage.invrntorytransferto.inventoryId;
                            }
                        };
                    },
                    function (error) {
                        console.log(error);
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                     

                    });
            }
          
        };
       //$scope.order.transferItems = [{ itemId: 0, itemCode: null, itemName: null, qty: 0.0, maxQ: 0.0, realted: null }];
        $scope.getItembyCode = function (data , index) {
            console.log('data', data);
            console.log('index', index);
            for (var i = 0; i < $scope.order.transferItems.length; i++) {
                if ($scope.order.transferItems.length > 1 && $scope.order.transferItems[i].itemCode === data.itemCode) {
                    $scope.order.transferItems[index].itemName = null;
                    var id1 = '#qty' + (i + 1);
                    setTimeout(function () { $(id1).focus() }, 200);
                    return;
                }
            };
            $scope.order.transferItems[index].itemCode = data.itemCode;
            $scope.order.transferItems[index].itemName = data.itemName;
            $scope.order.transferItems[index].itemId = data.itemId;
            $scope.order.transferItems[index].qty = 0.0;
            $scope.order.transferItems[index].note = null;
            $scope.order.transferItems[index].maxQ = data.qty;
            $scope.order.transferItems[index].realted = data.realted;

            //realted: null
            var id = '#qty' + $scope.order.transferItems.length;
            setTimeout(function () { $(id).focus() }, 200);
        }

        $scope.checkqty = function (item) {

             
            console.log('after return' , item);
           
            if (!item.qty) {
                item.qty = item.maxQ;
                if (item.maxQ > 0) {
                    toastr.warning("الكمية المتاحة من " + item.itemName + " " + item.maxQ);
                }
            }
            if (item.qty > item.maxQ) {
                item.qty = item.maxQ;
                if (item.maxQ > 0) {
                    toastr.warning("الكمية المتاحة من " + item.itemName + " " + item.maxQ);
                }
                
            };
       
        };

        $scope.keys = function (event) {

            console.log("$event", event);
            if (event.altKey) {
                switch (event.which) {
                    case 37:
                        //alert("shift + left arrow");
                        break;
                    case 38:
                        //alert("shift + up arrow");
                        break;
                    case 39:
                        //alert("shift + right arrow");
                        break;
                    case 78:
                        //$scope.addNewitemPropertyValue();
                        break;
                    default:
                        break;
                }
            }
        }


        $scope.addNewTransferitem = function () {
 
            for (var i = 0; i < $scope.order.transferItems.length; i++) {
                if ($scope.order.transferItems[i].qty <= 0 || !$scope.order.transferItems[i].itemCode || !$scope.order.transferItems[i].itemName || $scope.order.transferItems[i].qty > $scope.order.transferItems[i].maxQ) {
                    return;
                }
            }
           
           $scope.order.transferItems.push({ itemId: 0, itemCode: null, itemName: null, qty: 0.0, maxQ: 0.0, realted : null});
           var id = '#ui' + $scope.order.transferItems.length;
            setTimeout(function () { $(id).focus() }, 200);


        }
        $scope.removeTransfer = function (item) {
            console.log("Item : ", angular.toJson(item));
            if ($scope.order.transferItems && $scope.order.transferItems.length > 1) {
                $scope.order.transferItems.splice(item, 1);
            }
            
        };

        //order.orderNo
      
        $scope.newOrder = function () {
            console.log($scope.order);
            console.log(angular.toJson($scope.order));
            var neworder =  {
                "inventoryTransferId": 0,
                "orderNo": 0,
                "orderDate": moment().format('DD/MM/YYYY'),
                "orderDateSecond": "",
                "userNameFirst": "",
                "userNameSecond": "",
                "isDoneFirst": false,
                "isDoneSecond": false,
                "cancel": false,
                "isNewOrder": true,
                "transferFrom": angular.copy($scope.order.transferFrom),
                "transferTo": angular.copy($scope.order.transferTo),
                "transferItems": [
                  {
                      "itemId": 0,
                      "itemName": "",
                      "itemCode": "",
                      "qty": 0,
                      "price": 0,
                      "note": "",
                      "noteSecond": null,
                      "realted": "",
                      "itemGroupName": "",
                      "maxQ": 0
                  }
                ],
                "companyId": 0,
                "isCompany": true,
                "cancelDate": "",
                "userNameCancel": ""
            };
            $scope.order = neworder;
        };
        $scope.findOrderById = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.GetTransferOrderById(angular.toJson({ inventoryId: $scope.order.transferFrom.inventoryId, orderNo : parseInt(data)})).then(
                    function (result) {
                        $scope.order = result.data;
                        $scope.order.orderDate = moment(result.data.orderDate).format('DD/MM/YYYY');
                        $scope.order.cancelDate = moment($scope.order.cancelDate).format('DD/MM/YYYY');
                        $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('DD/MM/YYYY');
                    },
                    function (error) {
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        };
        $scope.vaildOrder = function () {

        };


        $scope.saveOrder = function () {

            var orderToSave = angular.copy($scope.order);
            if ($scope.order.isNewOrder) {
                orderToSave.orderDate = moment.utc();
            }
            AccountOrderspersistenceService.action.saveTransferOrder(angular.toJson(orderToSave)).then(
                function (result) {
                    $scope.order = result.data.order;
                    $scope.order.orderDate = moment(result.data.order.orderDate).format('DD/MM/YYYY');
                    $order.cancelDate = moment($order.cancelDate).format('DD/MM/YYYY');
                    $order.orderDateSecond = moment($order.orderDateSecond).format('DD/MM/YYYY');
                    $scope.items = result.data.items;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
          

        };

        $scope.cancelOrder = function () {

          
            var orderToSave = angular.copy($scope.order);
            orderToSave.orderDate = moment.utc();
            orderToSave.cancel = true;
            orderToSave.isNewOrder = false;
            AccountOrderspersistenceService.action.saveTransferOrder(angular.toJson(orderToSave)).then(
                function (result) {
                  
                    $scope.order = result.data.order;
                    $scope.order.orderDate = moment(result.data.order.orderDate).format('DD/MM/YYYY');
                    $order.cancelDate = moment($order.cancelDate).format('DD/MM/YYYY');
                    $order.orderDateSecond = moment($order.orderDateSecond).format('DD/MM/YYYY');
                    $scope.items = result.data.items;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    };

                });
        };
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.order.isCompany = $scope.branchGroub.isCompany
                $scope.getInventory();
            }
        }();
        
    });

}());


