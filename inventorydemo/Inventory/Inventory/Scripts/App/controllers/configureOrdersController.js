﻿
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("configureOrdersController", function ($scope, $timeout, _, OrdersConfigurationpersistenceService, BalanceSheetspersistenceService, $localStorage, $rootScope, settings, $filter,
                                                                 Offline, $location, $sce, EasyStoreUserspersistenceService, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {

        var bId;
        $scope.orderNamesSales =[];
        $scope.orderNamespur = [];
        $scope.selectedOrderSales = { salesId: 0, salesOrderName: null, salesName: null, salesReturnName: null, salesOrderIsNew : true};
        $scope.selectedOrderpur = { purId: 0, purOrderName: null, purName: null, purReturnName: null, purOrderIsNew: true };
        $scope.branchesGroub = [];
        
        $scope.branchGroub = $rootScope.mainbranchGroub;
        $scope.accountTypes = [{ typeId: 1, typeName: "مدين" }, { typeId: 2, typeName: "دائن" }]
        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    if (findCompanybr.length) {
                        $scope.accountCatogery = [];
                        $scope.accounts = [];
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        if ($scope.branchGroub.id !== '0') {
                            $('.nav-tabs a:first').tab('show');
                            $scope.GetOrderGroupNames();
                        }
                    }
                }

            }
        });

        $scope.accountCatogery = [];
        $scope.accounts = [];

        $scope.GetOrderGroupNames = function () {
            toastr.clear();
            OrdersConfigurationpersistenceService.action.getOrderConfigureName().then(
                  function (result) {
                      $scope.orderNamesSales = result.data.sales;
                      $scope.orderNamespur = result.data.pur;
                      $scope.accountCatogery = result.data.accountsCatogery;
                      $scope.accounts = result.data.accounts;
                      $scope.inventories = result.data.inventories.inventories;
                      $scope.salesConfig = result.data.inventories.sales;
                      $scope.salesCostConfig = result.data.inventories.salesCost;
                      $scope.inventoryConfig = result.data.inveConfig;
                  },
                  function (error) {
                      toastr.error("تعذر طلب البيانات في الوقت الحالي");
                  });
        };


       
   

        $scope.$watch('selectedOrderSales.salesId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findorder = angular.copy($filter('filter')($scope.orderNamesSales, { salesId: newVal }, true));
                if (findorder) {
                   
                    if (findorder.length) {
                        $scope.getOrderSalepur($scope.selectedOrderSales.salesId, 'sales')
                    }
                }

            }
        });
        $scope.$watch('selectedOrderpur.purId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findorder = angular.copy($filter('filter')($scope.orderNamespur, { purId: newVal }, true));
                if (findorder) {
                    if (findorder.length) {
                        $scope.getOrderSalepur($scope.selectedOrderpur.purId, 'pur')
                    }
                }

            }
        });

        $scope.getOrderSalepur = function (id, type) {
            if ($scope.branchGroub.id !== '0') {
                OrdersConfigurationpersistenceService.action.GetOrderbyId(JSON.stringify({ orderId: id, type: type })).then(
                        function (result) {
                            if (type === "sales") {
                                $scope.selectedOrderSales = result.data;
                            }
                            if (type === "pur") {
                                $scope.selectedOrderpur = result.data;
                            }

                        },
                        function (error) {
                            if (error.data) {
                                toastr.error(error.data.message);
                                if (type === "sales") {
                                    $scope.selectedOrderSales = { salesId: 0, salesName: null };
                                }
                                if (type === "pur") {
                                    $scope.selectedOrderpur = { purId: 0, purName: null };
                                }
                               
                               
                            } else {

                                toastr.error("لا يمكن اتمام العملية الان");
                                if (type === "sales") {
                                    $scope.selectedOrderSales = { salesId: 0, salesName: null };
                                }
                                if (type === "pur") {
                                    $scope.selectedOrderpur = { purId: 0, purName: null };
                                }
                            }
                        });
            }

        }

        $scope.newOrder = function (type) {
            if (type === "sales") {
                $scope.selectedOrderSales = { salesId: 0, salesOrderName: null, salesName: null, salesReturnName: null, salesOrderIsNew: true, terms: null, totalAccounts: null, tableAccounts:null };

            }
            if (type === "pur") {
                $scope.selectedOrderpur = { purId: 0, purOrderName: null, purName: null, purReturnName: null, purOrderIsNew: true, terms: null, totalAccounts: null, tableAccounts: null };
            }
        };
      
        $scope.addterm = function (type) {
            toastr.clear();
            if (type === "sales") {
                if (!$scope.selectedOrderSales.terms) {
                    $scope.selectedOrderSales.terms = [{ termId: 0, name: "" }];
                    return;
                }
                for (var i = 0; i <  $scope.selectedOrderSales.terms.length; i++) {
                    if (!$scope.selectedOrderSales.terms[i].name) {
                        toastr.warning("توجد حقول لم يتم استخدامها - قم بملائها قبل اضافة شروط واحكام جديدة");
                        return;
                    }
                }
                $scope.selectedOrderSales.terms.push({termId: 0 , name : ""});
            }
            if (type === "pur") {
                if (!$scope.selectedOrderpur.terms) {
                    $scope.selectedOrderpur.terms = [{ termId: 0, name: "" }];
                    return;
                }
                for (var i = 0; i < $scope.selectedOrderpur.terms.length; i++) {
                    if (!$scope.selectedOrderpur.terms[i].name) {
                        toastr.warning("توجد حقول لم يتم استخدامها - قم بملائها قبل اضافة شروط واحكام جديدة");
                        return;
                    }
                }
                $scope.selectedOrderpur.terms.push({termId : 0 , name: "" });
            }
        };

        $scope.removeterm = function (type, index , term) {
            if (type === 'sales') {
                $scope.selectedOrderSales.terms.splice(index, 1);

            }
            if (type === 'pur') {
                $scope.selectedOrderpur.terms.splice(index, 1);
            }
        }

        $scope.removepayMetod = function (type , index, paym) {
            if (type === 'sales') {
                $scope.selectedOrderSales.payMethod.splice(index, 1);
            }
            if (type === 'pur') {
                $scope.selectedOrderpur.payMethod.splice(index, 1);
            }
        };

        $scope.removetableAccount = function (type, index, account , istotal) {
            if (istotal) {
                if (type === 'sales') {
                    $scope.selectedOrderSales.totalAccounts.splice(index, 1);
                }
                if (type === 'pur') {
                    $scope.selectedOrderpur.totalAccounts.splice(index, 1);
                }

                return;
            }
            if (type === 'sales') {
                $scope.selectedOrderSales.tableAccounts.splice(index, 1);
            }
            if (type === 'pur') {
                $scope.selectedOrderpur.tableAccounts.splice(index, 1);
            }
        };
        $scope.removeInventory = function (index) {

            $scope.inventoryConfig.splice(index, 1);
        };

        $scope.inventoryConfig = [];
        $scope.getinventoryConfig = function () {
            $("#invenoty-model").modal('show');
        };
        $scope.addInventory = function (data) {
            toastr.clear();
            var isexist = angular.copy($filter('filter')($scope.inventoryConfig, { inventoryId: data.inventoryId }, true));
            if (isexist.length && isexist.length > 0) {
                toastr.warning("المخزن المحدد تم اختيارة من قبل لا يمكن تكرار الاعدادات");
                return;
            }
            var inveConfig = {
                inventoryId: data.inventoryId,
                inventoryName: data.inventoryName + " " + data.code,
                salesCostAccountId: 0,
                salesCostAccountName: '',
                salesAccountId: 0,
                salesAccountName: '',
                salesReturnId: 0,
                salesReturnName: '',
            };
            $scope.inventoryConfig.push(inveConfig);
        };

        $scope.saveInventory = function () {
            toastr.clear();
            for (var i = 0; i < $scope.inventoryConfig.length; i++) {

                if ($scope.inventoryConfig[i].salesCostAccountId === 0) {
                    var inv = ' - ' + $scope.inventoryConfig[i].inventoryName;
                    toastr.warning("برجاء اختيار حساب تكلفة المببعات قبل الحفظ" + inv);
                    return;
                }
                if ($scope.inventoryConfig[i].salesAccountId === 0)
                {
                    var inv = ' - ' + $scope.inventoryConfig[i].inventoryName;
                    toastr.warning("برجاء اختيار حساب المبيعات قبل الحفظ" + inv);
                    return;
                }
               
                if ($scope.inventoryConfig[i].salesReturnId === 0) {
                    var inv = ' - ' + $scope.inventoryConfig[i].inventoryName;
                    toastr.warning("برجاء اختيار حساب رد المببعات قبل الحفظ" + inv);
                    return;
                }

                if ($scope.inventoryConfig[i].salesReturnId === $scope.inventoryConfig[i].salesAccountId ) {
                    var inv = ' - ' + $scope.inventoryConfig[i].inventoryName;
                    toastr.warning("لا يمكن اختيار نفس الحساب للمبيعات ورد المبيعات" + inv);
                    return;
                }

            
            }
            console.log(JSON.stringify($scope.inventoryConfig));
            OrdersConfigurationpersistenceService.action.saveinventory(JSON.stringify($scope.inventoryConfig)).then(
                function (or) {

                    
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                });
        };

        $scope.typeforPaymethod = "";
        $scope.typeforTableAccounts = "";
        $scope.typefortotalAccounts = "";

        var childern = [];
        $scope.getDefaultAccount = function (id) {
            var isexist = angular.copy($filter('filter')($scope.accountCatogery, { accountId: id }, true));
            childern = isexist[0].childern;
            return childern;
        };

        $scope.getAccountsCatogery = function (type , edited , methed) {
            toastr.clear();
            $scope.toshow = 5;
            if (type === 'sales') {
                toastr.clear();
                if ($scope.selectedOrderSales.payMethod) {
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { payName: '' }, true));
                    if (isexist.length && isexist.length > 0) {
                        toastr.warning("حدد اسم طريقة الدفع قبل اختيار طريقة دفع اخري");
                        return;
                    }
                }
                
            }
            if (type === 'pur') {
                toastr.clear();
                if ($scope.selectedOrderpur.payMethod) {
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.payMethod, { payName: '' }, true));
                    if (isexist.length && isexist.length > 0) {
                        toastr.warning("حدد اسم طريقة الدفع قبل اختيار طريقة دفع اخري");
                        return;
                    }
                }
            }
            if ($scope.accountCatogery && $scope.accountCatogery.length == 0) {
                BalanceSheetspersistenceService.action.getOrderConfigureCatogery().then(
                    function (result) {
                        $scope.accountCatogery = result.data;
                        $scope.typeforPaymethod = type;
                        $("#accountCatogery-model").modal('show');
                        $scope.payMethodtoEditTrue = edited;
                        if (edited) {
                            $scope.payMethodToedit = methed;
                           
                        };
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
            else {
                $scope.typeforPaymethod = type;
                $scope.payMethodtoEditTrue = edited;
                if (edited) {
                    $scope.payMethodToedit = methed;
                    
                };
                $("#accountCatogery-model").modal('show');
            

            }
        };
        $scope.istotal = false;
        //getOrderConfigureAccounts
        $scope.getAccounts = function (type, edited, methed , istotal) {
            toastr.clear();
            $scope.istotal = istotal;
            if (istotal === true) {

                $scope.toshow = 5;
                if ($scope.accounts && $scope.accounts.length == 0) {
                    BalanceSheetspersistenceService.action.getOrderConfigureAccounts().then(
                        function (result) {
                            $scope.accounts = result.data;
                            $scope.typefortotalAccounts = type;

                            $scope.TableaccounttoEditTrue = edited;
                            if (edited) {
                                $scope.TableaccountToedit = methed;

                            };
                            $("#accounts-model").modal('show');
                        },
                        function (error) {
                            toastr.clear();
                            if (error.data) {
                                toastr.error(error.data.message);
                            } else {
                                toastr.error("لا يمكن اتمام العملية الان");
                            }
                        });
                }
                else {
                    $scope.typefortotalAccounts = type;

                    $scope.TableaccounttoEditTrue = edited;
                    if (edited) {
                        $scope.TableaccountToedit = methed;

                    };
                    $("#accounts-model").modal('show');


                }
                return;
            }
            $scope.toshow = 5;
            if ($scope.accounts && $scope.accounts.length == 0) {
                BalanceSheetspersistenceService.action.getOrderConfigureAccounts().then(
                    function (result) {
                        $scope.accounts = result.data;
                        $scope.typeforTableAccounts = type;
                        
                        $scope.TableaccounttoEditTrue = edited;
                        if (edited) {
                            $scope.TableaccountToedit = methed;
                            
                        };
                        $("#accounts-model").modal('show');
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
            else {
                $scope.typeforTableAccounts = type;

                $scope.TableaccounttoEditTrue = edited;
                if (edited) {
                    $scope.TableaccountToedit = methed;

                };
                $("#accounts-model").modal('show');

               
            }
        };

        $scope.setAccountTable = function (data, istotal) {
            toastr.clear();
            if ($scope.istotal === true) {
                console.log(istotal);
                if ($scope.TableaccountToedit && $scope.TableaccounttoEditTrue) {

                    if ($scope.typefortotalAccounts === 'sales') {
                        if (!$scope.selectedOrderSales.totalAccounts) {
                            $scope.selectedOrderSales.totalAccounts = [];
                        }
                        var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.totalAccounts, { accountId: data.accountId }, true));
                        if (isexist.length) {
                            toastr.warning("هذا الحساب محدد لطريقة دفع اخري");

                            return;
                        }

                        $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                        $scope.TableaccountToedit.accountId = data.accountId;
                        $("#accountCatogery-model").modal('hide');
                        toastr.clear();
                    }
                    if ($scope.typefortotalAccounts === 'pur') {
                        if (!$scope.selectedOrderpur.totalAccounts) {
                            $scope.selectedOrderpur.totalAccounts = [];
                        }
                        var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.totalAccounts, { accountId: data.accountId }, true));
                        if (isexist.length) {
                            toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                            return;
                        }

                        $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                        $scope.TableaccountToedit.accountId = data.accountId;
                        $("#accounts-model").modal('hide');
                        toastr.clear();
                    }

                    return;
                };

                if ($scope.typefortotalAccounts === 'sales') {
                    if (!$scope.selectedOrderSales.totalAccounts) {
                        $scope.selectedOrderSales.totalAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.totalAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                        return;
                    }
                    $scope.selectedOrderSales.totalAccounts.push({ totalAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                    toastr.clear();
                    $("#accounts-model").modal('hide');
                }
                if ($scope.typefortotalAccounts === 'pur') {
                    if (!$scope.selectedOrderpur.totalAccounts) {
                        $scope.selectedOrderpur.totalAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.totalAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                        return;
                    }
                    $scope.selectedOrderpur.totalAccounts.push({ totalAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                    toastr.clear();
                    $("#accouns-model").modal('hide');

                }
                return;
            }


            if ($scope.TableaccountToedit && $scope.TableaccounttoEditTrue) {

                if ($scope.typeforTableAccounts === 'sales') {
                    if (!$scope.selectedOrderSales.tableAccounts) {
                        $scope.selectedOrderSales.tableAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.tableAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");

                        return;
                    }
                 
                    $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                    $scope.TableaccountToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
                if ($scope.typeforTableAccounts === 'pur') {
                    if (!$scope.selectedOrderpur.tableAccounts) {
                        $scope.selectedOrderpur.tableAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.tableAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }

                    $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                    $scope.TableaccountToedit.accountId = data.accountId;
                    $("#accounts-model").modal('hide');
                    toastr.clear();
                }

                return;
            };

            if ($scope.typeforTableAccounts === 'sales') {
                if (!$scope.selectedOrderSales.tableAccounts) {
                    $scope.selectedOrderSales.tableAccounts = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.tableAccounts, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                    return;
                }
                $scope.selectedOrderSales.tableAccounts.push({ tableAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                toastr.clear();
                $("#accounts-model").modal('hide');
            }
            if ($scope.typeforTableAccounts === 'pur') {
                if (!$scope.selectedOrderpur.tableAccounts) {
                    $scope.selectedOrderpur.tableAccounts = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.tableAccounts, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                    return;
                }
                $scope.selectedOrderpur.tableAccounts.push({ tableAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                toastr.clear();
                $("#accouns-model").modal('hide');

            }
        };

        $scope.setAccountPay = function (data) {
            toastr.clear();
            if ($scope.payMethodToedit && $scope.payMethodtoEditTrue) {

                if ($scope.typeforPaymethod === 'sales') {
                    if (!$scope.selectedOrderSales.payMethod) {
                        $scope.selectedOrderSales.payMethod = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }
                  
                    $scope.payMethodToedit.accountName = data.accountName + " - " + data.code;
                    $scope.payMethodToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
                if ($scope.typeforPaymethod === 'pur') {
                    if (!$scope.selectedOrderpur.payMethod) {
                        $scope.selectedOrderpur.payMethod = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.payMethod, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }
                    
                    $scope.payMethodToedit.accountName = data.accountName + " - " + data.code;
                    $scope.payMethodToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
               
                return;
            };

            if ($scope.typeforPaymethod === 'sales') {
                if (!$scope.selectedOrderSales.payMethod) {
                    $scope.selectedOrderSales.payMethod = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                    return;
                }
                $scope.selectedOrderSales.payMethod.push({ payMethodId: 0, payName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, attchedRequest: true });
                $("#accountCatogery-model").modal('hide');
            }
            if ($scope.typeforPaymethod === 'pur') {
                if (!$scope.selectedOrderpur.payMethod) {
                    $scope.selectedOrderpur.payMethod = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.payMethod, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                    return;
                }
                $scope.selectedOrderpur.payMethod.push({ payMethodId: 0, payName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, attchedRequest: true });
                $("#accountCatogery-model").modal('hide');

            }
        };

        $scope.payMethodChanged = function (data, type) {
            if ($scope.typeforPaymethod === 'sales') {
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { payName: data }, true));
                if (isexist.length && isexist.length > 1) {
                    toastr.warning("طريقة الدفع موجودة بالفعل - قم باختيار اسم اخر");
                    return;
                }
            }

            if ($scope.typeforPaymethod === 'pur') {
                var isexist = angular.copy($filter('filter')($scope.selectedOrderpur.payMethod, { payName: data }, true));
                if (isexist.length && isexist.length > 1) {
                    toastr.warning("طريقة الدفع موجودة بالفعل - قم باختيار اسم اخر");
                    data = "";
                    return;
                }
            }
           
        };

        $scope.save = function (type) {
            toastr.clear();
            if (type == 'sales') {
            
                //saveSalesOrderconfig
                OrdersConfigurationpersistenceService.action.saveSalesOrderconfig(JSON.stringify($scope.selectedOrderSales)).then(
                    function (or) {
                      

                        $scope.selectedOrderSales = or.data;
                        var isexist = angular.copy($filter('filter')($scope.orderNamesSales, { salesId: $scope.selectedOrderSales.salesId }, true));
                        if (isexist && isexist.length > 0) {
                            isexist[0].salesOrderName = $scope.selectedOrderSales.salesOrderName;
                        } else {
                            $scope.orderNamesSales.push({ salesId: $scope.selectedOrderSales.salesId, salesOrderName: $scope.selectedOrderSales.salesOrderName })
                        }
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });

            }
            if (type == 'pur') {
                
                //savePurchaseOrderconfig
                OrdersConfigurationpersistenceService.action.savePurchaseOrderconfig(JSON.stringify($scope.selectedOrderpur)).then(
                      function (or) {
                          $scope.selectedOrderpur = or.data;
                          var isexist = angular.copy($filter('filter')($scope.orderNamespur, { purId: $scope.selectedOrderpur.purId }, true));
                          if (isexist && isexist.length > 0) {
                              isexist[0].purOrderName = $scope.selectedOrderpur.purOrderName;
                          } else {
                              $scope.orderNamespur.push({ purId: $scope.selectedOrderpur.purId, purOrderName: $scope.selectedOrderpur.purOrderName })
                          }
                      },
                      function (error) {
                          toastr.clear();
                          if (error.data) {
                              toastr.error(error.data.message);
                          } else {
                              toastr.error("لا يمكن اتمام العملية الان");
                          }
                      });

            }
        };

        $scope.checkOrderName = function (type) {
            toastr.clear();
            if (type == 'sales') {

                var isexist = angular.copy($filter('filter')($scope.orderNamesSales, {salesOrderName: $scope.selectedOrderSales.salesOrderName }, true));
                if (isexist && isexist.length > 0) {
                    toastr.warning("الاسم الذي تحاول ادخاله مسجل بالفعل");
                }
            }
            if(type == 'pur')
            {
                var isexist = angular.copy($filter('filter')($scope.orderNamespur, { purOrderName: $scope.selectedOrderpur.purOrderName }, true));
                if (isexist && isexist.length > 0) {
                    toastr.warning("الاسم الذي تحاول ادخاله مسجل بالفعل");
                } 
            }
        };

        $scope.isvaild = function (type) {
           
            toastr.clear();
            if (type === 'sales') {
                console.log("sales", JSON.stringify($scope.selectedOrderSales));
                if (!$scope.selectedOrderSales.salesOrderName) {
                    toastr.warning("يجب تحديد اسم النوع ");
                    return false;
                }
                if (!$scope.selectedOrderSales.salesName) {
                    toastr.warning("يجب تحديد عنوان الامر في حالة البيع");
                    return false;
                }
                if (!$scope.selectedOrderSales.salesReturnName) {
                    toastr.warning("يجب تحديد عنوان الامر في حالة رد المبيعات");
                    return false;
                }
                if (!$scope.selectedOrderSales.payMethod || $scope.selectedOrderSales.payMethod.length == 0) {
                    toastr.warning("يجب تحديد طريقة دفع/سداد واحدة علي الاقل");
                    return false;
                }

                for (var i = 0; i < $scope.selectedOrderSales.payMethod.length; i++) {
                    if (!$scope.selectedOrderSales.payMethod[i].payName) {
                        toastr.warning("يجب اختيار اسم لطريقة الدفع");
                        return false;
                    }
                }

                return true;
            }
            if (type === 'pur') {
                console.log("pur", JSON.stringify($scope.selectedOrderpur));
                if (!$scope.selectedOrderpur.purOrderName) {
                    toastr.warning("يجب تحديد اسم النوع ");
                    return false;
                }
                if (!$scope.selectedOrderpur.purName) {
                    toastr.warning("يجب تحديد عنوان الامر في حالة الشراء");
                    return false;
                }
                if (!$scope.selectedOrderpur.purReturnName) {
                    toastr.warning("يجب تحديد عنوان الامر في حالة رد رد المشتريات");
                    return false;
                }
                if (!$scope.selectedOrderpur.payMethod || $scope.selectedOrderpur.payMethod.length == 0) {
                    toastr.warning("يجب تحديد طريقة دفع/سداد واحدة علي الاقل");
                    return false;
                }

                for (var i = 0; i < $scope.selectedOrderpur.payMethod.length; i++) {
                    if (!$scope.selectedOrderpur.payMethod[i].payName) {
                        toastr.warning("يجب اختيار اسم لطريقة الدفع");
                        return false;
                    }
                }

                return true;
            }
        };
        //End Order Request 
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                if ($scope.branchGroub.id !== '0') {
                    $('.nav-tabs a:first').tab('show');
                    $scope.GetOrderGroupNames();
                }
            }
        }();
    });

}());


