﻿
'use strict';


(function () {

    var inventoryModule = angular.module('inventoryModule');
    inventoryModule.controller('loginController', function ($scope, $location, authService, $timeout, $rootScope, settings, $localStorage, EasyStoreUserspersistenceService , AccountspersistenceService, localStorageService) {
        $scope.savedSuccessfully = false;
        $scope.message = "";
        $scope.showlink = false;
        $scope.registration = {
            firstName: "",
            lastName: "",
            occupation: "",
            userName: "",
            email: "",
            phoneNumber: "",
            password: "",
            confirmPassword: ""
        };
        $scope.signUp = function () {

            authService.saveRegistration($scope.registration).then(function (response) {

                $scope.savedSuccessfully = true;
                $scope.message = "تم التسجيل بتجاح, سوف يتم ارسال رساله الي بريدك الالكتروني لتأكيد البريد اتبع الرابط في الرسالة المرسلة.";
                //startTimer();

            },
             function (response) {
                 var errors = [];
                 for (var key in response.data.modelState) {
                     for (var i = 0; i < response.data.modelState[key].length; i++) {
                         errors.push(response.data.modelState[key][i]);
                     }
                 }
                 $scope.message = "فشل في التسجيل:" + errors.join(' ');
             });
        };

        $scope.loginData = {
            userName: "",
            password: ""
        };

        $scope.RegenerateEmailConfirmation = [];

       

        $scope.login = function () {

            authService.login($scope.loginData).then(function (response) {
   
                console.log('user goes here')
               
                $localStorage.$reset();
                $rootScope.mainshow = true;
                $rootScope.mainUserInfo = {};
                $rootScope.mLogout = authService.logOut;
                var getuserInfo = function () {
                    EasyStoreUserspersistenceService.action.getusersand().then(
                function (result) {
                    console.log(result);
                    $rootScope.mainbranchGroub = result.data.branchGroup;

                    $rootScope.mainUserInfo = result.data.user;
                    $rootScope.mainbranchesGroub =result.data.companies;
                 
                   
                },
                function (error) {

                });
                }();

                $location.path('/index.html');
            },
             function (err) {
                 $scope.message = err.error_description;
                 if (err.error == 'un_conformed_email') {
                     console.log(err);
                    
                     $scope.showlink = true;
                 }
                 else {
                     $scope.showlink = false;
                    
                 }
             });
        };

        $scope.RegenerateEmailConfirmation = function () {
            $scope.showlink = false;
            $scope.message = 'تم الارسال بنجاح';
            authService.RegenerateEmailConfirmation($scope.loginData.userName).then(function (respone) {
               
            })
        }

    })

    //starting signup


}());