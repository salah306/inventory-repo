﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('BalanceSheetsremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet))
                {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/BalanceSheets', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/BalanceSheets', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            update: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet))
                {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/BalanceSheets/updateBalanceSheet', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/BalanceSheets/updateBalanceSheet', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            updateCustomBS: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/BalanceSheets/updateCustomBalanceSheet', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/BalanceSheets/updateCustomBalanceSheet', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            saveCustomBalanceSheet: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/BalanceSheets/saveCustomBalanceSheet', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/BalanceSheets/saveCustomBalanceSheet', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            getAccountsDataTypes: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/BalanceSheets/getAccountsDataTypes', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/BalanceSheets/getAccountsDataTypes', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            getAccountDataTypesForItemGroup: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/getAccountDataTypesForItemGroup/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            saveItemGroupAccounts: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/saveItemGroupAccounts', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/saveItemGroupAccounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },

            saveItemPropertyValueInputs: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
             
                    return $http.post('/api/ItemGroup/saveItemPropertyValueInputs', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
               

                return deferred.promise;
            },
            updateItemGroupName: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/updateItemGroupName', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/updateItemGroupName', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            updateItemGroupunit: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/updateItemGroupUnit', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/updateItemGroupUnit', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            UpdateItemGroupProperty: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/UpdateItemGroupProperty', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/UpdateItemGroupProperty', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            updateItemPropertyValues: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/updateItemPropertyValues', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/updateItemPropertyValues', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            deleteFromItemGroup: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/deleteFromItemGroup', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/deleteFromItemGroup', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            setInventoryValuationMethod: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/ItemGroup/setInventoryValuationMethod', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/ItemGroup/setInventoryValuationMethod', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            saveItemPropertyValues: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/saveItemPropertyValues/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            getinventoryValuatioMethods: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/getinventoryValuatioMethods/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            saveItemGroup: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/saveItemGroup/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            saveItemGroupProperty: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/saveItemGroupProperty/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            getItemsGroupNames: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/GetItemsGroupNames/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            
            GetitemsGroup: function (name) {
                var deferred = $q.defer();

                $http.get('/api/ItemGroup/GetitemsGroup/' + name)
                        .success(deferred.resolve)
                        .error(deferred.reject);

                return deferred.promise;
            },
            getAccPropertiesValue: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/getaccvalue/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
   
            
            delnode: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/delBalanceSheet', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/delBalanceSheet', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            delnodef: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/dBalanceSheetf', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/dBalanceSheetf', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            saveAccountProp: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/saveAccountCategoryProperties', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/saveAccountCategoryProperties', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            setaccountvalue: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/setaccvalue', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/setaccvalue', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            Updateaccountvalue: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/updateaccvalue', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/updateaccvalue', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
           

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/BalanceSheetsById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
         
            getTreeById: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/branchTreeById', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/branchTreeById', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getAllAccounts: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/getAllAccounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/getAllAccounts', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            saveCustomAccounts: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/saveCustomAccounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/getAllAccounts', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getCustomesAccounts: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/getCustomesAccounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/getCustomesAccounts', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getCustomTreeById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/financialListTreeById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
           
            GetAllFinancialList: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/BalanceSheets/GetAllFinancialList', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/BalanceSheets/GetAllFinancialList', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getaccountCategoryProperties: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/accountCategoryProperties/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                
                return deferred.promise;
            },
            
            
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (BalanceSheet) {
                    deferred.resolve(BalanceSheet.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            RemoveAccProperty: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/removeAccProperty/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            
        RemoveAccValue: function (name) {
            var deferred = $q.defer();

            $http.get('/api/BalanceSheets/removeaccvalue/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

            return deferred.promise;
        },
        GetOrderGroupbyId: function (name) {
            var deferred = $q.defer();

            $http.get('/api/OrderGroup/getOrderGroupbyId/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

            return deferred.promise;
        },
        GetOrderGroupPropType: function (name) {
            var deferred = $q.defer();

            $http.get('/api/OrderGroup/getOrderGroupPropType/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

            return deferred.promise;
        },
        GetaccountsTypes: function (name) {
            var deferred = $q.defer();

            $http.get('/api/OrderGroup/GetaccountsTypes/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

            return deferred.promise;
        },
       GetTotalaccounts: function (name) {
            var deferred = $q.defer();

           $http.get('/api/OrderGroup/GetTotalaccounts/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

            return deferred.promise;
       },
       getOrderGroupforBill: function (name) {
           var deferred = $q.defer();

           $http.get('/api/OrderGroup/getOrderGroupforBill/' + name)
                 .success(deferred.resolve)
                 .error(deferred.reject);

           return deferred.promise;
       },
        GetOrderGroupNames: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderGroupNames', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderGroupNames', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        getOrderConfigureName: function () {
            var deferred = $q.defer();
                return $http.post('/api/OrderGroup/getOrderConfigureName')
                 .success(deferred.resolve)
                 .error(deferred.reject);
            return deferred.promise;
        },
        getOrderConfigureCatogery: function () {
            var deferred = $q.defer();
            return $http.post('/api/OrderGroup/getCatogory')
                 .success(deferred.resolve)
                 .error(deferred.reject);
            return deferred.promise;
        },
        getOrderConfigureAccounts: function () {
            var deferred = $q.defer();
            return $http.post('/api/OrderGroup/getAccounts')
                 .success(deferred.resolve)
                 .error(deferred.reject);
            return deferred.promise;
        },
        getOrderSalePur: function (order) {
            var deferred = $q.defer();
            return $http.post('/api/OrderGroup/getOrderSalePur' , order)
             .success(deferred.resolve)
             .error(deferred.reject);
            return deferred.promise;
        },

        getOrderToBillById: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderToBillById', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderToBillById', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrderMainAccount: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderMainAccount', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderMainAccount', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccountsForSub: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderAccountsForSub', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderAccountsForSub', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccountsForSubitems: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/GetOrderAccountsForSubitems', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/GetOrderAccountsForSubitems', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccountsForPay: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderAccountsForPay', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderAccountsForPay', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccountsForOther: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderAccountsForOther', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderAccountsForOther', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrdersubAccount: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrdersubAccount', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrdersubAccount', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrderOtherAccount: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderOtherAccount', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderOtherAccount', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrderTotalAccount: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderTotalAccount', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderTotalAccount', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        saveBill: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveBill', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveBill', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
      getBillById: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getBillById', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getBillById', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccountsForTotal: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/GetOrderAccountsForTotal', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/GetOrderAccountsForTotal', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        GetOrderAccounts: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/getOrderAccounts', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/getOrderAccounts', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrderGroup: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderGroup', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderGroup', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        saveOrderorderGroupf: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderorderGroupf', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderorderGroupf', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        },
        SaveOrderProperty: function (company) {
            var it = company;
            var deferred = $q.defer();
            if (isJson(company)) {
                it = JSON.parse(company);
                return $http.post('/api/OrderGroup/saveOrderProperty', it)
                 .success(deferred.resolve)
                 .error(deferred.reject);
            } else {



                return $http.post('/api/OrderGroup/saveOrderProperty', it)
                  .success(deferred.resolve)
                  .error(deferred.reject);
            }

            return deferred.promise;
        }
        };
        return svc;
    });

    inventoryModule.factory('BalanceSheetslocalPersistenceStrategy',
        function ($q, localDBService, nullBalanceSheet, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (BalanceSheet) {
                        deferred.resolve(BalanceSheet.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (BalanceSheet) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = BalanceSheet.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, BalanceSheet, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());