
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("inventoryCardController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, AccountOrderspersistenceService, Offline,
                                                           $location, branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];
        $scope.InventoryAccounts = [];
        $scope.sortType = ''; // set the default sort type
        $scope.itemsperpagechange = true;
        $scope.branchGroub = {
            id: 0,
            name: 'حدد الشركة',
            fullName: 'حدد الشركة- الفرع'
        };

        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(3);
        var start = moment().startOf('month');
        var end = moment().endOf('month');
        $scope.nowDate = moment().format('DD/MM/YYYY');
        $scope.startDate = moment().startOf('month').toDate();
        $scope.endDate = moment().endOf('month').toDate();
        function cb(start, end) {
            $('#reportrange span').html('عن الفترة من ' + start.format('DD/MM/YYYY') + ' الي ' + end.format('DD/MM/YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
            $scope.getInventories();
        });

      
        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.itemsperpage = 50;
        
        $scope.sortReverse = false;  // set the default sort order

        $scope.$watch('sortType', function (newVal, oldVal) {
            console.log("Sort TYpe", newVal);
            if (newVal !== oldVal) {
                $scope.getInventories();
            }

        }, true);
        $scope.$watch('sortReverse', function (newVal, oldVal) {
            console.log("sortReverse", newVal);
            if (newVal !== oldVal) {
                $scope.getInventories();
            }

        }, true);

        $scope.$watch('itemsperpage', function (newVal, oldVal) {
            if (newVal !== oldVal && $scope.itemsperpagechange) {
                if (newVal > $scope.totalItems) {
                    $scope.itemsperpage = angular.copy($scope.totalItems);
                    if ($scope.itemsperpage > 200) {
                        $scope.itemsperpage = 200;
                    }
                }
                $scope.getInventories();
                
            }
            $scope.itemsperpagechange = true;

        });

        $scope.pageChanged = function () {
            $scope.getInventories();
        };

        $scope.loadCompanies = function () {
            AccountspersistenceService.action.Getbranchgroup(1).then(
                function (Companies) {
                    $scope.branchesGroub = Companies;

                },
                function (error) {
                    $scope.error = error;
                });
        };

        $scope.selectedInventory = { accountId: 0, accountName: "حدد المخزن" };
        $scope.$watch('branchGroub.id', function (newVal, oldVal) {
            console.log('selected branchhhhhh', newVal + '- old:' + oldVal)
            if (newVal !== oldVal) {
                var debitorSelected = angular.copy($filter('filter')($scope.branchesGroub, { id: $scope.branchGroub.id }, true));
                $scope.branchGroub = debitorSelected.length ? debitorSelected[0] : null;

                $scope.branchtoLink = $scope.branchesGroub.filter(function (el) { return el.companyId == $scope.branchGroub.companyId }, true);
                if (debitorSelected.length) {

                    var id = parseInt(debitorSelected[0].id);
                    bId = parseInt(angular.copy(id));
                    $scope.selectedInventory = { accountId: 0, accountName: "حدد المخزن" }
                    $("input:radio").attr("checked", false);
                    $scope.GetInventoryAccounts();


                }
            }
        });
    
        $scope.GetInventoryAccounts = function () {
            AccountOrderspersistenceService.action.GetinventoryAccounts(JSON.stringify({ companyId: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany })).then(
             function (result) {
                 console.log('OrderGroups', result)
                 $("input:radio").attr("checked", false);
                 $scope.InventoryAccounts = result.data;

             },
             function (error) {
                 toastr.error("حدث خطاء - اثناء طلب البيانات");
             });
        }

        $scope.$watch('selectedInventory.accountId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal && newVal !== 0) {
                $("input:radio").attr("checked", false);
                $scope.reportChange = null;
                var ordername = $filter('filter')($scope.InventoryAccounts, { accountId: newVal }, true);
                $scope.selectedInventory = angular.copy(ordername[0]);
                


            };
        });
       
     
        $scope.reportChange = null;
        $scope.items = [];
        $scope.$watch('reportChange', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            $scope.itemMove = [];
            $scope.items = [];
            $scope.selecteditemfor = "";
            $('#binditem').text('');
            if (newVal !== oldVal && newVal !== null && $scope.selectedInventory !== null && $scope.selectedInventory.accountId !== 0) {

                console.log('Report', newVal)
                //var ordername = $filter('filter')($scope.selectedPay.accounts, { accountId: newVal }, true);
                if ($scope.reportChange !== "allItem")
                {
                    AccountOrderspersistenceService.action.getInventoryitems(JSON.stringify({ companyId: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, reportName: newVal, inventoryId: $scope.selectedInventory.accountId })).then(
                       function (result) {
                           console.log('OrderGroups', result)
                           $scope.items = result.data;

                       },
                       function (error) {
                           toastr.error("حدث خطاء - اثناء طلب البيانات");
                       });

                } else {
                    $scope.getInventories();
                }
              

            };
        });
        $scope.itemMove = [];
        $scope.getInventories = function () {
          
            AccountOrderspersistenceService.action.getItemMoves(JSON.stringify({
                companyId: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany,
                reportName: $scope.reportChange,
                inventoryId: $scope.selectedInventory.accountId,
                subAccountId: $scope.selecteditemforId,
                minDate: $scope.startDate,
                maxDate: $scope.endDate,
                pageSize: $scope.itemsperpage,
                pageNumber: $scope.currentPage,
                orderby: $scope.sortType,
                orderType: $scope.sortReverse
            })).then(
               function (result) {
                   console.log('Moves', result)
                   $scope.itemMove = result.data;
                   $scope.totalItems = result.data.totalCount
                   if ($scope.itemsperpage > $scope.totalItems) {
                       $scope.itemsperpagechange = false;
                      
                       if ($scope.totalItems > 200) {
                           $scope.itemsperpage = 200;
                       } else {
                           $scope.itemsperpage = angular.copy($scope.totalItems);
                       }
                       
                   }
               },
               function (error) {
                   toastr.error("حدث خطاء - اثناء طلب البيانات");
               });
        };
      
        $scope.getItemMoves = function (item) {
            console.log('Item', item);
            $scope.selecteditemfor = item.itemPropertyValueName;
            $scope.selecteditemforId = item.itemPropertyValueId
            console.log('selected 1', $scope.selecteditems);
            $scope.getInventories();
         
        };



      

        //END
    });

}());


