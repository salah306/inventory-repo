(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountOrdersremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountOrders', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountOrders', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetAccountOrders: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountOrders/GetAccountOrdersDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountOrders/GetAccountOrdersDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
        

            getaccountCategoryProperties: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheets/accountCategoryProperties/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/AccountOrders')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/AccountOrders/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            GetinventoryAccounts: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/inventoryCard/GetinventoryAccounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/inventoryCard/GetinventoryAccounts', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getInventoryitems: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/inventoryCard/getInventoryitems', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/inventoryCard/getInventoryitems', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getInventory: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/inventoryTransfer/getInventory', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/inventoryTransfer/getInventory', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getInventoryTo: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/inventoryTransfer/getInventoryTo', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/inventoryTransfer/getInventoryTo', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            saveTransferOrder: function (o) {

                var deferred = $q.defer();
                    return $http.post('/api/inventoryTransfer/saveTransferOrder', o)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                return deferred.promise;
            },
            transferOrderOutforInById: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/transferOrderOutforInById', o)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                return deferred.promise;
            },
            saveTransferOrderin: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/saveTransferOrderin', o)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                return deferred.promise;
            },
            TransferOrderinById: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/TransferOrderinById', o)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                return deferred.promise;
            },
            GetTransferOrderById: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/GetTransferOrderById', o)
                 .success(deferred.resolve)
                 .error(deferred.reject);
                return deferred.promise;
            },
            getwaitinginCount: function () {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/getwaitinginCount')
                 .success(deferred.resolve)
                 .error(deferred.reject);
                return deferred.promise;
            },
            waitingOrderToin: function () {

                var deferred = $q.defer();
                return $http.post('/api/inventoryTransfer/waitingOrderToin')
                 .success(deferred.resolve)
                 .error(deferred.reject);
                return deferred.promise;
            },
            getItemMoves: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/inventoryCard/getItemMoves', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/inventoryCard/getItemMoves', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            orderReqestPur: function () {

                var deferred = $q.defer();
                    return $http.post('/api/OrderReqest/getInventory')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            orderReqestPurItems: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/OrderReqest/getInventoryItems',o)
                 .success(deferred.resolve)
                 .error(deferred.reject);

                return deferred.promise;
            },
            savePurOrderRequest: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/OrderReqest/savePurOrderRequest', o)
                 .success(deferred.resolve)
                 .error(deferred.reject);

                return deferred.promise;
            },
            getOrderRequestById: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/OrderReqest/getOrderRequestById', o)
                 .success(deferred.resolve)
                 .error(deferred.reject);

                return deferred.promise;
            },
            getAllOrderpurRequest: function (o) {

                var deferred = $q.defer();
                return $http.post('/api/OrderReqest/getAllOrderpurRequest', o)
                 .success(deferred.resolve)
                 .error(deferred.reject);

                return deferred.promise;
            },
            getwaitingPurOrderCounts: function () {

                var deferred = $q.defer();
                return $http.post('/api/OrderReqest/getwaitingOrder')
                 .success(deferred.resolve)
                 .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (AccountOrder) {
                    deferred.resolve(AccountOrder.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/AccountOrders/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountOrderslocalPersistenceStrategy',
        function ($q, localDBService, nullAccountOrder, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (AccountOrder) {
                        deferred.resolve(AccountOrder.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (AccountOrder) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = AccountOrder.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, AccountOrder, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, AccountOrder, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, AccountOrder, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());