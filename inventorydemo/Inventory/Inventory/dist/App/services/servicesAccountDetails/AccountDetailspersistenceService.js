(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountDetailspersistenceService',
        function ($q, Offline, AccountDetailsremotePersistenceStrategy, AccountDetailslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountDetailsremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountDetailslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountDetailsremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountDetail = function (name) {
                return AccountDetailsremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountDetail = function (name) {
                return AccountDetailslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountDetail = {},
                        localAccountDetail = {};

                    self.getRemoteAccountDetail(name).then(function (rAccountDetail) {

                        remoteAccountDetail = rAccountDetail;

                        self.getLocalAccountDetail(name).then(function (lAccountDetail) {

                            localAccountDetail = lAccountDetail;

                            if (localAccountDetail.modifiedDate > (new Date(remoteAccountDetail.modifiedDate))) {
                                deferred.resolve(localAccountDetail);
                            }
                            else {
                                deferred.resolve(remoteAccountDetail);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountDetail(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());