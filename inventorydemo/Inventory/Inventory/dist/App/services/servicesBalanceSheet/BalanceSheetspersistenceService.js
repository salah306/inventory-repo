(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('BalanceSheetspersistenceService',
        function ($q, Offline, BalanceSheetsremotePersistenceStrategy, BalanceSheetslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = BalanceSheetsremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = BalanceSheetslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = BalanceSheetsremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteBalanceSheet = function (name) {
                return BalanceSheetsremotePersistenceStrategy.getById(name);
            };

            self.getLocalBalanceSheet = function (name) {
                return BalanceSheetslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteBalanceSheet = {},
                        localBalanceSheet = {};

                    self.getRemoteBalanceSheet(name).then(function (rBalanceSheet) {

                        remoteBalanceSheet = rBalanceSheet;

                        self.getLocalBalanceSheet(name).then(function (lBalanceSheet) {

                            localBalanceSheet = lBalanceSheet;

                            if (localBalanceSheet.modifiedDate > (new Date(remoteBalanceSheet.modifiedDate))) {
                                deferred.resolve(localBalanceSheet);
                            }
                            else {
                                deferred.resolve(remoteBalanceSheet);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalBalanceSheet(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());