(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('BalanceSheetTypespersistenceService',
        function ($q, Offline, BalanceSheetTypesremotePersistenceStrategy, BalanceSheetTypeslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = BalanceSheetTypesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = BalanceSheetTypeslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = BalanceSheetTypesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteBalanceSheetType = function (name) {
                return BalanceSheetTypesremotePersistenceStrategy.getById(name);
            };

            self.getLocalBalanceSheetType = function (name) {
                return BalanceSheetTypeslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteBalanceSheetType = {},
                        localBalanceSheetType = {};

                    self.getRemoteBalanceSheetType(name).then(function (rBalanceSheetType) {

                        remoteBalanceSheetType = rBalanceSheetType;

                        self.getLocalBalanceSheetType(name).then(function (lBalanceSheetType) {

                            localBalanceSheetType = lBalanceSheetType;

                            if (localBalanceSheetType.modifiedDate > (new Date(remoteBalanceSheetType.modifiedDate))) {
                                deferred.resolve(localBalanceSheetType);
                            }
                            else {
                                deferred.resolve(remoteBalanceSheetType);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalBalanceSheetType(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());