
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("configureOrdersController", function ($scope, $timeout, _, AccountspersistenceService, BalanceSheetspersistenceService, $localStorage, $rootScope, settings, $filter,
                                                                 Offline, $location, $sce, EasyStoreUserspersistenceService, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {

        var bId;
        $scope.orderNamesSales =[];
        $scope.orderNamesPur = [];
        $scope.selectedOrderSales = {salesId : 0 , salesName : null};
        $scope.selectedOrderPur = { purId: 0, purName: null };
        $scope.branchesGroub = [];

        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    if (findCompanybr.length) {

                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        if ($scope.branchGroub.id !== '0') {
                            $('.nav-tabs a:first').tab('show');
                            $scope.GetOrderGroupNames();
                        }
                    }
                }

            }
        });

       

        $scope.GetOrderGroupNames = function () {
            BalanceSheetspersistenceService.action.getOrderConfigureName().then(
                  function (result) {
                      $scope.orderNamesSales = result.data.sales;
                      $scope.orderNamesPur = result.data.pur;
                      
                  },
                  function (error) {
                      toastr.error("حدث خطاء - اثناء طلب البيانات");
                  });
        };

        $scope.$watch('selectedOrderSales.salesId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findorder = angular.copy($filter('filter')($scope.orderNamesSales, { id: newVal }, true));
                if (findorder) {
                   
                    if (findorder.length) {
                        $scope.getOrderSalePur($scope.selectedOrderSales.salesId, 'sales')
                    }
                }

            }
        });
        $scope.$watch('selectedOrderPur.purId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findorder = angular.copy($filter('filter')($scope.orderNamesPur, { id: newVal }, true));
                if (findorder) {
                    if (findorder.length) {
                        $scope.getOrderSalePur($scope.selectedOrderPur.purId, 'pur')
                    }
                }

            }
        });

        $scope.getOrderSalePur = function (id, type) {
            if ($scope.branchGroub.id !== '0') {
                BalanceSheetspersistenceService.action.getOrderSalePur(JSON.stringify({ id: id, type: type })).then(
                        function (result) {
                            if (type === "sales") {
                                $scope.selectedOrderSales = result.data;
                            }
                            if (type === "pur") {
                                $scope.selectedOrderPur = result.data;
                            }

                        },
                        function (error) {
                            if (error.data) {
                                toastr.error(error.data.message);
                                if (type === "sales") {
                                    $scope.selectedOrderSales = { salesId: 0, salesName: null };
                                }
                                if (type === "pur") {
                                    $scope.selectedOrderPur = { purId: 0, purName: null };
                                }
                               
                               
                            } else {

                                toastr.error("لا يمكن اتمام العملية الان");
                                if (type === "sales") {
                                    $scope.selectedOrderSales = { salesId: 0, salesName: null };
                                }
                                if (type === "pur") {
                                    $scope.selectedOrderPur = { purId: 0, purName: null };
                                }
                            }
                        });
            }

        }


      
        $scope.addterm = function (type) {
            toastr.clear();
            if (type === "sales") {
                if (!$scope.selectedOrderSales.terms) {
                    $scope.selectedOrderSales.terms = [{ termId: 0, name: "" }];
                    return;
                }
                for (var i = 0; i <  $scope.selectedOrderSales.terms.length; i++) {
                    if (!$scope.selectedOrderSales.terms[i].name) {
                        toastr.warning("توجد حقول لم يتم استخدامها - قم بملائها قبل اضافة شروط واحكام جديدة");
                        return;
                    }
                }
                $scope.selectedOrderSales.terms.push({termId: 0 , name : ""});
            }
            if (type === "pur") {
                if (!$scope.selectedOrderPur.terms) {
                    $scope.selectedOrderPur.terms = [{ termId: 0, name: "" }];
                    return;
                }
                for (var i = 0; i < $scope.selectedOrderPur.terms.length; i++) {
                    if (!$scope.selectedOrderPur.terms[i].name) {
                        toastr.warning("توجد حقول لم يتم استخدامها - قم بملائها قبل اضافة شروط واحكام جديدة");
                        return;
                    }
                }
                $scope.selectedOrderPur.terms.push({termId : 0 , name: "" });
            }
        };

        $scope.removeterm = function (type, index , term) {
            if (type === 'sales') {
                if (term.termId === 0) {
                    $scope.selectedOrderSales.terms.splice(index, 1);
                }
               

            }
            if (type === 'pur') {
                if (term.termId === 0) {
                    $scope.selectedOrderPur.terms.splice(index, 1);
                }
            }
        }

        $scope.removepayMetod = function (type , index, paym) {
            if (type === 'sales') {
                if (paym.payMethodId === 0) {
                    $scope.selectedOrderSales.payMethod.splice(index, 1);
                }
            }
            if (type === 'pur') {
                if (paym.payMethodId === 0) {
                    $scope.selectedOrderPur.payMethod.splice(index, 1);
                }
            }
        };

        $scope.accountCatogery = [];
        $scope.accounts = [];
        $scope.typeforPaymethod = "";
        $scope.typeforTableAccounts = "";
        $scope.getAccountsCatogery = function (type , edited , methed) {
            //getOrderConfigureCatogery
            if (type === 'sales') {
                toastr.clear();
                if ($scope.selectedOrderSales.payMethod) {
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { payName: '' }, true));
                    if (isexist.length && isexist.length > 0) {
                        toastr.warning("حدد اسم طريقة الدفع قبل اختيار طريقة دفع اخري");
                        return;
                    }
                }
                
            }
            if (type === 'pur') {
                toastr.clear();
                if ($scope.selectedOrderSales.payMethod) {
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.payMethod, { payName: '' }, true));
                    if (isexist.length && isexist.length > 0) {
                        toastr.warning("حدد اسم طريقة الدفع قبل اختيار طريقة دفع اخري");
                        return;
                    }
                }
            }
            if ($scope.accountCatogery || $scope.accountCatogery.length == 0) {
                BalanceSheetspersistenceService.action.getOrderConfigureCatogery().then(
                    function (result) {
                        $scope.accountCatogery = result.data;
                        $scope.typeforPaymethod = type;
                        $("#accountCatogery-model").modal('show');
                        $scope.payMethodtoEditTrue = edited;
                        if (edited) {
                            $scope.payMethodToedit = methed;
                           
                        };
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
            else {
                $scope.typeforPaymethod = type;
                $scope.payMethodtoEditTrue = edited;
                if (edited) {
                    $scope.payMethodToedit = methed;
                    
                };
                $("#accountCatogery-model").modal('show');
            

            }
        };

        //getOrderConfigureAccounts
        $scope.getAccounts = function (type, edited, methed) {
            //getOrderConfigureCatogery
       
            if ($scope.accounts || $scope.accounts.length == 0) {
                BalanceSheetspersistenceService.action.getOrderConfigureAccounts().then(
                    function (result) {
                        $scope.accounts = result.data;
                        $scope.typeforTableAccounts = type;
                        
                        $scope.TableaccounttoEditTrue = edited;
                        if (edited) {
                            $scope.TableaccountToedit = methed;
                            
                        };
                        $("#accounts-model").modal('show');
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
            else {
                $scope.typeforTableAccounts = type;

                $scope.TableaccounttoEditTrue = edited;
                if (edited) {
                    $scope.TableaccountToedit = methed;

                };
                $("#accounts-model").modal('show');

               
            }
        };

       
    
        $scope.setAccountTable = function (data) {
            if ($scope.TableaccountToedit && $scope.TableaccounttoEditTrue) {

                if ($scope.typeforTableAccounts === 'sales') {
                    if (!$scope.selectedOrderSales.tableAccounts) {
                        $scope.selectedOrderSales.tableAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.tableAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");

                        return;
                    }
                 
                    $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                    $scope.TableaccountToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
                if ($scope.typeforTableAccounts === 'pur') {
                    if (!$scope.selectedOrderPur.tableAccounts) {
                        $scope.selectedOrderPur.tableAccounts = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.tableAccounts, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }

                    $scope.TableaccountToedit.accountName = data.accountName + " - " + data.code;
                    $scope.TableaccountToedit.accountId = data.accountId;
                    $("#accounts-model").modal('hide');
                    toastr.clear();
                }

                return;
            };

            if ($scope.typeforTableAccounts === 'sales') {
                if (!$scope.selectedOrderSales.tableAccounts) {
                    $scope.selectedOrderSales.tableAccounts = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.tableAccounts, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                    return;
                }
                $scope.selectedOrderSales.tableAccounts.push({ tableAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                toastr.clear();
                $("#accounts-model").modal('hide');
            }
            if ($scope.typeforTableAccounts === 'pur') {
                if (!$scope.selectedOrderPur.tableAccounts) {
                    $scope.selectedOrderPur.tableAccounts = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.tableAccounts, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("غير مسموح باضافة الحساب اكثر من مرة");
                    return;
                }
                $scope.selectedOrderPur.tableAccounts.push({ tableAccountId: 0, nickName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, accountType: 'مدين' });
                toastr.clear();
                $("#accouns-model").modal('hide');

            }
        };

        $scope.setAccountPay = function (data) {
            if ($scope.payMethodToedit && $scope.payMethodtoEditTrue) {

                if ($scope.typeforPaymethod === 'sales') {
                    if (!$scope.selectedOrderSales.payMethod) {
                        $scope.selectedOrderSales.payMethod = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }
                  
                    $scope.payMethodToedit.accountName = data.accountName + " - " + data.code;
                    $scope.payMethodToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
                if ($scope.typeforPaymethod === 'pur') {
                    if (!$scope.selectedOrderPur.payMethod) {
                        $scope.selectedOrderPur.payMethod = [];
                    }
                    var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.payMethod, { accountId: data.accountId }, true));
                    if (isexist.length) {
                        toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                        return;
                    }
                    
                    $scope.payMethodToedit.accountName = data.accountName + " - " + data.code;
                    $scope.payMethodToedit.accountId = data.accountId;
                    $("#accountCatogery-model").modal('hide');
                    toastr.clear();
                }
               
                return;
            };

            if ($scope.typeforPaymethod === 'sales') {
                if (!$scope.selectedOrderSales.payMethod) {
                    $scope.selectedOrderSales.payMethod = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                    return;
                }
                $scope.selectedOrderSales.payMethod.push({ payMethodId: 0, payName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, attchedRequest: true });
                $("#accountCatogery-model").modal('hide');
            }
            if ($scope.typeforPaymethod === 'pur') {
                if (!$scope.selectedOrderSales.payMethod) {
                    $scope.selectedOrderPur.payMethod = [];
                }
                var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.payMethod, { accountId: data.accountId }, true));
                if (isexist.length) {
                    toastr.warning("هذا الحساب محدد لطريقة دفع اخري");
                    return;
                }
                $scope.selectedOrderPur.payMethod.push({ payMethodId: 0, payName: "", accountName: data.accountName + " - " + data.code, accountId: data.accountId, attchedRequest: true });
                $("#accountCatogery-model").modal('hide');

            }
        };

        $scope.payMethodChanged = function (data, type) {
            if ($scope.typeforPaymethod === 'sales') {
                var isexist = angular.copy($filter('filter')($scope.selectedOrderSales.payMethod, { payName: data }, true));
                if (isexist.length && isexist.length > 1) {
                    toastr.warning("طريقة الدفع موجودة بالفعل - قم باختيار اسم اخر");
                    return;
                }
            }

            if ($scope.typeforPaymethod === 'pur') {
                var isexist = angular.copy($filter('filter')($scope.selectedOrderPur.payMethod, { payName: data }, true));
                if (isexist.length && isexist.length > 1) {
                    toastr.warning("طريقة الدفع موجودة بالفعل - قم باختيار اسم اخر");
                    data = "";
                    return;
                }
            }
           
        };




        //End Order Request 
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                if ($scope.branchGroub.id !== '0') {
                    $('.nav-tabs a:first').tab('show');
                    $scope.GetOrderGroupNames();
                }
            }
        }();
    });

}());


