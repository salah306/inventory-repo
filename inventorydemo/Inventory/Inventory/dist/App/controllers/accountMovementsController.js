(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("AccountMovementsController", function ($scope, _, AccountMovementspersistenceService, Offline, $location, $routeParams ) {

        var AccountMovementName = {
            "accountOrderPropertiesName": "",
            "accountName": "",
            "date" : ""
        };

        $scope.name = $routeParams.name;
        $scope.catname = $routeParams.catname;
        console.log(AccountMovementName);
        $scope.showList = false;
        $scope.AccountMovements = [];
        $scope.AccountMovementpropertyValues = [];
        $scope.PropertyValues = [];

        $scope.getAccountMovementProperties = [];

        $scope.AccountMovementProperties = function (i) {
            $scope.getAccountMovementProperties = i;
        };
        $scope.Orders = [{ id: 'Order1' }];



        $scope.addNewOrder = function () {
            var newItemNo = $scope.Orders.length + 1;
            $scope.Orders.push({ 'id': 'Order' + newItemNo });
        };

        $scope.OrdersTotal = function () {
            var total = 0;
            var cho = $scope.Orders;
            for (var i in cho) {
                total += parseFloat(cho[i].name);
            }

          
            return total
        };

        $scope.removeOrder = function () {
            var lastItem = $scope.Orders.length - 1;
            $scope.Orders.splice(lastItem);
        };

        var getAccountMovementsData = function () {

            AccountMovementspersistenceService.action.getById(AccountMovementName).then(
                function (AccountMovements) {
                    $scope.AccountMovements = AccountMovements;


                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountMovements.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

        var lazyGetAccountMovementsData = _.debounce(getAccountMovementsData, 2);

        Offline.on('confirmed-down', lazyGetAccountMovementsData);
        Offline.on('confirmed-up', lazyGetAccountMovementsData);

        lazyGetAccountMovementsData();

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountMovementRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountMovement = {};







        var hasAccountMovementToSave = function () {



            console.log($scope.addAccountMovement)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccountMovement.name)
                    && hasValue($scope.addAccountMovement.price)
                    && hasValue($scope.addAccountMovement.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccountMovement.id === null || $scope.addAccountMovement.id === undefined) {
                $scope.addAccountMovement.id = $scope.addAccountMovement.name;
            }

            var saveAccountMovement = hasAccountMovementToSave();


            var AccountMovement = $scope.addAccountMovement;


            //;

            AccountMovementspersistenceService.action.save(AccountMovement).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountMovement = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountMovementToEdit = [];

        $scope.editingAccountMovement = {};


        $scope.modify = function (AccountMovement) {
            for (var i = 0, length = $scope.AccountMovements.length; i < length; i++) {
                $scope.editingAccountMovement[$scope.AccountMovements[i].id] = false;
            }
            AccountMovementToEdit = angular.copy(AccountMovement);
            $scope.addAccountMovement = AccountMovement;
            $scope.editingAccountMovement[AccountMovement.id] = true;

        };


        $scope.update = function (AccountMovement) {
            $scope.editingAccountMovement[AccountMovement.id] = false;
        };

        $scope.cancel = function (AccountMovement) {

            AccountMovement.name = AccountMovementToEdit.name;
            AccountMovement.price = AccountMovementToEdit.price;
            AccountMovement.catogery = AccountMovementToEdit.catogery;

            $scope.editingAccountMovement[AccountMovement.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountMovements.length; i < length; i++) {
                $scope.editingAccountMovement[$scope.AccountMovements[i].id] = false;
            }
            $scope.addAccountMovement = {};
        };


        //delte
        var delteAccountMovement = {};
        $scope.AccountMovementToDelte = function (AccountMovement) {

            delteAccountMovement = AccountMovement;
        };


        $scope.delete = function () {


            AccountMovementspersistenceService.action.Delete(delteAccountMovement.id).then(
                function (result) {
                    $scope.AccountMovements.splice($scope.AccountMovements.indexOf(delteAccountMovement), 1);
                    delteAccountMovement = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountMovementspersistenceService.getById(AccountMovementName).then(
            function (AccountMovements) {
                $scope.AccountMovements = AccountMovements;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
