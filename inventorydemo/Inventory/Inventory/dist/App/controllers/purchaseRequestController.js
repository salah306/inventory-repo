
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("purchaseRequestController", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];
        $scope.waiting = 0;
        $scope.workinprogress = 0;
        jQuery('input[name="notfydate"]').daterangepicker({
            "singleDatePicker": true,
            "startDate": moment().format('YYYY-MM-DD'),
            "minDate": moment().format('YYYY-MM-DD'),
            "locale": {
                "format": "YYYY-MM-DD"
            }
        });
   
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(3);
        var start = moment().startOf('year');
        var end = moment().endOf('year');
        $scope.nowDate = moment().format('YYYY-MM-DD');
        $scope.startDate = moment().startOf('year').toDate();
        $scope.endDate = moment().endOf('year').toDate();
        function cb(start, end) {
            $('#reportrange span').html('عن الفترة من ' + start.format('YYYY-MM-DD') + ' الي ' + end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'السنة الحالية': [moment().startOf('year'), moment().endOf('year')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
        });

        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                        $scope.order = {
                            "purchaseOrderId": 0,
                            "orderNo": 0,
                            "orderDate":  moment().format('YYYY-MM-DD'),
                            "orderDateSecond": "",
                            "orderDateThird": "",
                            "recivedDate" :  moment().format('YYYY-MM-DD'),
                            "userName": "",
                            "userNameSecond": "",
                            "userNameThird": "",
                            "isDoneFirst": false,
                            "isDoneSecond": false,
                            "isDoneThird": false,
                            "cancel": false,
                            "isNewOrder": true,
                            "transferTo": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "itemsRequest": [
                              {
                                  "itemId": 0,
                                  "itemName": "",
                                  "itemCode": "",
                                  "qty": 0,
                                  "price": 0,
                                  "note": "",
                                  "realted": ""
                              }
                            ],
                            "companyId": 0,
                            "isCompany": true,
                            "cancelDate": "",
                            "userNameCancel": ""
                        };
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getInventory();
                    }
                }

            }
        });


        $scope.order = {
            "purchaseOrderId": 0,
            "orderNo": 0,
            "orderDate":  moment().format('YYYY-MM-DD'),
            "orderDateSecond": "",
            "orderDateThird": "",
            "recivedDate":   moment().format('YYYY-MM-DD'),
            "userName": "",
            "userNameSecond": "",
            "userNameThird": "",
            "isDoneFirst": false,
            "isDoneSecond": false,
            "isDoneThird": false,
            "cancel": false,
            "isNewOrder": true,
            "transferTo": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "itemsRequest": [
              {
                  "itemId": 0,
                  "itemName": "",
                  "itemCode": "",
                  "qty": 0,
                  "price": 0,
                  "note": "",
                  "realted": ""
              }
            ],
            "companyId": 0,
            "isCompany": true,
            "cancelDate": "",
            "userNameCancel": ""
        };
        $scope.items = [];
        $scope.$watch('order.transferTo.inventoryId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.inventory, { inventoryId: newVal }, true));
                $scope.order.transferTo = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };
            
                if (findinventory.length) {
                    $localStorage.invrntorytransfertoPurRequest = $scope.order.transferTo;
                    AccountOrderspersistenceService.action.orderReqestPurItems(JSON.stringify({ inventoryId: $scope.order.transferTo.inventoryId })).then(
                    function (result) {
                        $scope.items = result.data;
                    },
                    function (error) {
                        if (error.data) {
                            if (error.data.message) {
                                toastr.error(error.data.message);
                            }

                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
                }
            }
        });
       
        $scope.getInventory = function () {

            AccountOrderspersistenceService.action.orderReqestPur(JSON.stringify({ companyId: bId, isComapny: $scope.branchGroub.isCompany })).then(
              function (result) {
                  $scope.inventory = result.data.inventory;
                  $scope.waiting = result.data.waitingcount;
                  $scope.workinprogress = result.data.workinprogress;
              },
              function (error) {
                  if (error.data) {
                      if (error.data.message) {
                          toastr.error(error.data.message);
                      }

                  } else {
                      toastr.error("لا يمكن اتمام العملية الان");
                  }

              });
        };


        $scope.getItembyCode = function (data, index) {
            console.log('data', data);
            console.log('index', index);
            for (var i = 0; i < $scope.order.itemsRequest.length; i++) {
                if ($scope.order.itemsRequest.length > 1 && $scope.order.itemsRequest[i].itemCode === data.itemCode) {
                    $scope.order.itemsRequest[index].itemName = null;
                    var id1 = '#qty' + (i + 1);
                    setTimeout(function () { $(id1).focus() }, 200);
                    return;
                }
            };
            $scope.order.itemsRequest[index].itemCode = data.itemCode;
            $scope.order.itemsRequest[index].itemName = data.itemName;
            $scope.order.itemsRequest[index].itemId = data.itemId;
            $scope.order.itemsRequest[index].qty = 1;
            $scope.order.itemsRequest[index].note = null;
            $scope.order.itemsRequest[index].itemUnitType = data.itemUnitType;
            $scope.order.itemsRequest[index].itemUnitName = data.itemUnitName;
            $scope.order.itemsRequest[index].realted = data.realted;

            var id = '#qty' + $scope.order.itemsRequest.length;
            setTimeout(function () { $(id).focus() }, 200);
        }
        $scope.checkqty = function (data) {
            console.log(data);
            console.log('qty', data.qty);
            if (!isNaN(parseFloat(data.qty)) && isFinite(data.qty)) {
                if (data.qty <= 0) {
                    data.qty = 1;
                    toastr.warning("الكمية يجب ان تكون اكبر من صفر");
                    return;
                }
                if (data.itemUnitType === "عدد صحيح") {
                    
                    data.qty = parseInt(data.qty);
                    return;
                }
               
            } else {
                data.qty = 1;
                toastr.warning("وحدة قياس الصنف هي " + data.itemUnitName);
            }
        }

     

        $scope.keys = function (event) {

            console.log("$event", event);
            if (event.altKey) {
                switch (event.which) {
                    case 37:
                        //alert("shift + left arrow");
                        break;
                    case 38:
                        //alert("shift + up arrow");
                        break;
                    case 39:
                        //alert("shift + right arrow");
                        break;
                    case 78:
                        //$scope.addNewitemPropertyValue();
                        break;
                    default:
                        break;
                }
            }
        }


        $scope.addNewTransferitem = function () {

            for (var i = 0; i < $scope.order.itemsRequest.length; i++) {
                if ($scope.order.itemsRequest[i].qty <= 0 || !$scope.order.itemsRequest[i].itemCode || !$scope.order.itemsRequest[i].itemName) {
                    return;
                }
            }

            $scope.order.itemsRequest.push({ itemId: 0, itemCode: null, itemName: null, qty: 0.0, maxQ: 0.0, realted: null });
            var id = '#ui' + $scope.order.itemsRequest.length;
            setTimeout(function () { $(id).focus() }, 200);


        }
        $scope.removeTransfer = function (item) {
            console.log("Item : ", angular.toJson(item));
            if ($scope.order.itemsRequest && $scope.order.itemsRequest.length > 1) {
                $scope.order.itemsRequest.splice(item, 1);
            }

        };

        //order.orderNo

        $scope.newOrder = function () {
 
            var neworder = {
                "purchaseOrderId": 0,
                "orderNo": 0,
                "orderDate": moment().format('YYYY-MM-DD'),
                "orderDateSecond": "",
                "orderDateThird": "",
                "recivedDate": moment().format('YYYY-MM-DD'),
                "userName": "",
                "userNameSecond": "",
                "userNameThird": "",
                "isDoneFirst": false,
                "isDoneSecond": false,
                "isDoneThird": false,
                "cancel": false,
                "isNewOrder": true,
                "transferTo":angular.copy($scope.order.transferTo),
                "itemsRequest": [
                  {
                      "itemId": 0,
                      "itemName": "",
                      "itemCode": "",
                      "qty": 0,
                      "price": 0,
                      "note": "",
                      "realted": ""
                  }
                ],
                "companyId": 0,
                "isCompany": true,
                "cancelDate": "",
                "userNameCancel": ""
            };
            $scope.order = neworder;
        };
        $scope.findOrderById = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getOrderRequestById(angular.toJson({ orderNo :parseInt(data) })).then(
                    function (result) {
                        $scope.order = result.data;
                        $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                        $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                        $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                        $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                    },
                    function (error) {
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        };
        $scope.findOrderByIdModal = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getOrderRequestById(angular.toJson({ orderNo: parseInt(data) })).then(
                    function (result) {
                        $scope.order = result.data;
                        $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                        $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                        $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                        $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                        $("#waiting-model").modal("hide");
                    },
                    function (error) {
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        };

        $scope.saveOrder = function () {

            var orderToSave = angular.copy($scope.order);


            if ($scope.order.isNewOrder) {
                console.log(orderToSave.recivedDate);
                orderToSave.orderDate = moment.utc();
            }
            console.log(angular.toJson(orderToSave));
            AccountOrderspersistenceService.action.savePurOrderRequest(angular.toJson(orderToSave)).then(
                function (result) {
                    $scope.order = result.data;
                    $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                    $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                    $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                    $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });


        };

        $scope.cancelOrder = function () {


            var orderToSave = angular.copy($scope.order);
            orderToSave.orderDate = moment.utc();
            orderToSave.cancel = true;
            orderToSave.isNewOrder = false;
            AccountOrderspersistenceService.action.savePurOrderRequest(angular.toJson(orderToSave)).then(
                function (result) {

                    $scope.order.userNameCancel = $rootScope.mainUserInfo.userName;
                    $scope.order.cancelDate = moment().format('YYYY-MM-DD');
                    $scope.order.cancel = true;
                    $scope.isNewOrder = false;
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    };

                });
        };

     
        $scope.getWaitingCounts = function () {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getwaitingPurOrderCounts().then(
                    function (result) {
                        $scope.waiting = result.data.waitingcount;
                        $scope.workinprogress = result.data.workinprogress;
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        }

        $scope.currentPage = 1;
        $scope.ordertoQuery = { orderNo: 0, isDoneSecond: false, cancel: false, all: true, isDoneFirst : false, minDate: '', maxDate: '', pageSize: 15, pageNumber: 0 }
        $scope.ordertoQueryallChenge = function () {
            if ($scope.ordertoQuery.all) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.cancel = false;
            }
        

        };
        $scope.ordertoQueryallChengeFirst = function () {
            if ($scope.ordertoQuery.isDoneFirst) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }


        };

        $scope.ordertoQueryallChengeSecond = function () {
            if ($scope.ordertoQuery.isDoneSecond) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }

        };
        $scope.resetQuery = function () {
            $scope.ordertoQuery.isDoneFirst = false;
            $scope.ordertoQuery.all = true;
            $scope.ordertoQuery.cancel = false;
            $scope.ordertoQuery.isDoneSecond = false;
        };

        $scope.ordertoQueryallChengecancel = function () {
            if ($scope.ordertoQuery.cancel) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.isDoneSecond = false;
            }


        };

        $scope.totalItems = 0;
        $scope.waitingorder = [];
        $scope.getwaitingDonefirst = function () {
            $scope.ordertoQuery.isDoneFirst = true;
            $scope.ordertoQueryallChengeFirst();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                    function (result) {
                        $scope.waitingorder = result.data;
                        $("#waiting-model").modal("show");
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                        $scope.resetQuery();

                    });
        }

        $scope.getwaitingDonesecond= function () {
            $scope.ordertoQuery.isDoneSecond = true;
            $scope.ordertoQueryallChengeSecond();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                    function (result) {
                        $scope.waitingorder = result.data;
                        $("#waiting-model").modal("show");
                    },
                    function (error) {
                        toastr.clear();
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                        $scope.resetQuery();
                    });
        }

        $scope.getQueryRequest = function () {
    
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
            }
            
        
            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                    function (result) {
                        $scope.waitingorder = result.data;
                        $("#waiting-model").modal("show");
                    },
                    function (error) {
                        toastr.clear()
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                        $scope.resetQuery();
                    });
        }

        $scope.vaildOrder = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                if (!$scope.order.transferTo || $scope.order.transferTo.inventoryId == 0 || !$scope.order.transferTo.inventoryName) {
                    console.log("$scope.order.transferTo")
                    return false;
                }
                if ($scope.order.itemsRequest.length < 1) {
                    console.log("itemsRequest.length")
                    return false;
                }
                for (var i = 0; i < $scope.order.itemsRequest.length; i++) {

                    if (!$scope.order.itemsRequest[i].itemCode || !$scope.order.itemsRequest[i].itemName) {
                        console.log("itemsRequest[i].itemCode ")
                        return false;
                    }
                    
                    if (!isNaN(parseFloat($scope.order.itemsRequest[i].qty)) && isFinite($scope.order.itemsRequest[i].qty)) {
                        if ($scope.order.itemsRequest[i].qty <= 0) {
                            console.log("itemsRequest[i].qty <= 0")
                            return false;
                        }
                       

                    } else {
                        console.log("itemsRequest[i].qty NAN")
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        };
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.order.isCompany = $scope.branchGroub.isCompany
                $scope.getInventory();
            }
        }();

    });

}());


