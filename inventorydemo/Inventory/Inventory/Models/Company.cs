﻿using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Company
    {
        public Company()
        {
            //this.ApplicationUser = new HashSet<ApplicationUser>();
        }


 
        public Int32 CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        //InventoryValuatioMethods
        public Int32? InventoryValuationMethodId { get; set; }
        public virtual InventoryValuationMethod InventoryValuatioMethod { get; set; }
        public virtual List<Branch> Branchs { get; set; }
        
        public virtual List<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public int? MaxBranch { get; set; }
        public virtual List<ItemGroup> ItemGroups { get; set; }
        public virtual List<Unit> Units { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public bool closed { get; set; }
        public virtual List<CompanyProperties> CompanyProperties { get; set; }
    }

    public class CompanyProperties
    {
        public Int32 CompanyPropertiesId { get; set; }
        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public string propertyName { get; set; }
        public Int32 AccountsCatogeryTypesAcccountsId { get; set; }
        public AccountsCatogeryTypesAcccounts AccountsCatogeryTypesAcccounts { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual List<CompanyPropertyValues> CompanyPropertyValues { get; set; }
    }
    public class CompanyPropertyValues
    {
        public Int32 CompanyPropertyValuesId { get; set; }
        public Int32 CompanyPropertiesId { get; set; }
        public virtual CompanyProperties CompanyProperties { get; set; }
        public string propertyValue{ get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class InventoryValuationMethod
    {
        public Int32 InventoryValuationMethodId { get; set; }
        public string InventoryValuationMethodName { get; set; }
        public virtual List<Company> Companies { get; set; }
    }

    public class ApplicationUserComapny
    {
        public Int32 ApplicationUserComapnyId { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public virtual Branch Branch { get; set; }
        public Int32? BalanceSheetId { get; set; }
        [ForeignKey("BalanceSheetId")]
        public virtual BalanceSheet BalanceSheet { get; set; }
        public Int32? FinancialListId { get; set; }
        [ForeignKey("FinancialListId")]
        public virtual FinancialList FinancialList { get; set; }
        public Int32? ItemGroupId { get; set; }
        [ForeignKey("ItemGroupId")]
        public virtual ItemGroup ItemGroup { get; set; }

        public bool? UserAcceptedPolicy { get; set; }
        public bool? Locked { get; set; }
        public virtual List<settingPropertiesforUser> properties { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
    }

    public class settingProperties
    {
        public Int32 settingPropertiesId { get; set; }
        public string propertyName { get; set; }
        public string name { get; set; }
        public virtual List<settingRole> settingRole { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class settingRole
    {
        //يستطيع التعديل يستطيع الاطلاع يستطيع الاضافة
        public Int32 settingRoleId { get; set; }
        public string roleName { get; set; }
        public string name { get; set; }
        public Int32 settingPropertiesId { get; set; }
        public virtual settingProperties settingProperties { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class settingPropertiesforUser
    {
        public Int32 settingPropertiesforUserId { get; set; }
        public Int32 settingPropertiesId { get; set; }
        public virtual settingProperties settingProperties { get; set; }
       
        public Int32 ApplicationUserComapnyId { get; set; }
        public virtual ApplicationUserComapny ApplicationUserComapny { get; set; }
        public virtual List<settingRolesForUser> roles { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class settingRolesForUser
    {
        public Int32 settingRolesForUserId { get; set; }
        public Int32 settingPropertiesforUserId { get; set; }
        public virtual settingPropertiesforUser settingPropertiesforUser { get; set; }
        //فرع واحد اشتراك , دفاتر اليومية
        public string roleName { get; set; }
        public bool active { get; set; }
        public string RefrenceType { get; set; }
        public Int32? RefrenceId { get; set; }
        public Int32 settingRoleId { get; set; }
        public virtual settingRole settingRole { get; set; }
        public virtual List<RolepropertiesforUser> RolepropertiesforUser { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

    }

    public class RolepropertiesforUser
    {
        //يستطيع التعديل 
        public Int32 RolepropertiesforUserId { get; set; }
        public Int32 settingRoleId { get; set; }
        public virtual settingRole settingRole { get; set; }
        public bool isTrue { get; set; }
        public int typeRefrenceId { get; set; }
        public string typeRefrence { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

    public class RoleCollection
    {
        public Int32 RoleCollectionId { get; set; }
        public string RoleCollectionName { get; set; }
        public Int32 CompanyRoleId { get; set; }
        public virtual CompanyRole CompanyRole { get; set; }
        public Int32 ApplicationUserComapnyId { get; set; }
        public ApplicationUserComapny ApplicationUserComapny{ get; set; }

    }

    public class CompanyRole
    {
        public Int32 CompanyRoleId { get; set; }

        public string  Name { get; set; }
        public virtual  IList<RoleCollection> RoleCollections { get; set; }

        public virtual ICollection<Vocabulary> Vocabularies { get; set; }

       

    }


    public class Vocabulary
    {
        public Int32 VocabularyId { get; set; }
        public string Lang { get; set; }
        public string Name { get; set; }
        public Int32 CompanyRoleId { get; set; }
        public virtual CompanyRole CompanyRole { get; set; }

      
    }

    public class AppUserCompanyRole
    {
        public Int32 Id { get; set; }
        public Int32 ApplicationUserComapnyId { get; set; }
        public virtual ApplicationUserComapny ApplicationUserComapny { get; set; }

        public Int32 CompanyRoleId { get; set; }
        public virtual CompanyRole CompanyRole { get; set; }
        

    }

    public class UserCompanyRoleModel
    {
        public Int32 CatogeryId { get; set; }
     
        public string Catogery { get; set; }
    }


}