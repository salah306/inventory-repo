﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Account
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public  string AccountAliasName { get; set; }
        public string Code { get; set; }
        //public decimal? TotalDebit
        //{
        //    get
        //    {
        //        var sumdebit = this.AccountsMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum();
        //        return sumdebit;
        //    }

        //}
        //public decimal? TotalCrdit
        //{
        //    get
        //    {
        //        var sumCrdit = this.AccountsMovements.Select(a => a.Crdit).DefaultIfEmpty(0).Sum();
        //        return sumCrdit;
        //    }
        //}
        public Int32 AccountCategoryId { get; set; }
        public virtual AccountCategory AccountCategory { get; set; }
       

        public virtual List<AccountMovement> AccountsMovements { get; set; }
        public Int32? CustomAccountCategoryId { get; set; }
        //public virtual CustomAccountCategory CustomAccountCategory { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        public Int32? BranchId { get; set; }
        public Branch Branch { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public virtual List<SubAccount> SubAccounts { get; set; }
        public bool Activated { get; set; }
       
        public virtual List<AccountsforItemGroup> AccountsforItemGroups { get; set; }
        public virtual List<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }


    }

    public class SubAccount
    {
        public Int32 SubAccountId { get; set; }
        public string SubAccountName { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public Int32 AccountId { get; set; }
        public virtual Account Account{ get; set; }
      
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }

        public virtual List<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }
        public bool Activated { get; set; }
       // public virtual List<subAccountNpropertyValues> subAccountNpropertyValues { get; set; }
        public string Code { get; set; }
        public virtual List<SubAccountMovement> SubAccountMovements { get; set; }
       

        public Int32? ItemInfoId { get; set; }
        [ForeignKey("ItemInfoId")]
        public virtual TempItem ItemInfo { get; set; }
        //public decimal GetLastQtyIn()
        //{
        //    bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;

        //    return notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemPricein : 0;
        //}
        //public decimal GetLastQtyOut()
        //{
        //    bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;


        //    return notNULL? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemPriceOut : 0;
        //}
        //public decimal GetStockQty()
        //{

        //    bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;


        //    return notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemStockQuantity : 0;

        //}

        //public decimal GetStockQtyprop
        //{
        //    get
        //    {
        //        bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;

        //        return notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemStockQuantity : 0;

        //    }
        //}

        //public decimal GetPreviousStockQty()
        //{
        //    bool notNULL = this.SubAccountMovements != null &&  this.SubAccountMovements.Count() > 0 ;
        //    var lastmove = notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())) : null;

        //    return notNULL ? this.GetStockQty() - (lastmove.QuantityIn - lastmove.QuantityOut) : 0;
        //}

        //public decimal GetStocKBalancePrice()
        //{
        //    bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;
        //    var lastmove =notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())) : null;

        //    return notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemTotalStockPrice : 0;
        //}
        //public decimal GetPreviousStocBalancePrice()
        //{
        //    bool notNULL = this.SubAccountMovements != null && this.SubAccountMovements.Count() > 0;
        //    var lastmove =notNULL ? this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())) : null;
        //    return notNULL ? GetStocKBalancePrice() - ((lastmove.ItemPricein * lastmove.QuantityIn) - (lastmove.ItemPriceOut * lastmove.QuantityOut)) : 0;
        //}
        //public decimal GetUnitCostPrice()
        //{
        //    return this.GetStockQty() != 0 ? this.GetStocKBalancePrice() / this.GetStockQty() : 0;
        //}

        //public decimal GetPreviousUnitCostPrice()
        //{
        //    return this.GetPreviousStockQty() != 0 ? this.GetPreviousStocBalancePrice() / this.GetPreviousStockQty() : 0;
        //}

    }


}