﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class AppStorge
    {
        public branchGroupStVm branchGroup { get; set; }
        public billMainAccountSalesStVm billMainAccountSales { get; set; }
        public billMainAccountPurStVm billMainAccount { get; set; }
        public selectedOrderGroupNameStVm selectedOrderGroupName { get; set; }
        public inventoryStVm inventorySt { get; set; }

    }

    public class branchGroupSt
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int branchGroupStrId { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string companyName { get; set; }
        public string fullName { get; set; }
        public bool isCompany { get; set; }
        public int companyId { get; set; }
        public string realName { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public Int32 oId { get; set; }
    }

    public class branchGroupStVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string companyName { get; set; }
        public string fullName { get; set; }
        public bool isCompany { get; set; }
        public int companyId { get; set; }
        public string realName { get; set; }
        public Int32 oId { get; set; }
    }

    public class billMainAccountSalesSt
    {
        public int billMainAccountSalesStId { get; set; }
        public int accountId { get; set; }
        public string accountName { get; set; }
        public int value { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
    }

    public class billMainAccountSalesStVm
    {
        public int accountId { get; set; }
        public string accountName { get; set; }
        public int value { get; set; }
      
    }
    public class billMainAccountPurSt
    {
        public int billMainAccountPurStId { get; set; }
        public int accountId { get; set; }
        public string accountName { get; set; }
        public int value { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
    }

    public class billMainAccountPurStVm
    {
        public int accountId { get; set; }
        public string accountName { get; set; }
        public int value { get; set; }
      
    }

    public class selectedOrderGroupNameSt
    {
        public int selectedOrderGroupNameStId { get; set; }
        public int orderGroupId { get; set; }
        public string orderGroupName { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
    }

    public class selectedOrderGroupNameStVm
    {
        public int orderGroupId { get; set; }
        public string orderGroupName { get; set; }

    }

    public class inventorySt
    {
        public int inventoryStId { get; set; }
        public int accountId { get; set; }
        public string accountName { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
    }

    public class inventoryStVm
    {
        public int accountId { get; set; }
        public string accountName { get; set; }
    
    }


}