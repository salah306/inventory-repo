﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class UserTimelineComapny
    {
        public Int32 UserTimelineId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public string note { get; set; }
        public DateTime noteDate { get; set; }
        public string typeRefrence { get; set; }
        public Int32? typeRefrenceId { get; set; }
        public Int32  companyId { get; set; }
    }
}