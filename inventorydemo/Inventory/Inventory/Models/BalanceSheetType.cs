﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class BalanceSheetType
    {
        public Int32 BalanceSheetTypeId { get; set; }
        public string BalanceSheetTypeName { get; set; }
        public string Code { get; set; }

        public Int32 BalanceSheetId { get; set; }
        public virtual BalanceSheet BalanceSheet { get; set; }
        public virtual IList<AccountCategory> AccountCategories { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        public Int32? AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
    }
}