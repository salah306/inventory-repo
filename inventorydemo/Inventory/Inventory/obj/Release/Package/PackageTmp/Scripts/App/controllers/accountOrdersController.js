﻿(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountOrdersController", function ($scope, _, AccountOrderspersistenceService, Offline, $location, $routeParams ) {

        var AccountOrderName = $routeParams.name;
        $scope.name = $routeParams.name;
        $scope.catname = $routeParams.catname;
        console.log(AccountOrderName);
        $scope.showList = false;
        $scope.AccountOrders = [];
        $scope.AccountOrderpropertyValues = [];
        $scope.PropertyValues = [];

        $scope.getAccountOrderProperties = [];

        $scope.accountOrderProperties = function (i) {
            $scope.getAccountOrderProperties = i;
        };
        $scope.Orders = [{ id: 'Order1' }];



        $scope.addNewOrder = function () {
            var newItemNo = $scope.Orders.length + 1;
            $scope.Orders.push({ 'id': 'Order' + newItemNo });
        };

        $scope.OrdersTotal = function () {
            var total = 0;
            var cho = $scope.Orders;
            for (var i in cho) {
                total += parseFloat(cho[i].name);
            }

          
            return total
        };

        $scope.removeOrder = function () {
            var lastItem = $scope.Orders.length - 1;
            $scope.Orders.splice(lastItem);
        };

        var getData = function () {

            AccountOrderspersistenceService.action.getById(AccountOrderName).then(
                function (AccountOrders) {
                    $scope.AccountOrders = AccountOrders;


                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountOrders.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountOrderRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountOrder = {};







        var hasAccountOrderToSave = function () {



            console.log($scope.addAccountOrder)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccountOrder.name)
                    && hasValue($scope.addAccountOrder.price)
                    && hasValue($scope.addAccountOrder.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccountOrder.id === null || $scope.addAccountOrder.id === undefined) {
                $scope.addAccountOrder.id = $scope.addAccountOrder.name;
            }

            var saveAccountOrder = hasAccountOrderToSave();


            var AccountOrder = $scope.addAccountOrder;


            //;

            AccountOrderspersistenceService.action.save(AccountOrder).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountOrder = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountOrderToEdit = [];

        $scope.editingAccountOrder = {};


        $scope.modify = function (AccountOrder) {
            for (var i = 0, length = $scope.AccountOrders.length; i < length; i++) {
                $scope.editingAccountOrder[$scope.AccountOrders[i].id] = false;
            }
            AccountOrderToEdit = angular.copy(AccountOrder);
            $scope.addAccountOrder = AccountOrder;
            $scope.editingAccountOrder[AccountOrder.id] = true;

        };


        $scope.update = function (AccountOrder) {
            $scope.editingAccountOrder[AccountOrder.id] = false;
        };

        $scope.cancel = function (AccountOrder) {

            AccountOrder.name = AccountOrderToEdit.name;
            AccountOrder.price = AccountOrderToEdit.price;
            AccountOrder.catogery = AccountOrderToEdit.catogery;

            $scope.editingAccountOrder[AccountOrder.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountOrders.length; i < length; i++) {
                $scope.editingAccountOrder[$scope.AccountOrders[i].id] = false;
            }
            $scope.addAccountOrder = {};
        };


        //delte
        var delteAccountOrder = {};
        $scope.AccountOrderToDelte = function (AccountOrder) {

            delteAccountOrder = AccountOrder;
        };


        $scope.delete = function () {


            AccountOrderspersistenceService.action.Delete(delteAccountOrder.id).then(
                function (result) {
                    $scope.AccountOrders.splice($scope.AccountOrders.indexOf(delteAccountOrder), 1);
                    delteAccountOrder = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountOrderspersistenceService.getById(AccountOrderName).then(
            function (AccountOrders) {
                $scope.AccountOrders = AccountOrders;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
