namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purrequest3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchaseOrders", "RecivedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "RecivedDate");
        }
    }
}
