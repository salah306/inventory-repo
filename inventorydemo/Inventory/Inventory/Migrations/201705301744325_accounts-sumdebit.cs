namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountssumdebit : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Accounts", "TotalDebit");
            DropColumn("dbo.Accounts", "TotalCrdit");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accounts", "TotalCrdit", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Accounts", "TotalDebit", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
