namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertiesforItemGroups", "Show", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertiesforItemGroups", "Show");
        }
    }
}
