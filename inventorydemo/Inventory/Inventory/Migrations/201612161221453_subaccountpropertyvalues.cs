namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subaccountpropertyvalues : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PropertyValuesforItemGroups", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "SubAccountId" });
            CreateTable(
                "dbo.subAccountNpropertyValues",
                c => new
                    {
                        subAccountNpropertyValuesId = c.Int(nullable: false, identity: true),
                        PropertyValuesforItemGroupId = c.Int(nullable: false),
                        SubAccountId = c.Int(),
                    })
                .PrimaryKey(t => t.subAccountNpropertyValuesId)
                .ForeignKey("dbo.PropertyValuesforItemGroups", t => t.PropertyValuesforItemGroupId, cascadeDelete: true)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId)
                .Index(t => t.PropertyValuesforItemGroupId)
                .Index(t => t.SubAccountId);
            
            DropColumn("dbo.PropertyValuesforItemGroups", "SubAccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "SubAccountId", c => c.Int());
            DropForeignKey("dbo.subAccountNpropertyValues", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.subAccountNpropertyValues", "PropertyValuesforItemGroupId", "dbo.PropertyValuesforItemGroups");
            DropIndex("dbo.subAccountNpropertyValues", new[] { "SubAccountId" });
            DropIndex("dbo.subAccountNpropertyValues", new[] { "PropertyValuesforItemGroupId" });
            DropTable("dbo.subAccountNpropertyValues");
            CreateIndex("dbo.PropertyValuesforItemGroups", "SubAccountId");
            AddForeignKey("dbo.PropertyValuesforItemGroups", "SubAccountId", "dbo.SubAccounts", "SubAccountId");
        }
    }
}
