namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tempItems6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TempItems", "reorderPoint", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TempItems", "reorderPoint");
        }
    }
}
