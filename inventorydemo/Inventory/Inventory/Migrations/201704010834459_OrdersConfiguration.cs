namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersConfiguration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PurchaseOrdersConfigurations",
                c => new
                    {
                        PurchaseOrdersConfigurationId = c.Int(nullable: false, identity: true),
                        PurchaseOrderName = c.String(nullable: false),
                        PurchaseName = c.String(nullable: false),
                        PurchaseReturnName = c.String(nullable: false),
                        IsActiveOrder = c.Boolean(nullable: false),
                        OnlyInventory = c.Boolean(nullable: false),
                        DirectGlTransaction = c.Boolean(nullable: false),
                        TitleAccountNickName = c.String(),
                        TitleAccountCategoryId = c.Int(nullable: false),
                        TitleBalanceSheetTypeId = c.Int(),
                        DefaultPayAccountCategoryId = c.Int(),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PurchaseOrdersConfigurationId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AccountCategories", t => t.DefaultPayAccountCategoryId)
                .ForeignKey("dbo.AccountCategories", t => t.TitleAccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.TitleBalanceSheetTypeId)
                .Index(t => t.TitleAccountCategoryId)
                .Index(t => t.TitleBalanceSheetTypeId)
                .Index(t => t.DefaultPayAccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationPayMethods",
                c => new
                    {
                        PurchasesOrdersConfigurationPayMethodId = c.Int(nullable: false, identity: true),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        PayName = c.String(),
                        AccountId = c.Int(nullable: false),
                        DefaultChildAccountId = c.Int(),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationPayMethodId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.DefaultChildAccountId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.AccountId)
                .Index(t => t.DefaultChildAccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTableAccounts",
                c => new
                    {
                        PurchasesOrdersConfigurationTableAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        accountType = c.String(nullable: false),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        CurrentWorkerTableAccountId = c.String(maxLength: 128),
                        itemId = c.Int(),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        itemcostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        realted = c.String(),
                        ItemGroupId = c.Int(),
                        isInventory = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        serialNo = c.String(),
                        expireDate = c.DateTime(),
                        attchedRequest = c.Boolean(nullable: false),
                        isPercent = c.Boolean(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        firstItemNote = c.String(),
                        SecondItemNote = c.String(),
                        thirdItemNote = c.String(),
                        firstItemNoteId = c.Int(),
                        SecondItemNoteId = c.Int(),
                        thirdItemNoteId = c.Int(),
                        firstItemAccountNoteId = c.Int(),
                        secondItemAccountNoteId = c.Int(),
                        thirdItemAccountNoteId = c.Int(),
                        firstItemNoteDate = c.DateTime(),
                        secondItemNoteDate = c.DateTime(),
                        thirdItemNoteDate = c.DateTime(),
                        firstReferenceId = c.Int(),
                        firstReferenceType = c.Int(),
                        secondReferenceId = c.Int(),
                        secondReferenceType = c.Int(),
                        thirdReferenceId = c.Int(),
                        thirdReferenceType = c.Int(),
                        cancelDate = c.DateTime(),
                        doneFirst = c.Boolean(nullable: false),
                        doneSceond = c.Boolean(nullable: false),
                        doneThird = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        CurrentWorkerCancelId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.firstItemAccountNoteId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.secondItemAccountNoteId)
                .ForeignKey("dbo.Accounts", t => t.thirdItemAccountNoteId)
                .Index(t => t.AccountId)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerTableAccountId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.firstItemAccountNoteId)
                .Index(t => t.secondItemAccountNoteId)
                .Index(t => t.thirdItemAccountNoteId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.CurrentWorkerCancelId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTerms",
                c => new
                    {
                        PurchasesOrdersConfigurationTermId = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTermId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationTotalAccounts",
                c => new
                    {
                        PurchasesOrdersConfigurationTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        PurchaseOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationTotalAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.PurchaseOrdersConfigurations", t => t.PurchaseOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.PurchaseOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.PurchasesOrdersConfigurationAccountTypes",
                c => new
                    {
                        PurchasesOrdersConfigurationAccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(),
                    })
                .PrimaryKey(t => t.PurchasesOrdersConfigurationAccountTypeId);
            
            CreateTable(
                "dbo.SalesOrdersConfigurations",
                c => new
                    {
                        SalesOrdersConfigurationId = c.Int(nullable: false, identity: true),
                        SalesOrderName = c.String(nullable: false),
                        SalesName = c.String(nullable: false),
                        SalesReturnName = c.String(nullable: false),
                        IsActiveOrder = c.Boolean(nullable: false),
                        OnlyInventory = c.Boolean(nullable: false),
                        DirectGlTransaction = c.Boolean(nullable: false),
                        TitleAccountNickName = c.String(),
                        TitleAccountCategoryId = c.Int(nullable: false),
                        TitleBalanceSheetTypeId = c.Int(),
                        DefaultPayAccountCategoryId = c.Int(),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.SalesOrdersConfigurationId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AccountCategories", t => t.DefaultPayAccountCategoryId)
                .ForeignKey("dbo.AccountCategories", t => t.TitleAccountCategoryId, cascadeDelete: false)
                .ForeignKey("dbo.BalanceSheetTypes", t => t.TitleBalanceSheetTypeId)
                .Index(t => t.TitleAccountCategoryId)
                .Index(t => t.TitleBalanceSheetTypeId)
                .Index(t => t.DefaultPayAccountCategoryId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationPayMethods",
                c => new
                    {
                        SalessOrdersConfigurationPayMethodId = c.Int(nullable: false, identity: true),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        PayName = c.String(),
                        AccountId = c.Int(nullable: false),
                        DefaultChildAccountId = c.Int(),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationPayMethodId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.DefaultChildAccountId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.AccountId)
                .Index(t => t.DefaultChildAccountId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTableAccounts",
                c => new
                    {
                        SalessOrdersConfigurationTableAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        accountType = c.String(nullable: false),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        CurrentWorkerTableAccountId = c.String(maxLength: 128),
                        itemId = c.Int(),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        itemcostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        realted = c.String(),
                        ItemGroupId = c.Int(),
                        isInventory = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        serialNo = c.String(),
                        expireDate = c.DateTime(),
                        attchedRequest = c.Boolean(nullable: false),
                        isPercent = c.Boolean(nullable: false),
                        IsAccepted = c.Boolean(nullable: false),
                        IsCanceled = c.Boolean(nullable: false),
                        firstItemNote = c.String(),
                        SecondItemNote = c.String(),
                        thirdItemNote = c.String(),
                        firstItemNoteId = c.Int(),
                        SecondItemNoteId = c.Int(),
                        thirdItemNoteId = c.Int(),
                        firstItemAccountNoteId = c.Int(),
                        secondItemAccountNoteId = c.Int(),
                        thirdItemAccountNoteId = c.Int(),
                        firstItemNoteDate = c.DateTime(),
                        secondItemNoteDate = c.DateTime(),
                        thirdItemNoteDate = c.DateTime(),
                        firstReferenceId = c.Int(),
                        firstReferenceType = c.Int(),
                        secondReferenceId = c.Int(),
                        secondReferenceType = c.Int(),
                        thirdReferenceId = c.Int(),
                        thirdReferenceType = c.Int(),
                        cancelDate = c.DateTime(),
                        doneFirst = c.Boolean(nullable: false),
                        doneSceond = c.Boolean(nullable: false),
                        doneThird = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        CurrentWorkerCancelId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerTableAccountId)
                .ForeignKey("dbo.Accounts", t => t.firstItemAccountNoteId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.secondItemAccountNoteId)
                .ForeignKey("dbo.Accounts", t => t.thirdItemAccountNoteId)
                .Index(t => t.AccountId)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerTableAccountId)
                .Index(t => t.ItemGroupId)
                .Index(t => t.firstItemAccountNoteId)
                .Index(t => t.secondItemAccountNoteId)
                .Index(t => t.thirdItemAccountNoteId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.CurrentWorkerCancelId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTerms",
                c => new
                    {
                        SalessOrdersConfigurationTermId = c.Int(nullable: false, identity: true),
                        Note = c.String(),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTermId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationTotalAccounts",
                c => new
                    {
                        SalessOrdersConfigurationTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        AccountNickName = c.String(),
                        AccountTypeId = c.Int(nullable: false),
                        SalesOrdersConfigurationId = c.Int(nullable: false),
                        RowVersion = c.Binary(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationTotalAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.SalessOrdersConfigurationAccountTypes", t => t.AccountTypeId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SalesOrdersConfigurations", t => t.SalesOrdersConfigurationId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.AccountTypeId)
                .Index(t => t.SalesOrdersConfigurationId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SalessOrdersConfigurationAccountTypes",
                c => new
                    {
                        SalessOrdersConfigurationAccountTypeId = c.Int(nullable: false, identity: true),
                        AccountTypeName = c.String(),
                    })
                .PrimaryKey(t => t.SalessOrdersConfigurationAccountTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "AccountTypeId", "dbo.SalessOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.SalessOrdersConfigurationTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesOrdersConfigurations", "TitleBalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.SalesOrdersConfigurations", "TitleAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.SalessOrdersConfigurationTerms", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTerms", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "thirdItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "secondItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "firstItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerTableAccountId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "SalesOrdersConfigurationId", "dbo.SalesOrdersConfigurations");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "DefaultChildAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesOrdersConfigurations", "DefaultPayAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.SalesOrdersConfigurations", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalesOrdersConfigurations", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SalesOrdersConfigurations", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "AccountTypeId", "dbo.PurchasesOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "TitleBalanceSheetTypeId", "dbo.BalanceSheetTypes");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "TitleAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTerms", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTerms", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "thirdItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "secondItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "firstItemAccountNoteId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerTableAccountId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "PurchaseOrdersConfigurationId", "dbo.PurchaseOrdersConfigurations");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "DefaultChildAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "DefaultPayAccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrdersConfigurations", "BranchId", "dbo.Branches");
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.SalessOrdersConfigurationTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationTerms", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTerms", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerCancelId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "thirdItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "secondItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "firstItemAccountNoteId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "ItemGroupId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "CurrentWorkerTableAccountId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "DefaultChildAccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "SalesOrdersConfigurationId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "CompanyId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "BranchId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "DefaultPayAccountCategoryId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "TitleBalanceSheetTypeId" });
            DropIndex("dbo.SalesOrdersConfigurations", new[] { "TitleAccountCategoryId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTerms", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTerms", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerCancelId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "thirdItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "secondItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "firstItemAccountNoteId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "ItemGroupId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "CurrentWorkerTableAccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "AccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "DefaultChildAccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "AccountId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "PurchaseOrdersConfigurationId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "CompanyId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "BranchId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "DefaultPayAccountCategoryId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "TitleBalanceSheetTypeId" });
            DropIndex("dbo.PurchaseOrdersConfigurations", new[] { "TitleAccountCategoryId" });
            DropTable("dbo.SalessOrdersConfigurationAccountTypes");
            DropTable("dbo.SalessOrdersConfigurationTotalAccounts");
            DropTable("dbo.SalessOrdersConfigurationTerms");
            DropTable("dbo.SalessOrdersConfigurationTableAccounts");
            DropTable("dbo.SalessOrdersConfigurationPayMethods");
            DropTable("dbo.SalesOrdersConfigurations");
            DropTable("dbo.PurchasesOrdersConfigurationAccountTypes");
            DropTable("dbo.PurchasesOrdersConfigurationTotalAccounts");
            DropTable("dbo.PurchasesOrdersConfigurationTerms");
            DropTable("dbo.PurchasesOrdersConfigurationTableAccounts");
            DropTable("dbo.PurchasesOrdersConfigurationPayMethods");
            DropTable("dbo.PurchaseOrdersConfigurations");
        }
    }
}
