namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addtoPrint : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderPropertiesPayPurchases", "ToPrint", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderPropertiesPaySales", "ToPrint", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderPropertiesPaySales", "ToPrint");
            DropColumn("dbo.OrderPropertiesPayPurchases", "ToPrint");
        }
    }
}
