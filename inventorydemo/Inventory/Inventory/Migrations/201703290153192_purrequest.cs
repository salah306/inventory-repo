namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purrequest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderGroupPayMethodPurchases", "AccountId", c => c.Int());
            AddColumn("dbo.OrderGroupPayMethodSales", "AccountId", c => c.Int());
            CreateIndex("dbo.OrderGroupPayMethodPurchases", "AccountId");
            CreateIndex("dbo.OrderGroupPayMethodSales", "AccountId");
            AddForeignKey("dbo.OrderGroupPayMethodPurchases", "AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderGroupPayMethodSales", "AccountId", "dbo.Accounts", "AccountId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderGroupPayMethodSales", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderGroupPayMethodPurchases", "AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderGroupPayMethodSales", new[] { "AccountId" });
            DropIndex("dbo.OrderGroupPayMethodPurchases", new[] { "AccountId" });
            DropColumn("dbo.OrderGroupPayMethodSales", "AccountId");
            DropColumn("dbo.OrderGroupPayMethodPurchases", "AccountId");
        }
    }
}
