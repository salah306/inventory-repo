namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDb : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Warehouses", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Warehouses", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "AccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.Inventories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryInvoices", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "InventoryInvoiceId", "dbo.InventoryInvoices");
            DropForeignKey("dbo.InventoryOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "InventoryOrderId", "dbo.InventoryOrders");
            DropForeignKey("dbo.Items", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Items", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Inventories", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemCategories", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemCategoryProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemCategoryProperties", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemCategoryPropertiesValues", "ItemCategoryPropertiesId", "dbo.ItemCategoryProperties");
            DropForeignKey("dbo.Items", "ItemCategoryId", "dbo.ItemCategories");
            DropForeignKey("dbo.Inventories", "WarehouseId", "dbo.Warehouses");
            DropIndex("dbo.Warehouses", "AK_WarehouseName_WarehouseName");
            DropIndex("dbo.Warehouses", new[] { "BranchId" });
            DropIndex("dbo.Warehouses", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Inventories", new[] { "ItemId" });
            DropIndex("dbo.Inventories", new[] { "WarehouseId" });
            DropIndex("dbo.Inventories", new[] { "AccountOrderId" });
            DropIndex("dbo.Inventories", new[] { "InventoryOrderId" });
            DropIndex("dbo.Inventories", new[] { "InventoryInvoiceId" });
            DropIndex("dbo.Inventories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryInvoices", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Items", "AK_Item_ItemName");
            DropIndex("dbo.Items", new[] { "ItemCategoryId" });
            DropIndex("dbo.Items", new[] { "CompanyId" });
            DropIndex("dbo.Items", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategories", "AK_ItemCategory_ItemCategoryName");
            DropIndex("dbo.ItemCategories", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategoryProperties", "AK_ItemCategoryProperties_ItemCategoryPropertiesName");
            DropIndex("dbo.ItemCategoryProperties", new[] { "ItemCategoryId" });
            DropIndex("dbo.ItemCategoryProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", "AK_ItemCategoryPropertiesValues_ItemCategoryPropertiesValuesName");
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "ItemCategoryPropertiesId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "ItemId" });
            DropIndex("dbo.ItemCategoryPropertiesValues", new[] { "CurrentWorkerId" });
            DropTable("dbo.Warehouses");
            DropTable("dbo.Inventories");
            DropTable("dbo.InventoryInvoices");
            DropTable("dbo.InventoryOrders");
            DropTable("dbo.Items");
            DropTable("dbo.ItemCategories");
            DropTable("dbo.ItemCategoryProperties");
            DropTable("dbo.ItemCategoryPropertiesValues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ItemCategoryPropertiesValues",
                c => new
                    {
                        ItemCategoryPropertiesValuesId = c.Int(nullable: false, identity: true),
                        ItemCategoryPropertiesValuesName = c.String(maxLength: 150),
                        ItemCategoryPropertiesId = c.Int(nullable: false),
                        ItemId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryPropertiesValuesId);
            
            CreateTable(
                "dbo.ItemCategoryProperties",
                c => new
                    {
                        ItemCategoryPropertiesId = c.Int(nullable: false, identity: true),
                        ItemCategoryPropertiesName = c.String(nullable: false, maxLength: 150),
                        ItemCategoryId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryPropertiesId);
            
            CreateTable(
                "dbo.ItemCategories",
                c => new
                    {
                        ItemCategoryId = c.Int(nullable: false, identity: true),
                        ItemCategoryName = c.String(nullable: false, maxLength: 80),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemCategoryId);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        ItemId = c.Int(nullable: false, identity: true),
                        ItemName = c.String(nullable: false, maxLength: 150),
                        HasExpiryDate = c.Boolean(nullable: false),
                        Availability = c.Boolean(nullable: false),
                        ItemCategoryId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ItemId);
            
            CreateTable(
                "dbo.InventoryOrders",
                c => new
                    {
                        InventoryOrderId = c.Int(nullable: false, identity: true),
                        InventoryOrderDate = c.DateTime(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryOrderId);
            
            CreateTable(
                "dbo.InventoryInvoices",
                c => new
                    {
                        InventoryInvoiceId = c.Int(nullable: false, identity: true),
                        InventoryInvoiceDate = c.DateTime(nullable: false),
                        InventoryInvoiceCode = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryInvoiceId);
            
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        InventoryId = c.Int(nullable: false, identity: true),
                        InventoryInput = c.Decimal(precision: 18, scale: 2),
                        InventoryOutput = c.Decimal(precision: 18, scale: 2),
                        InventoryExpiryDate = c.DateTime(),
                        InventoryNotes = c.String(maxLength: 200),
                        ItemId = c.Int(nullable: false),
                        WarehouseId = c.Int(nullable: false),
                        AccountOrderId = c.Int(nullable: false),
                        InventoryOrderId = c.Int(nullable: false),
                        InventoryInvoiceId = c.Int(nullable: false),
                        AccountOrderhasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.InventoryId);
            
            CreateTable(
                "dbo.Warehouses",
                c => new
                    {
                        WarehouseId = c.Int(nullable: false, identity: true),
                        WarehouseName = c.String(nullable: false, maxLength: 200),
                        BranchId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.WarehouseId);
            
            CreateIndex("dbo.ItemCategoryPropertiesValues", "CurrentWorkerId");
            CreateIndex("dbo.ItemCategoryPropertiesValues", "ItemId");
            CreateIndex("dbo.ItemCategoryPropertiesValues", "ItemCategoryPropertiesId");
            CreateIndex("dbo.ItemCategoryPropertiesValues", "ItemCategoryPropertiesValuesName", name: "AK_ItemCategoryPropertiesValues_ItemCategoryPropertiesValuesName");
            CreateIndex("dbo.ItemCategoryProperties", "CurrentWorkerId");
            CreateIndex("dbo.ItemCategoryProperties", "ItemCategoryId");
            CreateIndex("dbo.ItemCategoryProperties", "ItemCategoryPropertiesName", name: "AK_ItemCategoryProperties_ItemCategoryPropertiesName");
            CreateIndex("dbo.ItemCategories", "CurrentWorkerId");
            CreateIndex("dbo.ItemCategories", "ItemCategoryName", unique: true, name: "AK_ItemCategory_ItemCategoryName");
            CreateIndex("dbo.Items", "CurrentWorkerId");
            CreateIndex("dbo.Items", "CompanyId");
            CreateIndex("dbo.Items", "ItemCategoryId");
            CreateIndex("dbo.Items", "ItemName", unique: true, name: "AK_Item_ItemName");
            CreateIndex("dbo.InventoryOrders", "CurrentWorkerId");
            CreateIndex("dbo.InventoryInvoices", "CurrentWorkerId");
            CreateIndex("dbo.Inventories", "CurrentWorkerId");
            CreateIndex("dbo.Inventories", "InventoryInvoiceId");
            CreateIndex("dbo.Inventories", "InventoryOrderId");
            CreateIndex("dbo.Inventories", "AccountOrderId");
            CreateIndex("dbo.Inventories", "WarehouseId");
            CreateIndex("dbo.Inventories", "ItemId");
            CreateIndex("dbo.Warehouses", "CurrentWorkerId");
            CreateIndex("dbo.Warehouses", "BranchId");
            CreateIndex("dbo.Warehouses", "WarehouseName", unique: true, name: "AK_WarehouseName_WarehouseName");
            AddForeignKey("dbo.Inventories", "WarehouseId", "dbo.Warehouses", "WarehouseId", cascadeDelete: true);
            AddForeignKey("dbo.Items", "ItemCategoryId", "dbo.ItemCategories", "ItemCategoryId", cascadeDelete: true);
            AddForeignKey("dbo.ItemCategoryPropertiesValues", "ItemCategoryPropertiesId", "dbo.ItemCategoryProperties", "ItemCategoryPropertiesId", cascadeDelete: true);
            AddForeignKey("dbo.ItemCategoryPropertiesValues", "ItemId", "dbo.Items", "ItemId", cascadeDelete: true);
            AddForeignKey("dbo.ItemCategoryPropertiesValues", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ItemCategoryProperties", "ItemCategoryId", "dbo.ItemCategories", "ItemCategoryId", cascadeDelete: true);
            AddForeignKey("dbo.ItemCategoryProperties", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ItemCategories", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Inventories", "ItemId", "dbo.Items", "ItemId", cascadeDelete: true);
            AddForeignKey("dbo.Items", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Items", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
            AddForeignKey("dbo.Inventories", "InventoryOrderId", "dbo.InventoryOrders", "InventoryOrderId", cascadeDelete: true);
            AddForeignKey("dbo.InventoryOrders", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Inventories", "InventoryInvoiceId", "dbo.InventoryInvoices", "InventoryInvoiceId", cascadeDelete: true);
            AddForeignKey("dbo.InventoryInvoices", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Inventories", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Inventories", "AccountOrderId", "dbo.AccountOrders", "AccountOrderId", cascadeDelete: true);
            AddForeignKey("dbo.Warehouses", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Warehouses", "BranchId", "dbo.Branches", "BranchId", cascadeDelete: true);
        }
    }
}
