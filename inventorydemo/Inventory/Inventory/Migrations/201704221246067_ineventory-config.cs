namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ineventoryconfig : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InventoriesConfigurtions",
                c => new
                    {
                        InventoriesConfigurtionId = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        SalesCostAccountId = c.Int(nullable: false),
                        SalesAccountId = c.Int(nullable: false),
                        SalesReturnId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoriesConfigurtionId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.InventoryId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesCostAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.SalesReturnId, cascadeDelete: false)
                .Index(t => t.InventoryId)
                .Index(t => t.SalesCostAccountId)
                .Index(t => t.SalesAccountId)
                .Index(t => t.SalesReturnId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesReturnId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesCostAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "SalesAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "InventoryId", "dbo.Accounts");
            DropForeignKey("dbo.InventoriesConfigurtions", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoriesConfigurtions", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoriesConfigurtions", "BranchId", "dbo.Branches");
            DropIndex("dbo.InventoriesConfigurtions", new[] { "CompanyId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "BranchId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesReturnId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesAccountId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "SalesCostAccountId" });
            DropIndex("dbo.InventoriesConfigurtions", new[] { "InventoryId" });
            DropTable("dbo.InventoriesConfigurtions");
        }
    }
}
