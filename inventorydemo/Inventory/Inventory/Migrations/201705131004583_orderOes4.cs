namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderOes4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderOes", "RefrenceOrderConfigId", c => c.Int());
            CreateIndex("dbo.OrderOes", "RefrenceOrderConfigId");
            AddForeignKey("dbo.OrderOes", "RefrenceOrderConfigId", "dbo.PurchaseOrdersConfigurations", "PurchaseOrdersConfigurationId");
            DropColumn("dbo.OrderOes", "RefrenceOrderConfig");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderOes", "RefrenceOrderConfig", c => c.Int());
            DropForeignKey("dbo.OrderOes", "RefrenceOrderConfigId", "dbo.PurchaseOrdersConfigurations");
            DropIndex("dbo.OrderOes", new[] { "RefrenceOrderConfigId" });
            DropColumn("dbo.OrderOes", "RefrenceOrderConfigId");
        }
    }
}
