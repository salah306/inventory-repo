namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class salesconfigureIsCompany2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Sales", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Purchases", new[] { "CompanyId" });
            DropIndex("dbo.Sales", new[] { "CompanyId" });
            AlterColumn("dbo.Purchases", "CompanyId", c => c.Int(nullable: false));
            AlterColumn("dbo.Sales", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Purchases", "CompanyId");
            CreateIndex("dbo.Sales", "CompanyId");
            AddForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: false);
            AddForeignKey("dbo.Sales", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sales", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Sales", new[] { "CompanyId" });
            DropIndex("dbo.Purchases", new[] { "CompanyId" });
            AlterColumn("dbo.Sales", "CompanyId", c => c.Int());
            AlterColumn("dbo.Purchases", "CompanyId", c => c.Int());
            CreateIndex("dbo.Sales", "CompanyId");
            CreateIndex("dbo.Purchases", "CompanyId");
            AddForeignKey("dbo.Sales", "CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies", "CompanyId");
        }
    }
}
