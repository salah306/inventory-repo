namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firgToMain : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderMainAccounts", "InventoryAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesCostAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderMainAccounts", new[] { "InventoryAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesCostAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesReturnAccount_AccountId" });
            DropColumn("dbo.OrderMainAccounts", "InventoryAccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesAccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesCostAccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesReturnAccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "InventoryAccount_AccountId", newName: "InventoryAccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesAccount_AccountId", newName: "SalesAccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesCostAccount_AccountId", newName: "SalesCostAccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesReturnAccount_AccountId", newName: "SalesReturnAccountId");
            AlterColumn("dbo.OrderMainAccounts", "InventoryAccountId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderMainAccounts", "SalesAccountId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderMainAccounts", "SalesCostAccountId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderMainAccounts", "SalesReturnAccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderMainAccounts", "InventoryAccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesCostAccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesAccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesReturnAccountId");
            AddForeignKey("dbo.OrderMainAccounts", "InventoryAccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.OrderMainAccounts", "SalesAccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.OrderMainAccounts", "SalesCostAccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.OrderMainAccounts", "SalesReturnAccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderMainAccounts", "SalesReturnAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesCostAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "InventoryAccountId", "dbo.Accounts");
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesReturnAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesCostAccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "InventoryAccountId" });
            AlterColumn("dbo.OrderMainAccounts", "SalesReturnAccountId", c => c.Int());
            AlterColumn("dbo.OrderMainAccounts", "SalesCostAccountId", c => c.Int());
            AlterColumn("dbo.OrderMainAccounts", "SalesAccountId", c => c.Int());
            AlterColumn("dbo.OrderMainAccounts", "InventoryAccountId", c => c.Int());
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesReturnAccountId", newName: "SalesReturnAccount_AccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesCostAccountId", newName: "SalesCostAccount_AccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "SalesAccountId", newName: "SalesAccount_AccountId");
            RenameColumn(table: "dbo.OrderMainAccounts", name: "InventoryAccountId", newName: "InventoryAccount_AccountId");
            AddColumn("dbo.OrderMainAccounts", "SalesReturnAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "SalesCostAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "SalesAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "InventoryAccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesCostAccount_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesAccount_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "InventoryAccount_AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesCostAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "InventoryAccount_AccountId", "dbo.Accounts", "AccountId");
        }
    }
}
