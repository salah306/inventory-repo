namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subaccountrealted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccounts", "Realted", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubAccounts", "Realted");
        }
    }
}
