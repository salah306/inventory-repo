namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CutomOrder2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomSubOrderAccounts",
                c => new
                    {
                        CustomSubOrderAccountId = c.Int(nullable: false, identity: true),
                        AliesAccountColName = c.String(),
                        AliesAccountColId = c.Int(nullable: false),
                        CustomSubAccountsOrderId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        AliesAccountCol_AccountId = c.Int(),
                        CustomSubAccountsOrder_CustomSubAccountsOrderId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomSubOrderAccountId)
                .ForeignKey("dbo.Accounts", t => t.AliesAccountCol_AccountId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.CustomSubAccountsOrders", t => t.CustomSubAccountsOrder_CustomSubAccountsOrderId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.AliesAccountCol_AccountId)
                .Index(t => t.CustomSubAccountsOrder_CustomSubAccountsOrderId);
            
            AddColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccountsId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId", c => c.Int());
            AddColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1", c => c.Int());
            CreateIndex("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId");
            CreateIndex("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1");
            AddForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId", "dbo.CustomSubOrderAccounts", "CustomSubOrderAccountId");
            AddForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1", "dbo.CustomSubOrderAccounts", "CustomSubOrderAccountId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1", "dbo.CustomSubOrderAccounts");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId", "dbo.CustomSubOrderAccounts");
            DropForeignKey("dbo.CustomSubOrderAccounts", "CustomSubAccountsOrder_CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders");
            DropForeignKey("dbo.CustomSubOrderAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomSubOrderAccounts", "AliesAccountCol_AccountId", "dbo.Accounts");
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "CustomSubAccountsOrder_CustomSubAccountsOrderId" });
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "AliesAccountCol_AccountId" });
            DropIndex("dbo.CustomSubOrderAccounts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomSubOrderAccount_CustomSubOrderAccountId1" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomSubOrderAccount_CustomSubOrderAccountId" });
            DropColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId1");
            DropColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccount_CustomSubOrderAccountId");
            DropColumn("dbo.CustomSubAccountsOrders", "CustomSubOrderAccountsId");
            DropTable("dbo.CustomSubOrderAccounts");
        }
    }
}
