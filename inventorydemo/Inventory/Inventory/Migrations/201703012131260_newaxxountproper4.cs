namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newaxxountproper4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId", c => c.Int(nullable: false));
            CreateIndex("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsTypesAcccountsId" });
            DropColumn("dbo.AccountCategoryProperties", "AccountsTypesAcccountsId");
        }
    }
}
