namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CutomOrder3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountOrders", "CompanyId", c => c.Int());
            AddColumn("dbo.AccountOrders", "IsCompany", c => c.Boolean(nullable: false));
            CreateIndex("dbo.AccountOrders", "CompanyId");
            AddForeignKey("dbo.AccountOrders", "CompanyId", "dbo.Companies", "CompanyId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountOrders", "CompanyId", "dbo.Companies");
            DropIndex("dbo.AccountOrders", new[] { "CompanyId" });
            DropColumn("dbo.AccountOrders", "IsCompany");
            DropColumn("dbo.AccountOrders", "CompanyId");
        }
    }
}
