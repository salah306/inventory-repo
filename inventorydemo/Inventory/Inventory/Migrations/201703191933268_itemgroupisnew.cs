namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemgroupisnew : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "uniqueId", c => c.String());
            AddColumn("dbo.PropertyValuesforItemGroups", "isNew", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValuesforItemGroups", "isNew");
            DropColumn("dbo.PropertyValuesforItemGroups", "uniqueId");
        }
    }
}
