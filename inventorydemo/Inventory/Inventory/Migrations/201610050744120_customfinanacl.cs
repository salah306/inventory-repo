namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customfinanacl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FinancialLists",
                c => new
                    {
                        FinancialListId = c.Int(nullable: false, identity: true),
                        FinancialListName = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.FinancialListId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.CustomBalanceSheets", "FinancialListId", c => c.Int(nullable: false));
            CreateIndex("dbo.CustomBalanceSheets", "FinancialListId");
            AddForeignKey("dbo.CustomBalanceSheets", "FinancialListId", "dbo.FinancialLists", "FinancialListId",cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FinancialLists", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CustomBalanceSheets", "FinancialListId", "dbo.FinancialLists");
            DropIndex("dbo.FinancialLists", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CustomBalanceSheets", new[] { "FinancialListId" });
            DropColumn("dbo.CustomBalanceSheets", "FinancialListId");
            DropTable("dbo.FinancialLists");
        }
    }
}
