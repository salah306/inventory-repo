namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newaxxountproper3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId" });
            DropColumn("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", c => c.Int());
            CreateIndex("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId");
        }
    }
}
