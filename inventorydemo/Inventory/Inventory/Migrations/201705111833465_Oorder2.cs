namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Oorder2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderRequests", "refrenceRequesterOrderType", c => c.String());
            AddColumn("dbo.OrderRequests", "refrenceRequesterOType", c => c.String());
            AddColumn("dbo.OrderRequests", "refrenceRequesterNo", c => c.Int());
            AddColumn("dbo.AccountOes", "name", c => c.String());
            AddColumn("dbo.OrderOes", "orderType", c => c.String());
            AddColumn("dbo.OrderOes", "type", c => c.String());
            AddColumn("dbo.PayOes", "payName", c => c.String());
            AddColumn("dbo.TotalAccountOes", "name", c => c.String());
            AddColumn("dbo.PurchaseOrders", "orderONo", c => c.Int());
            AddColumn("dbo.PurchaseOrders", "OType", c => c.String());
            AddColumn("dbo.PurchaseOrders", "OrderType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PurchaseOrders", "OrderType");
            DropColumn("dbo.PurchaseOrders", "OType");
            DropColumn("dbo.PurchaseOrders", "orderONo");
            DropColumn("dbo.TotalAccountOes", "name");
            DropColumn("dbo.PayOes", "payName");
            DropColumn("dbo.OrderOes", "type");
            DropColumn("dbo.OrderOes", "orderType");
            DropColumn("dbo.AccountOes", "name");
            DropColumn("dbo.OrderRequests", "refrenceRequesterNo");
            DropColumn("dbo.OrderRequests", "refrenceRequesterOType");
            DropColumn("dbo.OrderRequests", "refrenceRequesterOrderType");
        }
    }
}
