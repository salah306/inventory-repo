namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingsRole2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.settingRoles", "name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.settingRoles", "name");
        }
    }
}
