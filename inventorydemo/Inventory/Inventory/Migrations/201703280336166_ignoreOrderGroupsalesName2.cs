namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ignoreOrderGroupsalesName2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sales", "SalesName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sales", "SalesName");
        }
    }
}
