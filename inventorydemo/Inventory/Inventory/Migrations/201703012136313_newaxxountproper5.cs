namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newaxxountproper5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountsDataTypesId", c => c.Int(nullable: false));
            CreateIndex("dbo.AccountCategoryProperties", "AccountsDataTypesId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsDataTypesId" });
            DropColumn("dbo.AccountCategoryProperties", "AccountsDataTypesId");
        }
    }
}
