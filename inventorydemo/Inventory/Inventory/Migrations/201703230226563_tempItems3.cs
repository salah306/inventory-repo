namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tempItems3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TempItems", "Realted", c => c.String());
            AlterColumn("dbo.PropertyValuesforItemGroups", "Realted", c => c.String(nullable: false));
            AlterColumn("dbo.PropertyValuesforItemGroups", "parintType", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PropertyValuesforItemGroups", "parintType", c => c.String());
            AlterColumn("dbo.PropertyValuesforItemGroups", "Realted", c => c.String());
            DropColumn("dbo.TempItems", "Realted");
        }
    }
}
