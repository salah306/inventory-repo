namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tempItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TempItems",
                c => new
                    {
                        TempItemId = c.Int(nullable: false, identity: true),
                        code = c.String(),
                        name = c.String(),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        priceMin = c.Decimal(nullable: false, precision: 18, scale: 2),
                        priceMax = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExpireDate = c.DateTime(),
                        ItemGroupId = c.Int(nullable: false),
                        UnitId = c.Int(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.TempItemId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .ForeignKey("dbo.Units", t => t.UnitId, cascadeDelete: false)
                .Index(t => t.ItemGroupId)
                .Index(t => t.UnitId)
                .Index(t => t.CompanyId);
            
            AddColumn("dbo.ItemGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.ItemGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.AccountsforItemGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.AccountsforItemGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.PropertiesforItemGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.PropertiesforItemGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.PropertyValuesforItemGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.PropertyValuesforItemGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.TypeforItemGroups", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.TypeforItemGroups", "CurrentWorkerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.ItemGroups", "CurrentWorkerId");
            CreateIndex("dbo.AccountsforItemGroups", "CurrentWorkerId");
            CreateIndex("dbo.PropertiesforItemGroups", "CurrentWorkerId");
            CreateIndex("dbo.PropertyValuesforItemGroups", "CurrentWorkerId");
            CreateIndex("dbo.TypeforItemGroups", "CurrentWorkerId");
            AddForeignKey("dbo.AccountsforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ItemGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PropertiesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.PropertyValuesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.TypeforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TempItems", "UnitId", "dbo.Units");
            DropForeignKey("dbo.TempItems", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.TempItems", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.TypeforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PropertyValuesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PropertiesforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountsforItemGroups", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.TempItems", new[] { "CompanyId" });
            DropIndex("dbo.TempItems", new[] { "UnitId" });
            DropIndex("dbo.TempItems", new[] { "ItemGroupId" });
            DropIndex("dbo.TypeforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountsforItemGroups", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemGroups", new[] { "CurrentWorkerId" });
            DropColumn("dbo.TypeforItemGroups", "CurrentWorkerId");
            DropColumn("dbo.TypeforItemGroups", "RowVersion");
            DropColumn("dbo.PropertyValuesforItemGroups", "CurrentWorkerId");
            DropColumn("dbo.PropertyValuesforItemGroups", "RowVersion");
            DropColumn("dbo.PropertiesforItemGroups", "CurrentWorkerId");
            DropColumn("dbo.PropertiesforItemGroups", "RowVersion");
            DropColumn("dbo.AccountsforItemGroups", "CurrentWorkerId");
            DropColumn("dbo.AccountsforItemGroups", "RowVersion");
            DropColumn("dbo.ItemGroups", "CurrentWorkerId");
            DropColumn("dbo.ItemGroups", "RowVersion");
            DropTable("dbo.TempItems");
        }
    }
}
