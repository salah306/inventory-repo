namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderupdates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountMovements", "typeName", c => c.String());
            AddColumn("dbo.AccountMovements", "HasCanceled", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "AccountsOtherDataTypesId", c => c.Int());
            AddColumn("dbo.AccountOrders", "typeName", c => c.String());
            AddColumn("dbo.AccountOrders", "typeNameSecond", c => c.String());
            AddColumn("dbo.AccountOrders", "HasDoneFirst", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "HasDoneSecond", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "HasCanceled", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "attchedRequest", c => c.Boolean());
            CreateIndex("dbo.AccountOrders", "AccountsOtherDataTypesId");
            AddForeignKey("dbo.AccountOrders", "AccountsOtherDataTypesId", "dbo.AccountsDataTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountOrders", "AccountsOtherDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.AccountOrders", new[] { "AccountsOtherDataTypesId" });
            DropColumn("dbo.AccountOrders", "attchedRequest");
            DropColumn("dbo.AccountOrders", "HasCanceled");
            DropColumn("dbo.AccountOrders", "HasDoneSecond");
            DropColumn("dbo.AccountOrders", "HasDoneFirst");
            DropColumn("dbo.AccountOrders", "typeNameSecond");
            DropColumn("dbo.AccountOrders", "typeName");
            DropColumn("dbo.AccountOrders", "AccountsOtherDataTypesId");
            DropColumn("dbo.AccountMovements", "HasCanceled");
            DropColumn("dbo.AccountMovements", "typeName");
        }
    }
}
