namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purrequest2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ItemsRequests", "ItemGroupId", "dbo.ItemGroups");
            DropIndex("dbo.ItemsRequests", new[] { "ItemGroupId" });
            AddColumn("dbo.ItemsRequests", "SubAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.ItemsRequests", "LastCostPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.ItemsRequests", "CurrentCostPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            CreateIndex("dbo.ItemsRequests", "SubAccountId");
            AddForeignKey("dbo.ItemsRequests", "SubAccountId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
            DropColumn("dbo.ItemsRequests", "itemCode");
            DropColumn("dbo.ItemsRequests", "itemName");
            DropColumn("dbo.ItemsRequests", "ItemGroupId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ItemsRequests", "ItemGroupId", c => c.Int(nullable: false));
            AddColumn("dbo.ItemsRequests", "itemName", c => c.String());
            AddColumn("dbo.ItemsRequests", "itemCode", c => c.String());
            DropForeignKey("dbo.ItemsRequests", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.ItemsRequests", new[] { "SubAccountId" });
            DropColumn("dbo.ItemsRequests", "CurrentCostPrice");
            DropColumn("dbo.ItemsRequests", "LastCostPrice");
            DropColumn("dbo.ItemsRequests", "SubAccountId");
            CreateIndex("dbo.ItemsRequests", "ItemGroupId");
            AddForeignKey("dbo.ItemsRequests", "ItemGroupId", "dbo.ItemGroups", "ItemGroupId", cascadeDelete: true);
        }
    }
}
