namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordernum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TypeforItemGroups", "orderNum", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TypeforItemGroups", "orderNum");
        }
    }
}
