namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dattosubaccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccountOrders", "OrderDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SubAccountOrders", "OrderDate");
        }
    }
}
