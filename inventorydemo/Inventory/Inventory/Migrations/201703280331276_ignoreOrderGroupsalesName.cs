namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ignoreOrderGroupsalesName : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Sales", "SalesName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sales", "SalesName", c => c.Int(nullable: false));
        }
    }
}
