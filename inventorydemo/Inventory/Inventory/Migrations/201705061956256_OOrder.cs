namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountOes",
                c => new
                    {
                        AccountOId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        amountType = c.String(),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ItemsRequestO_ItemsRequestOId = c.Int(),
                    })
                .PrimaryKey(t => t.AccountOId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.ItemsRequestOes", t => t.ItemsRequestO_ItemsRequestOId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.ItemsRequestO_ItemsRequestOId);
            
            CreateTable(
                "dbo.OrderOes",
                c => new
                    {
                        OrderOId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        SubjectAccountId = c.Int(nullable: false),
                        OrderTitleName = c.String(),
                        OrderConfigName = c.String(),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        recivedDate = c.DateTime(nullable: false),
                        userId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userNameThird = c.String(),
                        userThirdId = c.String(maxLength: 128),
                        cancelById = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        RefrenceRequestId = c.Int(),
                        RefrenceRequestName = c.String(),
                        RefrenceInventoryOrderId = c.Int(),
                        RefrenceInventoryOrderName = c.String(),
                        RefrenceQueryId = c.Int(),
                        RefrenceQueryName = c.String(),
                        RefrenceBillId = c.Int(),
                        RefrenceBillName = c.String(),
                        RefrenceOrderConfig = c.Int(),
                    })
                .PrimaryKey(t => t.OrderOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.AspNetUsers", t => t.cancelById)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.SubjectAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferToOId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.userId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.SubjectAccountId)
                .Index(t => t.userId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.cancelById)
                .Index(t => t.TransferToOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.ItemsRequestOes",
                c => new
                    {
                        ItemsRequestOId = c.Int(nullable: false, identity: true),
                        SubAccountId = c.Int(nullable: false),
                        itemId = c.Int(nullable: false),
                        itemName = c.String(),
                        itemCode = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        costPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        realted = c.String(),
                        itemUnitType = c.String(),
                        itemUnitName = c.String(),
                        maxQ = c.Decimal(precision: 18, scale: 2),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.ItemsRequestOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.SubAccountId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.PayOes",
                c => new
                    {
                        PayOId = c.Int(nullable: false, identity: true),
                        PayId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        dueDate = c.DateTime(nullable: false),
                        note = c.String(),
                        type = c.String(),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.PayOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.PayId, cascadeDelete: false)
                .Index(t => t.PayId)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.TermOes",
                c => new
                    {
                        TermOId = c.Int(nullable: false, identity: true),
                        termName = c.String(nullable: false),
                        OrderOId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.TermOId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderOes", t => t.OrderOId, cascadeDelete: false)
                .Index(t => t.OrderOId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.TotalAccountOes",
                c => new
                    {
                        TotalAccountOId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        type = c.String(),
                        amountType = c.String(),
                        OrderO_OrderOId = c.Int(),
                    })
                .PrimaryKey(t => t.TotalAccountOId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderOes", t => t.OrderO_OrderOId)
                .Index(t => t.AccountId)
                .Index(t => t.OrderO_OrderOId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.OrderOes", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "userId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "TransferToOId", "dbo.Accounts");
            DropForeignKey("dbo.TotalAccountOes", "OrderO_OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.TotalAccountOes", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.TermOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.TermOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.TermOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.TermOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderOes", "SubjectAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PayOes", "PayId", "dbo.Accounts");
            DropForeignKey("dbo.PayOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.PayOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PayOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PayOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ItemsRequestOes", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.ItemsRequestOes", "OrderOId", "dbo.OrderOes");
            DropForeignKey("dbo.ItemsRequestOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemsRequestOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.ItemsRequestOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "ItemsRequestO_ItemsRequestOId", "dbo.ItemsRequestOes");
            DropForeignKey("dbo.OrderOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderOes", "cancelById", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountOes", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.AccountOes", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.AccountOes", "AccountId", "dbo.Accounts");
            DropIndex("dbo.TotalAccountOes", new[] { "OrderO_OrderOId" });
            DropIndex("dbo.TotalAccountOes", new[] { "AccountId" });
            DropIndex("dbo.TermOes", new[] { "CompanyId" });
            DropIndex("dbo.TermOes", new[] { "BranchId" });
            DropIndex("dbo.TermOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.TermOes", new[] { "OrderOId" });
            DropIndex("dbo.PayOes", new[] { "CompanyId" });
            DropIndex("dbo.PayOes", new[] { "BranchId" });
            DropIndex("dbo.PayOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PayOes", new[] { "OrderOId" });
            DropIndex("dbo.PayOes", new[] { "PayId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "CompanyId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "BranchId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "OrderOId" });
            DropIndex("dbo.ItemsRequestOes", new[] { "SubAccountId" });
            DropIndex("dbo.OrderOes", new[] { "CompanyId" });
            DropIndex("dbo.OrderOes", new[] { "BranchId" });
            DropIndex("dbo.OrderOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderOes", new[] { "TransferToOId" });
            DropIndex("dbo.OrderOes", new[] { "cancelById" });
            DropIndex("dbo.OrderOes", new[] { "userThirdId" });
            DropIndex("dbo.OrderOes", new[] { "userSecondId" });
            DropIndex("dbo.OrderOes", new[] { "userId" });
            DropIndex("dbo.OrderOes", new[] { "SubjectAccountId" });
            DropIndex("dbo.AccountOes", new[] { "ItemsRequestO_ItemsRequestOId" });
            DropIndex("dbo.AccountOes", new[] { "CompanyId" });
            DropIndex("dbo.AccountOes", new[] { "BranchId" });
            DropIndex("dbo.AccountOes", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountOes", new[] { "OrderOId" });
            DropIndex("dbo.AccountOes", new[] { "AccountId" });
            DropTable("dbo.TotalAccountOes");
            DropTable("dbo.TermOes");
            DropTable("dbo.PayOes");
            DropTable("dbo.ItemsRequestOes");
            DropTable("dbo.OrderOes");
            DropTable("dbo.AccountOes");
        }
    }
}
