namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountTypelinkacc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomBalanceSheets", "IsLinked", c => c.Boolean());
            AddColumn("dbo.CustomBalanceSheets", "LinkedAccountId", c => c.Int());
            AddColumn("dbo.CustomBalanceSheets", "LinkedAccountName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomBalanceSheets", "LinkedAccountName");
            DropColumn("dbo.CustomBalanceSheets", "LinkedAccountId");
            DropColumn("dbo.CustomBalanceSheets", "IsLinked");
        }
    }
}
