namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ignoreOrderGroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Purchases", "PurchasesName", c => c.String());
            AddColumn("dbo.Sales", "SalesName", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sales", "SalesName");
            DropColumn("dbo.Purchases", "PurchasesName");
        }
    }
}
