namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccountMovements", "Pricein", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "PriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "TotalPriceIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "TotalPriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "StockQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "StockCostPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "Price");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAccountMovements", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "StockCostPrice");
            DropColumn("dbo.SubAccountMovements", "StockQuantity");
            DropColumn("dbo.SubAccountMovements", "TotalPriceOut");
            DropColumn("dbo.SubAccountMovements", "TotalPriceIn");
            DropColumn("dbo.SubAccountMovements", "PriceOut");
            DropColumn("dbo.SubAccountMovements", "Pricein");
        }
    }
}
