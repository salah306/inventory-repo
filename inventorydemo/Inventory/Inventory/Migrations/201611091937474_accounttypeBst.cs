namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accounttypeBst : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSheetTypes", "AccountTypeId", c => c.Int(nullable: true));
            CreateIndex("dbo.BalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.BalanceSheetTypes", new[] { "AccountTypeId" });
            DropColumn("dbo.BalanceSheetTypes", "AccountTypeId");
        }
    }
}
