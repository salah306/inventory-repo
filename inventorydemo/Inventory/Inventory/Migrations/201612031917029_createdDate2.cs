namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdDate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountCategories", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.Accounts", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.BalanceSheetTypes", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BalanceSheetTypes", "CreatedDate");
            DropColumn("dbo.Accounts", "CreatedDate");
            DropColumn("dbo.AccountCategories", "CreatedDate");
        }
    }
}
