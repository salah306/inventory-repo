namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class compnaytoAccountProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountCategoryProperties", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.AccountCategoryProperties", "CompanyId");
            AddForeignKey("dbo.AccountCategoryProperties", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: false);
            DropColumn("dbo.AccountCategoryProperties", "AccountCategoryPropertiesValue");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountCategoryPropertiesValue", c => c.String());
            DropForeignKey("dbo.AccountCategoryProperties", "CompanyId", "dbo.Companies");
            DropIndex("dbo.AccountCategoryProperties", new[] { "CompanyId" });
            DropColumn("dbo.AccountCategoryProperties", "CompanyId");
        }
    }
}
