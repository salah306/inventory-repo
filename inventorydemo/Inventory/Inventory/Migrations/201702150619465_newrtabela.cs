namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newrtabela : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccountMovements", "ItemPricein", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemPriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemTotalPriceIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemTotalPriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemStockQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemStockPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "ItemTotalStockPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "Pricein");
            DropColumn("dbo.SubAccountMovements", "PriceOut");
            DropColumn("dbo.SubAccountMovements", "TotalPriceIn");
            DropColumn("dbo.SubAccountMovements", "TotalPriceOut");
            DropColumn("dbo.SubAccountMovements", "StockQuantity");
            DropColumn("dbo.SubAccountMovements", "StockCostPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAccountMovements", "StockCostPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "StockQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "TotalPriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "TotalPriceIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "PriceOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "Pricein", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "ItemTotalStockPrice");
            DropColumn("dbo.SubAccountMovements", "ItemStockPrice");
            DropColumn("dbo.SubAccountMovements", "ItemStockQuantity");
            DropColumn("dbo.SubAccountMovements", "ItemTotalPriceOut");
            DropColumn("dbo.SubAccountMovements", "ItemTotalPriceIn");
            DropColumn("dbo.SubAccountMovements", "ItemPriceOut");
            DropColumn("dbo.SubAccountMovements", "ItemPricein");
        }
    }
}
