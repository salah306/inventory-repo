namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billsp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillAccountItems",
                c => new
                    {
                        BillAccountItemsId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        SubAccountMovementId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BillAccountItemsId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccountMovements", t => t.SubAccountMovementId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SubAccountMovementId);
            
            CreateTable(
                "dbo.PurchasesBills",
                c => new
                    {
                        PurchasesBillId = c.Int(nullable: false, identity: true),
                        PurchasesBillName = c.String(),
                        type = c.String(),
                        BillNo = c.Int(nullable: false),
                        OrdersTypePurchasesType = c.String(),
                        OrdersTypePurchasesId = c.Int(nullable: false),
                        BillDate = c.DateTime(nullable: false),
                        BillMainAccountId = c.Int(nullable: false),
                        BillTitleAccountId = c.Int(nullable: false),
                        SubAccountOrderId = c.Int(nullable: false),
                        BillOtherAccountOrderId = c.Int(),
                        BillTotalAccountId = c.Int(nullable: false),
                        BillPaymethodId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PurchasesBillId)
                .ForeignKey("dbo.Accounts", t => t.BillMainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountOrders", t => t.BillOtherAccountOrderId)
                .ForeignKey("dbo.OrderGroupPayMethodPurchases", t => t.BillPaymethodId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTitleAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTotalAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrdersTypePurchases", t => t.OrdersTypePurchasesId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccountOrders", t => t.SubAccountOrderId, cascadeDelete: false)
                .Index(t => t.OrdersTypePurchasesId)
                .Index(t => t.BillMainAccountId)
                .Index(t => t.BillTitleAccountId)
                .Index(t => t.SubAccountOrderId)
                .Index(t => t.BillOtherAccountOrderId)
                .Index(t => t.BillTotalAccountId)
                .Index(t => t.BillPaymethodId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.BillPaymentPropertiesPurchases",
                c => new
                    {
                        BillPaymentPropertiesPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPayPurchasesId = c.Int(nullable: false),
                        PurchasesBillId = c.Int(nullable: false),
                        value = c.String(),
                        SalesBill_SalesBillId = c.Int(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesPurchasesId)
                .ForeignKey("dbo.OrderPropertiesPayPurchases", t => t.OrderPropertiesPayPurchasesId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId, cascadeDelete: false)
                .ForeignKey("dbo.SalesBills", t => t.SalesBill_SalesBillId)
                .Index(t => t.OrderPropertiesPayPurchasesId)
                .Index(t => t.PurchasesBillId)
                .Index(t => t.SalesBill_SalesBillId);
            
            CreateTable(
                "dbo.SalesBills",
                c => new
                    {
                        SalesBillId = c.Int(nullable: false, identity: true),
                        SalesBillName = c.String(),
                        type = c.String(),
                        BillNo = c.Int(nullable: false),
                        OrdersTypeSalesId = c.Int(nullable: false),
                        OrdersTypeSalesType = c.String(),
                        BillDate = c.DateTime(nullable: false),
                        BillMainAccountId = c.Int(nullable: false),
                        BillTitleAccountId = c.Int(nullable: false),
                        SubAccountOrderId = c.Int(nullable: false),
                        BillOtherAccountOrderId = c.Int(),
                        BillTotalAccountId = c.Int(nullable: false),
                        BillPaymethodId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SalesBillId)
                .ForeignKey("dbo.Accounts", t => t.BillMainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.AccountOrders", t => t.BillOtherAccountOrderId)
                .ForeignKey("dbo.OrderGroupPayMethodPurchases", t => t.BillPaymethodId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTitleAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.BillTotalAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrdersTypeSales", t => t.OrdersTypeSalesId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccountOrders", t => t.SubAccountOrderId, cascadeDelete: false)
                .Index(t => t.OrdersTypeSalesId)
                .Index(t => t.BillMainAccountId)
                .Index(t => t.BillTitleAccountId)
                .Index(t => t.SubAccountOrderId)
                .Index(t => t.BillOtherAccountOrderId)
                .Index(t => t.BillTotalAccountId)
                .Index(t => t.BillPaymethodId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.BillPaymentPropertiesSales",
                c => new
                    {
                        BillPaymentPropertiesSalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesId = c.Int(nullable: false),
                        SalesBillId = c.Int(nullable: false),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesSalesId)
                .ForeignKey("dbo.OrderPropertiesPaySales", t => t.OrderPropertiesPaySalesId, cascadeDelete: false)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId, cascadeDelete: false)
                .Index(t => t.OrderPropertiesPaySalesId)
                .Index(t => t.SalesBillId);
            
            AddColumn("dbo.SubAccountMovements", "SalesBillId", c => c.Int());
            AddColumn("dbo.SubAccountMovements", "PurchasesBillId", c => c.Int());
            CreateIndex("dbo.SubAccountMovements", "SalesBillId");
            CreateIndex("dbo.SubAccountMovements", "PurchasesBillId");
            AddForeignKey("dbo.SubAccountMovements", "PurchasesBillId", "dbo.PurchasesBills", "PurchasesBillId");
            AddForeignKey("dbo.SubAccountMovements", "SalesBillId", "dbo.SalesBills", "SalesBillId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillPaymentPropertiesSales", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillPaymentPropertiesSales", "OrderPropertiesPaySalesId", "dbo.OrderPropertiesPaySales");
            DropForeignKey("dbo.SubAccountMovements", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.SalesBills", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.SalesBills", "OrdersTypeSalesId", "dbo.OrdersTypeSales");
            DropForeignKey("dbo.SalesBills", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SalesBills", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SalesBills", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.SalesBills", "BillTotalAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesBills", "BillTitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalesBills", "BillPaymethodId", "dbo.OrderGroupPayMethodPurchases");
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "SalesBill_SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.SalesBills", "BillOtherAccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.SalesBills", "BillMainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.SubAccountMovements", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.PurchasesBills", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.PurchasesBills", "OrdersTypePurchasesId", "dbo.OrdersTypePurchases");
            DropForeignKey("dbo.PurchasesBills", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchasesBills", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchasesBills", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.PurchasesBills", "BillTotalAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesBills", "BillTitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.PurchasesBills", "BillPaymethodId", "dbo.OrderGroupPayMethodPurchases");
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases");
            DropForeignKey("dbo.PurchasesBills", "BillOtherAccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.PurchasesBills", "BillMainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.BillAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropForeignKey("dbo.BillAccountItems", "AccountId", "dbo.Accounts");
            DropIndex("dbo.BillPaymentPropertiesSales", new[] { "SalesBillId" });
            DropIndex("dbo.BillPaymentPropertiesSales", new[] { "OrderPropertiesPaySalesId" });
            DropIndex("dbo.SalesBills", new[] { "CompanyId" });
            DropIndex("dbo.SalesBills", new[] { "BranchId" });
            DropIndex("dbo.SalesBills", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SalesBills", new[] { "BillPaymethodId" });
            DropIndex("dbo.SalesBills", new[] { "BillTotalAccountId" });
            DropIndex("dbo.SalesBills", new[] { "BillOtherAccountOrderId" });
            DropIndex("dbo.SalesBills", new[] { "SubAccountOrderId" });
            DropIndex("dbo.SalesBills", new[] { "BillTitleAccountId" });
            DropIndex("dbo.SalesBills", new[] { "BillMainAccountId" });
            DropIndex("dbo.SalesBills", new[] { "OrdersTypeSalesId" });
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "SalesBill_SalesBillId" });
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "OrderPropertiesPayPurchasesId" });
            DropIndex("dbo.PurchasesBills", new[] { "CompanyId" });
            DropIndex("dbo.PurchasesBills", new[] { "BranchId" });
            DropIndex("dbo.PurchasesBills", new[] { "CurrentWorkerId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillPaymethodId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillTotalAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillOtherAccountOrderId" });
            DropIndex("dbo.PurchasesBills", new[] { "SubAccountOrderId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillTitleAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillMainAccountId" });
            DropIndex("dbo.PurchasesBills", new[] { "OrdersTypePurchasesId" });
            DropIndex("dbo.BillAccountItems", new[] { "SubAccountMovementId" });
            DropIndex("dbo.BillAccountItems", new[] { "AccountId" });
            DropIndex("dbo.SubAccountMovements", new[] { "PurchasesBillId" });
            DropIndex("dbo.SubAccountMovements", new[] { "SalesBillId" });
            DropColumn("dbo.SubAccountMovements", "PurchasesBillId");
            DropColumn("dbo.SubAccountMovements", "SalesBillId");
            DropTable("dbo.BillPaymentPropertiesSales");
            DropTable("dbo.SalesBills");
            DropTable("dbo.BillPaymentPropertiesPurchases");
            DropTable("dbo.PurchasesBills");
            DropTable("dbo.BillAccountItems");
        }
    }
}
