namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aliesnamedatatypes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountsDataTypes", "aliasName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountsDataTypes", "aliasName");
        }
    }
}
