namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderRequestId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountOrders", "OrderRequestId", c => c.Int());
            AddColumn("dbo.AccountOrders", "AccountsDataTypesId", c => c.Int());
            CreateIndex("dbo.AccountOrders", "OrderRequestId");
            CreateIndex("dbo.AccountOrders", "AccountsDataTypesId");
            AddForeignKey("dbo.AccountOrders", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id");
            AddForeignKey("dbo.AccountOrders", "OrderRequestId", "dbo.OrderRequests", "OrderRequestId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountOrders", "OrderRequestId", "dbo.OrderRequests");
            DropForeignKey("dbo.AccountOrders", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.AccountOrders", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.AccountOrders", new[] { "OrderRequestId" });
            DropColumn("dbo.AccountOrders", "AccountsDataTypesId");
            DropColumn("dbo.AccountOrders", "OrderRequestId");
        }
    }
}
