namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uniquaccounts : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Accounts", "AK_Accounts_AccountName");
            DropIndex("dbo.Accounts", new[] { "BranchId" });
            CreateIndex("dbo.Accounts", new[] { "AccountName", "BranchId" }, name: "AK_Accounts_AccountName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Accounts", "AK_Accounts_AccountName");
            CreateIndex("dbo.Accounts", "BranchId");
            CreateIndex("dbo.Accounts", "AccountName", name: "AK_Accounts_AccountName");
        }
    }
}
