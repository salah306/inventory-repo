namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersConfiguration2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId", c => c.Int(nullable: false));
            AddColumn("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId");
            CreateIndex("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId");
            AddForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.PurchasesOrdersConfigurationAccountTypes", "PurchasesOrdersConfigurationAccountTypeId", cascadeDelete: false);
            AddForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.SalessOrdersConfigurationAccountTypes", "SalessOrdersConfigurationAccountTypeId", cascadeDelete: false);
            DropColumn("dbo.PurchasesOrdersConfigurationTableAccounts", "accountType");
            DropColumn("dbo.SalessOrdersConfigurationTableAccounts", "accountType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalessOrdersConfigurationTableAccounts", "accountType", c => c.String(nullable: false));
            AddColumn("dbo.PurchasesOrdersConfigurationTableAccounts", "accountType", c => c.String(nullable: false));
            DropForeignKey("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.SalessOrdersConfigurationAccountTypes");
            DropForeignKey("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId", "dbo.PurchasesOrdersConfigurationAccountTypes");
            DropIndex("dbo.SalessOrdersConfigurationTableAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.PurchasesOrdersConfigurationTableAccounts", new[] { "AccountTypeId" });
            DropColumn("dbo.SalessOrdersConfigurationTableAccounts", "AccountTypeId");
            DropColumn("dbo.PurchasesOrdersConfigurationTableAccounts", "AccountTypeId");
        }
    }
}
