namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dates : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BalanceSheets", "CreatedDate");
            DropColumn("dbo.BalanceSheetTypes", "CreatedDate");
            DropColumn("dbo.AccountCategories", "CreatedDate");
            DropColumn("dbo.Accounts", "CreatedDate");
            DropColumn("dbo.SubAccounts", "CreatedDate");
            AddColumn("dbo.BalanceSheets", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.BalanceSheetTypes", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.AccountCategories", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.Accounts", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.SubAccounts", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));



        }

        public override void Down()
        {
            DropColumn("dbo.BalanceSheets", "CreatedDate");
            DropColumn("dbo.BalanceSheetTypes", "CreatedDate");
            DropColumn("dbo.AccountCategories", "CreatedDate");
            DropColumn("dbo.Accounts", "CreatedDate");
            DropColumn("dbo.SubAccounts", "CreatedDate");


        }
    }
}
