namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TypeforItemGroups", "Required", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TypeforItemGroups", "Required");
        }
    }
}
