namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountsCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountCategories", "Code", c => c.String());
            AddColumn("dbo.Accounts", "Code", c => c.String());
            AddColumn("dbo.BalanceSheets", "Code", c => c.String());
            AddColumn("dbo.BalanceSheetTypes", "Code", c => c.String());
            AddColumn("dbo.SubAccounts", "Code", c => c.String());
            DropColumn("dbo.AccountCategories", "AccountCategoryCode");
            DropColumn("dbo.Accounts", "AccountCode");
            DropColumn("dbo.BalanceSheets", "BalanceSheetCode");
            DropColumn("dbo.BalanceSheetTypes", "BalanceSheetTypeCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BalanceSheetTypes", "BalanceSheetTypeCode", c => c.String());
            AddColumn("dbo.BalanceSheets", "BalanceSheetCode", c => c.String());
            AddColumn("dbo.Accounts", "AccountCode", c => c.String());
            AddColumn("dbo.AccountCategories", "AccountCategoryCode", c => c.String());
            DropColumn("dbo.SubAccounts", "Code");
            DropColumn("dbo.BalanceSheetTypes", "Code");
            DropColumn("dbo.BalanceSheets", "Code");
            DropColumn("dbo.Accounts", "Code");
            DropColumn("dbo.AccountCategories", "Code");
        }
    }
}
