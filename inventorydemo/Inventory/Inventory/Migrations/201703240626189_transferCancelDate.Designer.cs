// <auto-generated />
namespace Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class transferCancelDate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(transferCancelDate));
        
        string IMigrationMetadata.Id
        {
            get { return "201703240626189_transferCancelDate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
