namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderRequest2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderRequests", "AccountsDataTypesId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderRequests", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderRequests", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.OrderRequestItems", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.OrderRequestItems", "CurrentWorkerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.OrderRequests", "AccountsDataTypesId");
            CreateIndex("dbo.OrderRequests", "CurrentWorkerId");
            CreateIndex("dbo.OrderRequestItems", "CurrentWorkerId");
            AddForeignKey("dbo.OrderRequests", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id",cascadeDelete: false);
            AddForeignKey("dbo.OrderRequests", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderRequestItems", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderRequestItems", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderRequests", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderRequests", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.OrderRequestItems", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderRequests", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderRequests", new[] { "AccountsDataTypesId" });
            DropColumn("dbo.OrderRequestItems", "CurrentWorkerId");
            DropColumn("dbo.OrderRequestItems", "RowVersion");
            DropColumn("dbo.OrderRequests", "CurrentWorkerId");
            DropColumn("dbo.OrderRequests", "RowVersion");
            DropColumn("dbo.OrderRequests", "AccountsDataTypesId");
        }
    }
}
