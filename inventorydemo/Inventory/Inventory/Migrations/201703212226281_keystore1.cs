namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class keystore1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.branchGroupSts");
            AddColumn("dbo.branchGroupSts", "branchGroupStrId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.branchGroupSts", "branchGroupStrId");
            DropColumn("dbo.branchGroupSts", "branchGroupStId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.branchGroupSts", "branchGroupStId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.branchGroupSts");
            DropColumn("dbo.branchGroupSts", "branchGroupStrId");
            AddPrimaryKey("dbo.branchGroupSts", "branchGroupStId");
        }
    }
}
