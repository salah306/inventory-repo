namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderRequests",
                c => new
                    {
                        OrderRequestId = c.Int(nullable: false, identity: true),
                        requestNo = c.Int(nullable: false),
                        mainAccountId = c.Int(nullable: false),
                        tilteAccountId = c.Int(nullable: false),
                        requestDate = c.DateTime(nullable: false),
                        dueDate = c.DateTime(nullable: false),
                        typeName = c.String(),
                        isDone = c.Boolean(nullable: false),
                        isToCancel = c.Boolean(nullable: false),
                        isQty = c.Boolean(nullable: false),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderRequestId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.mainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.tilteAccountId, cascadeDelete: false)
                .Index(t => t.mainAccountId)
                .Index(t => t.tilteAccountId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId);
            
            CreateTable(
                "dbo.OrderRequestItems",
                c => new
                    {
                        OrderRequestItemsId = c.Int(nullable: false, identity: true),
                        note = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OrderRequestId = c.Int(nullable: false),
                        refrenceType = c.String(),
                        refrenceTypeId = c.Int(),
                    })
                .PrimaryKey(t => t.OrderRequestItemsId)
                .ForeignKey("dbo.OrderRequests", t => t.OrderRequestId, cascadeDelete: false)
                .Index(t => t.OrderRequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderRequests", "tilteAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderRequests", "mainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderRequestItems", "OrderRequestId", "dbo.OrderRequests");
            DropForeignKey("dbo.OrderRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderRequests", "BranchId", "dbo.Branches");
            DropIndex("dbo.OrderRequestItems", new[] { "OrderRequestId" });
            DropIndex("dbo.OrderRequests", new[] { "BranchId" });
            DropIndex("dbo.OrderRequests", new[] { "CompanyId" });
            DropIndex("dbo.OrderRequests", new[] { "tilteAccountId" });
            DropIndex("dbo.OrderRequests", new[] { "mainAccountId" });
            DropTable("dbo.OrderRequestItems");
            DropTable("dbo.OrderRequests");
        }
    }
}
