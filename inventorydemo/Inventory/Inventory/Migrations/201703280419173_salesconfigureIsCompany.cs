namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class salesconfigureIsCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Purchases", "IsCompany", c => c.Boolean(nullable: false));
            AddColumn("dbo.Purchases", "CompanyId", c => c.Int());
            AddColumn("dbo.Purchases", "BranchId", c => c.Int());
            AddColumn("dbo.Purchases", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Purchases", "CurrentWorkerId", c => c.String(maxLength: 128));
            AddColumn("dbo.Sales", "IsCompany", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sales", "CompanyId", c => c.Int());
            AddColumn("dbo.Sales", "BranchId", c => c.Int());
            AddColumn("dbo.Sales", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.Sales", "CurrentWorkerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Purchases", "CompanyId");
            CreateIndex("dbo.Purchases", "BranchId");
            CreateIndex("dbo.Purchases", "CurrentWorkerId");
            CreateIndex("dbo.Sales", "CompanyId");
            CreateIndex("dbo.Sales", "BranchId");
            CreateIndex("dbo.Sales", "CurrentWorkerId");
            AddForeignKey("dbo.Purchases", "BranchId", "dbo.Branches", "BranchId");
            AddForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.Purchases", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.Sales", "BranchId", "dbo.Branches", "BranchId");
            AddForeignKey("dbo.Sales", "CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.Sales", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Sales", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Sales", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.Purchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Purchases", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Purchases", "BranchId", "dbo.Branches");
            DropIndex("dbo.Sales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Sales", new[] { "BranchId" });
            DropIndex("dbo.Sales", new[] { "CompanyId" });
            DropIndex("dbo.Purchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.Purchases", new[] { "BranchId" });
            DropIndex("dbo.Purchases", new[] { "CompanyId" });
            DropColumn("dbo.Sales", "CurrentWorkerId");
            DropColumn("dbo.Sales", "RowVersion");
            DropColumn("dbo.Sales", "BranchId");
            DropColumn("dbo.Sales", "CompanyId");
            DropColumn("dbo.Sales", "IsCompany");
            DropColumn("dbo.Purchases", "CurrentWorkerId");
            DropColumn("dbo.Purchases", "RowVersion");
            DropColumn("dbo.Purchases", "BranchId");
            DropColumn("dbo.Purchases", "CompanyId");
            DropColumn("dbo.Purchases", "IsCompany");
        }
    }
}
