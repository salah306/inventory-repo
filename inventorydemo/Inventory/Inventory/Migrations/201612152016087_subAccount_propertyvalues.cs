namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subAccount_propertyvalues : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "SubAccountId", c => c.Int());
            CreateIndex("dbo.PropertyValuesforItemGroups", "SubAccountId");
            AddForeignKey("dbo.PropertyValuesforItemGroups", "SubAccountId", "dbo.SubAccounts", "SubAccountId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PropertyValuesforItemGroups", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.PropertyValuesforItemGroups", new[] { "SubAccountId" });
            DropColumn("dbo.PropertyValuesforItemGroups", "SubAccountId");
        }
    }
}
