namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class branchproperty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BranchProperties",
                c => new
                    {
                        BranchPropertiesId = c.Int(nullable: false, identity: true),
                        BranchId = c.Int(nullable: false),
                        propertyName = c.String(),
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BranchPropertiesId)
                .ForeignKey("dbo.AccountsCatogeryTypesAcccounts", t => t.AccountsCatogeryTypesAcccountsId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountsCatogeryTypesAcccountsId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.BranchPropertyValues",
                c => new
                    {
                        BranchPropertyValuesId = c.Int(nullable: false, identity: true),
                        BranchPropertiesId = c.Int(nullable: false),
                        propertyValue = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.BranchPropertyValuesId)
                .ForeignKey("dbo.BranchProperties", t => t.BranchPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.BranchPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BranchProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BranchPropertyValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BranchPropertyValues", "BranchPropertiesId", "dbo.BranchProperties");
            DropForeignKey("dbo.BranchProperties", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.BranchProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropIndex("dbo.BranchPropertyValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BranchPropertyValues", new[] { "BranchPropertiesId" });
            DropIndex("dbo.BranchProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.BranchProperties", new[] { "AccountsCatogeryTypesAcccountsId" });
            DropIndex("dbo.BranchProperties", new[] { "BranchId" });
            DropTable("dbo.BranchPropertyValues");
            DropTable("dbo.BranchProperties");
        }
    }
}
