namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customfinanacl2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomAccounts", "AccountName", c => c.String());
            AddColumn("dbo.CustomAccounts", "AccountCatogeryName", c => c.String());
            AddColumn("dbo.CustomAccounts", "CurrentWorkerId", c => c.String());
            AddColumn("dbo.FinancialLists", "BranchId", c => c.Int(nullable: false));
            CreateIndex("dbo.FinancialLists", "BranchId");
            AddForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches", "BranchId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches");
            DropIndex("dbo.FinancialLists", new[] { "BranchId" });
            DropColumn("dbo.FinancialLists", "BranchId");
            DropColumn("dbo.CustomAccounts", "CurrentWorkerId");
            DropColumn("dbo.CustomAccounts", "AccountCatogeryName");
            DropColumn("dbo.CustomAccounts", "AccountName");
        }
    }
}
