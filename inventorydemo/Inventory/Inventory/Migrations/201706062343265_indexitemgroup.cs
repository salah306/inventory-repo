namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class indexitemgroup : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ItemGroups", new[] { "CompanyId" });
            DropIndex("dbo.Units", new[] { "CompanyId" });
            AlterColumn("dbo.ItemGroups", "ItemGroupName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Units", "UnitName", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.ItemGroups", new[] { "ItemGroupName", "CompanyId" }, unique: true, name: "IX_ItemGroupName");
            CreateIndex("dbo.Units", new[] { "UnitName", "CompanyId" }, unique: true, name: "IX_UnitName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Units", "IX_UnitName");
            DropIndex("dbo.ItemGroups", "IX_ItemGroupName");
            AlterColumn("dbo.Units", "UnitName", c => c.String());
            AlterColumn("dbo.ItemGroups", "ItemGroupName", c => c.String());
            CreateIndex("dbo.Units", "CompanyId");
            CreateIndex("dbo.ItemGroups", "CompanyId");
        }
    }
}
