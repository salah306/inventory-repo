namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingsRole3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUser_settingPropertiesforUserId", "dbo.settingPropertiesforUsers");
            DropIndex("dbo.settingRolesForUsers", new[] { "settingPropertiesforUser_settingPropertiesforUserId" });
            RenameColumn(table: "dbo.settingRolesForUsers", name: "settingPropertiesforUser_settingPropertiesforUserId", newName: "settingPropertiesforUserId");
            AddColumn("dbo.settingRolesForUsers", "settingRoleId", c => c.Int(nullable: false));
            AlterColumn("dbo.settingRolesForUsers", "roleName", c => c.String());
            AlterColumn("dbo.settingRolesForUsers", "settingPropertiesforUserId", c => c.Int(nullable: false));
            CreateIndex("dbo.settingRolesForUsers", "settingPropertiesforUserId");
            CreateIndex("dbo.settingRolesForUsers", "settingRoleId");
            AddForeignKey("dbo.settingRolesForUsers", "settingRoleId", "dbo.settingRoles", "settingRoleId",  cascadeDelete: false);
            AddForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUserId", "dbo.settingPropertiesforUsers", "settingPropertiesforUserId",  cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUserId", "dbo.settingPropertiesforUsers");
            DropForeignKey("dbo.settingRolesForUsers", "settingRoleId", "dbo.settingRoles");
            DropIndex("dbo.settingRolesForUsers", new[] { "settingRoleId" });
            DropIndex("dbo.settingRolesForUsers", new[] { "settingPropertiesforUserId" });
            AlterColumn("dbo.settingRolesForUsers", "settingPropertiesforUserId", c => c.Int());
            AlterColumn("dbo.settingRolesForUsers", "roleName", c => c.Int(nullable: false));
            DropColumn("dbo.settingRolesForUsers", "settingRoleId");
            RenameColumn(table: "dbo.settingRolesForUsers", name: "settingPropertiesforUserId", newName: "settingPropertiesforUser_settingPropertiesforUserId");
            CreateIndex("dbo.settingRolesForUsers", "settingPropertiesforUser_settingPropertiesforUserId");
            AddForeignKey("dbo.settingRolesForUsers", "settingPropertiesforUser_settingPropertiesforUserId", "dbo.settingPropertiesforUsers", "settingPropertiesforUserId");
        }
    }
}
