namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uniqueId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillAccountItems", "BillSubAccountItemsId", "dbo.BillSubAccountItems");
            DropIndex("dbo.BillAccountItems", new[] { "BillSubAccountItemsId" });
            AddColumn("dbo.BillAccountItems", "uniqueId", c => c.String());
            AddColumn("dbo.BillSubAccountItems", "uniqueId", c => c.String());
            DropColumn("dbo.BillAccountItems", "BillSubAccountItemsId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BillAccountItems", "BillSubAccountItemsId", c => c.Int(nullable: false));
            DropColumn("dbo.BillSubAccountItems", "uniqueId");
            DropColumn("dbo.BillAccountItems", "uniqueId");
            CreateIndex("dbo.BillAccountItems", "BillSubAccountItemsId");
            AddForeignKey("dbo.BillAccountItems", "BillSubAccountItemsId", "dbo.BillSubAccountItems", "BillSubAccountItemsId", cascadeDelete: true);
        }
    }
}
