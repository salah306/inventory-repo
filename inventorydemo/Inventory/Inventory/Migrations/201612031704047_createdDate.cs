namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createdDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSheets", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));

        }

        public override void Down()
        {
            DropColumn("dbo.BalanceSheets", "CreatedDate");
        }
    }
}
