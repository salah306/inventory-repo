﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountPropertyVM
    {
        public string propertyId { get; set; }
        public string propertyName { get; set; }
        public IQueryable<AccountPropertyValuesVM> propertyValues { get; set; }
        public Int32 PublicId { get; set; }

    }

    public class mainAccName
    {
        public string accountName { get; set; }
        public string accountType { get; set; }
        public bool isNew { get; set; }
        public Int32 accountId { get; set; }
        public bool active { get; set; }
    }
    public class accountPropetyCompanyBranch
    {
        public int CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
    }

    public class accountPropetywaiting
    {
        public string accountType { get; set; }
        public string waitType { get; set; }
    }

    public class OrderWaitingVm
    {
        public int id { get; set; }
        public string type { get; set; }
        public int orderNo { get; set; }
        public DateTime date { get; set; }
        public string user { get; set; }
        public decimal amount { get; set; }
        public string typeName { get; set; }
    }

    public class OrdersWaitingVm
    {
        public string type { get; set; }
        public List<OrderWaitingVm> orders { get; set; }
    }

    public class AddaccountPropetyCompanyBranch
    {
        public int CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public string accountCategoryPropertiesName { get; set; }
        public string typeName { get; set; }
        public Int32 accountCategoryPropertiesId { get; set; }
    }

    public class getAccountMovementsproperty
    {
        public Int32 id { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string search { get; set; }
        public string orderby { get; set; }
        public bool orderType { get; set; }
        public bool IsCompany { get; set; }
        public bool AllAccounts { get; set; }
        public Int32 companyId { get; set; }
        public string accountType { get; set; }
    }


    public class getitemInfoVM
    {
      
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public string search { get; set; }
        public string orderby { get; set; }
        public Int32 InventoryId { get; set; }
        public Int32 GroupId { get; set; }
        public bool orderType { get;  set; }
    }
    public class itemInfoToReturnwithCount
    {
        public List<itemInfoToReturn> items { get; set; }
        public int count { get; set; }
        public double totalPages { get;  set; }
        public int pageSize { get; set; }
    }

    public class iteminfoandCount
    {
        public List<itemInfoToReturn> items { get; set; }
        public Int32 totalcount { get; set; }
    }
    public class itemInfoToReturn
    {
     

        public Int32 itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public string groupName { get; set; }
        public Int32 groupId { get; set; }
        public string unitName { get; set; }
        public Int32 unitId { get; set; }
        public string unitType { get; set; }
        public Int32 unitTypeId { get; set; }
        public decimal unitPrice { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal costPrice { get { return this.qty > 0? Math.Round((this.totalcostPrice / this.qty) , 2) : 0; } }
        public decimal totalcostPrice { get; set; }
        public DateTime? LastMoveDate { get; set; }
        public Int32 InventoryId { get; set; }
        public string InventoryName { get; set; }
        public List<itemInventoryDetailsVM> Inventory { get; set; }
        public string reorderPoint { get;  set; }
        public bool showInventory { get; set; }
        public string expireinDayes { get;  set; }
        public decimal COGS { get;  set; }
        public decimal AvgInventory { get;  set; }
        public decimal turnOverRate { get { return this.AvgInventory > 0 ?Math.Round((this.COGS / this.AvgInventory) , 2) : 0; } }
        public decimal avgStock { get { return turnOverRate > 0 ? Math.Round((this.totalDays / turnOverRate)) : 0; } }
        public int totalDays { get; set; }


    }
  
   
    public class itemInventoryDetailsVM
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal costPrice { get; set; }
    }

    public class AddaccountPropetyValueCompanyBranch
    {
        public int CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public Int32 accountCategoryPropertiesId { get; set; }
        public Int32 accountId { get; set; }
        public string value { get; set; }
        public Int32 valueId { get; set; }
    }
    public class getAccountInfoAndProp
    {
        public int CompanyId { get; set; }
        public bool isCompany { get; set; }
        public string accountType { get; set; }
        public Int32 accountId { get; set; }
    }

    public class accountInfoToreturn
    {

        public List<accountPropertiesValuesVm> accountValues { get; set; }
    }
    public class accountPropertiesValuesVm
    {
        public Int32 valueId { get; set; }
        public string valueName { get; set; }
        public Int32 accountCategoryPropertiesId { get; set; }

    }

    public class accountCategoryPropertiesVm
    {
        // { accountCategoryPropertiesId: 1, accountCategoryPropertiesName: "تليفون", typeName: "هاتف ارضي" },

        public Int32 accountCategoryPropertiesId { get; set; }
        public string accountCategoryPropertiesName { get; set; }
        public Int32 typeId { get; set; }
        public string typeName { get; set; }
    }

    public class accountsName
    {

        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public Int32 AccountCatogeryId { get; set; }
        public string code { get; set; }
    }

    public class accCatogeryTypesAccountVm
    {
        public Int32 typeId { get; set; }
        public string typeName { get; set; }
    }
    public class ReturnedAccountProperty
    {
        public List<accountCategoryPropertiesVm> properties { get; set; }
        public List<accountsName> accountsName { get; set; }
        public List<accCatogeryTypesAccountVm> types { get; set; }
        public int waitingOrder { get; set; }
        public int waitingRequest { get; set; }
        public int notfy { get; set; }

    }

    public class AccountsMovesProperty
    {
        public Int32 moveId { get; set; }
        public string note { get; set; }
        public string accountName { get; set; }
        public decimal debit { get; set; }
        public decimal crdit { get; set; }
        public decimal pBalance { get; set; }
        public decimal Balance { get; set; }
        public string spBalance { get; set; }
        public string sBalance { get; set; }
        public Int32 indexdId { get; set; }

    }

}