﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountCategoriesVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string Debit { get; set; }
        public string Crdit { get; set; }
        public string BalanceSheetTypesId { get; set; }
        public Int32 PublicId { get; set; }
        public accountTypeAccVm accountType { get; set; }
        public virtual List<AccountsVm> Accounts { get; set; }
        public virtual List<UserRole> UserRoles { get; set; }

    }

    public class accountTypeAccVm
    {
        public string TypeName { get; set; }
        public string aliasName { get; set; }
    }

    public class AccountCatogeryLedger
    {
        public Int32 id { get; set; }
        public string name { get; set; }
        public int Code { get; set; }
        public decimal? debit { get; set; }
        public decimal? crdit { get; set; }
        //use as parentName , parentId
        public Int32 AccountCatogeryId { get; set; }
        public string AccountCatogeryName { get; set; }
        public Int32 branchId { get; set; }
        public decimal total { get; set; }
        public string summery { get; set; }
        public string summeryDate { get; set; }
        public string psummeryDate { get; set; }
        public DateTime? minDate { get; set; }
        public DateTime? maxDate { get; set; }

        public decimal? sumDebit { get; set; }
        public decimal? sumCrdit { get; set; }
    }
    public class AccountCategoryPropertiesVM
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string DataTypeName { get; set; }
        public Int32   DataTypeId { get; set; }
        public string AccountCategoryName { get; set; }
        public Int32 AccountCategoryId { get; set; }

    }

    public  class AccountCategoryPropertiesValuesVM
    {
        public Int32 Id { get; set; }
        public string PropertName { get; set; }
        public string DataTypeName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public List<AccPropertiesValuesVM> Values { get; set; }
        
    }

    public class AccPropertiesValuesVM
    {
        public Int32 Id { get; set; }
        public string value { get; set; }
      
    }


    public class SetAccPropertiesValuesVM
    {
        public Int32 AccountPropertyId { get; set; }
        public string value { get; set; }
        public string DataTypeName { get; set; }
        public Int32 SubAccountId { get; set; }

    }

    public class UpdateAccPropertiesValuesVM
    {
        public Int32 AccountPropertyvalId { get; set; }
        public string value { get; set; }
       

    }


}