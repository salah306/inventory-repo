﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class BranchesVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public Int32 CompaniesId { get; set; }
        public Int32 PublicId { get; set; }
    }

    public class BranchesGroupVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string companyName { get; set; }
        public string fullName { get; set; }
        public bool isCompany { get; set; }
        public Int32 companyId { get; set; }
        public string realName { get; set; }
        public Int32 oId { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string phone2 { get; set; }
        public string email { get; set; }
    }
 


}