﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class BalanceSheetsVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string BranchId { get; set; }
        public Int32 PublicId { get; set; }
        public Int32 BranchesId { get; set; }
        public string accounttype { get; set; }
        public virtual List<BalanceSheetTypesVm> BalanceSheetTypes { get; set; }
        public virtual List<UserRole> UserRoles { get; set; }

    }

    public class BalanceSheetNode
    {
        public string text { get; set; }
        public string old { get; set; }
        public string[] parents { get; set; }
    }

    public class UpdateBalanceSheetNode
    {
        public string text { get; set; }
        public string old { get; set; }
        public string[] parents { get; set; }
    }

    public class GetAccountsForList
    {
        public Int32 companyId { get; set; }
        public string accountCatogeryId { get; set; }

    }

}