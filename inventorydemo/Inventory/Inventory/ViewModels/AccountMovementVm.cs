﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountMovementVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public DateTime mdate { get; set; }
        public string Debit { get; set; }
        public string Crdit { get; set; }
        public IQueryable<SecondAccount> SecondAccount { get; set; }
        public string BranchName {get; set; }
        public Int32 AccountOrderId { get; set; }
        public string AccountOrderPropertiesName { get; set; }
        public string Note { get; set; }
        public Int32 PublicId { get; set; }

    }

    public class SecondAccount
    {
        public string SecondAccountName { get; set; }
        public string SecondAccountCategory { get; set; }
        public string Debit { get; set; }
        public string Crdit { get; set; }
        public string BranchName { get; set; }
        public string Note { get; set; }


    }

    public class getAccountMovements
    {
        public Int32 id { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string search { get; set; }
        public string orderby { get; set; }
        public bool orderType { get; set; }
        public bool IsCompany { get; set; }
        public bool AllAccounts { get; set; }
    }

    public class getfinancialStatements
    {
        public string id { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public bool IsCompany { get; set; }
        public bool AllAccounts { get; set; }

    }


    public class AccountsMoves
    {
        public Int32 moveId { get; set; }
        public string note { get; set; }
        public  decimal debit { get; set; }
        public decimal crdit { get; set; }
        public DateTime date { get; set; }
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public Int32 orderId { get; set; }
        public Int32 orderNo { get; set; }
        public Int32? branchId { get; set; }
        public string summery { get; set; }
        public string summeryDate { get; set; }
        public string PsummeryDate { get; set; }
        public decimal Pdebit { get; set; }
        public decimal Pcrdit { get; set; }
        public bool iscomplex { get; set; }
        public bool IsCompany { get; set; }
        public Int32? CompanyId { get; set; }
        public string orderNote { get; set; }
        public Int32? refrenceId { get; set; }
        public string typeRefrence { get; set; }
        public Int32 indexdId { get; set; }
        public decimal firstB { get; set; }
        public decimal balance { get; set; }

    }

    public class AccountsMovesforProperty
    {
        public Int32 moveId { get; set; }
        public string note { get; set; }
        public decimal debit { get; set; }
        public decimal crdit { get; set; }
        public decimal balance { get; set; }
        public DateTime date { get; set; }
        public Int32 orderNo { get; set; }
        public string orderNote { get; set; }
        public Int32? refrenceId { get; set; }
        public string typeRefrence { get; set; }
        public Int32 indexdId { get; set; }

    }
    public class GetSetOrder
    {
        public Int32 orderId { get; set; }
        public Int32 orderNo { get; set; }
        public Int32 branchId { get; set; }
        public DateTime orderDate { get; set; }
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public List<OrderAccounts> secondAccount { get; set; }
        public bool isDebit { get; set; }
        public string tilte { get; set; }
        public string orderNote { get; set; }
        public bool IsCompany { get; set; }


    }


}