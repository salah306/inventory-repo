﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountOrderPropertiesVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public Int32 PublicId { get; set; }

    }
}