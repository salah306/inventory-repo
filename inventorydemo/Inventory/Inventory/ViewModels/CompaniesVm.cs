﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Inventory.ViewModels
{
    public class CompaniesVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string CurrentWorkerId { get; set; }
        public Int32 PublicId { get; set; }
        public virtual List<BranchesVm> Branches { get; set; }

        
    }

    public class getCompanyBranchproperty
    {
        public Int32 comapnyId { get; set; }
        public bool iscompany { get; set; }


    }

    public class SetCompanyBranchpropertyValue
    {
        public Int32 comapnyId { get; set; }
        public bool iscompany { get; set; }
        public Int32 propertiesId { get; set; }
        public Int32 ValueId  { get; set; }
        public string value { get; set; }

    }
    public class setCompanyBranchproperty
    {
        public Int32 comapnyId { get; set; }
        public bool iscompany { get; set; }
        public Int32 propertiesId { get; set; }
        public string propertiesName { get; set; }
        public string typeName { get; set; }
    }

    public class companybranchInfo
    {
        public Int32 comapnyId { get; set; }
        public Int32? branchId { get; set; }
        public string name { get; set; }
        public bool iscompany { get; set; }
        public List<CompanyBranchProperty> properties { get; set; }
    }

    public class CompanyBranchProperty
    {
        public Int32 propertiesId { get; set; }
        public string propertiesName { get; set; }
        public string typeName { get; set; }
        public Int32 comapnyId { get; set; }
        public Int32? branchId { get; set; }
        public List<CompanyBranchPropertyValue> propertyValue { get; set; }
    }

    public class CompanyBranchPropertyValue
    {
        public int valueId { get; set; }
        public string value { get; set; }
        public Int32 propertiesId { get; set; }
    }

    public class checkUserEmail
    {
        
        public string Email { get; set; }
    }

   
}