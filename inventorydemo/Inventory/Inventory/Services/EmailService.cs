﻿using Microsoft.AspNet.Identity;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using Twilio;

namespace Inventory.Services
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
           


            await configSendGridasync(message);


        }


     

        // Use NuGet to install SendGrid (Basic C# client lib) 
        private async Task configSendGridasync(IdentityMessage message)
        {
          
                var myMessage = new SendGridMessage();

                myMessage.AddTo(message.Destination);
                myMessage.From = new System.Net.Mail.MailAddress("customerservice@sheetScripts.com", "SheetScript");
                myMessage.Subject = message.Subject;
                myMessage.Text = message.Body;
                myMessage.Html = message.Body;

                var credentials = new NetworkCredential(ConfigurationManager.AppSettings["emailService:Account"],
                                                        ConfigurationManager.AppSettings["emailService:Password"]);

                // Create a Web transport for sending email.
                var transportWeb = new Web(credentials);

                // Send the email.
                if (transportWeb != null)
                {
                    await transportWeb.DeliverAsync(myMessage);
                }
                else
                {
                    //Trace.TraceError("Failed to create Web transport.");
                    await Task.FromResult(0);
                }


           
          

        }
    }


    public class SmsService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            try
            {
                const string accountSid = "PN5d1a49ff9966e898605f776a4ab3fab0";
                const string authToken = "8181fefcc52ad76d43d6e0e1ec184c41";
                const string phoneNumber = "+18442575509";

                var twilioRestClient = new TwilioRestClient(accountSid, authToken);
             
                // Send the email.
                if (twilioRestClient != null)
                {
                    twilioRestClient.SendMessage(phoneNumber ,message.Destination, message.Body);

                }
                else
                {
                    //Trace.TraceError("Failed to create Web transport.");
                    await Task.FromResult(0);
                }

            }
            catch (Exception ex)
            {
                var exe = ex;
                throw;
            }
          
        }
    }

}