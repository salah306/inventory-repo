﻿using Inventory.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inventory.Services;
using System.Threading.Tasks;
using Inventory.Controllers;
using Inventory.ViewModels;
using System.Text.RegularExpressions;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;

namespace Inventory.DataLayer
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
          
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var appDbContext = context.Get<ApplicationDbContext>();
            var appUserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(appDbContext));

            // Configure validation logic for usernames
            appUserManager.UserValidator = new UserValidator<ApplicationUser>(appUserManager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            appUserManager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
            
            appUserManager.EmailService = new Services.EmailService() ;
            appUserManager.SmsService = new SmsService();


            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                appUserManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    //Code for email confirmation and reset password life time
                    TokenLifespan = TimeSpan.FromHours(6)
                };
            }


        
          
            return appUserManager;
        }


        public string GenerateRandomNo()
        {
            int _min = 0000000;
            int _max = 9999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }


    }

    public class CompanyUserManager
    {
        /// <summary>
        /// <para>Company</para> 
        /// <para>Branch</para> 
        /// <para>BalanceSheetSetup</para>  
        /// <para>BalanceSheetTypeSetup</para> 
        /// <para>AccountCategorySetup</para> 
        /// <para>GeneralJournal</para> 
        /// <para>SubGl</para> 
        /// <para>Gl</para> 
        /// <para>FinancialLists</para> 
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="byRefrenceType"></param>
        /// <param name="typeName"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<settingRolesForUser> GetNewUserRole(string uid, bool byRefrenceType, string typeName, string name)
        {
            var db = new ApplicationDbContext();
            var getrole = db.settingRolesForUser.Where(s => s.settingPropertiesforUser.settingProperties.name == name
                                                              && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == typeName).ToList();


            return getrole;
        }
        public IQueryable<BranchesGroupVm> GetBranchesGroup(string uid, bool IsCompany)
        {
            // setChartforCompany(1005);

            var db = new ApplicationDbContext();
            //var getCompany = db.settingRolesForUser.Where(s => s.settingPropertiesforUser.settingProperties.name == "Company"
            //                                                    && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Company")
            //                                                    .Select(a => a.RefrenceId);

            var returndCompany = from s in db.settingRolesForUser
                              where s.settingPropertiesforUser.settingProperties.name == "Company"
                              && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Company"
                             join c in db.Companies on s.RefrenceId equals c.CompanyId
                              select new BranchesGroupVm()
                              {
                                  id = c.CompanyId + "C",
                                  name = c.CompanyName,
                                  companyName = "شركات",
                                  fullName = c.CompanyName,
                                  isCompany = true,
                                  companyId = c.CompanyId,
                                  realName = c.CompanyName,
                                  oId = c.CompanyId,
                              };

            var returndBranch = from s in db.settingRolesForUser
                              where s.settingPropertiesforUser.settingProperties.name == "Branch"
                              && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Branch"
                             join c in db.Branchs on s.RefrenceId equals c.BranchId
                              select new BranchesGroupVm()
                              {
                                  id = c.BranchId + "B",
                                  name = c.BranchName + " - " + c.Company.CompanyName,
                                  companyName = "فروع",
                                  fullName = c.BranchName + " - " + c.Company.CompanyName,
                                  isCompany = false,
                                  companyId = c.CompanyId,
                                  realName = c.BranchName,
                                  oId = c.BranchId
                              };
            returndCompany.ToList().AddRange(returndBranch);
            return returndCompany;

        }

        public OrderRequestMoveVm getOrderRequestMoveById(getOrderRequest or)
        {
            var db = new ApplicationDbContext();
            var accType = "";

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "securitiesrecive":
                    accType = "اوراق قبض";
                    break;
                case "securitiespay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }
            if (string.IsNullOrEmpty(accType))
            {
                return null;
            }
            Int32 companyId = 0;

            if (or.isCompany)
            {
                companyId = or.CompanyId;

            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }

            var findR = db.OrderMoveRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.OrderNo == or.requestNo && a.typeName == or.typeName);
            if (findR == null)
            {
               return null;
            }
            var r = db.orderRequest.Find(findR.orderRequestId);
            OrderRequestMoveVm om = new OrderRequestMoveVm()
            {
                OrderNo = findR.OrderNo,
                mainAccount = new accountforRequestVm() { accountId = findR.mainAccountId, accountName = findR.mainAccount.AccountName, Code = findR.mainAccount.Code },
                tilteAccount = new accountforRequestVm() { accountId = findR.tilteAccountId, accountName = findR.tilteAccount.AccountName, Code = findR.tilteAccount.Code },
                typeName = findR.typeName,
                accountType = or.accountType,
                amount = findR.amount,
                OrderDate = findR.OrderDate,
                isDone = findR.isDone,
                note = findR.note,
                isToCancel = findR.isToCancel,
                orderMoveRequestId = 0,
                attchedRequest = findR.attchedRequest,
                isNewRequest = false,
                requestNo = findR.attchedRequest ? findR.orderRequestId.GetValueOrDefault() : 0,
                userName = db.Users.Find(findR.CurrentWorkerId) == null ? "" : db.Users.Where(u => u.Id == findR.CurrentWorkerId).Select(a => a.FirstName + " " + a.LastName).SingleOrDefault(),
                RequestTypeName = findR.attchedRequest ? findR.orderRequest.AccountsDataTypes.TypeName : "",
                orderRequest = new OrderRequesVm()
                {
                    
                    requestNo = r.requestNo,
                    accountType = r.AccountsDataTypes.TypeName,
                    requestDate = r.requestDate,
                    isDone = r.isDone,
                    isQty = r.isQty,
                    mainAccount = new accountforRequestVm() { accountId = r.mainAccountId, accountName = r.mainAccount.AccountName, Code = r.mainAccount.Code },
                    tilteAccount = new accountforRequestVm() { accountId = r.mainAccountId, accountName = r.mainAccount.AccountName, Code = r.mainAccount.Code },
                    isNewRequest = false,
                    isToCancel = r.isToCancel,
                    orderRequestId = r.OrderRequestId,
                    typeName = r.typeName,
                    dueDate = findR.OrderDate,
                    userName = db.Users.Find(r.CurrentWorkerId) == null ? "" : db.Users.Where(u => u.Id == r.CurrentWorkerId).Select(a => a.FirstName + " " + a.LastName).SingleOrDefault(),
                    items = r.items.Select(i => new OrderRequestitemsVm
                    {
                        note = i.note,
                        price = i.price,
                        qty = i.qty,
                        amount = !r.isQty ? i.price : (i.price * i.qty),
                        refrenceType = i.refrenceType,
                        refrenceTypeId = i.refrenceTypeId.GetValueOrDefault(),
                        OrderRequestitemsId = i.OrderRequestItemsId
                    }).ToList()
                }
            };
            return om;
        }
        
        public void setTempItems(Int32 companyId , Int32 iGId)
        {

          


            new System.Threading.Tasks.Task(() =>
            {
                var db = new ApplicationDbContext();
              


                var itemsValues = db.PropertyValuesforItemGroups.Where(i => i.PropertiesforItemGroup.ItemGroup.CompanyId == companyId && i.PropertiesforItemGroup.ItemGroupId == iGId).ToList();
                var ItemstoRemove = db.TempItems.Where(a => a.CompanyId == companyId && a.ItemGroupId == iGId);
                if (ItemstoRemove != null && ItemstoRemove.Count() > 0)
                {
                    db.TempItems.RemoveRange(ItemstoRemove);
                    
                
                }
                
                if (itemsValues != null)
                {
                  
                    var iG = itemsValues.Where(t => t.parintType == "كود" || t.parintType == "صنف" || t.parintType == "سعر" || t.parintType == "حد الطلب" || t.parintType == "مدة صلاحية").GroupBy(a => a.Realted)
                        .Select(a => new
                        {
                            code = a.FirstOrDefault(c => c.parintType == "كود").PropertyValuesforItemGroupName,
                            name = a.Where(c => c.parintType == "صنف").Select(c => c.PropertyValuesforItemGroupName).FirstOrDefault(),
                            price = a.FirstOrDefault(c => c.parintType == "سعر").PropertyValuesforItemGroupName,
                            reorderPoint = a.FirstOrDefault(c => c.parintType == "حد الطلب").PropertyValuesforItemGroupName,
                            ItemGroupId = a.FirstOrDefault().PropertiesforItemGroup.ItemGroupId,
                            UnitId = a.FirstOrDefault().PropertiesforItemGroup.ItemGroup.UnitId,
                            ExpireinDayes = a.Where(c => c.parintType == "مدة صلاحية").Select(c => c.PropertyValuesforItemGroupName).DefaultIfEmpty("0").FirstOrDefault(),
                            Realted = a.Key
                        }).ToList();

                    var tempI = iG.Select(a => new TempItem()
                    {
                        code = a.code,
                        price = ConvertTodecmial(a.price),
                        name = a.name,
                        UnitId = a.UnitId,
                        ItemGroupId = a.ItemGroupId,
                        CompanyId = companyId,
                        ExpireinDayes = a.ExpireinDayes,
                        Realted = a.Realted,
                        reorderPoint = a.reorderPoint

                    }).ToList();
                  
                    db.TempItems.AddRange(tempI);
                    db.SaveChanges();
                };
            }).Start();

        }
        public void setAccountsCodes(Int32 companyId)
        {
            new System.Threading.Tasks.Task(() =>
            {
                var db = new ApplicationDbContext();
                Company company = db.Companies.Find(companyId);

                if (company != null)
                {
                    var Tbs = (from bs in db.BalanceSheets
                               where bs.CompanyId == companyId
                               orderby bs.CreatedDate
                               select new AccountsTable
                               {
                                   Id = bs.BalanceSheetId + "Bs",
                                   AccountLevel = "رئيسي",
                                   AccountName = bs.BalanceSheetName,
                                   TitleForAdd = "اضافة حـ عام",
                                   Code = db.BalanceSheets.Where(a => a.CompanyId == companyId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                                   ParentId = bs.CompanyId.ToString(),
                                   AccountType = bs.AccountType.AccountTypeName

                               }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                    var Tbst = (from bst in db.BalanceSheetTypes
                                where bst.BalanceSheet.CompanyId == companyId
                                orderby bst.CreatedDate
                                select new AccountsTable
                                {
                                    Id = bst.BalanceSheetTypeId + "Bst",
                                    AccountLevel = "عام",
                                    AccountName = bst.BalanceSheetTypeName,
                                    TitleForAdd = "اضافة حـ مساعد",
                                    ParentId = bst.BalanceSheetId + "Bs",
                                    AccountType = bst.AccountType.AccountTypeName,
                                    Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                                }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                    List<AccountsTable> Taccounts = new List<AccountsTable>();


                    var TAcc = (from Acc in db.AccountCategories
                                where Acc.BalanceSheetType.BalanceSheet.CompanyId == companyId
                                orderby Acc.CreatedDate
                                select new AccountsTable
                                {
                                    Id = Acc.AccountCategoryId + "Acc",
                                    AccountLevel = "مساعد",
                                    AccountName = Acc.AccountCategoryName,
                                    AccountType = Acc.AccountType.AccountTypeName,
                                    TitleForAdd = "اضافة حـ فرعي",
                                    Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                                    ParentId = Acc.BalanceSheetTypeId + "Bst",
                                    AccountsDataTypesId = (Int32)Acc.AccountsDataTypesId,
                                    AccountsDataTypesName = Acc.AccountsDataTypes.TypeName,
                                }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




                    var TAc = (from Ac in db.Accounts
                               where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId
                               orderby Ac.CreatedDate
                               select new AccountsTable
                               {
                                   Id = Ac.AccountId + "Ac",
                                   AccountLevel = "فرعي",
                                   TitleForAdd = "اضافة حـ جزئي",
                                   AccountName = Ac.AccountName,
                                   Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                                   ParentId = Ac.AccountCategoryId + "Acc",
                                   linkedAccountId = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId : Ac.BranchId,
                                   linkedAccountName = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : Ac.Branch.BranchName,
                                   isCompany = Ac.Branch == null ? true : false,

                                   Activated = Ac.Activated
                               }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                    var TSac = (from Ac in db.SubAccounts
                                where Ac.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId
                                orderby Ac.CreatedDate
                                select new AccountsTable
                                {
                                    Id = Ac.SubAccountId + "Sac",
                                    AccountLevel = "جزئي",
                                    AccountName = Ac.SubAccountName,
                                    Code = db.SubAccounts.Where(a => a.AccountId == Ac.AccountId).OrderBy(a => a.CreatedDate).Count(x => x.SubAccountId < Ac.SubAccountId) + 1,
                                    ParentId = Ac.AccountId + "Ac"
                                }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                    foreach (var acc in TAc)
                    {
                        foreach (var ac in TSac)
                        {

                            if (ac[ac.Count - 1].ParentId == acc[0].Id)
                            {
                                ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                                acc.AddRange(ac);
                            }
                        }
                    }

                    foreach (var acc in TAcc)
                    {
                        foreach (var ac in TAc)
                        {

                            if (ac[0].ParentId == acc[0].Id)
                            {

                                acc.AddRange(ac);
                            }
                        }
                    }

                    foreach (var bst in Tbst)
                    {
                        foreach (var acc in TAcc)
                        {

                            if (acc[0].ParentId == bst[0].Id)
                            {
                                bst.AddRange(acc);
                            }
                        }
                    }

                    foreach (var bs in Tbs)
                    {
                        foreach (var bst in Tbst)
                        {

                            if (bst[0].ParentId == bs[0].Id)
                            {


                                bs.AddRange(bst);

                            }
                        }
                    }

                    foreach (var i in Tbs)
                    {
                        foreach (var a in i)
                        {
                            i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                        }
                    }
                    Tbs.OrderBy(a => a[0].Code);

                    foreach (var acc in Tbs)
                    {
                        foreach (var a in acc)
                        {
                            if (a.AccountLevel == "رئيسي")
                            {
                                var id = ConvertToId(a.Id);
                                var account = db.BalanceSheets.Find(id);
                                account.Code = a.Code.ToString();
                                if (account != null)
                                {
                                    db.Entry(account).State = EntityState.Modified;
                                    db.SaveChanges();
                                }

                            }
                            if (a.AccountLevel == "عام")
                            {
                                var id = ConvertToId(a.Id);
                                var account = db.BalanceSheetTypes.Find(id);
                                account.Code = a.Code.ToString();
                                if (account != null)
                                {
                                    db.Entry(account).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            if (a.AccountLevel == "مساعد")
                            {
                                var id = ConvertToId(a.Id);
                                var account = db.AccountCategories.Find(id);
                                account.Code = a.Code.ToString();
                                if (account != null)
                                {
                                    db.Entry(account).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            if (a.AccountLevel == "فرعي")
                            {
                                var id = ConvertToId(a.Id);
                                var account = db.Accounts.Find(id);
                                account.Code = a.Code.ToString();
                                if (account != null)
                                {
                                    db.Entry(account).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                            if (a.AccountLevel == "جزئي")
                            {
                                var id = ConvertToId(a.Id);
                                var account = db.SubAccounts.Find(id);
                                account.Code = a.Code.ToString();
                                if (account != null)
                                {
                                    db.Entry(account).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }
                    };
                }


            }).Start();
        }

        public bool CheckDataType(string value , string type)
        {
            bool successfullyParsed = false;
           

            switch (type)
            {
                case "عدد صحيح":
                    Int32 intId = 0;
                    successfullyParsed = int.TryParse(value, out intId);
                    break;
                case "عدد عشري":
                    decimal decimalId = 0.0m;
                    successfullyParsed = decimal.TryParse(value, out decimalId);

                    break;
                case "بريد الكتروني":
                    successfullyParsed = IsValidEmail(value);
                    break;
                case "هاتف محمول":
                    
                    successfullyParsed = Int32.TryParse(value, out intId);
                    break;
                case "هاتف ارضي":
                    successfullyParsed = Int32.TryParse(value, out intId);
                    break;
                case "ملاحظات":
                    successfullyParsed = true;
                    break;
                case "عنوان":
                    successfullyParsed =true;
                    break;
                case "صور - مستندات":
                    successfullyParsed = true;
                    break;
                default :
                    successfullyParsed = false;
                    break;


            }

            return successfullyParsed;
        }

        public List<OrderVm> GetAccountOrders(Int32? branchId , bool isCompany)
        {
            var db = new ApplicationDbContext();

            List<OrderVm> orderlist = new List<OrderVm>();
            if (isCompany)
            {
                var orders = (from o in db.AccountOrders
                              where o.CompanyId == branchId
                              && o.IsCompany == true
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  IsCompany =true,
                                  branchId = o.BranchId,
                                  CompanyId = o.CompanyId,
                                  orderNote = o.OrderNote,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }
                   ).ToList();

                orderlist = orders;
            }
            else
            {
                var orders = (from o in db.AccountOrders
                              where o.BranchId == branchId
                              && o.IsCompany == false
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  IsCompany = false,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  CompanyId = o.CompanyId,
                                  orderNote = o.OrderNote,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }
           ).ToList();
                orderlist = orders;

            }
            return orderlist;
        }

        public GetSetOrder GetOrder (GetSetOrder getOrder)
        {

         

            // isdebit mean : اذن صرف
            var db = new ApplicationDbContext();
            var order = db.AccountOrders.Find(getOrder.orderId);
            var firstAccount = db.Accounts.Find(getOrder.accountId);
            if (getOrder.isDebit)
            {
                var accType = firstAccount.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                if(accType == "دائن")
                {
                    getOrder.isDebit = false;
                }
            }
            else if(!getOrder.isDebit)
            {
                var accType = firstAccount.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                if (accType == "دائن")
                {
                    getOrder.isDebit =true;
                }
            }
            var grder = new GetSetOrder()
            {
              branchId = getOrder.branchId,
              accountId = getOrder.accountId,
              accountName = getOrder.isDebit ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ",
              isDebit = getOrder.isDebit
            };
            if(order != null)
            {
            
                var isde = db.AccountMovements.Any(a => a.AccountOrderId == getOrder.orderId && a.AccountId == getOrder.accountId && a.Debit > 0);
                var checkCounts = GetAccountOrders(getOrder.branchId , getOrder.IsCompany).Where(a => a.orderId == getOrder.orderId).FirstOrDefault();
                if (!(checkCounts.crditorAccounts.Count > 1 && checkCounts.debitorAccounts.Count > 1))
                {
                  switch (isde)
                    {
                    case true:
                        grder.secondAccount = (from m in db.AccountMovements where 
                                     m.AccountOrderId == getOrder.orderId && m.Crdit > 0 select
                                     new OrderAccounts
                                     {
                                         amount = m.Crdit , 
                                         account = new AccountObject()
                                         {
                                             id = m.AccountId , 
                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName , 
                                             value = m.Accounts.AccountName, 
                                             status = true
                                             
                                         } , id = m.AccountOrderId
                                     }).ToList();

                            grder.accountId = getOrder.accountId;
                            grder.accountName = isde ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ";
                            grder.tilte = "تعديل قيد اضافة رقم " + order.OrderNo;
                            grder.orderNo = order.OrderNo;
                            grder.orderId = order.AccountOrderId;
                            grder.orderDate = order.OrderDate;
                            grder.orderNote = order.OrderNote;
                            grder.isDebit = isde;
                            break;
                   case false:
                        grder.secondAccount = (from m in db.AccountMovements where
                                               m.AccountOrderId == getOrder.orderId && m.Debit > 0 select
                                     new OrderAccounts
                                     {
                                         amount = m.Debit , 
                                         account = new AccountObject()
                                         {
                                             id = m.AccountId , 
                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName , 
                                             value = m.Accounts.AccountName, 
                                             status = true
                                             
                                         } ,
                                         id = m.AccountOrderId
                                     }).ToList();
                            grder.orderNo = order.OrderNo;
                            grder.accountId = getOrder.accountId;
                            grder.accountName = isde ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ";
                            grder.orderNote = order.OrderNote;
                            grder.tilte = "تعديل قيد صرف رقم " + order.OrderNo;
                            grder.orderId = order.AccountOrderId;
                            grder.orderDate = order.OrderDate;
                            grder.isDebit = isde;
                            break;
                    }
                }
        
            }

            else 
            {
                switch (getOrder.isDebit)
                {
                    case true:
                        grder.secondAccount = new List<OrderAccounts>() {

                            new OrderAccounts()
                            {
                                id = null,
                                amount = 0,
                                account = new AccountObject()
                                {
                                    id = 0,
                                    AccountCatogery = "",
                                    value = ""
                                   
                                }
                            }

                        };
                        if (getOrder.IsCompany)
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId  && a.IsCompany == true).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1).ToString();

                        }
                        else
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1).ToString();

                        }
                        grder.accountId = getOrder.accountId;

                        break;
                    case false:
                        grder.secondAccount = new List<OrderAccounts>() {

                            new OrderAccounts()
                            {
                                id = null,
                                amount = 0,
                                account = new AccountObject()
                                {
                                    id = 0,
                                    AccountCatogery = "",
                                    value = ""

                                }
                            }

                        };
                        if(getOrder.IsCompany)
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1).ToString();

                        }
                        else
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1).ToString();

                        }
                        grder.accountId = getOrder.accountId;

                        break;
                }
            }

            grder.IsCompany = getOrder.IsCompany;
            return grder;

        }
        public AccountsMoves SetOrder(GetSetOrder setOrder ,string currentWorkerId  , DateTime? mind , DateTime? maxd = null)
        {
            var db = new ApplicationDbContext();
            var order = db.AccountOrders.Find(setOrder.orderId);
            if(order != null)
            {
                order.CurrentWorkerId = currentWorkerId;
                order.OrderNote = setOrder.orderNote;
                db.Entry(order).State = EntityState.Modified;
                var accToDel = db.AccountMovements.Where(a => a.AccountOrderId == order.AccountOrderId);
                db.AccountMovements.RemoveRange(accToDel);
                decimal totalAmount = 0;
                List<AccountMovement> secondAcc = new List<AccountMovement>();
                foreach (var o in setOrder.secondAccount)
                {
                    totalAmount = totalAmount + o.amount;
                    var move = new AccountMovement()
                    {
                        AccountId = o.account.id,
                        AccountMovementDate = order.OrderDate,
                        AccountOrderId = order.AccountOrderId,
                        Debit = setOrder.isDebit ? 0 : o.amount,
                        Crdit = setOrder.isDebit ? o.amount :0,
                        CurrentWorkerId = currentWorkerId
                    };
                    secondAcc.Add(move);

                }

                var Firstacc = new AccountMovement()
                {
                    AccountId = setOrder.accountId,
                    AccountMovementDate = order.OrderDate,
                    AccountOrderId = order.AccountOrderId,
                    Debit = setOrder.isDebit ? totalAmount : 0,
                    Crdit = setOrder.isDebit ? 0 : totalAmount,
                    CurrentWorkerId = currentWorkerId
                };

                secondAcc.Add(Firstacc);
                db.AccountMovements.RemoveRange(accToDel);
                db.AccountMovements.AddRange(secondAcc);
                db.SaveChanges();

            }
            else
            {
               
                order = new AccountOrder
                {
                    OrderDate = DateTime.Now,
                    BranchId = setOrder.branchId,
                    OrderNote = setOrder.orderNote,
                    CurrentWorkerId = currentWorkerId,
                    OrderNo = db.AccountOrders.Where(a => a.BranchId == setOrder.branchId).Count() != 0 ?  db.AccountOrders.Where(a => a.BranchId == setOrder.branchId).Max(a => a.OrderNo) + 1 : 1,
                    AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId
                };

                db.AccountOrders.Add(order);
                db.SaveChanges();
                db.Entry(order).GetDatabaseValues();
                decimal totalAmount = 0;
                List<AccountMovement> secondAcc = new List<AccountMovement>();
                foreach(var o in setOrder.secondAccount)
                {
                    totalAmount = totalAmount + o.amount;
                    var move = new AccountMovement()
                    {
                        AccountId = o.account.id, 
                        AccountMovementDate = order.OrderDate,
                        AccountOrderId = order.AccountOrderId,
                     
                        Debit = setOrder.isDebit ? 0 : o.amount,
                        Crdit = setOrder.isDebit ? o.amount : 0,
                        CurrentWorkerId = currentWorkerId
                    };
                    secondAcc.Add(move);

                }

                var Firstacc = new AccountMovement()
                {
                    AccountId = setOrder.accountId,
                    AccountMovementDate = DateTime.Now,
                    AccountOrderId = order.AccountOrderId,
                    Debit = setOrder.isDebit ? totalAmount : 0,
                    Crdit = setOrder.isDebit ? 0 : totalAmount,
                    CurrentWorkerId = currentWorkerId
                };

                secondAcc.Add(Firstacc);
                db.AccountMovements.AddRange(secondAcc);
                db.SaveChanges();
            }

            var ord = GetAccountmoves(setOrder.accountId  ,mind , maxd).Where(a => a.orderId == order.AccountOrderId).FirstOrDefault();
            return ord;

        }


        public List<AccountsMoves> GetAccountmoves(Int32 accountId , DateTime? mind , DateTime? maxd = null)
        {
            var db = new ApplicationDbContext();
            var checkformove = db.AccountMovements.Where(ac => ac.AccountId == accountId).FirstOrDefault();
            var sumer = 0m;
            var sumerDate = 0m;
            var PsumerDate = 0m;
            var pdebit = 0m;
            var pcrdit = 0m;
            if (checkformove != null)
            {
                sumer = (decimal?)(db.AccountMovements.Where(ac => ac.AccountId == accountId).Sum(ac => (decimal?)ac.Debit ?? 0) - db.AccountMovements.Where(ac => ac.AccountId == accountId).Sum(ac => (decimal?)ac.Crdit ?? 0)) ?? 0;
                if(mind != null)
                {
                    sumerDate = (from ac in db.AccountMovements
                                 where (ac.AccountId == accountId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd)
                                 select ac.Debit).DefaultIfEmpty(0).Sum() 
                                 - 
                                 (from ac in db.AccountMovements where 
                                  (ac.AccountId == accountId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd)
                                  select ac.Crdit).DefaultIfEmpty(0).Sum();
                   
                    PsumerDate = (from ac in db.AccountMovements
                                  where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                                  select ac.Debit).DefaultIfEmpty(0).Sum() 
                                  - 
                                  (from ac in db.AccountMovements
                                   where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                                   select ac.Crdit).DefaultIfEmpty(0).Sum();
                    pdebit = (from ac in db.AccountMovements
                              where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                              select ac.Debit).DefaultIfEmpty(0).Sum();
                   pcrdit = (from ac in db.AccountMovements
                             where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                             select ac.Crdit).DefaultIfEmpty(0).Sum();

                }
            }
            var ord = db.AccountMovements.Find(accountId);

           
            var moves = (from o in db.AccountMovements
                         where o.AccountId == accountId
                         select new AccountsMoves
                         {
                             moveId = o.AccountMovementId,
                             orderNote = o.AccountOrder.OrderNote,
                             accountId = o.AccountId,
                             accountName = o.Accounts.AccountName,
                             debit = o.Debit,
                             crdit = o.Crdit,
                             Pdebit = pdebit,
                             Pcrdit = pcrdit,
                             refrenceId = o.AccountOrder.OrderIdRefrence,
                             typeRefrence = o.AccountOrder.OrderTypeRefrence,
                             date = o.AccountMovementDate,
                             orderId = o.AccountOrderId,
                             orderNo = o.AccountOrder.OrderNo,
                             note = o.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName,
                             CompanyId = o.AccountOrder.CompanyId,
                             branchId = o.AccountOrder.BranchId,
                             summery = sumer > 0 ? ("رصيد الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("رصيد الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? (" رصيد الحساب " + 0.ToString()) :(" رصيد الحساب " + 0.ToString()),
                             summeryDate = sumerDate > 0 ? ("رصيد الفترة الحالية " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("رصيد الفترة الحالية " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? (" رصيد الفترة الحالية " + 0.ToString()) : (" رصيد الفترة الحالية " + 0.ToString()),
                             PsummeryDate = PsumerDate > 0 ? ("رصيد بداية الفترة " + Math.Abs(PsumerDate).ToString() + " ( مدين )") : PsumerDate < 0 ? ("رصيد بداية الفترة " + Math.Abs(PsumerDate).ToString() + " ( دائن )") : PsumerDate == 0 ? (" رصيد بداية الفترة " + 0.ToString()) : (" رصيد بداية الفترة " + 0.ToString()),
                             IsCompany = o.AccountOrder.IsCompany,
                             iscomplex = true

                         }
                       ).ToList();

            Int32 numid = 0;
            foreach(var o in moves)
            {
                numid += 1;
                o.indexdId = numid;

                var checkcom = GetAccountOrders(o.IsCompany == false ? o.branchId : o.CompanyId  , o.IsCompany).Where(a => a.orderId == o.orderId).FirstOrDefault();
               
                if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count > 1)
                {
                    o.iscomplex = true;
                }
               else if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count == 1)
                {
                    var checkfirstacc = checkcom.debitorAccounts.Where(a => a.account.id == o.accountId).FirstOrDefault();
                    if(checkfirstacc == null)
                    {
                        o.iscomplex = true;
                    }
                    else
                    {
                        o.iscomplex = false;
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count > 1)
                {
                    var checkfirstacc = checkcom.crditorAccounts.Where(a => a.account.id == o.accountId).FirstOrDefault();
                    if (checkfirstacc == null)
                    {
                        o.iscomplex = true;
                    }
                    else
                    {
                        o.iscomplex = false;
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count == 1)
                {
                   
                    o.iscomplex = false;
                }

             
            }

            return moves;
        }

        public List<AccountCatogeryLedger> GetAccountmovesCatogery(Int32 accountCatogerryId , DateTime mind , DateTime maxd , bool isCompany , bool allAccounts)
        {
            var db = new ApplicationDbContext();
            if (isCompany)
            {
                if (allAccounts)
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    var psumerDate = 0m;
                    if (checkformove != null)
                    {
                        sumer = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        psumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    }


                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.Accounts
                              where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountId,
                                  name = cat.AccountName,
                                  debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                                  crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                                  psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),


                              }).ToList();


                    return Ledger;
                }
                else
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.Accounts.BranchId == null).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    var psumerDate = 0m;
                    if (checkformove != null)
                    {
                        sumer = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where(a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where(a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        psumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    }


                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.Accounts
                              where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                              && cat.BranchId == null
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountId,
                                  name = cat.AccountName,
                                  debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                                  crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                                  psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),
                                  

                              }).ToList();


                    return Ledger;
                }
               
            }
            else
            {
                var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.Accounts.BranchId != null).FirstOrDefault();
                var sumer = 0m;
                var sumerDate = 0m;
                var psumerDate = 0m;
                if (checkformove != null)
                {
                    sumer = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                    sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where(a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                    psumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                }


                var Ledger = new List<AccountCatogeryLedger>();


                var Vaildgroups = new List<AccountCategory>();
                // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                Ledger = (from cat in db.Accounts
                          where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                         &&  cat.BranchId != null
                          select new AccountCatogeryLedger()
                          {
                              id = cat.AccountId,
                              name = cat.AccountName,
                              debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                              crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                              minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                              maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                              summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                              summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                              psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),


                          }).ToList();


                return Ledger;
            }

        }

        public List<AccountCatogeryLedger> GetCustomAccountmovesCatogery(Int32 customaAccountCatogerryId, DateTime mind, DateTime maxd)
        {
            var db = new ApplicationDbContext();
            var checkformove = db.CustomAccounts.Where(ac => ac.CustomAccountCategoryId == customaAccountCatogerryId).FirstOrDefault();
            var sumer = 0m;
            var sumerDate = 0m;
            var psumerDate = 0m;
            if (checkformove != null)
            {
                sumer = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                sumerDate = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                psumerDate = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
            }


            var Ledger = new List<AccountCatogeryLedger>();


            var Vaildgroups = new List<AccountCategory>();
            // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

            Ledger = (from cat in db.CustomAccounts
                      where cat.CustomAccountCategoryId == customaAccountCatogerryId
                      select new AccountCatogeryLedger()
                      {
                          id = cat.AccountId,
                          name = cat.Account.AccountName,
                          debit = cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                          crdit = Math.Abs(cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                          minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                          maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                          summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                          summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                          psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),
                          total = sumerDate,
                          branchId =(int) cat.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId

                      }).ToList();
            foreach(var l in Ledger)
            {
                var code = GetAccountCode(l.branchId, l.id + "Ac");
                l.Code = code;
            }

            return Ledger;
        }

        public List<AccountCatogeryLedger> GetAllAccountmovesCatogery(Int32 companyId, DateTime mind, DateTime maxd , bool isCompany , bool allAccounts)
        {
            var db = new ApplicationDbContext();
            if (isCompany)
            {
                if (allAccounts)
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    if (checkformove != null)
                    {

                        sumer = db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId)
                                .Select(ac => ac.Debit).DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();
                        sumerDate = db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(a => a.Debit)
                                    .DefaultIfEmpty(0)
                                    .Sum()
                                    -
                                    db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                    .Sum();

                    }

                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.AccountCategories
                              where cat.BalanceSheetType.BalanceSheet.CompanyId == companyId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountCategoryId,
                                  name = cat.AccountCategoryName,
                                  debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                                  crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                                  sumDebit = 0,
                                  sumCrdit = 0,
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                                  AccountCatogeryId = cat.BalanceSheetTypeId,
                                  AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                              }).ToList();


                    return Ledger;
                }
                else
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    if (checkformove != null)
                    {

                        sumer = db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null)
                                .Select(ac => ac.Debit).DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();
                        sumerDate = db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId== null &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(a => a.Debit)
                                    .DefaultIfEmpty(0)
                                    .Sum()
                                    -
                                    db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                    .Sum();

                    }

                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.AccountCategories
                              where cat.BalanceSheetType.BalanceSheet.CompanyId == companyId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountCategoryId,
                                  name = cat.AccountCategoryName,
                                  debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                                  crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                                  sumDebit = 0,
                                  sumCrdit = 0,
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId  && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                                  AccountCatogeryId = cat.BalanceSheetTypeId,
                                  AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                              }).ToList();


                    return Ledger;
                }
            }
            else
            {
                var checkformove = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).FirstOrDefault();
                var sumer = 0m;
                var sumerDate = 0m;
                if (checkformove != null)
                {

                    sumer = db.AccountMovements
                            .Where(ac => ac.Accounts.BranchId== companyId)
                            .Select(ac => ac.Debit).DefaultIfEmpty(0)
                            .Sum()
                            -
                            db.AccountMovements
                            .Where(ac => ac.Accounts.BranchId == companyId)
                            .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                            .Sum();
                    sumerDate = db.AccountMovements
                                .Where(ac => ac.Accounts.BranchId == companyId &&
                                             ac.AccountMovementDate >= mind &&
                                             ac.AccountMovementDate <= maxd)
                                .Select(a => a.Debit)
                                .DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.BranchId == companyId &&
                                             ac.AccountMovementDate >= mind &&
                                             ac.AccountMovementDate <= maxd)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();

                }

                var Ledger = new List<AccountCatogeryLedger>();


                var Vaildgroups = new List<AccountCategory>();
                // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();
                var branch = db.Branchs.Find(companyId);
                Ledger = (from cat in db.AccountCategories
                          where cat.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId
                          
                          select new AccountCatogeryLedger()
                          {
                              id = cat.AccountCategoryId,
                              name = cat.AccountCategoryName,
                              debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                              crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                              sumDebit = 0,
                              sumCrdit = 0,
                              minDate = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Min(ad => ad.AccountMovementDate) : mind,
                              maxDate = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Max(ad => ad.AccountMovementDate) : maxd,
                              summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                              summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                              AccountCatogeryId = cat.BalanceSheetTypeId,
                              AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                          }).ToList();


                return Ledger;
            }
            
        }

        public List<Alllists> getfinancialStatement(int CompanyId)
        {
            var db = new ApplicationDbContext();


          
            List<GetFinancialList> ftree = new List<Models.GetFinancialList>();
            List<Alllists> listes4 = new List<Alllists>();
          
                var cacc = (from c in db.CustomAccountCategories
                            where c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == CompanyId
                            select new FListes
                            {
                                Id = c.CustomAccountCategoryId + "Acc",
                                Name = c.AccountCategoryName,
                                Level = 4,
                                ParentId = c.CustomBalanceSheetTypeId + "Bst",
                                ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName,
                                amount = 0,
                                Code = db.CustomAccountCategories.Where(a => a.CustomBalanceSheetTypeId == c.CustomBalanceSheetTypeId).OrderBy(a => a.RowVersion).Count(x => x.CustomAccountCategoryId < c.CustomAccountCategoryId) + 1,

                            }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

                var bst = (from c in db.CustomBalanceSheetTypes
                           where c.CustomBalanceSheet.FinancialList.CompanyId == CompanyId
                           select new FListes
                           {
                               Id = c.CustomBalanceSheetTypeId + "Bst",
                               Name = c.BalanceSheetTypeName,
                               Level = 3,
                               ParentId = c.CustomBalanceSheetId + "Bs",
                               ParentName = c.CustomBalanceSheet.BalanceSheetName,
                               amount = 0,
                               Code = db.CustomBalanceSheetTypes.Where(a => a.CustomBalanceSheetId == c.CustomBalanceSheetId).OrderBy(a => a.RowVersion).Count(x => x.CustomBalanceSheetId < c.CustomBalanceSheetId) + 1,

                           }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

                var bs = (from c in db.CustomBalanceSheets
                          where c.FinancialList.CompanyId == CompanyId
                          select new FListes
                          {
                              Id = c.CustomBalanceSheetId + "Bs",
                              Name = c.BalanceSheetName,
                              Level = 2,
                              ParentId = c.FinancialListId + "F",
                              ParentName = c.FinancialList.FinancialListName,
                              amount = 0,
                              Code = db.CustomBalanceSheets.Where(a => a.FinancialListId == c.FinancialListId).OrderBy(a => a.RowVersion).Count(x => x.CustomBalanceSheetId < c.CustomBalanceSheetId) + 1

                          }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

                var fl = (from c in db.FinancialLists
                          where c.CompanyId == CompanyId
                          select new FListes
                          {
                              Id = c.FinancialListId + "F",
                              Name = c.FinancialListName,
                              Level = 1,
                              ParentId = c.CompanyId.ToString(),
                              ParentName = c.Company.CompanyName,
                              amount = 0,
                              Code = db.FinancialLists.Where(a => a.CompanyId == CompanyId).OrderBy(a => a.RowVersion).Count(x => x.FinancialListId < c.FinancialListId) + 1

                          }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();

                List<FListes> listes = new List<FListes>();
                foreach (var caccount in cacc)
                {

                    listes.AddRange(caccount);
                    foreach (var ccatogery in bst)
                    {
                        foreach (var ccatogeryItem in ccatogery)
                        {
                            if (caccount[0].ParentId == ccatogeryItem.Id)
                            {
                                ccatogeryItem.forLevel = ccatogeryItem.ParentId;
                                // caccount.Add(ccatogery[0]);

                                foreach (var c in caccount)
                                {
                                    c.forLevel = ccatogeryItem.ParentId;
                                }
                                listes.Add(ccatogeryItem);
                            }

                        }

                    }
                }


                List<FListes> listes2 = new List<FListes>();
                var groupLists = (from l in listes select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();


                foreach (var g in groupLists)
                {
                    var last = g[g.Count - 1];
                    foreach (var b in bs)
                    {
                        foreach (var Ib in b)
                        {
                            if (Ib.Id == last.ParentId)
                            {
                                g.Add(Ib);
                                foreach (var c in g)
                                {
                                    c.forLevel = last.ParentId;
                                }
                            }
                        }
                    }
                    listes2.AddRange(g);
                }

                var groupLists2 = (from l in listes2 select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
                List<FListes> listes3 = new List<FListes>();

                foreach (var g in groupLists2)
                {
                    var last = g[g.Count - 1];
                    foreach (var b in fl)
                    {
                        foreach (var Ib in b)
                        {
                            if (Ib.Id == last.ParentId)
                            {
                                g.Add(Ib);
                                foreach (var c in g)
                                {
                                    c.forLevel = last.ParentId;
                                }
                            }
                        }
                    }
                    listes3.AddRange(g);
                }


                var groupLists4 = (from l in listes3 select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();

                foreach (var l in groupLists4)
                {

                    listes4.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, Listes = l });
                }

                foreach (var l in listes4)
                {
                    l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
                }
        
            var No = 0;
            foreach (var l in listes4)
            {
                var accounts = l.Listes.Count();
                
                foreach(var a in l.Listes)
                {
                    if(a.Level == 4)
                    {
                        a.Code = 1 + No;
                        No++;
                    }
                    else
                    {
                        a.Code = 0;
                    }
                   
                }

               
            }
            
            return listes4;
        }

     
        public OrderVm GetAccountOrderById(int id , Int32 branchId , bool isCompany)
        {
            var db = new ApplicationDbContext();
            if (isCompany)
            {
                var orders = (from o in db.AccountOrders
                              where o.OrderNo == id && o.CompanyId == branchId
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  orderNote = o.OrderNote,
                                  IsCompany = false,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }).FirstOrDefault();
                return orders;
            }
            else
            {
                var orders = (from o in db.AccountOrders
                              where o.OrderNo == id && o.BranchId == branchId
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  orderNote = o.OrderNote,
                                  IsCompany = false,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }).FirstOrDefault();
                return orders;
            }

        }
        public Int32 ConvertToId(string id)
        {
            var numAlpha = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = numAlpha.Match(id);
            var num = Convert.ToInt32( match.Groups["Numeric"].Value);


            return num;
        }

        public string GetUniqueKey(int max)
        {
            int maxSize = max;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            { result.Append(chars[b % (chars.Length - 1)]); }
            return result.ToString();
        }

        public List<Alllists> fListes(isCompanyBranch set)
        {
            var db = new ApplicationDbContext();
            Int32 id = 0;
            if (set.IsCompany)
            {
                Company financialList = db.Companies.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return null;
                }
            }
            else
            {
                Branch financialList = db.Branchs.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return null;
                }
            }


            List<GetFinancialList> ftree = new List<Models.GetFinancialList>();

            var cac = (from c in db.CustomAccounts where c.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id select new FListes { Id = c.CustomAccountId + "Ac", Show = false, Name = c.Account.AccountName, Level = 5, ParentId = c.CustomAccountCategoryId + "Acc", LevelName = "فرعي", ParentName = c.CustomAccountCategory.AccountCategoryName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var cacc = (from c in db.CustomAccountCategories select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomAccountCategoryId + "Acc", Show = false, childern = c.CustomAccount.Count(), Name = c.AccountCategoryName, Level = 4, AddTitle = "اضافة حسابات فرعية", Addlink = "ربط مع حساب مساعد", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "مساعد", ParentId = c.CustomBalanceSheetTypeId + "Bst", ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bst = ((from c in db.CustomBalanceSheetTypes select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetTypeId + "Bst", Show = false, childern = c.CustomAccountCategories.Count(), Name = c.BalanceSheetTypeName, Level = 3, AddTitle = "اضافة حساب مساعد", Addlink = "ربط مع حساب عام", LevelName = "عام", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, ParentId = c.CustomBalanceSheetId + "Bs", ParentName = c.CustomBalanceSheet.BalanceSheetName })).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bs = (from c in db.CustomBalanceSheets select c).OrderBy(r => r.CreatedDate).Where(c => c.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetId + "Bs", Name = c.BalanceSheetName, Show = true, accountType = c.AccountType.AccountTypeName, Level = 2, childern = c.CustomBalanceSheetTypes.Count(), AddTitle = "اضافة حساب عام", Addlink = "ربط مع حساب رئيسي", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "رئيسي", ParentId = c.FinancialListId + "F", ParentName = c.FinancialList.FinancialListName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var fl = (from c in db.FinancialLists where c.CompanyId == id orderby c.CreatedDate select new FListes { Id = c.FinancialListId + "F", forLevel = c.FinancialListId + "F", Name = c.FinancialListName, Level = 1, Show = true, ParentId = c.BranchId.ToString(), childern = c.CustomBalanceSheets.Count(), ParentName = c.Branch.BranchName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();

            List<FListes> listes = new List<FListes>();

            foreach (var mainList in fl)
            {
                foreach (var f in mainList)
                {
                    listes.Add(f);
                    foreach (var balancesheet in bs)
                    {
                        foreach (var customBS in balancesheet)
                        {
                            if (customBS.ParentId == f.Id)
                            {
                                customBS.forLevel = f.Id;
                                listes.Insert(listes.IndexOf(f), customBS);
                                foreach (var Bstype in bst)
                                {
                                    foreach (var CustomBst in Bstype)
                                    {
                                        if (CustomBst.ParentId == customBS.Id)
                                        {
                                            CustomBst.forLevel = f.Id;
                                            listes.Insert(listes.IndexOf(customBS), CustomBst);
                                            foreach (var CustomAccountc in cacc)
                                            {
                                                foreach (var CustomAcc in CustomAccountc)
                                                {
                                                    if (CustomAcc.ParentId == CustomBst.Id)
                                                    {
                                                        CustomAcc.forLevel = f.Id;
                                                        listes.Insert(listes.IndexOf(CustomBst), CustomAcc);
                                                        foreach (var Customacco in cac)
                                                        {
                                                            foreach (var CustomAc in Customacco)
                                                            {
                                                                if (CustomAc.ParentId == CustomAcc.Id)
                                                                {
                                                                    CustomAc.forLevel = f.Id;
                                                                    listes.Insert(listes.IndexOf(CustomAcc), CustomAc);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }



                        }
                    }
                }
            }
            var g = listes.GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
            List<Alllists> listes5 = new List<Alllists>();

            foreach (var l in g)
            {

                listes5.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, forWidget = false, Listes = l });
            }

            foreach (var l in listes5)
            {
                l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
            }

            return listes5;
        }

        public string GetBstType(Int32 accountId)
        {
            var db = new ApplicationDbContext();
            var ac = db.BalanceSheetTypes.Find(accountId);
            if (ac != null)
            {
                if (ac.AccountType != null)
                {
                    return ac.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheet.AccountType != null)
                {
                    return ac.BalanceSheet.AccountType.AccountTypeName;
                }
               
                else
                {
                    return ac.BalanceSheet.AccountType.AccountTypeName;

                }
            }
            else
            {
                return "";
            }


        }

        public string GetAccountCategoryType(Int32 accountId)
        {
            var db = new ApplicationDbContext();
            var ac = db.AccountCategories.Find(accountId);
            if(ac != null)
            {
                if (ac.AccountType != null)
                {
                    return ac.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheetType.AccountType != null)
                {
                    return ac.BalanceSheetType.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheetType.AccountType == null && ac.BalanceSheetType.BalanceSheet.AccountType != null)
                {
                    return ac.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                }
                else
                {
                    return ac.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;

                }
            }
            else
            {
                return "";
            }
         

        }


        public decimal GetAccountamountType(Int32 accountId ,bool isDebit)
        {
            var db = new ApplicationDbContext();
           
                var ac = db.Accounts.Find(accountId);
                if (ac != null)
                {
                    if (ac.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين")
                    {
                        if (isDebit)
                        {
                        return 1;
                        }
                        else
                        {
                        return -1;
                        }
                      
                    }
                    else if (ac.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن")
                   {
                        if (isDebit)
                        {
                            return -1;
                        }
                        else
                        {
                            return 1;
                        }
                    }

                return 1;
                
               }
                else
                {
                    return 1;
                }
        }


        public decimal GetCustomAccountamountType(Int32 accountIdLevel2)
        {
            var db = new ApplicationDbContext();

            var ac = db.CustomBalanceSheets.Find(accountIdLevel2);
            if (ac != null)
            {
                if (ac.AccountType.AccountTypeName == "مدين")
                {
                    return -1;

                }
                else if (ac.AccountType.AccountTypeName == "دائن")
                {
                    return 1;
                }

                return -1;

            }
            else
            {
                return -1;
            }
        }
        public decimal ConvertTodecmial(string id)
        {
            var numAlpha = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = numAlpha.Match(id);
            var num = Convert.ToDecimal(match.Groups["Numeric"].Value);


            return num;
        }

        public ItemGroupVM GetAllItems(Int32 itemGroupId)
        {
           var db = new ApplicationDbContext();

        var returnedItems = (from i in db.ItemGroups
                                 where i.ItemGroupId == itemGroupId
                                 select new ItemGroupVM
                                 {
                                     ItemGroupId = i.ItemGroupId,
                                     ItemGroupName = i.ItemGroupName,
                                     Properties = (from p in i.Properties
                                                   where p.ItemGroupId == i.ItemGroupId
                                                   orderby p.orderNum
                                                   select new PropertiesforItemGroupVM
                                                   {
                                                       propertyId = p.PropertiesforItemGroupId,
                                                       propertyName = p.PropertiesforItemGroupName,
                                                       Show = p.Show,
                                                       propertyValues = (from pv in p.PropertyValuesforItemGroup
                                                                         where pv.PropertiesforItemGroupId == p.PropertiesforItemGroupId
                                                                         select new PropertyValuesforItemGroupVM
                                                                         {
                                                                             propertyValueId = pv.PropertyValuesforItemGroupId,
                                                                             propertyId = p.PropertiesforItemGroupId,
                                                                             propertyValueName = pv.PropertyValuesforItemGroupName,
                                                                             value = pv.PropertyValuesforItemGroupValue,
                                                                             typeName = p.Type.TypeName
                                                                         }).ToList(),
                                                       Type = (new TypeforItemGroupVM { typeId = p.TypeforItemGroupId, typeName = p.Type.TypeName, show = p.Type.show, description = p.Type.description, propertyId = p.PropertiesforItemGroupId })

                                                   }).ToList(),
                                     Accounts = (from acc in i.Accounts
                                                 where acc.ItemGroupId == i.ItemGroupId
                                                 select new AccountsforItemGroupVM
                                                 {
                                                     AccountId = acc.AccountId,
                                                     accountName = acc.Account.AccountName,
                                                     code = "",
                                                     ItemGroupId = acc.ItemGroupId,

                                                 }).ToList(),
                                     CompanyId = i.CompanyId,
                                     Unit = (new UnitVM { UnitId = i.UnitId, UnitName = i.Unit.UnitName, UnitTypeId = i.Unit.UnitTypeId })

                                 }).SingleOrDefault();
            if(returnedItems != null)
            {
                foreach (var acc in returnedItems.Accounts)
                {
                    acc.code = GetAccountCode(returnedItems.CompanyId, acc.AccountId + "Ac").ToString();
                }
            }
         

            return returnedItems;
        }
        public IQueryable<AccountsCatogeryDataTypes> GetAllDataTypes(SetAccountsCatogeryDataTypes cat )
        {
            var db = new ApplicationDbContext();
            var dataty = (from c in db.AccountsDataTypes select new AccountsDataTypesVm
            {
                Id =c.Id , 
                TypeName = c.TypeName , 
                IsTrue = db.AccountCategories.Any(a => a.AccountCategoryId == cat.accountId && a.AccountsDataTypesId == c.Id)
            });

            Int32 btypId = Convert.ToInt32(ConvertToId(cat.BalanceSheetTypeId));
            var dt = (from d in db.AccountCategories
                      where d.AccountCategoryId == cat.accountId
                      select new AccountsCatogeryDataTypes() {
                          Id = d.AccountCategoryId,
                          CatogeryName = d.AccountCategoryName,
                          AccountsDataTypes = dataty.ToList()

                      });
            return dt;
        }

        public Int32 GetAccountCode(int branchiId , string accountIdForCode)
        {
            var db = new ApplicationDbContext();
            var Tbs = (from bs in db.BalanceSheets
                       where bs.CompanyId == branchiId
                       orderby bs.RowVersion
                       select new AccountsTable
                       {
                           Id = bs.BalanceSheetId + "Bs",
                           Code = db.BalanceSheets.Where(a => a.CompanyId == branchiId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                           ParentId = bs.CompanyId.ToString(),

                       }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            var Tbst = (from bst in db.BalanceSheetTypes
                        where bst.BalanceSheet.CompanyId == branchiId
                        orderby bst.RowVersion
                        select new AccountsTable
                        {
                            Id = bst.BalanceSheetTypeId + "Bst",
                            ParentId = bst.BalanceSheetId + "Bs",
                            Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            List<AccountsTable> Taccounts = new List<AccountsTable>();


            var TAcc = (from Acc in db.AccountCategories
                        where Acc.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                        orderby Acc.RowVersion
                        select new AccountsTable
                        {
                            Id = Acc.AccountCategoryId + "Acc",
                            Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                            ParentId = Acc.BalanceSheetTypeId + "Bst"
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




            var TAc = (from Ac in db.Accounts
                       where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                       orderby Ac.RowVersion
                       select new AccountsTable
                       {
                           Id = Ac.AccountId + "Ac",
                           Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                           ParentId = Ac.AccountCategoryId + "Acc"
                       }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            foreach (var acc in TAcc)
            {
                foreach (var ac in TAc)
                {

                    if (ac[ac.Count - 1].ParentId == acc[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                        acc.AddRange(ac);
                    }
                }
            }

            foreach (var bst in Tbst)
            {
                foreach (var acc in TAcc)
                {

                    if (acc[0].ParentId == bst[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        //acc.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.Code = int.Parse(acc[0].Code.ToString() + c.Code.ToString()));

                        bst.AddRange(acc);
                    }
                }
            }

            foreach (var bs in Tbs)
            {
                foreach (var bst in Tbst)
                {

                    if (bst[0].ParentId == bs[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);

                        bs.AddRange(bst);

                    }
                }
            }

            foreach (var i in Tbs)
            {
                foreach (var a in i)
                {
                    i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                }
            }
            Tbs.OrderBy(a => a[0].Code);
            var accountcode = 0;
            //foreach(var i in Tbs)
            //{
            //    accountcode = i.Where(a => a.Id == accountIdForCode).FirstOrDefault().Code;
            //}

            foreach (var t in Tbs)
            {
                foreach(var i in t)
                {
                    if(i.Id == accountIdForCode)
                    {
                        accountcode = i.Code;
                    }
                }
            }

            return accountcode;
        }

        public IQueryable<AccountCategoryPropertiesVM> GetAccountCatogeryProperties(Int32 accountCatogeryId)
        {
            var db = new ApplicationDbContext();
            var dt = (from d in db.AccountCategoryProperties
                     // where d.AccountCategoryId == accountCatogeryId
                      select new AccountCategoryPropertiesVM()
                      {
                          Id = d.AccountCategoryPropertiesId ,
                          Name = d.AccountCategoryPropertiesName,
                         // AccountCategoryId = d.AccountCategoryId , 
                         // DataTypeId = d.DataTypeId,
                        //  DataTypeName = d.DataType.TypeName,
                         // AccountCategoryName = d.AccountCategory.AccountCategoryName
                      }
                      );
            return dt;
        }

        public IQueryable<AccountCategoryPropertiesValuesVM> GetAccountCatogeryPropertiesValue(Int32 accountId)
        {
            var db = new ApplicationDbContext();
            var accid = db.Accounts.Find(accountId);
            var dt = (from d in db.AccountCategoryProperties
                      //where d.AccountCategoryId ==accid.AccountCategoryId
                      select new AccountCategoryPropertiesValuesVM()
                      {
                          Id = d.AccountCategoryPropertiesId,
                          PropertName = d.AccountCategoryPropertiesName,
                         // DataTypeName = d.DataType.TypeName,
                          //AccountCategoryId = d.AccountCategoryId,
                          AccountId = accountId,
                          AccountName = accid.AccountName,
                          Values = (from v in db.AccountCategoryPropertiesValues
                                    where v.AccountCategoryPropertiesId == d.AccountCategoryPropertiesId
                                    && v.AccountId == accountId
                                    select new AccPropertiesValuesVM() {
                                       Id = v.AccountCategoryPropertiesValueId,
                                       value = v.AccountCategoryPropertiesValueName
                                    } ).ToList()
                      });
            var cc = dt;
            return dt;
        }


        public static bool IsValidEmail(string email)
        {
            // source: http://thedailywtf.com/Articles/Validating_Email_Addresses.aspx
            Regex rx = new Regex(
            @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
            return rx.IsMatch(email);
        }
  

        public bool RoleExists (string role)
        {
            var appDbContext = new ApplicationDbContext();

            var userRoles = appDbContext.CompanyRoles.Any(r => r.Name == role);

            return userRoles;
        }


        public CompanyRole AddCompanyRole(string name)
        {

            if (RoleExists(name))
            {
                throw new System.ArgumentException("هذا الاسم مسجل بالفعل في قائمة الصلاحيات", "الاسم مسجل");

            }

            else
            {
                var appDbContext = new ApplicationDbContext();

                CompanyRole comrole = new CompanyRole() { Name = name };
                appDbContext.CompanyRoles.Add(comrole);
                appDbContext.SaveChangesAsync();
                return comrole; 
            } 

        }

        /// <summary>
        /// You can use it to get Data according to company level i.e: Company , Branch , BalanceSheet etc...
        /// </summary>
        /// <param name="catogeryId"></param>
        /// <param name="lang"></param>
        /// <param name="catogery"></param>
        /// <returns></returns>

        public List<UserRole> GetUserRolesLang(Int32 catogeryId , string lang , string catogery)
        {
          var db = new ApplicationDbContext();



            var comapnyRole = (from r in db.ApplicationUsersComapnies
                               where r.CompanyId == catogeryId
                               select new UserRole()
                               {
                                  
                                   Email = r.ApplicationUser.Email,
                                   
                                   Rolelevels = (from b in db.CompanyRoles
                                                 where b.Name.Contains(catogery)
                                                 select new Rolelevels()
                                                 {
                                                     RoleName = b.Name,
                                                     Name = b.Vocabularies.FirstOrDefault(vn => vn.Lang == lang && vn.CompanyRoleId == b.CompanyRoleId).Name ?? b.Name,
                                                     IsAuthorized = db.AppUserCompanyRoles.Where(ra => ra.ApplicationUserComapnyId == r.ApplicationUserComapnyId).ToList().Any(a => a.CompanyRole.Name.Contains(b.Name) || a.CompanyRole.Vocabularies.Any(v => v.Name.Contains(b.Vocabularies.FirstOrDefault(vn => vn.Lang == lang && vn.CompanyRoleId == b.CompanyRoleId).Name)))
                                                 }).ToList()
                               }).ToList();
            var c = comapnyRole;

            return comapnyRole;
        }

        public List<CompaniesVm> GetAllCompanies(string userId)
        {

            var db = new ApplicationDbContext();

            var getRole = GetNewUserRole(userId, true, "Company", "Company");
            var getRoleBranch = GetNewUserRole(userId, true, "Branch", "Branch");

            var getCompany = db.Companies.ToList().Where(a => getRole.Any(r => r.RefrenceId == a.CompanyId))
                .Select(i => new CompaniesVm {
                    id = i.CompanyName,
                    name = i.CompanyName,
                    CurrentWorkerId = i.CurrentWorker.Email,
                    PublicId = i.CompanyId,
                    Branches = db.Branchs.Where(b => b.CompanyId == i.CompanyId).ToList().Where(a => getRoleBranch.Any(r => r.RefrenceId == a.BranchId))
                    .Select(b => new BranchesVm {
                        id = b.BranchName,
                        name = b.BranchName,
                        PublicId = b.BranchId,
                        CompaniesId = i.CompanyId,

                    }).ToList()
                }).ToList();

            return getCompany;
            
        }

        public IQueryable<BranchesVm> GetAllBranchs(Int32 companyId)
        {

            var db = new ApplicationDbContext();
            //  var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));
            //  var admin = roleManager.FindByName("Admin");
            // var notInThisRole = db.ApplicationUsersComapnies.Where(m => m.ApplicationUser.Roles == admin).ToList();
            var Branches = (from b in db.Branchs
                            where (b.Company.CompanyId == companyId)
                            select new BranchesVm()
                            {
                                id = b.BranchName,
                                name = b.BranchName,
                                PublicId = b.BranchId,
                                CompaniesId = companyId,
                               
                            });
           
            return Branches;
        }
    
        public IQueryable<BranchTree> GetAllFinancialListTree(Int32 FinancialListId)
        {
            
            var db = new ApplicationDbContext();
            //  var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));
            //  var admin = roleManager.FindByName("Admin");
            // var notInThisRole = db.ApplicationUsersComapnies.Where(m => m.ApplicationUser.Roles == admin).ToList();
            var balanceSheets = from b in db.FinancialLists
                                where b.FinancialListId == FinancialListId
                                select new BranchTree()
                                {
                                    id = b.FinancialListId.ToString(),
                                    text = b.FinancialListName,
                                    children = (from i in db.CustomBalanceSheets
                                                where i.FinancialListId == b.FinancialListId
                                                select new BalanceSheetTree()
                                                {
                                                    id = i.CustomBalanceSheetId.ToString() + "CBs",
                                                    text = i.BalanceSheetName,
                                                    children = (from bt in db.CustomBalanceSheetTypes
                                                                where bt.CustomBalanceSheetId == i.CustomBalanceSheetId
                                                                select new BalanceSheetTypeTree()
                                                                {
                                                                    id = bt.CustomBalanceSheetTypeId.ToString() + "CBst",
                                                                    text = bt.BalanceSheetTypeName,
                                                                    children = (from ac in db.CustomAccountCategories
                                                                                where ac.CustomBalanceSheetTypeId == bt.CustomBalanceSheetTypeId
                                                                                select new AccountCategoryTree()
                                                                                {
                                                                                    id = ac.CustomAccountCategoryId + "CAcc",
                                                                                    text = ac.AccountCategoryName,
                                                                                    children = (from at in db.CustomAccounts
                                                                                                where at.CustomAccountCategoryId == ac.CustomAccountCategoryId
                                                                                                select new AccountTree()
                                                                                                {
                                                                                                    id = at.CustomAccountId.ToString() + "CAc",
                                                                                                    text = at.Account.AccountName

                                                                                                }).ToList()

                                                                                }).ToList()

                                                                }).ToList()

                                                }).ToList()
                                };
            var gg = balanceSheets;

            return balanceSheets;
        }
       
      

    }
}