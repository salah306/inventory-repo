﻿using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Inventory.DataLayer
{
    public class ApplicationRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public static bool IAuthorized(string id, string rName)
        {

            var userManager =
              new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            var rolesForUser = userManager.GetRoles(id);

            if (!rolesForUser.Contains(rName))
            {
                return false;
            }
            else
            {
                return true;
            }


        }
        public IQueryable<CompaniesVm> GetAllCompanies()
        {
            var userManager =
              new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var companyUserManager = new CompanyUserManager();

            var companies = from i in db.Companies
                            select new CompaniesVm()
                            {
                                id = i.CompanyName,
                                name = i.CompanyName,
                                CurrentWorkerId = i.CurrentWorker.Email,
                                PublicId = i.CompanyId,
                                Branches = (from b in db.Branchs
                                            where (b.Company.CompanyId == i.CompanyId)
                                            select new BranchesVm()
                                            {
                                                id = b.BranchName,
                                                name = b.BranchName,
                                                PublicId = b.BranchId,
                                                CompaniesId = i.CompanyId,
                                               

                                            }).ToList()

                            };

            return companies;
        }

        public IQueryable<BranchesVm> GetAllBranches()
        {

            var balanceSheets = from i in db.Branchs
                                select new BranchesVm()
                                {
                                    id = i.BranchName,
                                    name = i.BranchName,
                                    CompaniesId = i.Company.CompanyId,
                                    PublicId = i.BranchId

                                };

            return balanceSheets;
        }

     

        public IQueryable<BranchTree> GetAllBalanceSheets()
        {
            var bc = from b in db.Companies
                     select new BranchTree()
                     {
                         id = b.CompanyId.ToString(),
                         text = b.CompanyName,
                         children = (from i in db.BalanceSheets
                                     where i.CompanyId == b.CompanyId
                                     select new BalanceSheetTree()
                                     {
                                         id = i.BalanceSheetId.ToString(),
                                         text = i.BalanceSheetName,
                                         children = (from bt in db.BalanceSheetTypes
                                                     where bt.BalanceSheetId == i.BalanceSheetId
                                                     select new BalanceSheetTypeTree()
                                                     {
                                                         id = bt.BalanceSheetTypeId.ToString(),
                                                         text = bt.BalanceSheetTypeName,
                                                         children = (from ac in db.AccountCategories
                                                                     where ac.BalanceSheetTypeId == bt.BalanceSheetTypeId
                                                                     select new AccountCategoryTree()
                                                                     {
                                                                         id = ac.AccountCategoryName,
                                                                         text = ac.AccountCategoryName

                                                                     }).ToList()

                                                     }).ToList()

                                     }).ToList()
                     };


            return bc;
        }
        public IQueryable<BalanceSheetTypesVm> GetAllBalanceSheetTypes()
        {

            var balanceSheetTypes = from i in db.BalanceSheetTypes
                                  select new BalanceSheetTypesVm()
                                  {
                                      id = i.BalanceSheetTypeName,
                                      name = i.BalanceSheetTypeName,
                                      BalanceSheetsId = i.BalanceSheet.BalanceSheetName,
                                      PublicId = i.BalanceSheetTypeId

                                  };

            return balanceSheetTypes;
        }

        public IQueryable<AccountCategoriesVm> GetAllAccountCategories()
        {

            var accountCategory = from i in db.AccountCategories
                                  select new AccountCategoriesVm()
                                  {
                                      id = i.AccountCategoryName,
                                      name = i.AccountCategoryName,
                                      Debit = db.AccountMovements.Where(a => a.Accounts.AccountCategoryId == i.AccountCategoryId).Sum(d => d.Debit).ToString(),
                                      Crdit = db.AccountMovements.Where(a => a.Accounts.AccountCategoryId == i.AccountCategoryId).Sum(d => d.Crdit).ToString(),
                                      BalanceSheetTypesId = i.BalanceSheetType.BalanceSheetTypeName,
                                      PublicId = i.AccountCategoryId

                                  };

            return accountCategory;
        }

        public IQueryable<AccountsVm> GetAllAccounts()
        {
            var account = from i in db.Accounts
                          select new AccountsVm()
                          {
                              id = i.AccountName,
                              name = i.AccountName,
                              PublicId = i.AccountId




                          };

            return account;
        }


        public IQueryable<AccountsVm>  GetAllAccounts(string name)
        {
            var account = (from i in db.Accounts
                           where (i.AccountCategory.AccountCategoryName == name)
                           select new AccountsVm()
                           {
                               id = i.AccountName,
                               name = i.AccountName,
                               Debit = db.AccountMovements.Where(a => a.AccountId == i.AccountId).Sum(d => d.Debit).ToString(),
                               Crdit = db.AccountMovements.Where(a => a.AccountId == i.AccountId).Sum(d => d.Crdit).ToString(),
                               AccountCategoriesId = i.AccountCategory.AccountCategoryName ,
                               PublicId = i.AccountId


                           });

            return account;
        }

        public IQueryable<AccountObject> SearchAllAccounts(Int32 branchId , bool IsCompany)
        {
            if (IsCompany)
            {
                var account = (from i in db.Accounts
                               where (i.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branchId)
                               && i.BranchId == null
                               && i.Activated == true
                               select new AccountObject()
                               {
                                   id = i.AccountId,
                                   value = i.AccountName,
                                   AccountCatogery = i.AccountCategory.AccountCategoryName,
                                   status = false


                               });
                return account;
            }
            else
            {
                var account = (from i in db.Accounts
                               where (i.BranchId == branchId)
                               && i.Activated == true
                               select new AccountObject()
                               {
                                   id = i.AccountId,
                                   value = i.AccountName,
                                   AccountCatogery = i.AccountCategory.AccountCategoryName,
                                   status = false


                               });
                return account;
            }
      

            //var filter = account.Where(x => !notAccount.Any(y => y == x.value));

          
        }


        public IQueryable<AccountObject> SearchAllFinancialLists(Int32 branchId)
        {
            var account = (from i in db.FinancialLists
                           where (i.CompanyId == branchId)
                           select new AccountObject()
                           {
                               id = i.FinancialListId,
                               value = i.FinancialListName,
                               status = false


                           });

            //var filter = account.Where(x => !notAccount.Any(y => y == x.value));

            return account;
        }



        public IQueryable<AccountObject> SearchAllAccountsCatogery(Int32 branchId , bool isCompany)
        {
            if (isCompany)
            {
                var account = (from i in db.AccountCategories
                               where (i.BalanceSheetType.BalanceSheet.CompanyId == branchId)
                               select new AccountObject()
                               {
                                   id = i.AccountCategoryId,
                                   value = i.AccountCategoryName,
                                   AccountCatogery = i.BalanceSheetType.BalanceSheetTypeName,
                                   status = false


                               });

                return account;
            }
            else
            {
                var branch = db.Branchs.Find(branchId);

                var account = (from i in db.AccountCategories
                               where (i.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId)
                               select new AccountObject()
                               {
                                   id = i.AccountCategoryId,
                                   value = i.AccountCategoryName,
                                   AccountCatogery = i.BalanceSheetType.BalanceSheetTypeName,
                                   status = false


                               });

                return account;
            }

        }



        //public AccountDetailsVm GetAllAccountDetails(string id)
        //{
        //    var account = (from i in db.Accounts
        //                   where (i.AccountName == id)
        //                   select new AccountDetailsVm()
        //                   {
        //                       id = i.AccountName,
        //                       name = i.AccountName,
        //                       accountMovement = (from b in db.AccountMovements
        //                                          where (b.AccountId == i.AccountId)
        //                                          select new AccountMovementVm()
        //                                          {
        //                                              id = b.AccountMovementDate.ToString(),
        //                                              name = b.AccountMovementDate.ToString(),
        //                                              mdate = b.AccountMovementDate,
        //                                              Debit = b.Debit.ToString(),
        //                                              Crdit = b.Crdit.ToString(),
        //                                              AccountOrderId = b.AccountOrderId,
        //                                              AccountOrderPropertiesName = b.AccountOrder.AccountOrderProperties.AccountOrderPropertiesName,
        //                                              SecondAccount = (from c in db.AccountMovements
        //                                                               where (c.AccountOrderId == b.AccountOrderId)
        //                                                               && (c.Accounts.AccountId != b.AccountId)
        //                                                               select new SecondAccount()
        //                                                               {
        //                                                                   SecondAccountName = c.Accounts.AccountName,
        //                                                                   SecondAccountCategory = c.Accounts.AccountCategory.AccountCategoryName
        //                                                               }),
        //                                              BranchName = b.Brabch.BranchName

        //                                          }),
        //                       accountpropertyValues = (from b in db.AccountCategoryProperties
        //                                                where (b.AccountCategoryId == i.AccountCategoryId)
        //                                                select new AccountPropertyVM()
        //                                                {
        //                                                    propertyId = b.AccountCategoryPropertiesName,
        //                                                    propertyName = b.AccountCategoryPropertiesName,
        //                                                    propertyValues = (from c in db.AccountCategoryPropertiesValues
        //                                                                      where (c.AccountCategoryPropertiesId == b.AccountCategoryPropertiesId)
        //                                                                      && (c.AccountId == i.AccountId)
        //                                                                      select new AccountPropertyValuesVM()
        //                                                                      {
        //                                                                          valueId = c.AccountCategoryPropertiesValueName,
        //                                                                          valueName = c.AccountCategoryPropertiesValueName

        //                                                                      })

        //                                                }),
        //                       publicId = i.AccountId,
        //                       accountOrderProperties = (from o in db.AccountOrderProperties
        //                                                 select new AccountOrderPropertiesVm() {

        //                                                     id = o.AccountOrderPropertiesName,
        //                                                     name = o.AccountOrderPropertiesName

        //                                                }),

        //                   }).FirstOrDefault();

        //    return account;
        //}

        public IQueryable<AccountPropertyValuesVM> GetAllAccountPropertyValues()
        {
            var accountPropertyVlaues = from b in db.AccountCategoryPropertiesValues
                                        select new AccountPropertyValuesVM()
                                        {
                                            valueId = b.AccountCategoryPropertiesValueName,
                                            valueName = b.AccountCategoryPropertiesValueName,
                                            PublicId = b.AccountCategoryPropertiesValueId


                                        };

            return accountPropertyVlaues;
        }


        //public IQueryable<AccountMovementVm> GetAllAccountMovements()
        //{
        //    var accountMovements = from b in db.AccountMovements
        //                           select new AccountMovementVm()
        //                           {
        //                               AccountOrderId = b.AccountOrderId,
        //                               AccountOrderPropertiesName = b.AccountOrder.AccountOrderProperties.AccountOrderPropertiesName,
        //                               BranchName = b.Brabch.BranchName,
        //                               Crdit = b.Crdit.ToString(),
        //                               Debit = b.Debit.ToString(),
        //                               id = b.AccountMovementDate.ToString(),
        //                               mdate = b.AccountMovementDate,
        //                               name = b.Accounts.AccountName,
        //                               Note = b.AccountMovementNote,
        //                               SecondAccount = (from c in db.AccountMovements
        //                                                where (c.AccountOrderId == b.AccountOrderId)
        //                                                && (c.Accounts.AccountId != b.AccountId)
        //                                                select new SecondAccount()
        //                                                {
        //                                                    SecondAccountName = c.Accounts.AccountName,
        //                                                    SecondAccountCategory = c.Accounts.AccountCategory.AccountCategoryName,
        //                                                    Crdit = c.Crdit.ToString(),
        //                                                    Debit = c.Debit.ToString(),
        //                                                    Note = c.AccountMovementNote
        //                                                }),
        //                               PublicId = b.AccountMovementId


        //                           };

        //    return accountMovements;
        //}


        //public async Task SaveAccountMovements(AccountMovementVm accountMovementVm)
        //{
        //    AccountMovement aMovemmnt = new AccountMovement()
        //    {
        //        AccountMovementDate = accountMovementVm.mdate,
        //        AccountId = db.Accounts.First(n => n.AccountName == accountMovementVm.name).AccountId,
        //        Debit = Convert.ToDecimal(accountMovementVm.Debit),
        //        Crdit = Convert.ToDecimal(accountMovementVm.Crdit),
        //        BranchId = db.AccountMovements.First(n => n.Brabch.BranchName == accountMovementVm.BranchName).BranchId,
        //        AccountOrderId = db.AccountOrders.First(n => n.OrderDate == accountMovementVm.mdate && n.AccountOrderProperties.AccountOrderPropertiesName == accountMovementVm.AccountOrderPropertiesName).AccountOrderId,
        //        AccountMovementNote = accountMovementVm.Note
        //    };

        //    db.AccountMovements.Add(aMovemmnt);

        //    List<AccountMovement> secondAccountMovement = new List<AccountMovement>();
        //    foreach (var SecondAccount in accountMovementVm.SecondAccount)
        //    {
        //        AccountMovement sMovemmnt = new AccountMovement()
        //        {
        //            AccountMovementDate = accountMovementVm.mdate,
        //            AccountId = db.Accounts.First(n => n.AccountName == SecondAccount.SecondAccountName).AccountId,
        //            Debit = Convert.ToDecimal(SecondAccount.Debit),
        //            Crdit = Convert.ToDecimal(SecondAccount.Crdit),
        //            BranchId = db.AccountMovements.First(n => n.Brabch.BranchName == SecondAccount.BranchName).BranchId,
        //            AccountOrderId = db.AccountOrders.First(n => n.OrderDate == accountMovementVm.mdate && n.AccountOrderProperties.AccountOrderPropertiesName == accountMovementVm.AccountOrderPropertiesName).AccountOrderId,
        //            AccountMovementNote = SecondAccount.Note
        //        };
        //        secondAccountMovement.Add(sMovemmnt);
        //    }

        //    db.AccountMovements.AddRange(secondAccountMovement);
        //    await  db.SaveChangesAsync();
        //}



        //async Task<IHttpActionResult> AddAccountOrder(AccountOrderVm accountOrderVm)
        //{
        //AccountOrder accountOrder = db.AccountOrders.First(a => a.OrderDate == accountOrderVm.date);
        //if(accountOrder.AccountOrderId == 0)
        //{
        //    accountOrder = new AccountOrder()
        //    {
        //        OrderDate = accountOrderVm.date,
        //        AccountOrderPropertiesId = db.AccountOrderProperties.First(p => p.AccountOrderPropertiesName == accountOrderVm.AccountMovementFrom.AccountOrderPropertiesName).AccountOrderPropertiesId

        //    };

        //    db.AccountOrders.Add(accountOrder);
        //    await db.SaveChangesAsync();
        //    accountOrder = db.AccountOrders.First(a => a.OrderDate == accountOrderVm.date);
        //    AccountMovement accountMovementFrom = new AccountMovement()
        //    {
        //        AccountMovementDate = accountOrderVm.AccountMovementFrom.mdate,
        //        Crdit = Convert.ToDecimal(accountOrderVm.AccountMovementFrom.Crdit),
        //        Debit = Convert.ToDecimal(accountOrderVm.AccountMovementFrom.Debit),
        //        AccountOrderId  = accountOrder.AccountOrderId,
        //        BranchId  = db.Branchs.First(b => b.BranchName == accountOrderVm.AccountMovementFrom.BranchName).BranchId,
        //        AccountId = db.Accounts.First(a => a.AccountName == accountOrderVm.AccountMovementFrom.name).AccountId,

        //    };
        //    List<AccountMovement> AccountMovements = null;
        //    foreach(var i in accountOrderVm.AccountMovementsTo)
        //    {
        //        AccountMovement accountMovementTo = new AccountMovement()
        //        {
        //            AccountMovementDate = i.mdate,
        //            Crdit = Convert.ToDecimal(i.Crdit),
        //            Debit = Convert.ToDecimal(i.Debit),
        //            AccountOrderId = accountOrder.AccountOrderId,
        //            BranchId = db.Branchs.First(b => b.BranchName == i.BranchName).BranchId,
        //            AccountId = db.Accounts.First(a => a.AccountName == i.name).AccountId,

        //        };
        //        AccountMovements.Add(accountMovementTo);
        //    }

        //    db.AccountMovements.AddRange(AccountMovements);
        //    await db.SaveChangesAsync();

        //}

        //}

        //public IQueryable<AccountPropertyValuesVM> GetAllAccountPropertyValues(Int32 accountCategoryId)
        //{
        //    var accountPropertyVlaues = from i in db.AccountCategoryPropertiesValues
        //                                where i.AccountCategoryProperties.AccountCategoryId == accountCategoryId
        //                                select new AccountPropertyValuesVM()
        //                                {
        //                                    valueId = i.AccountCategoryPropertiesValueName,
        //                                    valueName = i.AccountCategoryPropertiesValueName,
        //                                    propertyId = i.AccountCategoryProperties.AccountCategoryPropertiesName,
        //                                    propertyName = i.AccountCategoryProperties.AccountCategoryPropertiesName

        //                                };

        //    return accountPropertyVlaues;
        //}

    }
}