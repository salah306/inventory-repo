﻿using Inventory.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }




        public DbSet<InventoryRequest> InventoryRequests { get; set; }

        public DbSet<InventoryItemsRequest> InventoryItemsRequests { get; set; }

        


        public DbSet<AccountO> AccountOs { get; set; }

        public DbSet<ItemsRequestO> ItemsRequestOs { get; set; }

        public DbSet<PayO> PayOs { get; set; }
        public DbSet<TotalAccountO> TotalAccountOs { get; set; }
        public DbSet<TermO> TermOs { get; set; }
        public DbSet<OrderO> OrderOs { get; set; }



        public DbSet<InventoriesConfigurtion> InventoriesConfigurtion { get; set; }

        public DbSet<SalesOrdersConfiguration> SalesOrdersConfigurations { get; set; }
        public DbSet<SalessOrdersConfigurationPayMethod> SalessOrdersConfigurationPayMethods { get; set; }
        public DbSet<SalessOrdersConfigurationTerm> SalessOrdersConfigurationTerms { get; set; }
        public DbSet<SalessOrdersConfigurationTableAccount> SalessOrdersConfigurationTableAccounts { get; set; }
        public DbSet<SalessOrdersConfigurationTotalAccount> SalessOrdersConfigurationTotalAccounts { get; set; }
        public DbSet<SalessOrdersConfigurationAccountType> SalessOrdersConfigurationAccountTypes { get; set; }

        public DbSet<PurchaseOrdersConfiguration> PurchaseOrdersConfigurations { get; set; }
        public DbSet<PurchasesOrdersConfigurationPayMethod> PurchasesOrdersConfigurationPayMethods { get; set; }
        public DbSet<PurchasesOrdersConfigurationTerm> PurchasesOrdersConfigurationTerms { get; set; }
        public DbSet<PurchasesOrdersConfigurationTableAccount> PurchasesOrdersConfigurationTableAccounts { get; set; }
        public DbSet<PurchasesOrdersConfigurationTotalAccount> PurchasesOrdersConfigurationTotalAccounts { get; set; }
        public DbSet<PurchasesOrdersConfigurationAccountType> PurchasesOrdersConfigurationAccountTypes { get; set; }

        public DbSet<TempItem> TempItems { get; set; }
        public DbSet<PurchaseOrder> PurchaseOrder { get; set; }

        public DbSet<ItemsRequest> ItemsRequest { get; set; }

        public DbSet<CompanyProperties> CompanyProperties { get; set; }
        public DbSet<CompanyPropertyValues> CompanyPropertyValues { get; set; }
        public DbSet<BranchProperties> BranchProperties { get; set; }
        public DbSet<BranchPropertyValues> BranchPropertyValues { get; set; }

        public DbSet<InventoryTransferEntity> InventoryTransferEntity { get; set; }

        public DbSet<InventoryItems> InventoryItems { get; set; }

        public DbSet<OrderMoveRequest> OrderMoveRequest { get; set; }
        public DbSet<branchGroupSt> branchGroupSt { get; set; }
        public DbSet<billMainAccountSalesSt> billMainAccountSalesSt { get; set; }
        public DbSet<billMainAccountPurSt> billMainAccountPurSt { get; set; }
        public DbSet<selectedOrderGroupNameSt> selectedOrderGroupNameSt { get; set; }
        public DbSet<inventorySt> inventorySt { get; set; }

        public DbSet<OrderRequest> orderRequest { get; set; }
        //OrderRequestItems
        public DbSet<OrderRequestItems> OrderRequestItems { get; set; }
        public DbSet<AccountsCatogeryTypesAcccounts> AccountsCatogeryTypesAcccounts { get; set; }
        public DbSet<settingProperties> settingProperties { get; set; }
        public DbSet<settingRole> settingRole { get; set; }
        public DbSet<settingPropertiesforUser> settingPropertiesforUser { get; set; }
        public DbSet<settingRolesForUser> settingRolesForUser { get; set; }
        public DbSet<RolepropertiesforUser> RolepropertiesforUser { get; set; }

        //BillSubAccountItems
        public DbSet<BillSubAccountItems> BillSubAccountItems { get; set; }
        public DbSet<BillOtherAccountBill> BillOtherAccountBill { get; set; }
        public DbSet<ItemsRow> ItemsRows { get; set; }

        //itemsRow
        public DbSet<SalesBill> SalesBills { get; set; }
        public DbSet<PurchasesBill> PurchasesBills { get; set; }
        public DbSet<BillAccountItems> BillAccountItems { get; set; }
        public DbSet<BillPaymentProperties> BillPaymentProperties { get; set; }
        public DbSet<BillPaymentPropertiesSales> BillPaymentPropertiesSales { get; set; }
        public DbSet<InventoryValuationMethod> InventoryValuationMethods { get; set; }

        public DbSet<OrderGroupPayMethodSales> OrderGroupPayMethodSales { get; set; }
        public DbSet<OrderGroupPayMethodPurchases> OrderGroupPayMethodPurchases { get; set; }
        public DbSet<Sales> Sales { get; set; }
        public DbSet<Purchases> Purchases { get; set; }
        public DbSet<OrdersTypeSales> OrdersTypeSales { get; set; }
        public DbSet<OrderPropertiesPayPurchases> OrderPropertiesPayPurchases { get; set; }
        public DbSet<OrderPropertiesPaySales> OrderPropertiesPaySales { get; set; }

        public DbSet<OrdersTypePurchases> OrdersTypePurchases { get; set; }
       // public DbSet<subAccountNpropertyValues> subAccountNpropertyValues { get; set; }
        public DbSet<Vocabulary> Vocabularies { get; set; }
        public DbSet<OrderGroup> OrderGroup { get; set; }
        public DbSet<OrderMainAccount> OrderMainAccounts { get; set; }
        public DbSet<OrderPropertyType> OrderPropertyTypes { get; set; }

        public DbSet<OrderAccountsforSubAccountSales> OrderAccountsforSubAccountSales { get; set; }
        public DbSet<OrderAccountsforSubAccountPurchases> OrderAccountsforSubAccountPurchases { get; set; }

        public DbSet<OrderOtherTotalAccountSales> OrderOtherTotalAccountSales { get; set; }
        public DbSet<OrderOtherTotalAccountPurchases> OrderOtherTotalAccountPurchases { get; set; }
        
        public DbSet<TempItempropertyValues> TempItempropertyValues { get; set; }
        public DbSet<SubAccount> SubAccounts { get; set; }
        public DbSet<ItemGroup> ItemGroups { get; set; }
        public DbSet<PropertiesforItemGroup> PropertiesforItemGroups { get; set; }
        public DbSet<PropertyValuesforItemGroup> PropertyValuesforItemGroups { get; set; }
        public DbSet<TypeforItemGroup> TypeforItemGroups { get; set; }
        public DbSet<AccountsforItemGroup> AccountsforItemGroups { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<UnitType> UnitTypes { get; set; }
       

        public DbSet<SubAccountMovement> SubAccountMovements { get; set; }
        public DbSet<SubAccountOrder> SubAccountOrders { get; set; }

        public DbSet<CustomAccount> CustomAccounts { get;set;}
        public DbSet<CustomAccountCategory> CustomAccountCategories { get; set; }
        public DbSet<CustomBalanceSheetType> CustomBalanceSheetTypes { get; set; }
        public DbSet<CustomBalanceSheet> CustomBalanceSheets { get; set; }
        public DbSet<FinancialList> FinancialLists { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<ActivityLog> ActivityLogs { get; set; }

        public DbSet<Company> Companies { get; set; }
        public DbSet<RoleCollection> RoleCollections { get; set; }
        public DbSet<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        public DbSet<CompanyRole> CompanyRoles { get; set; }
        public DbSet<AppUserCompanyRole> AppUserCompanyRoles { get; set; }

        public DbSet<BalanceSheet> BalanceSheets { get; set; }
        public DbSet<BalanceSheetType> BalanceSheetTypes { get; set; }
        public DbSet<Branch> Branchs { get; set; }
      
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }
        public DbSet<AccountMovement> AccountMovements { get; set; }
        public DbSet<AccountCategory> AccountCategories { get; set; }
        public DbSet<AccountCategoryProperties> AccountCategoryProperties { get; set; }
        public DbSet<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }
        public DbSet<AccountOrderProperties> AccountOrderProperties { get; set; }
        public DbSet<AccountOrder> AccountOrders { get; set; }
    

        public DbSet<AccountsDataTypes> AccountsDataTypes { get; set; }

        //protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entry, IDictionary<object, object> items)
        //{
        //    var result = new DbEntityValidationResult(entry, new List<DbValidationError>());


        //    if (entry.Entity is OrderO && (entry.State == EntityState.Added || entry.State == EntityState.Modified))
        //    {
              
        //    }


        //    if (result.ValidationErrors.Count > 0)
        //    {
        //        return result;
        //    }
        //    else
        //    {
        //        return base.ValidateEntity(entry, items);
        //    }
        //}


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BalanceSheetConfiguration());
            modelBuilder.Configurations.Add(new VocabularyConfiguration());
            modelBuilder.Configurations.Add(new ApplicationRoleConfiguration());
            modelBuilder.Configurations.Add(new ApplicationUserComapnyConfiguration());
            modelBuilder.Configurations.Add(new CompanyConfiguration());
            modelBuilder.Configurations.Add(new CompanyRoleConfiguration());
            modelBuilder.Configurations.Add(new AppUserCompanyRoleConfiguration());
            modelBuilder.Configurations.Add(new BranchConfiguration());
           
           
            modelBuilder.Configurations.Add(new AccountConfiguration());
            modelBuilder.Configurations.Add(new AccountTypeConfiguration());
            modelBuilder.Configurations.Add(new AccountMovementConfiguration());
            modelBuilder.Configurations.Add(new AccountCategoryConfiguration());
            modelBuilder.Configurations.Add(new AccountCategoryPropertiesConfiguration());
            modelBuilder.Configurations.Add(new AccountCategoryPropertiesValuesConfiguration());
            modelBuilder.Configurations.Add(new AccountOrderPropertiesConfiguration());
            modelBuilder.Configurations.Add(new AccountOrderConfiguration());
            modelBuilder.Configurations.Add(new ApplicationUserConfiguration());




            base.OnModelCreating(modelBuilder);
        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}