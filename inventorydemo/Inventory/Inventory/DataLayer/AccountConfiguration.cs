﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class AccountConfiguration : EntityTypeConfiguration<Account>
    {
        public AccountConfiguration()
        {
            Property(Ac => Ac.AccountName)
               .HasMaxLength(200)
               .IsRequired()
               .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_Accounts_AccountName" , 1) { IsUnique = false }));
           
            Property(co => co.BranchId)
                .IsOptional()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_Accounts_AccountName", 2) { IsUnique = false }));

            Property(Ac => Ac.AccountAliasName)
                .HasMaxLength(200)
                .IsOptional();

            Property(Ac => Ac.CustomAccountCategoryId)
               .IsOptional();
            
            //Property(Ac => Ac.TotalDebit).HasPrecision(18, 2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            //Property(Ac => Ac.TotalCrdit).HasPrecision(18, 2).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);


        }
    }
}