﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class AccountCategoryConfiguration : EntityTypeConfiguration<AccountCategory>
    {
        public AccountCategoryConfiguration()
        {

            Property(co => co.AccountCategoryName)
               .HasMaxLength(200)
               .IsRequired()
               .HasColumnAnnotation("Index",
               new IndexAnnotation(new IndexAttribute("AK_AccountCategory_AccountCategoryName", 1) { IsUnique = true }));
            Property(co => co.BalanceSheetTypeId)
                .IsRequired()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_AccountCategory_AccountCategoryName", 2) { IsUnique = true }));
            Property(co => co.AccountTypeId)
               .IsOptional();

        }
    }
}