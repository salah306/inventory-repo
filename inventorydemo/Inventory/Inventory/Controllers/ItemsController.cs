﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Inventory.Utility;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/items")]

    public class ItemsController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private itemsManager itemsManager;
        public ItemsController()
        {
            this.itemsManager = new itemsManager(this.db);
        }

        [Route("getUnits")]
        [HttpPost]
        public IHttpActionResult getUnits()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

            if (company == null)
            {
                return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");
            }

           

            var types = db.TypeforItemGroups.Where(a => a.show).Select(u => new TypeDTO
            {
                typeId = u.TypeforItemGroupId,
                typeName = u.TypeName
            });
            var units = db.Units.Where(a => a.CompanyId == company.companyId).Select(u => new UnitDTO
            {
                unitId = u.UnitId,
                unitName = u.UnitName,
                isNew = false,
                type = new TypeDTO {
                    typeId = u.UnitType.UnitTypeId,
                    typeName = u.UnitType.UnitTypeName
                }
            });
            var UnitTypes = db.UnitTypes.Select(t => new TypeDTO()
            {
                typeId = t.UnitTypeId,
                typeName = t.UnitTypeName

            });

            var invetory = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId)
                .Select(i => new InventoryyDTO
                {
                    inventoryId = i.AccountId,
                    inventoryName = i.AccountName + " - " + i.Code

                });
            var nullobj = new
            {
                typeDTO = new TypeDTO { isNew = true, typeId = 0, typeName = null },
                UnitDTO = new UnitDTO { isNew = true, unitId = 0, unitName = null, type = UnitTypes.FirstOrDefault() },
                itemsGroupDTO = new itemsGroupDTO { isNew = true, groupId = 0, groupName = null, inventoryies = null, properties = new List<PropertyDTO> { new PropertyDTO() { propertyNameId = 0, propertyName = null, isNew = true, type = types.FirstOrDefault() } }, unit = units.FirstOrDefault() },
                invetoryDTO = new InventoryyDTO { inventoryId = 0, inventoryName = null, isNew = true }
            };
            try
            {
                var getInfo = itemsManager.getItemInfo(new getitemInfoVM() { GroupId = 0, InventoryId = 0, orderType = true, pageNumber = 1, pageSize = 10, orderby = "qty" }, company);
                var groups = itemsManager.getAllGroupsByCompany(company.companyId);
                var toReturn = new { inventories = invetory, types = types, newObjctes = nullobj, units = units, unitTypes = UnitTypes, itemGroups = groups, items = getInfo.items, count = getInfo.count, totalPages = getInfo.totalPages, pageSize = getInfo.pageSize };

                return Ok(toReturn);
            }
            catch(Exception e)
            {

                return BadRequest(e.Message);
            }


        }

        [Route("getitemInfo")]
        [HttpPost]
        public IHttpActionResult Get(getitemInfoVM or)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");
            }

            
            or.pageSize = 10;


            var getInfo = itemsManager.getItemInfo(or, company);
            var toReturn = new { items = getInfo.items, count = getInfo.count, totalPages = getInfo.totalPages, pageSize = getInfo.pageSize };

            return Ok(toReturn);
        }


        [Route("save")]
        [HttpPost]
        public IHttpActionResult save(itemsGroupDTO group)
        {
       
            if (!ModelState.IsValid)
            {
                var err = ModelState.Values.SelectMany(m => m.Errors)
                          .Select(e => e.ErrorMessage)
                          .ToList();
                string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                return BadRequest(tocoorect);
            }

            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            try
            {
          
                var save = itemsManager.saveGroup(group, company, uid);
                return Ok(save);
            }
            catch (DbEntityValidationException ex)
            {
                var err = ex.EntityValidationErrors.SelectMany(m => m.ValidationErrors)
                               .Select(e => e.ErrorMessage)
                               .ToList();
                string tocoorect = "لا يمكن اتمام العملية لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                var gr = itemsManager.GetGroupByName(group.groupName, company.companyId);

                return Content(HttpStatusCode.BadRequest, new { err = tocoorect, Group = gr });


            }


        }

        [Route("getItemsbyGroupId/{groupId}")]
        [HttpGet]
        public IHttpActionResult getItemsbyGroup(int groupId)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");
            }
            return Ok(itemsManager.getItemsbyGroup(groupId , company));
            //getItemsbyGroup
        }
    }
}
