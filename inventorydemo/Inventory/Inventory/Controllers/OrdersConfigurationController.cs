﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/ordersconfiguration")]
    [Authorize]
    public class OrdersConfigurationController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private OrdersConfigurationManager OrdersConfigurationManager;
        public OrdersConfigurationController()
        {
            this.OrdersConfigurationManager = new OrdersConfigurationManager(this.db);
        }


        [Route("getOrdersName")]
        [HttpPost]
      
        public IHttpActionResult GetOrdersName()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var namesPur = db.PurchaseOrdersConfigurations.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId))
               .Select(n => new
               {
                   PurId = n.PurchaseOrdersConfigurationId,
                   PurOrderName = n.PurchaseOrderName
               }).ToList();

            var namesSales = db.SalesOrdersConfigurations.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId))
             .Select(n => new
             {
                 salesId = n.SalesOrdersConfigurationId,
                 salesOrderName = n.SalesOrderName
             }).ToList();

            var getCat = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                          && a.AccountsDataTypes.aliasName != "inventory" && a.AccountsDataTypes.aliasName != "cust" && a.AccountsDataTypes.aliasName != "pur")
                              .Select(c => new
                              {
                                  AccountId = c.AccountCategoryId,
                                  AccountName = c.AccountCategoryName,
                                  Code = c.Code,
                                  Childern = c.Accounts.Where(a => a.BranchId == (company.isCompany ? (int?)null : company.oId)).Select(a => new
                                  {
                                      AccountId = a.AccountId,
                                      AccountName = a.AccountName,
                                      Code = a.Code
                                  }).ToList()
                              }).ToList();

            var getCa = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.AccountCategory.AccountsDataTypes.aliasName == "other" && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(c => new
                {
                    AccountId = c.AccountId,
                    AccountName = c.AccountName,
                    Code = c.Code
                }).ToList();
            #region Inventories
            var inv = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
             .Select(i => new
             {
                 InventoryId = i.AccountId,
                 InventoryName = i.AccountName,
                 code = i.Code
             }).ToList();
            var getCrditedAccounts = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن"
            && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.BranchId == (company.isCompany ? (int?)null : company.oId)
            && a.AccountCategory.AccountsDataTypes.aliasName == "sales")
                .Select(i => new
                {
                    SalesAccountId = i.AccountId,
                    SalesAccountName = i.AccountName,
                    code = i.Code
                }).ToList();

            var getDbitedAccounts = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين"
               && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.AccountCategory.AccountsDataTypes.aliasName == "cost-sales")
                 .Select(i => new
                 {
                     SalesCostAccountId = i.AccountId,
                     SalesCostAccountName = i.AccountName,
                     code = i.Code
                 }).ToList();

            var toReturn = new { Inventories = inv, sales = getCrditedAccounts, salesCost = getDbitedAccounts };
            #endregion
            #region getinventoryConfig
            var invConfig = db.InventoriesConfigurtion.Where(i => i.IsCompany == company.isCompany && i.CompanyId == company.companyId && i.BranchId == (company.isCompany ? (int?)null : company.oId)).
                Select(ic => new inventoriesConfigurtionVm()
                {
                    inventoryId = ic.InventoryId,
                    inventoryName = ic.Inventory.AccountName + ic.Inventory.Code,
                    salesAccountId = ic.SalesAccountId,
                    salesAccountName = ic.SalesAccount.AccountName + ic.SalesAccount.Code,
                    salesCostAccountId = ic.SalesCostAccountId ,
                    salesCostAccountName = ic.SalesCostAccount.AccountName + ic.SalesCostAccount.Code,
                    salesReturnId = ic.SalesReturnId,
                    salesReturnName = ic.SalesReturn.AccountName + ic.SalesReturn.Code

                }).ToList();
            #endregion

            var returnd = new { sales = namesSales, pur = namesPur , accounts = getCa , accountsCatogery = getCat , Inventories = toReturn , inveConfig = invConfig };

            return Ok(returnd);

        }

        [Route("GetOrderbyId")]
        [HttpPost]
        public IHttpActionResult GetOrderbyId(SetOrdersConfigurations o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if(o.type == "sales")
            {
                var order = OrdersConfigurationManager.getSalesOrderconfgById(o, company);
                if(order == null)
                {
                    return BadRequest("الاعدادت غير مسجلة او غير متاحة في الوقت الحالي");
                }
                return Ok(order);
                
            }
            if (o.type == "pur")
            {
                var order = OrdersConfigurationManager.getPurOrderconfgById(o, company);
                if (order == null)
                {
                    return BadRequest("الاعدادت غير مسجلة او غير متاحة في الوقت الحالي");
                }
                return Ok(order);
            }
            return BadRequest("الاعدادت غير متاحة");

        }

        [Route("getAccounts")]
        [HttpPost]
        public IHttpActionResult getAccounts()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var getCat = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.AccountCategory.AccountsDataTypes.aliasName == "other" && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(c => new
                {
                    AccountId = c.AccountId,
                    AccountName = c.AccountName,
                    Code = c.Code
                }).ToList();
            if (getCat == null || getCat.Count == 0)
            {
                return BadRequest("لا يوجد حسابات جاهزة الان - اختار شركة اخري او قم باعداد الحسابات");
            }
            return Ok(getCat);
        }


        [Route("getCatogory")]
        [HttpPost]
        public IHttpActionResult getCatogoryFor()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var getCat = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.AccountsDataTypes.aliasName != "inventory" && a.AccountsDataTypes.aliasName != "cust" && a.AccountsDataTypes.aliasName != "pur")
                .Select(c => new
                {
                    AccountId = c.AccountCategoryId,
                    AccountName = c.AccountCategoryName,
                    Code = c.Code,
                    Childern = c.Accounts.Where(a => a.BranchId == (company.isCompany ? (int?)null : company.oId)).Select(a => new
                    {
                        AccountId = a.AccountId,
                        AccountName = a.AccountName,
                        Code = a.Code
                    }).ToList()
                }).ToList();
            if (getCat == null || getCat.Count == 0)
            {
                return BadRequest("لا يوجد حسابات جاهزة الان - اختار شركة اخري او قم باعداد الحسابات");
            }
            return Ok(getCat);
        }

        [Route("saveSalesOrderconfig")]
        [HttpPost]
        public IHttpActionResult saveSalesOrderconfig(SalesOrdersConfigurationVm o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            try
            {
                if (o.salesOrderIsNew)
                {

                    #region New Insert
                    #region checkvaild
                    if (!ModelState.IsValid)
                    {
                        var err = ModelState.Values.SelectMany(m => m.Errors)
                                  .Select(e => e.ErrorMessage)
                                  .ToList();
                        string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                        foreach (var er in err)
                        {
                            tocoorect = tocoorect + "<br>" + er;
                        }
                        return BadRequest(tocoorect);
                    }
                    #endregion

                    var or = new SalesOrdersConfiguration()
                    {
                        SalesOrderName = o.salesOrderName,
                        SalesReturnName = o.salesReturnName,
                        SalesName = o.salesName,
                        CompanyId = company.companyId,
                        IsCompany = company.isCompany,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        CurrentWorkerId = uid,
                        IsActiveOrder = true,
                        DirectGlTransaction = false,
                        DefaultPayAccountCategoryId = o.defaultPayName == 0 ? o.payMethod.FirstOrDefault().accountId : o.defaultPayName,
                        TitleAccountCategoryId = db.AccountCategories.FirstOrDefault(a => a.AccountsDataTypes.aliasName == "cust").AccountCategoryId,
                        OnlyInventory = false,
                        TitleAccountNickName = "العميل",
                        Terms =o.terms == null ? null: o.terms.Select(tr => new SalessOrdersConfigurationTerm()
                        {
                            Note = tr.name,
                            CurrentWorkerId = uid,
                        }).ToList(),
                        PayMethod = o.payMethod.Select(pm => new SalessOrdersConfigurationPayMethod()
                        {
                            AccountCategoryId = pm.accountId,
                            CurrentWorkerId = uid,
                            PayName = pm.payName,
                            DefaultChildAccountId = pm.defaultChildAccountId,

                        }).ToList(),
                        TableAccounts = o.tableAccounts == null ? null : o.tableAccounts.Select(ta => new SalessOrdersConfigurationTableAccount()
                        {
                            AccountId = ta.accountId,
                            AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                            AccountTypeId = db.SalessOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).SalessOrdersConfigurationAccountTypeId,

                        }).ToList(),
                        TotalAccounts = o.totalAccounts == null ? null : o.totalAccounts.Select(ta => new SalessOrdersConfigurationTotalAccount()
                        {
                            AccountId = ta.accountId,
                            AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                            AccountTypeId = db.SalessOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).SalessOrdersConfigurationAccountTypeId,

                        }).ToList(),

                    };

                    db.SalesOrdersConfigurations.Add(or);
                    db.SaveChanges();
                    var toreturn = OrdersConfigurationManager.getSalesOrderconfgById(new SetOrdersConfigurations { orderId = or.SalesOrdersConfigurationId, type = "sales" }, company);

                    return Ok(toreturn);
                    #endregion
                }
                else
                {
                    #region Not New Update
                    #region checkvaild
                    if (!ModelState.IsValid)
                    {
                        var err = ModelState.Values.SelectMany(m => m.Errors)
                                .Select(e => e.ErrorMessage)
                                .ToList();
                        string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                        foreach (var er in err)
                        {
                            tocoorect = tocoorect + "<br>" + er;
                        }
                        return BadRequest(tocoorect);
                    }
                    #endregion

                    var or = db.SalesOrdersConfigurations.Where(a => a.SalesOrdersConfigurationId == o.salesId && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).SingleOrDefault();
                    if (or == null)
                    {
                        return BadRequest("اعداد الامر الذي تحاول تحديثة غير متاح حاليا او غير مسجل");
                    }
                    or.SalesOrderName = o.salesOrderName;
                    or.SalesName = o.salesName;
                    or.SalesReturnName = o.salesReturnName;
                    or.CurrentWorkerId = uid;
                    or.DefaultPayAccountCategoryId = o.defaultPayName;
                    or.IsActiveOrder = true;
                    or.DirectGlTransaction = false;
                    or.TitleAccountCategoryId = db.AccountCategories.FirstOrDefault(a => a.AccountsDataTypes.aliasName == "cust").AccountCategoryId;


                    db.SalessOrdersConfigurationTerms.RemoveRange(or.Terms);
                    db.SalessOrdersConfigurationTableAccounts.RemoveRange(or.TableAccounts);
                    db.SalessOrdersConfigurationTotalAccounts.RemoveRange(or.TotalAccounts);
                    db.SalessOrdersConfigurationPayMethods.RemoveRange(or.PayMethod);
                    or.Terms = o.terms.Select(tr => new SalessOrdersConfigurationTerm()
                    {
                        CurrentWorkerId = uid,
                        Note = tr.name,
                    }).ToList();

                    or.PayMethod = o.payMethod.Select(pm => new SalessOrdersConfigurationPayMethod()
                    {
                        AccountCategoryId = pm.accountId,
                        CurrentWorkerId = uid,
                        PayName = pm.payName,
                        DefaultChildAccountId = pm.defaultChildAccountId,

                    }).ToList();
                    or.TableAccounts = o.tableAccounts.Select(ta => new SalessOrdersConfigurationTableAccount()
                    {
                        AccountId = ta.accountId,
                        AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                        AccountTypeId = db.SalessOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).SalessOrdersConfigurationAccountTypeId,

                    }).ToList();
                    or.TotalAccounts = o.totalAccounts.Select(ta => new SalessOrdersConfigurationTotalAccount()
                    {
                        AccountId = ta.accountId,
                        AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                        AccountTypeId = db.SalessOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).SalessOrdersConfigurationAccountTypeId,

                    }).ToList();
                    db.Entry(or).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    var toreturn = OrdersConfigurationManager.getSalesOrderconfgById(new SetOrdersConfigurations { orderId = or.SalesOrdersConfigurationId, type = "sales" }, company);

                    return Ok(toreturn);
                    #endregion
                }
            }
            catch 
            {
                return BadRequest("بعض البيانات التي تحاول تسجيلها غير صحيحة او مكررة او لا تملك الصلاحية لادخالها");
            }
           

        }


        [Route("savePurchaseOrderconfig")]
        [HttpPost]
        public IHttpActionResult savePurchaseOrderconfig(PurchaseOrdersConfigurationVm o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            try
            {
                if (o.PurOrderIsNew)
                {

                    #region New Insert
                    #region checkvaild
                    if (!ModelState.IsValid)
                    {
                        var err = ModelState.Values.SelectMany(m => m.Errors)
                                  .Select(e => e.ErrorMessage)
                                  .ToList();
                        string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                        foreach (var er in err)
                        {
                            tocoorect = tocoorect + "<br>" + er;
                        }
                        return BadRequest(tocoorect);
                    }
                    #endregion

                    var or = new PurchaseOrdersConfiguration()
                    {
                        PurchaseOrderName = o.PurOrderName,
                        PurchaseReturnName = o.PurReturnName,
                        PurchaseName = o.PurName,
                        CompanyId = company.companyId,
                        IsCompany = company.isCompany,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        CurrentWorkerId = uid,
                        IsActiveOrder = true,
                        DirectGlTransaction = false,
                        DefaultPayAccountCategoryId = o.defaultPayName == 0 ? o.payMethod.FirstOrDefault().accountId : o.defaultPayName,
                        TitleAccountCategoryId = db.AccountCategories.FirstOrDefault(a => a.AccountsDataTypes.aliasName == "pur").AccountCategoryId,
                        OnlyInventory = false,
                        TitleAccountNickName = "المورد",
                        Terms = o.terms == null ? null : o.terms.Select(tr => new PurchasesOrdersConfigurationTerm()
                        {
                            Note = tr.name,
                            CurrentWorkerId = uid,
                        }).ToList(),
                        PayMethod = o.payMethod.Select(pm => new PurchasesOrdersConfigurationPayMethod()
                        {
                            AccountCategoryId = pm.accountId,
                            CurrentWorkerId = uid,
                            PayName = pm.payName,
                            DefaultChildAccountId = pm.defaultChildAccountId,

                        }).ToList(),
                        TableAccounts = o.tableAccounts == null ? null : o.tableAccounts.Select(ta => new PurchasesOrdersConfigurationTableAccount()
                        {
                            AccountId = ta.accountId,
                            AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                            AccountTypeId = db.PurchasesOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).PurchasesOrdersConfigurationAccountTypeId,

                        }).ToList(),
                        TotalAccounts = o.totalAccounts == null ? null : o.totalAccounts.Select(ta => new PurchasesOrdersConfigurationTotalAccount()
                        {
                            AccountId = ta.accountId,
                            AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                            AccountTypeId = db.PurchasesOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).PurchasesOrdersConfigurationAccountTypeId,

                        }).ToList(),

                    };

                    db.PurchaseOrdersConfigurations.Add(or);
                    db.SaveChanges();
                    var toreturn = OrdersConfigurationManager.getPurOrderconfgById(new SetOrdersConfigurations { orderId = or.PurchaseOrdersConfigurationId, type = "pur" }, company);

                    return Ok(toreturn);
                    #endregion
                }
                else
                {
                    #region Not New Update
                    #region checkvaild
                    if (!ModelState.IsValid)
                    {
                        var err = ModelState.Values.SelectMany(m => m.Errors)
                                  .Select(e => e.ErrorMessage)
                                  .ToList();
                        string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                        foreach (var er in err)
                        {
                            tocoorect = tocoorect + "<br>" + er;
                        }
                        return BadRequest(tocoorect);
                    }
                    #endregion

                    var or = db.PurchaseOrdersConfigurations.Where(a => a.PurchaseOrdersConfigurationId == o.PurId && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).SingleOrDefault();
                    if (or == null)
                    {
                        return BadRequest("اعداد الامر الذي تحاول تحديثة غير متاح حاليا او غير مسجل");
                    }
                    or.PurchaseOrderName = o.PurOrderName;
                    or.PurchaseName = o.PurName;
                    or.PurchaseReturnName = o.PurReturnName;
                    or.CurrentWorkerId = uid;
                    or.DefaultPayAccountCategoryId = o.defaultPayName;
                    or.IsActiveOrder = true;
                    or.DirectGlTransaction = false;
                    or.TitleAccountCategoryId = db.AccountCategories.FirstOrDefault(a => a.AccountsDataTypes.aliasName == "pur").AccountCategoryId;

                    db.PurchasesOrdersConfigurationTerms.RemoveRange(or.Terms);
                    db.PurchasesOrdersConfigurationTableAccounts.RemoveRange(or.TableAccounts);
                    db.PurchasesOrdersConfigurationTotalAccounts.RemoveRange(or.TotalAccounts);
                    db.PurchasesOrdersConfigurationPayMethods.RemoveRange(or.PayMethod);
                    or.Terms = o.terms.Select(tr => new PurchasesOrdersConfigurationTerm()
                    {
                        CurrentWorkerId = uid,
                        Note = tr.name,
                    }).ToList();

                    or.PayMethod = o.payMethod.Select(pm => new PurchasesOrdersConfigurationPayMethod()
                    {
                        AccountCategoryId = pm.accountId,
                        CurrentWorkerId = uid,
                        PayName = pm.payName,
                        DefaultChildAccountId = pm.defaultChildAccountId,

                    }).ToList();
                    or.TableAccounts = o.tableAccounts.Select(ta => new PurchasesOrdersConfigurationTableAccount()
                    {
                        AccountId = ta.accountId,
                        AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                        AccountTypeId = db.PurchasesOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).PurchasesOrdersConfigurationAccountTypeId,

                    }).ToList();
                    or.TotalAccounts = o.totalAccounts.Select(ta => new PurchasesOrdersConfigurationTotalAccount()
                    {
                        AccountId = ta.accountId,
                        AccountNickName = string.IsNullOrEmpty(ta.nickName) ? ta.accountName : ta.nickName,
                        AccountTypeId = db.PurchasesOrdersConfigurationAccountTypes.SingleOrDefault(a => a.AccountTypeName == ta.accountType).PurchasesOrdersConfigurationAccountTypeId,

                    }).ToList();
                    db.Entry(or).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    var toreturn = OrdersConfigurationManager.getPurOrderconfgById(new SetOrdersConfigurations { orderId = or.PurchaseOrdersConfigurationId, type = "pur" }, company);

                    return Ok(toreturn);
                    #endregion
                }
            }
            catch 
            {
                return BadRequest("بعض البيانات التي تحاول تسجيلها غير صحيحة او مكررة او لا تملك الصلاحية لادخالها");
            }


        }

        [Route("Getinventory")]
        [HttpPost]
        public IHttpActionResult getinventory()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var inv = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(i => new
                {
                    InventoryId = i.AccountId,
                    InventoryName = i.AccountName,
                    code = i.Code
                }).ToList();
            var getCrditedAccounts = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن"
            && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.BranchId == (company.isCompany ? (int?)null : company.oId)
            && a.AccountCategory.AccountsDataTypes.aliasName == "sales")
                .Select(i => new
                {
                    SalesAccountId = i.AccountId,
                    SalesAccountName = i.AccountName,
                    code = i.Code
                }).ToList();

         var getDbitedAccounts = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين"
            && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.BranchId == (company.isCompany ? (int?)null : company.oId)
            && a.AccountCategory.AccountsDataTypes.aliasName == "cost-sales")
              .Select(i => new
              {
                  SalesCostAccountId = i.AccountId,
                  SalesCostAccountName = i.AccountName,
                  code = i.Code
              }).ToList();

            var toReturn = new { Inventories = inv, sales = getCrditedAccounts, salesCost = getDbitedAccounts };
            return Ok(toReturn);
        }

        [Route("saveinventory")]
        [HttpPost]
        public IHttpActionResult SaveInventory(List<inventoriesConfigurtionVm> inv)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            #region checkingOrder
            foreach (var i in inv)
            {
                if(i.salesAccountId == i.salesReturnId)
                {
                    return BadRequest("لا يمكن استخدام نفس الحساب للمبيعات ورد المبيعات");
                }
                var invacc = db.Accounts.SingleOrDefault(a => a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.AccountId == i.inventoryId);
                if(invacc == null)
                {
                    return BadRequest("حساب المخزون غير متاح لهذه الاعدادت");

                }
              var salacc =  db.Accounts.SingleOrDefault(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن"
                           && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                           && a.BranchId == (company.isCompany ? (int?)null : company.oId)
                           && a.AccountCategory.AccountsDataTypes.aliasName == "sales" && a.AccountId == i.salesAccountId);
               if (salacc == null)
                {
                    return BadRequest("حساب المبيعات غير متاح لهذه الاعدادت");

                }
                var salreturnacc = db.Accounts.SingleOrDefault(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن"
                          && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                          && a.BranchId == (company.isCompany ? (int?)null : company.oId)
                          && a.AccountCategory.AccountsDataTypes.aliasName == "sales" && a.AccountId == i.salesReturnId);
                if (salreturnacc == null)
                {
                    return BadRequest("حساب رد المبيعات غير متاح لهذه الاعدادت");

                }

                var salcost = db.Accounts.SingleOrDefault(a => a.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين"
               && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.AccountCategory.AccountsDataTypes.aliasName == "cost-sales" && a.AccountId == i.salesCostAccountId);
                if (salcost == null)
                {
                    return BadRequest("حساب تكلفة المبيعات غير متاح لهذه الاعدادت");

                }
            }
            #endregion

            #region save-update-delete
           var oldConfig = db.InventoriesConfigurtion.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).ToList();
            if(oldConfig != null && oldConfig.Count > 0)
            {
                db.InventoriesConfigurtion.RemoveRange(oldConfig);
            }
            var configList = inv.Select(i => new InventoriesConfigurtion()
            {
                CompanyId = company.companyId,
                BranchId = company.isCompany ? (int?)null : company.oId,
                IsCompany = company.isCompany,
                CurrentWorkerId = uid,
                InventoryId = i.inventoryId,
                SalesAccountId = i.salesAccountId,
                SalesCostAccountId = i.salesCostAccountId,
                SalesReturnId = i.salesReturnId,
                
            }).ToList();
            db.InventoriesConfigurtion.AddRange(configList);

            #endregion
            db.SaveChanges();
            return Ok();
        }
    }
}
