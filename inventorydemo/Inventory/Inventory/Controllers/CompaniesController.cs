﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using System.Security.Claims;

namespace Inventory.Controllers
{
    [RoutePrefix("api/Companies")]
    [Authorize]
    public class CompaniesController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();
        // GET: api/Companies
        [Route("Companies")]
        
        public List<CompaniesVm> GetCompanies()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            

            var companies = this.AppCompanyUserManager.GetAllCompanies(uid);
            return companies;
        }
        [Route("companiesById/{id:int}")]
        // GET: api/Companies/5
        [ResponseType(typeof(CompaniesVm))]
        public async Task<IHttpActionResult> GetCompany(int id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }
            string uid = RequestContext.Principal.Identity.GetUserId();

            var comVM = this.AppCompanyUserManager.GetAllCompanies(uid).Where(c => c.PublicId == company.CompanyId).SingleOrDefault();
            return Ok(comVM);
        }


        [Route("getUserCompanyRole")]
        // GET: api/Companies/5
        [ResponseType(typeof(List<UserRole>))]
        [System.Web.Http.HttpPost]
        public IHttpActionResult GetUserCompanyRole(UserCompanyRoleModel userRole)
        {
            
            return Ok(this.AppCompanyUserManager.GetUserRolesLang(userRole.CatogeryId, "Arabic", userRole.Catogery));
        }

        [Route("chickuser")]
        [HttpPost]
        public IHttpActionResult chickuser(checkUserEmail email)
        {
            bool userAlreadyExists = db.Users.Any(x => x.Email == email.Email);
            if (userAlreadyExists)
            {
                return Ok(userAlreadyExists);

            }
            else
            {
                return NotFound();
            }

          
        }

        [Route("updateCompany")]
        [HttpPost]
        public IHttpActionResult UpdateCompany(CompaniesVm com)
        {
            var uid = RequestContext.Principal.Identity.GetUserId();
            var cCom = db.Companies.SingleOrDefault(a => a.CompanyId == com.PublicId && a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if(cCom == null)
            {
                return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
            }
            var checkForName = db.Companies.Any(a => a.CompanyName == com.name && a.CompanyId != cCom.CompanyId && a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if (checkForName)
            {
                return BadRequest("توجد شركة مسجلة بنفس الاسم");
            }

            cCom.CompanyName = com.name;
            cCom.CurrentWorkerId = uid;
            db.Entry(cCom).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }


        [Route("updateBranch")]
        [HttpPost]
        public IHttpActionResult updateBranch(BranchesVm com)
        {
            var uid = RequestContext.Principal.Identity.GetUserId();
            var cCom = db.Branchs.SingleOrDefault(a => a.BranchId == com.PublicId && a.CompanyId == com.CompaniesId && a.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if (cCom == null)
            {
                return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
            }
            var checkForName = db.Branchs.Any(a => a.BranchName == com.name && a.BranchId != cCom.BranchId &&a.CompanyId == cCom.CompanyId && a.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if (checkForName)
            {
                return BadRequest("يوجد فرع مسجلة بنفس الاسم");
            }

            cCom.BranchName = com.name;
            cCom.CurrentWorkerId = uid;
            db.Entry(cCom).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }

        // POST: api/Companies
        [ResponseType(typeof(CompaniesVm))]
        public IHttpActionResult PostCompany()
        {
            
            var uid = RequestContext.Principal.Identity.GetUserId();
            var user = db.Users.Find(uid);

            var checkCompanycount = db.Companies.Where(a => a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)).Count();
            if(checkCompanycount >= user.MaxCompany)
            {
                return BadRequest("لقد تجاوزت الحد الاقصي من الشركات المسموح لك باضافتها");
            };

            var cName = "شركة" + this.AppCompanyUserManager.GetUniqueKey(4);
            do
            {
                cName = "شركة" + this.AppCompanyUserManager.GetUniqueKey(4);
            } while (db.Companies.Any(c => c.CompanyName == cName && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid )));

            var nowdate = DateTime.Now.Date;
            var enddate = nowdate.AddYears(1);
            var  company = new Company()
            {
                CompanyName = cName,
                CurrentWorkerId = uid,
                MaxBranch = 0 ,
                startDate = nowdate,
                endDate = enddate,
              
                CompanyProperties = new List<CompanyProperties>()
                {
                    new CompanyProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "عنوان").AccountsCatogeryTypesAcccountsId,
                        propertyName = "العنوان",
                        CurrentWorkerId = uid,
                        CompanyPropertyValues = new List<CompanyPropertyValues>()
                        {
                            new CompanyPropertyValues() {CurrentWorkerId = uid , propertyValue = "عنوان الشركة هنا"  }
                        }
                        
                    },

                    new CompanyProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "هاتف ارضي").AccountsCatogeryTypesAcccountsId,
                        propertyName = "هاتف",
                        CurrentWorkerId = uid,
                        CompanyPropertyValues = new List<CompanyPropertyValues>()
                        {
                            new CompanyPropertyValues() {CurrentWorkerId = uid , propertyValue = "002010000000000"  }
                        }

                    }
                   
                },
             

            };
            if(company.ApplicationUsersComapnies == null)
            {
                company.ApplicationUsersComapnies = new List<ApplicationUserComapny>();
            }
            company.ApplicationUsersComapnies.Add(new ApplicationUserComapny()
            {
                ApplicationUserId = uid,
                CompanyId = company.CompanyId
            });

            db.Companies.Add(company);
            db.SaveChanges();

            CompaniesVm co = new CompaniesVm()
            {
                id = company.CompanyName,
                PublicId = company.CompanyId,
                name = company.CompanyName
            };
            return Ok(co);


        }

        [Route("saveBranch")]
        [HttpPost]
        public IHttpActionResult saveBranch(CompaniesVm com)
        {
            var uid = RequestContext.Principal.Identity.GetUserId();
            var maxBranch = db.Companies.SingleOrDefault(a => a.CompanyId == com.PublicId && a.CompanyName == com.name && a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)).MaxBranch;

            var checkBranchCount = db.Branchs.Where(a => a.CompanyId == com.PublicId &&  a.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)).Count();
            if (checkBranchCount >= maxBranch)
            {
                return BadRequest("لقد تجاوزت الحد الاقصي من الشركات المسموح لك باضافتها");
            };

            var cName = "فرع" + this.AppCompanyUserManager.GetUniqueKey(4);
            do
            {
                cName = "فرع" + this.AppCompanyUserManager.GetUniqueKey(4);
            } while (db.Branchs.Any(c => c.BranchName == cName && c.CompanyId == com.PublicId && c.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)));

            var nowdate = DateTime.Now.Date;
            var enddate = nowdate.AddYears(1);
            var company = new Branch()
            {
                BranchName = cName,
                CurrentWorkerId = uid,
                CompanyId = com.PublicId,

                BranchProperties = new List<BranchProperties>()
                {
                    new BranchProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "عنوان").AccountsCatogeryTypesAcccountsId,
                        propertyName = "العنوان",
                        CurrentWorkerId = uid,
                        BranchPropertyValues = new List<BranchPropertyValues>()
                        {
                            new BranchPropertyValues() {CurrentWorkerId = uid , propertyValue = "عنوان الفرع هنا"  }
                        }

                    },

                    new BranchProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "هاتف ارضي").AccountsCatogeryTypesAcccountsId,
                        propertyName = "هاتف",
                        CurrentWorkerId = uid,
                        BranchPropertyValues = new List<BranchPropertyValues>()
                        {
                            new BranchPropertyValues() {CurrentWorkerId = uid , propertyValue = "002010000000000"  }
                        }

                    }

                },


            };


            db.Branchs.Add(company);
            db.SaveChanges();

            BranchesVm co = new BranchesVm()
            {
                id = company.BranchName,
                PublicId = company.BranchId,
                name = company.BranchName
            };
            return Ok(co);
        }

        // DELETE: api/Companies/5
        [ResponseType(typeof(Company))]
        public async Task<IHttpActionResult> DeleteCompany(int id)
        {
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            db.Companies.Remove(company);
            await db.SaveChangesAsync();

            return Ok(company);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CompanyExists(int id)
        {
            return db.Companies.Count(e => e.CompanyId == id) > 0;
        }

        [Route("getTypesInfo")]
        [HttpPost]
        public IHttpActionResult getTypesInfo()
        {
            var types = db.AccountsCatogeryTypesAcccounts.Select(a => new
            {
                typeId = a.AccountsCatogeryTypesAcccountsId,
                typeName = a.TypeName

            }).ToList();
            return Ok(types);
        }

        [Route("getCompanyInfo")]
        [HttpPost]
        public  IHttpActionResult getCompanyInfo(getCompanyBranchproperty companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
            

            if (companyBranch.iscompany)
            {
                var com = db.Companies.SingleOrDefault(a => a.CompanyId == company.CompanyId);
                if(com != null)
                {
                    var info = new companybranchInfo()
                    {
                        branchId = 0,
                        comapnyId = com.CompanyId,
                        iscompany = true,
                        name = com.CompanyName,
                        properties = com.CompanyProperties == null ? new List<CompanyBranchProperty>() : com.CompanyProperties.Select(a => new CompanyBranchProperty()
                        {
                            branchId = 0 ,
                            comapnyId = com.CompanyId,
                            propertiesId = a.CompanyPropertiesId,
                            propertiesName = a.propertyName,
                            typeName =db.AccountsCatogeryTypesAcccounts.Find(a.AccountsCatogeryTypesAcccountsId).TypeName,
                            propertyValue = a.CompanyPropertyValues == null ? new List<CompanyBranchPropertyValue>() : a.CompanyPropertyValues.Select(v => new CompanyBranchPropertyValue()
                            {
                                propertiesId = a.CompanyPropertiesId,
                                value = v.propertyValue,
                                valueId = v.CompanyPropertyValuesId
                            }).ToList()
                            

                        }).ToList()
                    };
                    return Ok(info);
                }
                else
                {
                    return Ok(new companybranchInfo());
                }
               
            }
            else
            {
                var com = db.Branchs.SingleOrDefault(a => a.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);
                if (com != null)
                {
                    var info = new companybranchInfo()
                    {
                        branchId = com.BranchId,
                        comapnyId = com.CompanyId,
                        iscompany = false,
                        name = string.Format("{0} ({1})" , com.BranchName , com.Company.CompanyName),
                        properties = com.BranchProperties.Count() == 0 ? new List<CompanyBranchProperty>() : com.BranchProperties.Select(a => new CompanyBranchProperty()
                        {
                            branchId = com.BranchId,
                            comapnyId = com.CompanyId,
                            propertiesId = a.BranchPropertiesId,
                            propertiesName = a.propertyName,
                            typeName = db.AccountsCatogeryTypesAcccounts.Find(a.AccountsCatogeryTypesAcccountsId).TypeName,
                            propertyValue = a.BranchPropertyValues == null ? new List<CompanyBranchPropertyValue>() : a.BranchPropertyValues.Select(v => new CompanyBranchPropertyValue()
                            {
                                propertiesId = a.BranchPropertiesId,
                                value = v.propertyValue,
                                valueId = v.BranchPropertyValuesId
                            }).ToList()


                        }).ToList()
                    };
                    return Ok(info);
                }
                else
                {
                    return Ok(new companybranchInfo());
                }
            }
            
        }

        [Route("addCompanyBranchProperty")]
        [HttpPost]
        public IHttpActionResult addCompanyBranchProperty(getCompanyBranchproperty companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
          
            if (companyBranch.iscompany)
            {
                var cName = "ملاحظات" + this.AppCompanyUserManager.GetUniqueKey(2);
                do
                {
                    cName = "ملاحظات" + this.AppCompanyUserManager.GetUniqueKey(2);
                } while (db.CompanyProperties.Any(c => c.CompanyId == company.CompanyId && c.propertyName == cName));

                var newProperty = new CompanyProperties
                {
                    CompanyId = company.CompanyId,
                    AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == "ملاحظات").AccountsCatogeryTypesAcccountsId,
                    CurrentWorkerId = uid,
                    propertyName = cName,

                };
                db.CompanyProperties.Add(newProperty);
                db.SaveChanges();
                var returnprop = new CompanyBranchProperty()
                {
                    branchId = 0,
                    comapnyId = company.CompanyId,
                    propertiesId = newProperty.CompanyPropertiesId,
                    propertiesName = newProperty.propertyName,
                    typeName = newProperty.AccountsCatogeryTypesAcccounts.TypeName,
                    propertyValue = new List<CompanyBranchPropertyValue>()
                };
                return Ok(returnprop);
            }
            else
            {
                var cName = "ملاحظات" + this.AppCompanyUserManager.GetUniqueKey(2);
                do
                {
                    cName = "ملاحظات" + this.AppCompanyUserManager.GetUniqueKey(2);
                } while (db.BranchProperties.Any(c => c.BranchId == companyBranch.comapnyId && c.Branch.CompanyId == company.CompanyId && c.propertyName == cName));
                var newProperty = new BranchProperties
                {
                    BranchId = companyBranch.comapnyId,
                    AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == "ملاحظات").AccountsCatogeryTypesAcccountsId,
                    CurrentWorkerId = uid,
                    propertyName = cName,

                };
                db.BranchProperties.Add(newProperty);
                db.SaveChanges();
                var returnprop = new CompanyBranchProperty()
                {
                    branchId = newProperty.BranchId,
                    comapnyId = company.CompanyId,
                    propertiesId = newProperty.BranchPropertiesId,
                    propertiesName = newProperty.propertyName,
                    typeName = newProperty.AccountsCatogeryTypesAcccounts.TypeName,
                    propertyValue = new List<CompanyBranchPropertyValue>()
                };
                return Ok(returnprop);
            }

         

        }

        [Route("updateCompanyBranchProperty")]
        [HttpPost]
        public IHttpActionResult UpdateCompanyBranchProperty(setCompanyBranchproperty companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
            if (companyBranch.iscompany)
            {
                var checkproperty = db.CompanyProperties.SingleOrDefault(a => a.CompanyPropertiesId == companyBranch.propertiesId && a.CompanyId == company.CompanyId);
                if(checkproperty == null)
                {
                    return BadRequest("الخاصية غير مسجلة");
                }
               var checknewName = db.CompanyProperties.Any(a => a.propertyName == companyBranch.propertiesName && a.CompanyPropertiesId != companyBranch.propertiesId && a.CompanyId == company.CompanyId);
                if (checknewName)
                {
                    return BadRequest("الخاصية مسجلة من قبل");

                };

                var findtype = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == companyBranch.typeName);
                
               
                checkproperty.propertyName = companyBranch.propertiesName;
                checkproperty.AccountsCatogeryTypesAcccountsId = findtype != null ? findtype.AccountsCatogeryTypesAcccountsId : checkproperty.AccountsCatogeryTypesAcccountsId;
                db.Entry(checkproperty).State = EntityState.Modified;
                db.SaveChanges();
                var returnprop = new CompanyBranchProperty()
                {
                    branchId = 0,
                    comapnyId = checkproperty.CompanyId,
                    propertiesId = checkproperty.CompanyPropertiesId,
                    propertiesName = checkproperty.propertyName,
                    typeName = checkproperty.AccountsCatogeryTypesAcccounts.TypeName,
                    propertyValue = checkproperty.CompanyPropertyValues.Count() == 0 ? new List<CompanyBranchPropertyValue>() : checkproperty.CompanyPropertyValues.Select(v => new CompanyBranchPropertyValue()
                    {
                        propertiesId = v.CompanyPropertiesId,
                        value = v.propertyValue,
                        valueId = v.CompanyPropertyValuesId
                    }).ToList()
                };
                return Ok(returnprop);

            }
            else
            {
                var checkproperty = db.BranchProperties.SingleOrDefault(a => a.BranchPropertiesId == companyBranch.propertiesId && a.Branch.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);
                if (checkproperty == null)
                {
                    return BadRequest("الخاصية غير مسجلة");
                }
                var checknewName = db.BranchProperties.Any(a => a.BranchPropertiesId != companyBranch.propertiesId && a.propertyName == companyBranch.propertiesName && a.Branch.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);
                if (checknewName)
                {
                    return BadRequest("الخاصية مسجلة من قبل");

                };

                var findtype = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == companyBranch.typeName);


                checkproperty.propertyName = companyBranch.propertiesName;
                checkproperty.AccountsCatogeryTypesAcccountsId = findtype != null ? findtype.AccountsCatogeryTypesAcccountsId : checkproperty.AccountsCatogeryTypesAcccountsId;
                db.Entry(checkproperty).State = EntityState.Modified;
                db.SaveChanges();
                var returnprop = new CompanyBranchProperty()
                {
                    branchId = checkproperty.BranchId,
                    comapnyId = company.CompanyId,
                    propertiesId = checkproperty.BranchPropertiesId,
                    propertiesName = checkproperty.propertyName,
                    typeName = checkproperty.AccountsCatogeryTypesAcccounts.TypeName,
                    propertyValue = checkproperty.BranchPropertyValues.Count() == 0 ? new List<CompanyBranchPropertyValue>() : checkproperty.BranchPropertyValues.Select(v => new CompanyBranchPropertyValue()
                    {
                        propertiesId = v.BranchPropertiesId,
                        value = v.propertyValue,
                        valueId = v.BranchPropertyValuesId
                    }).ToList()
                };
                return Ok(returnprop);
            }
        }

        [Route("delCompanyBranchProperty")]
        [HttpPost]
        public IHttpActionResult DelCompanyBranchProperty(setCompanyBranchproperty companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
            if (companyBranch.iscompany)
            {
                var checkproperty = db.CompanyProperties.SingleOrDefault(a => a.CompanyPropertiesId == companyBranch.propertiesId && a.CompanyId == company.CompanyId);
                if (checkproperty == null)
                {
                    return Ok();
                }
                db.CompanyProperties.Remove(checkproperty);
                db.SaveChanges();
                return Ok();

            }
            else
            {
                var checkproperty = db.BranchProperties.SingleOrDefault(a => a.BranchPropertiesId == companyBranch.propertiesId && a.Branch.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);

                if (checkproperty == null)
                {
                    return Ok();
                }
                db.BranchProperties.Remove(checkproperty);
                db.SaveChanges();
                return Ok();
            }
        }

        [Route("updateCompanyBranchPropertyValue")]
        [HttpPost]
        public IHttpActionResult UpdateCompanyBranchPropertyValue(SetCompanyBranchpropertyValue companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
            if (companyBranch.iscompany)
            {
                var pEX = db.CompanyProperties.SingleOrDefault(a => a.CompanyPropertiesId == companyBranch.propertiesId && a.CompanyId == company.CompanyId);
                if (pEX == null)
                {
                    return BadRequest("الخاصية غير متاحة");
                }
                if (pEX.AccountsCatogeryTypesAcccounts == null)
                {
                    pEX.AccountsCatogeryTypesAcccounts = db.AccountsCatogeryTypesAcccounts.Find(pEX.AccountsCatogeryTypesAcccountsId);
                }
                var tvalue = false;
                switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                {
                    case "عدد طبيعي":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        break;
                    case "عدد عشري":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        break;
                    case "بريد الكتروني":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.CompanyPropertyValues.Any(a => a.CompanyProperties.CompanyId == company.CompanyId && a.CompanyProperties.AccountsCatogeryTypesAcccounts.TypeName == "بريد الكتروني" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "هاتف محمول":

                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.CompanyPropertyValues.Any(a => a.CompanyProperties.CompanyId == company.CompanyId && a.CompanyProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف محمول" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "هاتف ارضي":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.CompanyPropertyValues.Any(a => a.CompanyProperties.CompanyId == company.CompanyId && a.CompanyProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف ارضي" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "ملاحظات":
                        tvalue = true;
                        break;
                    case "عنوان":
                        tvalue = true;
                        break;
                    default:
                        tvalue = false;
                        break;
                }
                if (!tvalue)
                {
                    return BadRequest("لا يمكن تسجيل هذه القيمة");
                }
                var findpropertValue = db.CompanyPropertyValues.SingleOrDefault(a => a.CompanyPropertiesId == companyBranch.propertiesId && a.CompanyPropertyValuesId == companyBranch.ValueId && a.CompanyProperties.CompanyId == company.CompanyId);
                if (findpropertValue == null)
                {
                    return BadRequest("القيمة غير مسجلة او غير متاحة للتعديل");

                }
                findpropertValue.propertyValue = companyBranch.value;
                db.Entry(findpropertValue).State = EntityState.Modified;
                db.SaveChanges();
                var returnprop = new CompanyBranchPropertyValue()
                {
                    propertiesId = findpropertValue.CompanyPropertiesId,
                    value = findpropertValue.propertyValue,
                    valueId = findpropertValue.CompanyPropertyValuesId
                };
                return Ok(returnprop);
            }
            else
            {
                //branch
                var pEX = db.BranchProperties.SingleOrDefault(a => a.BranchPropertiesId == companyBranch.propertiesId && a.Branch.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);
                if(pEX == null)
                {
                    return BadRequest("الخاصية غير متاحة");
                }
                if (pEX.AccountsCatogeryTypesAcccounts == null)
                {
                    pEX.AccountsCatogeryTypesAcccounts = db.AccountsCatogeryTypesAcccounts.Find(pEX.AccountsCatogeryTypesAcccountsId);
                }
                var tvalue = false;
                switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                {
                    case "عدد طبيعي":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        break;
                    case "عدد عشري":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        break;
                    case "بريد الكتروني":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.BranchPropertyValues.Any(a => a.BranchProperties.Branch.CompanyId == company.CompanyId  && a.BranchProperties.BranchId == companyBranch.comapnyId && a.BranchProperties.AccountsCatogeryTypesAcccounts.TypeName == "بريد الكتروني" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "هاتف محمول":

                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.BranchPropertyValues.Any(a => a.BranchProperties.Branch.CompanyId == company.CompanyId && a.BranchProperties.BranchId == companyBranch.comapnyId && a.BranchProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف محمول" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "هاتف ارضي":
                        tvalue = this.AppCompanyUserManager.CheckDataType(companyBranch.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                        if (db.BranchPropertyValues.Any(a => a.BranchProperties.Branch.CompanyId == company.CompanyId && a.BranchProperties.BranchId == companyBranch.comapnyId && a.BranchProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف ارضي" && a.propertyValue == companyBranch.value))
                        {
                            tvalue = false;
                        }
                        break;
                    case "ملاحظات":
                        tvalue = true;
                        break;
                    case "عنوان":
                        tvalue = true;
                        break;
                    default:
                        tvalue = false;
                        break;
                }
                if (!tvalue)
                {
                    return BadRequest("لا يمكن تسجيل هذه القيمة");
                }

                var findpropertValue = db.BranchPropertyValues.SingleOrDefault(a => a.BranchPropertiesId == companyBranch.propertiesId && a.BranchPropertyValuesId == companyBranch.ValueId && a.BranchProperties.Branch.CompanyId == company.CompanyId && a.BranchProperties.BranchId == companyBranch.comapnyId);
                if(findpropertValue == null)
                {
                    return BadRequest("القيمة غير مسجلة او غير متاحة للتعديل");

                }
                findpropertValue.propertyValue = companyBranch.value;
                db.Entry(findpropertValue).State = EntityState.Modified;
                db.SaveChanges();
                var returnprop = new CompanyBranchPropertyValue()
                {
                    propertiesId = findpropertValue.BranchPropertiesId,
                    value = findpropertValue.propertyValue,
                    valueId = findpropertValue.BranchPropertyValuesId
                };
                return Ok(returnprop);
            }
         
            
        }


        [Route("addCompanyBranchPropertyValue")]
        [HttpPost]
        public IHttpActionResult AddCompanyBranchPropertyValue(SetCompanyBranchpropertyValue companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion
            if (companyBranch.iscompany)
            {
                var pEX = db.CompanyProperties.SingleOrDefault(a => a.CompanyPropertiesId == companyBranch.propertiesId && a.CompanyId == company.CompanyId);
                if (pEX == null)
                {
                    return BadRequest("الخاصية غير متاحة");
                }
                if (pEX.AccountsCatogeryTypesAcccounts == null)
                {
                    pEX.AccountsCatogeryTypesAcccounts = db.AccountsCatogeryTypesAcccounts.Find(pEX.AccountsCatogeryTypesAcccountsId);
                }
                var tvalue = "";
                switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                {
                    case "عدد طبيعي":
                        tvalue = "12345";
                        break;
                    case "عدد عشري":
                        tvalue = "9.999";
                        break;
                    case "بريد الكتروني":
                        tvalue = "example@mail.com";
                        break;
                    case "هاتف محمول":

                        tvalue = "002010000000";
                        break;
                    case "هاتف ارضي":
                        tvalue = "002020000000";
                        break;
                    case "ملاحظات":
                        tvalue = "ادخل النص هنا";
                        break;
                    case "عنوان":
                        tvalue = "ادخل العنوان هنا";
                        break;
                    default:
                        tvalue = null;
                        break;
                }
                if (string.IsNullOrEmpty(tvalue))
                {
                    return BadRequest("تعذر تسجيل القيمة المدخلة");
                }

                var propertvalue = new CompanyPropertyValues()
                {
                    CompanyPropertiesId = pEX.CompanyPropertiesId,
                    propertyValue = tvalue,
                    CurrentWorkerId = uid,

                };
                db.CompanyPropertyValues.Add(propertvalue);
                db.SaveChanges();
                var returnprop = new CompanyBranchPropertyValue()
                {
                    propertiesId = propertvalue.CompanyPropertiesId,
                    value = propertvalue.propertyValue,
                    valueId = propertvalue.CompanyPropertyValuesId
                };
                return Ok(returnprop);
            }
            else
            {
                var pEX = db.BranchProperties.SingleOrDefault(a => a.BranchPropertiesId == companyBranch.propertiesId && a.Branch.CompanyId == company.CompanyId && a.BranchId == companyBranch.comapnyId);
                if (pEX == null)
                {
                    return BadRequest("الخاصية غير متاحة");
                }
                if (pEX.AccountsCatogeryTypesAcccounts == null)
                {
                    pEX.AccountsCatogeryTypesAcccounts = db.AccountsCatogeryTypesAcccounts.Find(pEX.AccountsCatogeryTypesAcccountsId);
                }
                var tvalue = "";
                switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                {
                    case "عدد طبيعي":
                        tvalue = "12345";
                        break;
                    case "عدد عشري":
                        tvalue = "9.999";
                        break;
                    case "بريد الكتروني":
                        tvalue = "example@mail.com";
                        break;
                    case "هاتف محمول":

                        tvalue = "002010000000";
                        break;
                    case "هاتف ارضي":
                        tvalue = "002020000000";
                        break;
                    case "ملاحظات":
                        tvalue = "ادخل النص هنا";
                        break;
                    case "عنوان":
                        tvalue = "ادخل العنوان هنا";
                        break;
                    default:
                        tvalue = null;
                        break;
                }
                if (string.IsNullOrEmpty(tvalue))
                {
                    return BadRequest("تعذر تسجيل القيمة المدخلة");
                }

                var propertvalue = new BranchPropertyValues()
                {
                    BranchPropertiesId = pEX.BranchPropertiesId,
                    propertyValue = tvalue,
                    CurrentWorkerId = uid,

                };
                db.BranchPropertyValues.Add(propertvalue);
                db.SaveChanges();
                var returnprop = new CompanyBranchPropertyValue()
                {
                    propertiesId = propertvalue.BranchPropertiesId,
                    value = propertvalue.propertyValue,
                    valueId = propertvalue.BranchPropertyValuesId
                };
                return Ok(returnprop);
            }


        }

        [Route("delCompanyBranchPropertyValue")]
        [HttpPost]
        public IHttpActionResult DelCompanyBranchPropertyValue(SetCompanyBranchpropertyValue companyBranch)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            #region check froCompany
            var company = new Company();
            if (companyBranch.iscompany)
            {
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == companyBranch.comapnyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            else
            {
                var fbranch = db.Branchs.Find(companyBranch.comapnyId);

                if (fbranch == null)
                {
                    return BadRequest("الفرع غير مسجل او غير متاح لهذة العملية");
                }
                var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == fbranch.CompanyId && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if (fcompany == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
                }
                company = fcompany;
            }
            #endregion

            if (companyBranch.iscompany)
            {
                var checkvalue = db.CompanyPropertyValues.SingleOrDefault(a => a.CompanyProperties.CompanyId == company.CompanyId && a.CompanyPropertyValuesId == companyBranch.ValueId && a.CompanyPropertiesId == companyBranch.propertiesId);
                if(checkvalue == null)
                {
                    return Ok();
                }
                db.CompanyPropertyValues.Remove(checkvalue);
                db.SaveChanges();
                return Ok();

            }
            else
            {
                var checkvalue = db.BranchPropertyValues.SingleOrDefault(a => a.BranchProperties.Branch.CompanyId == company.CompanyId && a.BranchPropertyValuesId == companyBranch.ValueId && a.BranchPropertyValuesId == companyBranch.propertiesId && a.BranchProperties.BranchId == companyBranch.comapnyId);
                if (checkvalue == null)
                {
                    return Ok();
                }
                db.BranchPropertyValues.Remove(checkvalue);
                db.SaveChanges();
                return Ok();
            }
        }


    }

   
}