﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
  
    [RoutePrefix("api/POrder")]
    public class POrderController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private POrderManager POrderManager;
        public POrderController()
        {
            this.POrderManager = new POrderManager(this.db);
        }

        [Route("getOrderGroupNames")]
        [HttpPost]
        public IHttpActionResult GetOrderGroupNames(ordertypeVM o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var findInv = db.InventoriesConfigurtion.Where(a => a.Inventory.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.Inventory.AccountCategory.AccountsDataTypes.aliasName == "inventory").ToList();
            if (findInv == null)
            {
                return BadRequest("المخازن غير متاحة للعمليات في الوقت الحالي قم باعداد المخازن او تأكد من امتلاك الصلاحيات اللازمة");

            }
            var returninv = findInv.Select(a => new ViewModels.InventoryTransfer()
            {
                inventoryId = a.InventoryId,
                inventoryName = a.Inventory.AccountName,
                code = a.Inventory.Code
            }).ToList();
            var doneFirst = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false).Count();
            var donesecond = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false && a.type == o.type && a.orderType == o.orderType).Count();
            var returnd = new { waitingcount = doneFirst, workinprogress = donesecond, inventory = returninv };

            if (o.type == "pur")
            {
                var purOrdersName = db.PurchaseOrdersConfigurations.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).ToList()
                 .Select(n => new
                 {
                     OrderGroupId = n.PurchaseOrdersConfigurationId,
                     OrderGroupName = o.orderType == "pur" ? n.PurchaseOrderName : n.PurchaseReturnName
                     
                 }).ToList();
                var subjectAccount = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "pur" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).
                  Select(s => new {

                      SubjectAccountId = s.AccountId,
                      SubjectAccountName = s.AccountName + " - " + s.Code
                  }).ToList();

                return Ok(new { OrdersNames = purOrdersName  , returnd = returnd , subjectAccount  = subjectAccount });
            }
       
            if (o.type == "sales")
            {
                var purOrdersName = db.SalesOrdersConfigurations.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).ToList()
                 .Select(n => new
                 {
                     OrderGroupId = n.SalesOrdersConfigurationId,
                     OrderGroupName = o.orderType == "sales" ? n.SalesOrderName : n.SalesReturnName
                 }).ToList();
                var subjectAccount = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "cust" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).
                 Select(s => new {

                     SubjectAccountId = s.AccountId,
                     SubjectAccountName = s.AccountName + " - " + s.Code
                 }).ToList();
                return Ok(new { OrdersNames = purOrdersName, returnd = returnd, subjectAccount = subjectAccount });
            }
         

            return BadRequest("التموذج غير متاح");
        }

        [Route("getOrderGroupforBill/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderGroupforBill(int id)
        {

            var group = POrderManager.getOrderforbillbyId(id);

            return Ok(group);


        }

        [Route("GetOrderTemplatebyId")]
        [HttpPost]
        public IHttpActionResult GetOrderTemplatebyId(SetOrdersConfigurationsO o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (o.type == "sales")
            {
                var order =POrderManager.getSalesOrderconfgById(new SetOrdersConfigurations { orderId = o.orderId , type = o.type}, company);

                if (order == null)
                {
                    return BadRequest("الاعدادت غير مسجلة او غير متاحة في الوقت الحالي");
                }


                var tableAcc = order.tableAccounts.Select(a => new AccountOVm
                {
                    id = a.accountId,
                    name = a.nickName,
                    amount = 0.0m,
                    type = db.Accounts.Where(ac => ac.AccountId == a.accountId).Select(t => t.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName).SingleOrDefault() == "مدين" ? "debit" :"crdit",
                    amountType = "جـ"

                }).ToList();
                var payM = order.payMethod.Select(a => new PayOVm
                {
                    payId = a.payMethodId,
                    payName = a.payName,
                    amount = 0.0m,
                    type = db.AccountCategories.Where(ac => ac.AccountCategoryId == a.accountId).SingleOrDefault().BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit",
                    dueDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    accounts = db.AccountCategories.Where(ac => ac.AccountCategoryId == a.accountId).SingleOrDefault().Accounts.
                    Select(acc => new payvmAccounts()
                    {
                        accountId = acc.AccountId,
                        accountName = acc.AccountName + " - " + acc.Code
                    }).ToList(),
                    defaultChildAccountId = a.defaultChildAccountId
                }).ToList();
                var totalAcc = order.totalAccounts.Select(a => new TotalAccountOVm
                {
                    id = a.accountId,
                    name = a.nickName,
                    amount = 0.0m,
                    type = db.Accounts.Where(ac => ac.AccountId == a.accountId).Select(t => t.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName).SingleOrDefault() == "مدين" ? "debit" : "crdit",
                    amountType = "جـ"

                }).ToList();

                var terms = order.terms.Select(a => new TermOVm
                {
                    termId = a.termId,
                    termName = a.name,
                }).ToList();

                var orderOt = new OrderOVm
                {
                    orderType = o.orderType,
                    oType = o.type,
                    orderName = o.orderType == "sales" ? order.salesName : order.salesReturnName,
                    orderDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    SubjectAccount = new SubjectAccountOVm(),
                    itemsRequest = new List<ItemsRequestOVm>()
                    {
                        new ItemsRequestOVm
                        {
                            accounts = tableAcc
                        }
                    },
                    totalAccount = totalAcc,
                    pay = new List<PayOVm>(),
                    terms = terms,
                    companyId = company.oId,
                    isCompany = company.isCompany,
                    isNewOrder = true,
                    orderConfigId = o.orderId

                };
                return Ok(new { ordertemp = orderOt , tableAccounts = tableAcc , payMethods = payM , totalAccount = totalAcc , terms = terms  });

            }
            if (o.type == "pur")
            {
                var order = POrderManager.getPurOrderconfgById(new SetOrdersConfigurations { orderId = o.orderId, type = o.type }, company);

                if (order == null)
                {
                    return BadRequest("الاعدادت غير مسجلة او غير متاحة في الوقت الحالي");
                }


                var tableAcc = order.tableAccounts.Select(a => new AccountOVm
                {
                    id = a.accountId,
                    name = a.nickName,
                    amount = 0.0m,
                    type = db.Accounts.Where(ac => ac.AccountId == a.accountId).Select(t => t.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName).SingleOrDefault() == "مدين" ? "debit" : "crdit",
                    amountType = "جـ"

                }).ToList();
                var payM = order.payMethod.Select(a => new PayOVm
                {
                    payId = a.payMethodId,
                    payName = a.payName,
                    amount = 0.0m,
                    type = db.AccountCategories.Where(ac => ac.AccountCategoryId == a.accountId).SingleOrDefault().BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit",
                    dueDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    accounts = db.AccountCategories.Where(ac => ac.AccountCategoryId == a.accountId).SingleOrDefault().Accounts.
                    Select(acc => new payvmAccounts()
                    {
                        accountId = acc.AccountId,
                        accountName = acc.AccountName + " - " + acc.Code
                    }).ToList(),
                    defaultChildAccountId = a.defaultChildAccountId

                }).ToList();
                var totalAcc = order.totalAccounts.Select(a => new TotalAccountOVm
                {
                    id = a.accountId,
                    name = a.nickName,
                    amount = 0.0m,
                    type = db.Accounts.Where(ac => ac.AccountId == a.accountId).Select(t => t.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName).SingleOrDefault() == "مدين" ? "debit" : "crdit",
                    amountType = "جـ"

                }).ToList();

                var terms = order.terms.Select(a => new TermOVm
                {

                    termId = a.termId,
                    termName = a.name,
                }).ToList();

                var orderOt = new OrderOVm
                {
                    orderType = o.orderType,
                    oType = o.type,
                    orderName = o.orderType == "pur" ? order.PurName : order.PurReturnName,
                    orderDate = DateTime.Now.ToString("yyyy-MM-dd"),
                    SubjectAccount = new SubjectAccountOVm(),
                    itemsRequest = new List<ItemsRequestOVm>()
                    {
                        new ItemsRequestOVm
                        {
                            accounts = tableAcc
                        }
                    },
                    totalAccount = totalAcc,
                    pay =new List<PayOVm>(),
                    terms = terms,
                    companyId = company.oId,
                    isCompany = company.isCompany,
                    isNewOrder = true,
                    orderConfigId = o.orderId
                };
               
                return Ok(new { ordertemp = orderOt, tableAccounts = tableAcc, payMethods = payM, totalAccount = totalAcc, terms = terms});

            }
            return BadRequest("الاعدادت غير متاحة");

        }

        [Route("getOrderbyId")]
        [HttpPost]
        public IHttpActionResult getOrderOId(getorderO o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var toreturn =POrderManager.OrderOById(o.orderNo, o.orderType, o.type, company);
            
            if (toreturn == null)
            {
                return BadRequest("طلب الشراء غير مسجل او غير متاح لهذة العملية");
            }
            var tableAcc = toreturn.itemsRequest.FirstOrDefault().accounts.ToList();
            var payM = toreturn.pay.ToList();
            var terms = toreturn.terms.ToList();
            var totalAcc = toreturn.totalAccount.ToList();
            return Ok(new { order = toreturn , tableAccounts = tableAcc, payMethods = payM, totalAccount = totalAcc, terms = terms });
        }


        [Route("saveOrder")]
        [HttpPost]
        public IHttpActionResult saveOrder(OrderOVm o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (!ModelState.IsValid)
            {
                var err = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();
                string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                return BadRequest(tocoorect);
            }
            try
            {
                #region NewOrder
                if (o.isNewOrder)
                {
                #region purOrders
                var oNo = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderType == o.orderType && a.type == o.oType).
     Select(id => id.orderNo).DefaultIfEmpty(0).Max() + 1;
                // var orderNo = db.OrderOs.Where(a => a.)
                if (o.requestOrderAttched)
                {
                    var findrequest = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.PurchaseOrderId == o.orderrequestId && a.cancel == false && a.OrderType == o.orderType && a.OType == o.oType).SingleOrDefault();
                    if (findrequest == null)
                    {
                        return BadRequest("لا يوجد طلب مسجل بهذا الرقم");
                    }
                    findrequest.isDoneSecond = true;
                    findrequest.userSecondId = uid;
                    findrequest.orderDateSecond = Convert.ToDateTime(o.orderDate);
                    findrequest.orderONo = oNo;
                    findrequest.OrderType = o.orderType;
                    findrequest.OType = o.oType;
                    db.Entry(findrequest).State = System.Data.Entity.EntityState.Modified;
                }

                var orderTosave = new OrderO()
                {
                    orderNo = oNo,
                    orderDate = Convert.ToDateTime(o.orderDate),
                    recivedDate = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                    orderDateSecond = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                    orderDateThird = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                    orderType = o.orderType,
                    PurchaseOrderId = o.requestOrderAttched ? o.orderrequestId : (int?)null,
                    requestOrderAttched = o.requestOrderAttched,
                    type = o.oType,
                    SubjectAccountId = o.SubjectAccount.SubjectAccountId,
                    TransferToOId = o.transferTo.inventoryId,
                    RefrenceRequestId = o.requestOrderAttched ? o.orderrequestId : (int?)null,
                    RefrenceRequestName = o.oType,
                    OrderTitleName = o.orderName,
                    isNewOrder = false,
                    isDoneFirst = true,
                    CurrentWorkerId = uid,
                    terms = o.terms.Count() > 0 ? o.terms.Select(t => new TermO()
                    {

                        termName = t.termName,
                        CurrentWorkerId = uid,
                        IsCompany = company.isCompany,
                        CompanyId = company.companyId,
                        BranchId = company.isCompany ? (int?)null : company.oId,

                    }).ToList() : null,
                    itemsRequest = o.itemsRequest.Select(i => new ItemsRequestO()
                    {
                        itemCode = i.itemCode,
                        itemName = i.itemName,
                        itemId = i.itemId,
                        qty = i.qty,
                        price = i.price,
                        maxQ = i.maxQ,
                        realted = i.realted,
                        itemUnitName = i.itemUnitName,
                        itemUnitType = i.itemUnitType,
                        note = i.note,
                        accounts = i.accounts.Count() > 0 ? i.accounts.Select(acc => new AccountO()
                        {
                            AccountId = acc.id,
                            amount = acc.amount,
                            amountType = acc.amountType,
                            CurrentWorkerId = uid,
                            IsCompany = company.isCompany,
                            CompanyId = company.companyId,
                            BranchId = company.isCompany ? (int?)null : company.oId,
                            name = acc.name

                        }).ToList() : null,

                        CurrentWorkerId = uid,
                        IsCompany = company.isCompany,
                        CompanyId = company.companyId,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        SubAccountId = i.itemId,


                    }).ToList(),
                    pay = o.pay.Count() > 0 ? o.pay.Select(p => new PayO()
                    {
                        AccountId = p.accountId,
                        PayId = p.payId,
                        amount = p.amount,
                        dueDate = Convert.ToDateTime(p.dueDate),
                        type = p.type,
                        CurrentWorkerId = uid,
                        IsCompany = company.isCompany,
                        CompanyId = company.companyId,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        note = p.note,
                        PayName = p.payName

                    }).ToList() : null,
                    totalAccount = o.totalAccount.Count() > 0 ? o.totalAccount.Select(t => new TotalAccountO()
                    {
                        AccountId = t.id,
                        amount = t.amount,
                        amountType = t.amountType,
                        type = t.type,
                        name = t.name

                    }).ToList() : null,
                    InventoryRequests = new InventoryRequest()
                    {
                        orderNo = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.OrderType == o.orderType && a.OType == o.oType).Select(a => a.orderNo).DefaultIfEmpty(0).Max() + 1,
                        OType = o.oType,
                        OrderType = o.orderType,
                        orderDate = Convert.ToDateTime(o.orderDate),
                        orderDateThird = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                        orderDateSecond = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                        cancelDate = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                      
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        CompanyId = company.companyId,
                        IsCompany = company.isCompany,
                        orderONo = oNo,
                        isDoneFirst = true,
                        isNewOrder = false,
                        RecivedDate = Convert.ToDateTime(o.recivedDate ?? o.orderDate),
                        TransferToId = o.transferTo.inventoryId,
                        userFirstId = uid,

                        itemsRequest = o.itemsRequest.Select(i => new InventoryItemsRequest()
                        {
                            SubAccountId = i.itemId,
                            qty = i.qty,
                            price = i.price,
                            Realted = i.realted,

                        }).ToList(),
                    },
                    userId = uid,
                    IsCompany = company.isCompany,
                    CompanyId = company.companyId,
                    BranchId = company.isCompany ? (int?)null : company.oId,
                    RefrenceOrderConfigId = o.orderConfigId == 0 ? (int?)null : o.orderConfigId
                };
                db.OrderOs.Add(orderTosave);
                //db.SaveChanges();
                #region sending Requestes (payments)
                if (orderTosave.pay != null && orderTosave.pay.Count() > 0)
                {
                    var payRequestes = new List<OrderRequest>();
                    foreach (var p in orderTosave.pay)
                    {

                        Int32 accTypeId = db.Accounts.SingleOrDefault(a => a.AccountId == p.AccountId).AccountCategory.AccountsDataTypesId ?? db.AccountsDataTypes.SingleOrDefault(t => t.aliasName == "other").Id;
                        string settypeName = null;
                        string requestNote = null;
                        string n = null;
                        string ac = null;
                        switch (o.orderType)
                        {
                            case "pur":

                                settypeName = p.type == "debit" ? "pay" : "recive";
                                n = settypeName == "pay" ? "طلب صرف لـ" : "طلب اضافة من";
                                ac = o.SubjectAccount.SubjectAccountName + " - " + o.SubjectAccount.code;
                                requestNote = string.Format("{0} {1} - {2} رقم {3}", n, ac, o.orderName, oNo);
                                break;
                            case "purReturn":
                                settypeName = p.type == "debit" ? "recive" : "pay";
                                n = settypeName == "pay" ? "طلب صرف لـ" : "طلب اضافة من";
                                ac = o.SubjectAccount.SubjectAccountName + " - " + o.SubjectAccount.code;
                                requestNote = string.Format("{0} {1} - {2} رقم {3}", n, ac, o.orderName, oNo);
                                break;
                            case "sales":
                                settypeName = p.type == "debit" ? "recive" : "pay";
                                n = settypeName == "pay" ? "طلب صرف لـ" : "طلب اضافة من";
                                ac = o.SubjectAccount.SubjectAccountName + " - " + o.SubjectAccount.code;
                                requestNote = string.Format("{0} {1} - {2} رقم {3}", n, ac, o.orderName, oNo);
                                break;
                            case "salesReturn":
                                settypeName = p.type == "debit" ? "pay" : "recive";
                                n = settypeName == "pay" ? "طلب صرف لـ" : "طلب اضافة من";
                                ac = o.SubjectAccount.SubjectAccountName + " - " + o.SubjectAccount.code;
                                requestNote = string.Format("{0} {1} - {2} رقم {3}", n, ac, o.orderName, oNo);
                                break;
                        }
                        if (string.IsNullOrEmpty(settypeName))
                        {
                            return BadRequest("لم نتمكن من ارسال طلبات الدفع والسداد في الوقت الحالي - تم الغاء العملية");
                        }
                        var newRequest = new OrderRequest()
                        {
                            CompanyId = company.companyId,
                            AccountsDataTypesId = accTypeId,
                            BranchId = company.isCompany == true ? (int?)null : company.oId,
                            isQty = false,
                            mainAccountId = p.AccountId,
                            tilteAccountId = o.SubjectAccount.SubjectAccountId,
                            requestNo = db.orderRequest.Where(a => a.AccountsDataTypesId == accTypeId && a.CompanyId == company.companyId && a.BranchId == (company.isCompany == true ? (int?)null : company.oId) && a.typeName == settypeName).Count() + 1,
                            CurrentWorkerId = uid,
                            dueDate = p.dueDate,
                           
                            typeName = settypeName,
                            requestDate = Convert.ToDateTime(o.orderDate),
                            items = new List<OrderRequestItems>()
                            {
                                new OrderRequestItems()
                                {
                                CurrentWorkerId = uid,
                                note =requestNote,
                                price = p.amount,
                                qty = 1,
                                refrenceType =o.oType,
                                refrenceTypeId = oNo,
                                }
                            },
                            refrenceRequesterOType = o.oType,
                            refrenceRequesterNo =oNo,
                            refrenceRequesterOrderType = o.orderType
                        };
                        payRequestes.Add(newRequest);
                    }
                    db.orderRequest.AddRange(payRequestes);
                }

               
                    db.SaveChanges();
                    var toreturn =POrderManager.OrderOById(orderTosave.orderNo, orderTosave.orderType, orderTosave.type, company);
                    return Ok(toreturn);
               
                #endregion


               
                #endregion
            }
            #endregion
            #region oldOrder
            if (!o.isNewOrder)
            {
                var orderToEdit = db.OrderOs.Where(a => a.OrderOId == o.OrderId && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderType == o.orderType && a.type == o.oType ).SingleOrDefault();

                if(orderToEdit == null)
                {
                    return BadRequest("الامر غير متاح للتعديل - يرجي التاكد من البيانات او المدخلة او انك تملك الصلاحيات الكافية لاجرء التعديل");
                }
                if (orderToEdit.cancel)
                {
                    return BadRequest("لقد تم الغاء الامر من قبل - لا يمكن اجراء التعديل");

                }
                if (orderToEdit.InventoryRequests.isDoneSecond)
                {
                    
                    if(o.oType == "sales" || o.oType == "purReturn")
                    {
                        return BadRequest("لقد تم اصدار اذن صرف  من المخازن - لا يمكن تعديل الامر الان");
                    }
                    else
                    {
                        return BadRequest("لقد تم اصدار اذن اضافة  من المخازن - لا يمكن تعديل الامر الان");
                    }
                }
                if (orderToEdit.isDoneSecond)
                {
                    return BadRequest("لقد تم اصدار فاتورة - لا يمكن التعديل علي الامر");

                }

                if (orderToEdit.pay.Count() > 0)
                {

                    db.orderRequest.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany == true ? (int?)null : company.oId) && a.refrenceRequesterNo == orderToEdit.orderNo && a.refrenceRequesterOType == orderToEdit.type && a.refrenceRequesterOrderType == o.orderType).ToList().
                       ForEach(p =>
                       {
                           if (p.isDone)
                           {
                               string note =string.Format( "لقد تم ارفاق الطلب باذن من {0} لصالح {1}" , p.mainAccount.AccountName + " - " + p.mainAccount.Code , p.tilteAccount.AccountName + " - " + p.mainAccount.Code);

                               ModelState.AddModelError("طلبات", "لا يمكن التعديل علي الامر" + " " + note);
                           }
                       });
                }

                if (!ModelState.IsValid)
                {
                    var err = ModelState.Values.SelectMany(m => m.Errors)
                                     .Select(e => e.ErrorMessage)
                                     .ToList();
                    string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                    foreach (var er in err)
                    {
                        tocoorect = tocoorect + "<br>" + er;
                    }
                    return BadRequest(tocoorect);
                }
                #region isToCancel
                if (o.cancel)
                {
                    orderToEdit.cancel = true;
                    orderToEdit.CurrentWorkerId = uid;
                    orderToEdit.cancelById = uid;
                    orderToEdit.orderDateSecond = Convert.ToDateTime(o.orderDate);
                    orderToEdit.InventoryRequests.cancel = true;
                    orderToEdit.InventoryRequests.cancelDate = Convert.ToDateTime(o.orderDate);
                    orderToEdit.InventoryRequests.userCancelId =uid;
                    if (orderToEdit.requestOrderAttched)
                    {
                        orderToEdit.PurchaseOrder.cancel = true;
                        orderToEdit.PurchaseOrder.userCancelId = uid;
                        orderToEdit.PurchaseOrder.cancelDate = Convert.ToDateTime(o.orderDate);
                    }
                     db.Entry(orderToEdit).State = System.Data.Entity.EntityState.Modified;
                   if (orderToEdit.pay != null && orderToEdit.pay.Count() > 0)
                    {

                        var rrequestp = db.orderRequest.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany == true ? (int?)null : company.oId) && a.refrenceRequesterNo == orderToEdit.orderNo && a.refrenceRequesterOType == orderToEdit.type && a.refrenceRequesterOrderType == o.orderType).ToList();
                        foreach (var p in rrequestp)
                        {
                            p.isToCancel = true;
                            p.CurrentWorkerId = uid;
                           
                            db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                   
                    

                    db.SaveChanges();
                }
                #endregion
                return Ok();
            }
            #endregion
            return BadRequest();
           

            }
            catch (DbEntityValidationException ex)
            {
                var err = ex.EntityValidationErrors.SelectMany(m => m.ValidationErrors)
                               .Select(e => e.ErrorMessage)
                               .ToList();
                string tocoorect = "لا يمكن اتمام العملية لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                return BadRequest(tocoorect);
               
            }
        }


        [Route("getAllOrderwaiting")]
        [HttpPost]
        public IHttpActionResult getwaitingOrder(getwaitinPurRequest o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

            if (o.orderNo != 0)
            {
                var sorder = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == o.orderNo && a.orderType == o.OrderType && a.type == o.OType)
                    .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.user.FirstName + " " + a.user.LastName
                    }).ToList();
                if (sorder == null || sorder.Count == 0)
                {
                    return BadRequest("لا يوجد اي طلب مسجل بهذا الرقم");
                }
                var toreturn1 = new
                {
                    orders = sorder,
                    TotalCount = 1,
                    totalPages = 1,
                };
                return Ok(toreturn1);
            }
            if (o.all)
            {
                var totalCount = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderType == o.OrderType && a.type == o.OType).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) &&  a.orderType == o.OrderType && a.type == o.OType)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.user.FirstName + " " + a.user.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

            else if (o.isDoneFirst)
            {
                var totalCount = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false && a.orderType == o.OrderType && a.type == o.OType).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false && a.orderType == o.OrderType && a.type == o.OType)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.user.FirstName + " " + a.user.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.cancel)
            {
                var totalCount = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel && a.orderType == o.OrderType && a.type == o.OType).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel && a.orderType == o.OrderType && a.type == o.OType)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.cancelBy.FirstName + " " + a.cancelBy.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.isDoneSecond)
            {
                var totalCount = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false && a.orderType == o.OrderType && a.type == o.OType).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false && a.orderType == o.OrderType && a.type == o.OType)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.user.FirstName + " " + a.user.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else
            {
                var totalCount = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderType == o.OrderType && a.type == o.OType).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.OrderOs.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderType == o.OrderType && a.type == o.OType)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferToO.AccountName + " - " + a.TransferToO.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.orderDateSecond.ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.user.FirstName + " " + a.user.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

        }


    }
}
