﻿using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class OrderReqestManager
    {
        private ApplicationDbContext db;

       
        public OrderReqestManager(ApplicationDbContext db)
        {
            this.db = db;
        }
        public PurchaseOrderVm getPurRequestById(Int32 orderNo, branchGroupSt company)
        {
            var o = db.PurchaseOrder.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == orderNo);
            if (o == null)
            {
                return null;
            }
            var orderReturn = new PurchaseOrderVm
            {
                cancel = o.cancel,
                cancelDate = o.cancelDate.GetValueOrDefault(),
                CompanyId = o.CompanyId,
                isDoneFirst = o.isDoneFirst,
                isDoneSecond = o.isDoneSecond,
                isDoneThird = o.isDoneThird,
                isNewOrder = o.isNewOrder,
                orderDate = o.orderDate,
                orderDateSecond = o.orderDateSecond,
                orderDateThird = o.orderDateThird,
                orderNo = o.orderNo,
                PurchaseOrderId = o.PurchaseOrderId,
                recivedDate = o.RecivedDate,
                TransferTo = new purOrderRequestTo { inventoryId = o.TransferToId, code = o.TransferTo.Code, inventoryName = o.TransferTo.AccountName },
                userName = o.userFirst == null ? null : o.userFirst.FirstName + " " + o.userFirst.LastName,
                userNameSecond = o.userSecond == null ? null : o.userSecond.FirstName + " " + o.userSecond.LastName,
                userNameThird = o.userThird == null ? null : o.userThird.FirstName + " " + o.userThird.LastName,
                userNameCancel = o.userCancel == null ? null : o.userCancel.FirstName + " " + o.userCancel.LastName,
                itemsRequest = (from i in o.itemsRequest
                                join it in getItemsByinventoryId(o.TransferToId, company) on i.SubAccountId equals it.subAccountId
                                where i.qty > 0
                                select new ItemsRequestVm()
                                {
                                    ItemsRequestId = i.ItemsRequestId,
                                    itemCode = it.code,
                                    itemGroupName = it.ItemGroupName,
                                    itemName = it.name,
                                    itemId = i.SubAccountId,
                                    itemUnitName = it.UnitName,
                                    itemUnitType = it.UnitType,
                                    note = i.note,
                                    noteSecond = i.noteSecond,
                                    price = i.price,
                                    qty = i.qty,
                                }).ToList()
            };


            return orderReturn;
        }

        public IEnumerable<TempItemVm> getItemsByinventoryId(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
          
            var items = (from a in db.SubAccounts
                         where a.AccountId == inventoryId
                         && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                         && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)
                         && a.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                         select new TempItemVm()
                         {
                             subAccountId = a.SubAccountId,
                             name = a.SubAccountName,
                             code = a.ItemInfo.code,
                             avQty = (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                             ItemGroupName = a.ItemInfo.ItemGroup.ItemGroupName,
                             UnitName = a.ItemInfo.Unit.UnitName,
                             UnitType = a.ItemInfo.Unit.UnitType.UnitTypeName,
                             price = a.ItemInfo.price,
                             reorderPoint = a.ItemInfo.reorderPoint,
                             ExpireinDayes = a.ItemInfo.ExpireinDayes,
                             costPrice = addCostPrice ? (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault() : 0,
                             ItemInventory = new tempItemInventoryVm { inventoryId = a.AccountId, code = a.Account.Code, inventoryName = a.Account.AccountName, branch = a.Account.BranchId == (int?)null ? "" : a.Account.Branch.BranchName }

                         });



            if (items == null || !items.Any())
            {
                return null;
            }
            return items;
        }

        public InventoryRequestOrderVm getInventoryRequestById(Int32 orderNo, string orderType, Int32 orderId, branchGroupSt company)
        {
          
            string orderType1 = null;
            string orderType2 = null;
            if (orderType == "in")
            {
                orderType1 = "pur";
                orderType2 = "salesReturn";

            }
            if (orderType == "out")
            {
                orderType1 = "purReturn";
                orderType2 = "sales";
            }
            var o = db.InventoryRequests.SingleOrDefault(a => a.InventoryRequestId == orderId && a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == orderNo && (a.OrderType == orderType1 || a.OrderType == orderType2));
            if (o == null)
            {
                return null;
            }
            var orderReturn = new InventoryRequestOrderVm
            {
                cancel = o.cancel,
                cancelDate = o.cancelDate.GetValueOrDefault(),
                CompanyId = o.CompanyId,
                isDoneFirst = o.isDoneFirst,
                isDoneSecond = o.isDoneSecond,
                isDoneThird = o.isDoneThird,
                isNewOrder = false,
                orderDate = o.orderDate,
                orderDateSecond = DateTime.Now,
                orderDateThird = o.orderDateThird,
                orderNo = o.orderNo,
                OrderType = orderType,
                ONo = o.orderONo.GetValueOrDefault(),
                orderId = o.InventoryRequestId,
                recivedDate = o.RecivedDate,
                TransferTo = new purOrderRequestTo { inventoryId = o.TransferToId, code = o.TransferTo.Code, inventoryName = o.TransferTo.AccountName },
                userName = o.userFirst == null ? null : o.userFirst.FirstName + " " + o.userFirst.LastName,
                userNameSecond = o.userSecond == null ? null : o.userSecond.FirstName + " " + o.userSecond.LastName,
                userNameThird = o.userThird == null ? null : o.userThird.FirstName + " " + o.userThird.LastName,
                userNameCancel = o.userCancel == null ? null : o.userCancel.FirstName + " " + o.userCancel.LastName,
                itemsRequest = (from i in o.itemsRequest
                                join it in getItemsByinventoryId(o.TransferToId, company) on i.SubAccountId equals it.subAccountId
                                where i.qty > 0
                                select new ItemsRequestVm()
                                {
                                    ItemsRequestId = i.InventoryRequestId,
                                    itemCode = it.code,
                                    itemGroupName = it.ItemGroupName,
                                    itemName = it.name,
                                    itemId = i.SubAccountId,
                                    itemUnitName = it.UnitName,
                                    itemUnitType = it.UnitType,
                                    note = i.note,
                                    noteSecond = i.noteSecond,
                                    price = i.price,
                                    qty = i.qty,
                                    maxQ = (from q in i.SubAccount.SubAccountMovements where q.SubAccountMovementDate == (from d in i.SubAccount.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                                 
                                })
            };


            return orderReturn;
        }
        public InventoryToQty getTransferInfo(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
            var items = getitemAvailableByinventoryId(inventoryId, company, addCostPrice);
            if (items == null)
            {
                return null;
            }
            var transferInventory = getInventoryMatchTransfers(company, inventoryId);
            if (transferInventory == null)
            {
                return null;
            }
            InventoryToQty toReturn = new InventoryToQty()
            {
                items = items,
                InventoryTransferTo = transferInventory
            };
            return toReturn;
        }

        public IEnumerable<itemAvailable> getitemAvailableByinventoryId(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {

            var items = (from a in db.SubAccounts
                         where a.AccountId == inventoryId
               && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                         select new itemAvailable()
                         {
                             itemId = a.SubAccountId,
                             itemName = a.SubAccountName,
                             itemCode = a.ItemInfo.code,
                             Qty = (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                             GroupName = a.ItemInfo.ItemGroup.ItemGroupName,
                             price = a.ItemInfo.price,
                             costPrice = addCostPrice ? (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault() : 0,

                         });



            if (items == null || items.Count() == 0)
            {
                return null;
            }
            return items;
        }

        public IEnumerable<InventoryTransfer> getInventoryMatchTransfers(branchGroupSt company, Int32 inventoryId)
        {

            var findInvFrom = (from b in db.Accounts
                               where b.AccountId == inventoryId
                               && b.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                               && b.BranchId == (company.isCompany ? (int?)null : company.oId)
                               && b.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                               select b).SingleOrDefault().AccountsforItemGroups;

            if (findInvFrom == null)
            {
                return null;

            }
            var findInvTo = (from a in db.Accounts
                             where a.AccountId != inventoryId
                             && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                            && a.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                             select a);
            if (findInvTo == null)
            {
                return null;

            }

            var invintoryGroup = (from g1 in findInvFrom
                                  from g2 in findInvTo
                                  where g2.AccountsforItemGroups.Any(c => c.ItemGroupId == g1.ItemGroupId)
                                  select g2).Distinct().Select(a => new InventoryTransfer()
                                  {
                                      inventoryId = a.AccountId,
                                      inventoryName = a.AccountName,
                                      code = a.Code
                                  });



            if (invintoryGroup == null)
            {
                return null;

            }

            return invintoryGroup;
        }


    }
}