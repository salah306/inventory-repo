﻿using Inventory.DataLayer;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Inventory.Managers
{
    //public class GenericRepository<TEntity> where TEntity : class
    //{
    //    internal ApplicationDbContext context;
    //    internal DbSet<TEntity> dbSet;

    //    public GenericRepository(ApplicationDbContext context)
    //    {
    //        this.context = context;
    //        this.dbSet = context.Set<TEntity>();
    //    }

    //    public virtual IEnumerable<TEntity> Get(
    //        Expression<Func<TEntity, bool>> filter = null,
    //        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
    //        string includeProperties = "")
    //    {
    //        IQueryable<TEntity> query = dbSet;

    //        if (filter != null)
    //        {
    //            query = query.Where(filter);
    //        }

    //        foreach (var includeProperty in includeProperties.Split
    //            (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
    //        {
    //            query = query.Include(includeProperty);
    //        }

    //        if (orderBy != null)
    //        {
    //            return orderBy(query).ToList();
    //        }
    //        else
    //        {
    //            return query.ToList();
    //        }
    //    }

    //    public virtual TEntity GetByID(object id)
    //    {
    //        return dbSet.Find(id);
    //    }

    //    public virtual void Insert(TEntity entity)
    //    {
    //        dbSet.Add(entity);
    //    }

    //    public virtual void Delete(object id)
    //    {
    //        TEntity entityToDelete = dbSet.Find(id);
    //        Delete(entityToDelete);
    //    }

    //    public virtual void Delete(TEntity entityToDelete)
    //    {
    //        if (context.Entry(entityToDelete).State == EntityState.Detached)
    //        {
    //            dbSet.Attach(entityToDelete);
    //        }
    //        dbSet.Remove(entityToDelete);
    //    }

    //    public virtual void Update(TEntity entityToUpdate)
    //    {
    //        dbSet.Attach(entityToUpdate);
    //        context.Entry(entityToUpdate).State = EntityState.Modified;
    //    }
    //}

    //public class UnitOfWork : IDisposable
    //{
    //    private ApplicationDbContext context = new ApplicationDbContext();
    //    private GenericRepository<Account> departmentRepository;

    //    public GenericRepository<Account> DepartmentRepository
    //    {
    //        get
    //        {

    //            if (this.departmentRepository == null)
    //            {
    //                this.departmentRepository = new GenericRepository<Account>(context);
    //            }
    //            return departmentRepository;
    //        }
    //    }


    //    public void Save()
    //    {
    //        context.SaveChanges();
    //    }

    //    private bool disposed = false;

    //    protected virtual void Dispose(bool disposing)
    //    {
    //        if (!this.disposed)
    //        {
    //            if (disposing)
    //            {
    //                context.Dispose();
    //            }
    //        }
    //        this.disposed = true;
    //    }

    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }
    //}
}