﻿using Inventory.DataLayer;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class POrderManager
    {
        private ApplicationDbContext db;
        public POrderManager(ApplicationDbContext db)
        {
            this.db = db;
        }
        public OrderGroupforBillVm getOrderforbillbyId(Int32 id)
        {
           
            var group = (from c in db.OrderGroup
                         where c.OrderGroupId == id
                         select new OrderGroupforBillVm()
                         {
                             OrderGroupId = c.OrderGroupId,
                             OrderGroupName = c.OrderGroupName,
                             CompanyBranchId = c.CompanyBranchId,
                             IsCompany = c.IsCompany,
                             type = new List<OrderGroupTypeForBill>()
                             {
                                 new OrderGroupTypeForBill {typeId = 1 , TypeName = "مبيعات"  },
                                 new OrderGroupTypeForBill {typeId = 2 , TypeName = "مشتريات"},
                                 new OrderGroupTypeForBill {typeId = 3 , TypeName = "تحويلات"}

                             },
                             orderMainAccounts = (from i in c.Sales.orderMainAccounts
                                                  select new orderInventotyAccounts()
                                                  {
                                                      accountId = i.InventoryAccountId,
                                                      accountName = i.InventoryAccount.AccountName
                                                  }).ToList()

                         }).SingleOrDefault();

            return group;
        }
        public SalesOrdersConfigurationVm getSalesOrderconfgById(SetOrdersConfigurations o, branchGroupSt company)
        {

            if (o.type == "sales")
            {
            
                #region sales
                var order = db.SalesOrdersConfigurations.Where(a => a.SalesOrdersConfigurationId == o.orderId && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                    .ToList()
                    .Select(or => new SalesOrdersConfigurationVm()
                    {
                        salesId = or.SalesOrdersConfigurationId,
                        salesOrderName = or.SalesOrderName,
                        salesName = or.SalesName,
                        salesReturnName = or.SalesReturnName,
                        defaultPayName = or.DefaultPayAccountCategoryId.GetValueOrDefault(),
                        salesOrderIsNew = false,
                        payMethod = or.PayMethod.Select(paym => new SalesPayMethodVm()
                        {
                            payMethodId = paym.SalessOrdersConfigurationPayMethodId,
                            accountId = paym.AccountCategoryId,
                            accountName = paym.AccountCategory.AccountCategoryName + " - " + paym.AccountCategory.Code,
                            payName = paym.PayName,
                            defaultChildAccountId = paym.DefaultChildAccountId,
                            attchedRequest = true,

                        }).ToList(),
                        terms = or.Terms.Select(tr => new SalesTermVm()
                        {
                            termId = tr.SalessOrdersConfigurationTermId,
                            name = tr.Note
                        }).ToList(),
                        tableAccounts = or.TableAccounts.Select(ta => new SalesTableAccountVm()
                        {
                            tableAccountId = ta.SalessOrdersConfigurationTableAccountId,
                            accountId = ta.AccountId,
                            accountName = ta.Account.AccountName + " - " + ta.Account.Code,
                            accountType = ta.AccountType.AccountTypeName,
                            nickName = ta.AccountNickName

                        }).ToList(),
                        totalAccounts = or.TotalAccounts.Select(to => new SalesTotalAccountVm()
                        {
                            totalAccountId = to.SalessOrdersConfigurationTotalAccountId,
                            accountId = to.AccountId,
                            accountName = to.Account.AccountName + " - " + to.Account.Code,
                            accountType = to.AccountType.AccountTypeName,
                            nickName = to.AccountNickName
                        }).ToList()
                    }).SingleOrDefault();

                if (order == null)
                {
                    return null;

                }
                return order;
                #endregion

            }

            return null;
        }

        public PurchaseOrdersConfigurationVm getPurOrderconfgById(SetOrdersConfigurations o, branchGroupSt company)
        {

            if (o.type == "pur")
            {
           
                #region pur
                var order = db.PurchaseOrdersConfigurations.Where(a => a.PurchaseOrdersConfigurationId == o.orderId && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                     .ToList()
                    .Select(or => new PurchaseOrdersConfigurationVm()
                    {
                        PurId = or.PurchaseOrdersConfigurationId,
                        PurOrderName = or.PurchaseOrderName,
                        PurName = or.PurchaseName,
                        PurReturnName = or.PurchaseReturnName,
                        defaultPayName = or.DefaultPayAccountCategoryId.GetValueOrDefault(),
                        PurOrderIsNew = false,
                        payMethod = or.PayMethod.Select(paym => new PurchasePayMethodVm()
                        {
                            payMethodId = paym.PurchasesOrdersConfigurationPayMethodId,
                            accountId = paym.AccountCategoryId,
                            accountName = paym.AccountCategory.AccountCategoryName + " - " + paym.AccountCategory.Code,
                            payName = paym.PayName,
                            defaultChildAccountId = paym.DefaultChildAccountId,
                            attchedRequest = true,

                        }).ToList(),
                        terms = or.Terms.Select(tr => new PurchaseTermVm()
                        {
                            termId = tr.PurchasesOrdersConfigurationTermId,
                            name = tr.Note
                        }).ToList(),
                        tableAccounts = or.TableAccounts.Select(ta => new PurchaseTableAccountVm()
                        {
                            tableAccountId = ta.PurchasesOrdersConfigurationTableAccountId,
                            accountId = ta.AccountId,
                            accountName = ta.Account.AccountName + " - " + ta.Account.Code,
                            accountType = ta.AccountType.AccountTypeName,
                            nickName = ta.AccountNickName

                        }).ToList(),
                        totalAccounts = or.TotalAccounts.Select(to => new PurchaseTotalAccountVm()
                        {
                            totalAccountId = to.PurchasesOrdersConfigurationTotalAccountId,
                            accountId = to.AccountId,
                            accountName = to.Account.AccountName + " - " + to.Account.Code,
                            accountType = to.AccountType.AccountTypeName,
                            nickName = to.AccountNickName
                        }).ToList()
                    }).SingleOrDefault();

                if (order == null)
                {
                    return null;

                }
                return order;
                #endregion
            }
            return null;
        }

        public OrderOVm OrderOById(Int32 orderNo, string orderType, string type, branchGroupSt company)
        {
       
            var o = db.OrderOs.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == orderNo && a.type == type && a.orderType == orderType);
            if (o == null)
            {
                return null;
            }
            var orderReturn = new OrderOVm
            {
                orderNo = o.orderNo,
                orderType = o.orderType,

                isNewOrder = false,
                orderDate = o.orderDate.ToString("yyyy-MM-dd"),
                orderDateSecond = o.orderDateSecond.ToString("yyyy-MM-dd"),
                orderDateThird = o.orderDateThird.ToString("yyyy-MM-dd"),
                orderName = o.OrderTitleName,
                companyId = o.CompanyId,
                cancel = o.cancel,
                cancelDate = o.orderDateSecond.ToString("yyyy-MM-dd"),
                isCompany = company.isCompany,
                OrderId = o.OrderOId,
                isDoneFirst = o.isDoneFirst,
                isDoneSecond = o.isDoneSecond,
                isDoneThird = o.isDoneThird,
                oType = o.type,
                orderrequestId = o.PurchaseOrderId.GetValueOrDefault(),
                recivedDate = o.recivedDate.ToString("yyyy-MM-dd"),
                SubjectAccount = new SubjectAccountOVm { SubjectAccountId = o.SubjectAccountId, SubjectAccountName = o.SubjectAccount.AccountName + o.SubjectAccount.Code, code = o.SubjectAccount.Code },
                transferTo = new TransferToOVm { inventoryId = o.TransferToOId, code = o.TransferToO.Code, inventoryName = o.TransferToO.AccountName + " - " + o.TransferToO.Code },
                requestOrderAttched = o.RefrenceRequestId == (int?)null ? false : true,
                orderrequestNo = o.PurchaseOrder != null ? o.PurchaseOrder.orderNo : 0,

                orderConfigId = o.RefrenceOrderConfigId.GetValueOrDefault(),
                userName = o.user != null ? o.user.FirstName + " " + o.user.LastName : null,
                userNameCancel = o.cancelBy != null ? o.cancelBy.FirstName + " " + o.cancelBy.LastName : null,
                userNameSecond = o.userSecond != null ? o.userSecond.FirstName + " " + o.userSecond.LastName : null,
                userNameThird = o.userThird != null ? o.userThird.FirstName + " " + o.userThird.LastName : null,
                itemsRequest = o.itemsRequest.Select(i => new ItemsRequestOVm()
                {
                    itemCode = i.itemCode,
                    itemId = i.itemId,
                    itemName = i.itemName,
                    itemUnitName = i.itemUnitName,
                    itemUnitType = i.itemUnitType,
                    note = i.note,
                    maxQ = i.maxQ,
                    price = i.price,
                    qty = i.qty,
                    realted = i.realted,
                    accounts = i.accounts != null && i.accounts.Count() > 0 ? i.accounts.Select(a => new AccountOVm()
                    {
                        amount = a.amount,
                        amountType = a.amountType,
                        id = a.AccountId,
                        name = a.name,
                        type = a.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit",

                    }).ToList() : new List<AccountOVm>(),
                }).ToList(),
                pay = o.pay != null && o.pay.Count() > 0 ? o.pay.Select(p => new PayOVm()
                {
                    accountId = p.AccountId,
                    accountName = p.Account.AccountName,
                    note = p.note,
                    payName = p.PayName,
                    code = p.Account.Code,
                    payId = p.PayId,
                    type = p.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit",
                    dueDate = p.dueDate.ToString("yyyy-MM-dd"),
                    amount = p.amount,
                    accounts = new List<payvmAccounts>() { new payvmAccounts() { accountId = p.AccountId, accountName = p.Account.AccountName + " - " + p.Account.Code } }
                }).ToList() : new List<PayOVm>(),
                totalAccount = o.totalAccount != null && o.totalAccount.Count() > 0 ? o.totalAccount.Select(t => new TotalAccountOVm()
                {
                    amount = t.amount,
                    amountType = t.amountType,
                    id = t.AccountId,
                    name = t.name,
                    type = t.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit",

                }).ToList() : new List<TotalAccountOVm>(),
                terms = o.terms != null && o.terms.Count() > 0 ? o.terms.Select(t => new TermOVm()
                {
                    termId = t.TermOId,
                    termName = t.termName
                }).ToList() : new List<TermOVm>(),



            };
            return orderReturn;
        }
    }
}