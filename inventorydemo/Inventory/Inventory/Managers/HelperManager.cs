﻿using Inventory.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Inventory.Managers
{
    public class HelperManager
    {
        private ApplicationDbContext db;

        public HelperManager()
        {
           
        }
        public HelperManager(ApplicationDbContext db)
        {
            this.db = db;
        }

        public bool CheckDataType(string value, string type)
        {
            bool successfullyParsed = false;


            switch (type)
            {
                case "عدد صحيح":
                    Int32 intId = 0;
                    successfullyParsed = int.TryParse(value, out intId);
                    break;
                case "عدد عشري":
                    decimal decimalId = 0.0m;
                    successfullyParsed = decimal.TryParse(value, out decimalId);

                    break;
                case "بريد الكتروني":
                    successfullyParsed = ValidEmail(value);
                    break;
                case "هاتف محمول":

                    successfullyParsed = Int32.TryParse(value, out intId);
                    break;
                case "هاتف ارضي":
                    successfullyParsed = Int32.TryParse(value, out intId);
                    break;
                case "ملاحظات":
                    successfullyParsed = true;
                    break;
                case "عنوان":
                    successfullyParsed = true;
                    break;
                case "صور - مستندات":
                    successfullyParsed = true;
                    break;
                default:
                    successfullyParsed = false;
                    break;


            }

            return successfullyParsed;
        }

        public static bool ValidEmail(string email)
        {
            // source: http://thedailywtf.com/Articles/Validating_Email_Addresses.aspx
            Regex rx = new Regex(
            @"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
            return rx.IsMatch(email);
        }

        public Int32 ConvertToId(string id)
        {
            var numAlpha = new Regex("(?<Alpha>[a-zA-Z]*)(?<Numeric>[0-9]*)");
            var match = numAlpha.Match(id);
            var num = Convert.ToInt32(match.Groups["Numeric"].Value);


            return num;
        }

        public string GetUniqueKey(int max)
        {
            int maxSize = max;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            { result.Append(chars[b % (chars.Length - 1)]); }
            return result.ToString();
        }

        public string GenerateRandomNo()
        {
            int _min = 0000000;
            int _max = 9999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max).ToString();
        }

    }
}