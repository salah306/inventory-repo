﻿using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class BalanceSheetsManager :HelperManager
    {
        private ApplicationDbContext db;

        public BalanceSheetsManager(ApplicationDbContext db)
        {
            this.db = db;
        }

        public IQueryable<BranchTree> GetAllBalanceSheets()
        {
            var bc = from b in db.Companies
                     select new BranchTree()
                     {
                         id = b.CompanyId.ToString(),
                         text = b.CompanyName,
                         children = (from i in db.BalanceSheets
                                     where i.CompanyId == b.CompanyId
                                     select new BalanceSheetTree()
                                     {
                                         id = i.BalanceSheetId.ToString(),
                                         text = i.BalanceSheetName,
                                         children = (from bt in db.BalanceSheetTypes
                                                     where bt.BalanceSheetId == i.BalanceSheetId
                                                     select new BalanceSheetTypeTree()
                                                     {
                                                         id = bt.BalanceSheetTypeId.ToString(),
                                                         text = bt.BalanceSheetTypeName,
                                                         children = (from ac in db.AccountCategories
                                                                     where ac.BalanceSheetTypeId == bt.BalanceSheetTypeId
                                                                     select new AccountCategoryTree()
                                                                     {
                                                                         id = ac.AccountCategoryName,
                                                                         text = ac.AccountCategoryName

                                                                     })

                                                     })

                                     })
                     };


            return bc;
        }

        public IQueryable<BalanceSheetTypesVm> GetAllBalanceSheetTypes()
        {

            var balanceSheetTypes = from i in db.BalanceSheetTypes
                                    select new BalanceSheetTypesVm()
                                    {
                                        id = i.BalanceSheetTypeName,
                                        name = i.BalanceSheetTypeName,
                                        BalanceSheetsId = i.BalanceSheet.BalanceSheetName,
                                        PublicId = i.BalanceSheetTypeId

                                    };

            return balanceSheetTypes;
        }

        public IQueryable<AccountCategoryPropertiesVM> GetAccountCatogeryProperties(Int32 accountCatogeryId)
        {
            
            var dt = (from d in db.AccountCategoryProperties
                      select new AccountCategoryPropertiesVM()
                      {
                          Id = d.AccountCategoryPropertiesId,
                          Name = d.AccountCategoryPropertiesName,
                         
                      }
                      );
            return dt;
        }

        public IQueryable<AccountsCatogeryDataTypes> GetAllDataTypes(SetAccountsCatogeryDataTypes cat)
        {
        
            var dataty = (from c in db.AccountsDataTypes
                          select new AccountsDataTypesVm
                          {
                              Id = c.Id,
                              TypeName = c.TypeName,
                              IsTrue = db.AccountCategories.Any(a => a.AccountCategoryId == cat.accountId && a.AccountsDataTypesId == c.Id)
                          });

            Int32 btypId = Convert.ToInt32(ConvertToId(cat.BalanceSheetTypeId));
            var dt = (from d in db.AccountCategories
                      where d.AccountCategoryId == cat.accountId
                      select new AccountsCatogeryDataTypes()
                      {
                          Id = d.AccountCategoryId,
                          CatogeryName = d.AccountCategoryName,
                          AccountsDataTypes = dataty.ToList()

                      });
            return dt;
        }
        public IQueryable<AccountCategoryPropertiesValuesVM> GetAccountCatogeryPropertiesValue(Int32 accountId)
        {
          
            var accid = db.Accounts.Find(accountId).AccountName;
            var dt = (from d in db.AccountCategoryProperties
                      select new AccountCategoryPropertiesValuesVM()
                      {
                          Id = d.AccountCategoryPropertiesId,
                          PropertName = d.AccountCategoryPropertiesName,
                          AccountId = accountId,
                          AccountName = accid,
                          Values = (from v in db.AccountCategoryPropertiesValues
                                    where v.AccountCategoryPropertiesId == d.AccountCategoryPropertiesId
                                    && v.AccountId == accountId
                                    select new AccPropertiesValuesVM()
                                    {
                                        Id = v.AccountCategoryPropertiesValueId,
                                        value = v.AccountCategoryPropertiesValueName
                                    }).ToList()
                      });
            var cc = dt;
            return dt;
        }

        public IEnumerable<AccountsTableFlatten> bsTree(Int32 companyId)
        {
            //IQueryable<AccountsTable> ListAccountsTable;
            Int32 indexp = 0;
            var tree =
            db.BalanceSheets.Where(b => b.CompanyId == companyId).ToList().OrderBy(cd => cd.CreatedDate).Select((bs, bsIndex) => new AccountsTableFlatten()
            {
                Id = bs.BalanceSheetId + "Bs",
                AccountLevel = "رئيسي",
                AccountName = bs.BalanceSheetName,
                TitleForAdd = "اضافة حـ عام",
                Code = bs.Code,
                ParentId = bs.CompanyId.ToString(),
                AccountType = bs.AccountType.AccountTypeName,
                indexNO = indexp++,
                Childern = bs.BalanceSheetTypes.OrderBy(cd => cd.CreatedDate).Select((bst, bstIndex) => new AccountsTableFlatten()
                {
                    Id = bst.BalanceSheetTypeId + "Bst",
                    AccountLevel = "عام",
                    AccountName = bst.BalanceSheetTypeName,
                    TitleForAdd = "اضافة حـ مساعد",
                    ParentId = bs.BalanceSheetId + "Bs",
                    AccountType = bst.AccountType.AccountTypeName,
                    Code = bst.Code,
                    indexNO = indexp + 2,
                    Childern = bst.AccountCategories.OrderBy(cd => cd.CreatedDate).Select((acc, accIndex) => new AccountsTableFlatten()
                    {
                        Id = acc.AccountCategoryId + "Acc",
                        AccountLevel = "مساعد",
                        AccountName = acc.AccountCategoryName,
                        AccountType = acc.AccountType.AccountTypeName,
                        TitleForAdd = "اضافة حـ فرعي",
                        Code = acc.Code,
                        ParentId = bst.BalanceSheetTypeId + "Bst",
                        AccountsDataTypesId = (Int32)acc.AccountsDataTypesId,
                        AccountsDataTypesName = acc.AccountsDataTypes.TypeName,
                        indexNO = indexp + 3,
                        Childern = acc.Accounts.OrderBy(cd => cd.CreatedDate).Select((ac, acIndex) => new AccountsTableFlatten()
                        {
                            Id = ac.AccountId + "Ac",
                            AccountLevel = "فرعي",
                            TitleForAdd = "اضافة حـ جزئي",
                            AccountName = ac.AccountName,
                            Code = ac.Code,
                            ParentId = acc.AccountCategoryId + "Acc",
                            linkedAccountId = ac.Branch == null ? bs.CompanyId : ac.BranchId,
                            linkedAccountName = ac.Branch == null ? bs.Company.CompanyName : ac.Branch.BranchName,
                            isCompany = ac.Branch == null ? true : false,
                            Activated = ac.Activated,
                            indexNO = indexp +4,
                            Childern = ac.SubAccounts.OrderBy(cd => cd.CreatedDate).Select((sac, sacIndex) => new AccountsTableFlatten()
                            {
                                Id = sac.SubAccountId + "Sac",
                                AccountLevel = "جزئي",
                                AccountName = sac.SubAccountName,
                                Code = sac.Code,
                                ParentId = ac.AccountId + "Ac",
                                indexNO = indexp + 1, 
                            })
                        })
                    })
                })
            }).Flatten(myObject => myObject.Childern)
        .ToList();

            var trre3 = tree.ToList();


            var trre4 = tree.OrderBy(cod => cod.Code).ToList();
            var trrsse4 = tree.OrderBy(cod => cod.indexNO).ToList();
            var trr333 = tree.GroupBy(a => a.ParentId).SelectMany(a => a.Key);
            var trre6 = tree.OrderBy(cod => cod.Id).ToList();
            var trre7 = tree.OrderBy(cod => cod.ParentId).ToList();

            var trre5 = tree.OrderBy(cod => cod.ParentId == cod.ParentId).ToList();

            var trre66 = tree.OrderByDescending(cod => cod.ParentId).ToList();

            var trr3ss33 = tree.GroupBy(a => a.ParentId).SelectMany(a => a);


            return tree;
        }

    



    }
    public static class LinqExtensions
    {
        /// <summary>
        ///   This method extends the LINQ methods to flatten a collection of 
        ///   items that have a property of children of the same type.
        /// </summary>
        /// <typeparam name = "T">Item type.</typeparam>
        /// <param name = "source">Source collection.</param>
        /// <param name = "childPropertySelector">
        ///   Child property selector delegate of each item. 
        ///   IEnumerable'T' childPropertySelector(T itemBeingFlattened)
        /// </param>
        /// <returns>Returns a one level list of elements of type T.</returns>
        public static IEnumerable<T> Flatten<T>(
            this IEnumerable<T> source,
            Func<T, IEnumerable<T>> childPropertySelector)
        {
            return source
                .Flatten((itemBeingFlattened, objectsBeingFlattened) =>
                         childPropertySelector(itemBeingFlattened));
        }

        /// <summary>
        ///   This method extends the LINQ methods to flatten a collection of 
        ///   items that have a property of children of the same type.
        /// </summary>
        /// <typeparam name = "T">Item type.</typeparam>
        /// <param name = "source">Source collection.</param>
        /// <param name = "childPropertySelector">
        ///   Child property selector delegate of each item. 
        ///   IEnumerable'T' childPropertySelector
        ///   (T itemBeingFlattened, IEnumerable'T' objectsBeingFlattened)
        /// </param>
        /// <returns>Returns a one level list of elements of type T.</returns>
        public static IEnumerable<T> Flatten<T>(
            this IEnumerable<T> source,
            Func<T, IEnumerable<T>, IEnumerable<T>> childPropertySelector)
        {
            return source
                .Concat(source
                            .Where(item => childPropertySelector(item, source) != null)
                            .SelectMany(itemBeingFlattened =>
                                        childPropertySelector(itemBeingFlattened, source)
                                            .Flatten(childPropertySelector)));
        }
    }


    public static class treeflat
    {

        //public static IEnumerable<T> Traverse<T>(this T root)
        //{
        //    var stack = new Stack<T>();
        //    stack.Push(root);
        //    while (stack.Count > 0)
        //    {
        //        var current = stack.Pop();
        //        yield return current;
        //        foreach (var child in current.)
        //            stack.Push(child);
        //    }
        //}
        //public static IEnumerable<T> Flatten<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> getChildren)
        //{
        //    var stack = new Stack<T>();
        //    foreach (var item in items)
        //        stack.Push(item);

        //    while (stack.Count > 0)
        //    {
        //        var current = stack.Pop();
        //        yield return current;

        //        var children = getChildren(current);
        //        if (children == null) continue;

        //        foreach (var child in children)
        //            stack.Push(child);
        //    }
        //}

    }
    public class AccountsTableFlatten
    {
        public string Id { get; set; }
        public string AccountName { get; set; }
        public string AccountLevel { get; set; }
        public string AccountType { get; set; }
        public string ParentId { get; set; }
        public string TitleForAdd { get; set; }
        public string Code { get; set; }
        public string fullCode { get; set; }
        public Int32? linkedAccountId { get; set; }
        public string linkedAccountName { get; set; }
        public bool isCompany { get; set; }
        public Int32 AccountsDataTypesId { get; set; }
        public bool Activated { get; set; }
        public string AccountsDataTypesName { get; set; }
        public IEnumerable<AccountsTableFlatten> Childern { get; set; }
        public Int32 indexNO { get; set; }
    }
}