﻿using System;
using Microsoft.Owin;
using Owin;
using Inventory.dl;
using Inventory.bl.Managers;
using Microsoft.Owin.Security.OAuth;
using System.Configuration;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;
using Inventory.api.Providers;
using Inventory.bl.Services;
using System.Web;

[assembly: OwinStartup(typeof(Inventory.api.Startup))]

namespace Inventory.api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            ConfigureAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            ConfigureOAuthTokenConsumption(app);
        }

        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);

            OAuthAuthorizationServerOptions OAuthOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(5),
                Provider = new ApplicationOAuthProvider(),
                //in production https://doMain To do
                AccessTokenFormat = new JWTToken("http://localhost:8080")
            };
            app.UseOAuthAuthorizationServer(OAuthOptions);

        }

        private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        {

            var issuer = "http://localhost:8080";
            string clientId = ConfigurationManager.AppSettings["as:ClientId"];
            byte[] clientSecret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["as:ClientSecret"]);

            // Api controllers with an [Authorize] attribute will be validated with JWT
            app.UseJwtBearerAuthentication(
                new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] { clientId },
                    IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                    {
                        new SymmetricKeyIssuerSecurityTokenProvider(issuer, clientSecret)
                    }
                });


            ClientService.sendToClient(new ClientData() { url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) });

        }
    }
}
