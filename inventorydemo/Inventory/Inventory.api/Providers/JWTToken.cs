﻿using Inventory.bl.Managers;
using Inventory.dl.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Thinktecture.IdentityModel.Tokens;

namespace Inventory.api.Providers
{
    public class JWTToken : ISecureDataFormat<AuthenticationTicket>
    {



        private const string clientPropertyKey = "client";

        private readonly string _issuer = string.Empty;

        public JWTToken(string issuer)
        {
            _issuer = issuer;
        }

        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            string clientId = data.Properties.Dictionary.ContainsKey(clientPropertyKey) ? data.Properties.Dictionary[clientPropertyKey] : null;

            if (string.IsNullOrWhiteSpace(clientId)) throw new InvalidOperationException("AuthenticationTicket.Properties does not include client");

            Client client = ClientsStore.FindClient(clientId);

            string symmetricKeyAsBase64 = client.Base64Secret;

            var keyByteArray = TextEncodings.Base64Url.Decode(symmetricKeyAsBase64);

           // var signingKey = new HmacSigningCredentials(keyByteArray);
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(keyByteArray);

            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(_issuer, clientId, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingCredentials);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
  
   
}