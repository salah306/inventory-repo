﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using Microsoft.AspNet.Identity;
using Inventory.dl;
using Inventory.bl.DTO;
using Inventory.bl.Managers;
using Inventory.dl.Models;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/Accounts")]
    public class AccountsController : BaseApiController
    {
        
        private AccountsManager AccountsManager;
        public AccountsController()
        {
            this.AccountsManager = new AccountsManager(this.context);
        }
        // GET: api/Accounts
        public IQueryable<AccountsVm> GetAccounts()
        {
            return db.Accounts.Select(i =>new AccountsVm
            {
                id = i.AccountName,
                name = i.AccountName,
                PublicId = i.AccountId
            });
        }

        // GET: api/Accounts/5
        [ResponseType(typeof(IQueryable<AccountsVm>))]
        [ActionName("AccountsById")]

        public IQueryable<AccountsVm> GetAccount(string id)
        {
            return db.Accounts.Where(a => a.AccountCategory.AccountCategoryName == id).
                 Select(i => new AccountsVm
                 {
                     id = i.AccountName,
                     name = i.AccountName,
                     Debit = db.AccountMovements.Where(a => a.AccountId == i.AccountId).Sum(d => d.Debit).ToString(),
                     Crdit = db.AccountMovements.Where(a => a.AccountId == i.AccountId).Sum(d => d.Crdit).ToString(),
                     AccountCategoriesId = i.AccountCategory.AccountCategoryName,
                     PublicId = i.AccountId
                 });
        }

        // GET: api/Accounts/5
       // [ResponseType(typeof(IQueryable<BranchesGroupVm>))]
        [Route("Getbranchgroup/{id:int}")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetBranchGroup(int id)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
           
            return Ok(AccountsManager.GetBranchesGroup(uid , id == 1 ? true : false));
        }


        //[ResponseType(typeof(IQueryable<AccountObject>))]
        [Route("GetAccountsNames")]
        [HttpPost]
        public IHttpActionResult GetSearchAccount(isCompanyBranch set)
        {
            //Int32 id = Convert.ToInt32(bId);
            //var r = repository.SearchAllAccounts(svm.branchId, svm.accSearch, listacc);
          return Ok(AccountsManager.SearchAllAccounts(set.Id , set.IsCompany));
        }


        //[ResponseType(typeof(IQueryable<AccountObject>))]
        [Route("GetfinancialStatements/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetSearchfinancialStatements(Int32 id)
        {
            //Int32 id = Convert.ToInt32(bId);
            //var r = repository.SearchAllAccounts(svm.branchId, svm.accSearch, listacc);
            return Ok(AccountsManager.SearchAllFinancialLists(id));
        }

        //[ResponseType(typeof(IQueryable<AccountObject>))]
        [Route("GetAccountsNamesCatogery")]
        [HttpPost]
        public IHttpActionResult GetSearchAccountCatogery(isCompanyBranch set)
        {
            //Int32 id = Convert.ToInt32(bId);
            //var r = repository.SearchAllAccounts(svm.branchId, svm.accSearch, listacc);
            return Ok(AccountsManager.SearchAllAccountsCatogery(set.Id , set.IsCompany));
        }

        // PUT: api/Accounts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccount(int id, Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != account.AccountId)
            {
                return BadRequest();
            }

            db.Entry(account).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Accounts
        [ResponseType(typeof(Account))]
        public async Task<IHttpActionResult> PostAccount(Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Accounts.Add(account);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = account.AccountId }, account);
        }

        // DELETE: api/Accounts/5
        [ResponseType(typeof(Account))]
        public async Task<IHttpActionResult> DeleteAccount(int id)
        {
            Account account = await db.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }

            db.Accounts.Remove(account);
            await db.SaveChangesAsync();

            return Ok(account);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountExists(int id)
        {
            return db.Accounts.Count(e => e.AccountId == id) > 0;
        }
    }
}