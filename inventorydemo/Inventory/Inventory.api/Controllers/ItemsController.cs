﻿
using Inventory.bl.DTO;
using Inventory.bl.Managers;
using Inventory.dl;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/items")]

    public class ItemsController : BaseApiController
    {
      
        private itemsManager itemsManager;
        public ItemsController()
        {
           
            this.itemsManager = new itemsManager(context , RequestContext.Principal.Identity.GetUserId());
        }

        [Route("getUnits")]
        [HttpPost]
        public IHttpActionResult getUnits()
        {
            try
            {
                string uid = RequestContext.Principal.Identity.GetUserId();
                var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

                if (company == null) return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");

                var units = itemsManager.getUnit(company);

                if (!units.IsValid) return BadRequest(units.GetErrsAsHtmlNewLine());

                return Ok(units.GetMainObject());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
          
        }

        [Route("getitemInfo")]
        [HttpPost]
        public IHttpActionResult Get(getitemInfoVM or)
        {

            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");
            }
            or.pageSize = 10;
            var getInfo = itemsManager.getItemInfo(or, company);
            var toReturn = new { items = getInfo.items, count = getInfo.count, totalPages = getInfo.totalPages, pageSize = getInfo.pageSize };

            return Ok(toReturn);
        }


        [Route("save")]
        [HttpPost]
        public IHttpActionResult save(itemsGroupDTO group)
        {
       
            if (!ModelState.IsValid)
            {
                var err = ModelState.Values.SelectMany(m => m.Errors)
                          .Select(e => e.ErrorMessage)
                          .ToList();
                string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                return BadRequest(tocoorect);
            }

            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            try
            {
          
                var save = itemsManager.saveGroup(group, company);
                return Ok(save);
            }
            catch (DbEntityValidationException ex)
            {
                var err = ex.EntityValidationErrors.SelectMany(m => m.ValidationErrors)
                               .Select(e => e.ErrorMessage)
                               .ToList();
                string tocoorect = "لا يمكن اتمام العملية لعدم اكتمال البيانات التالية :" + "<br>";
                foreach (var er in err)
                {
                    tocoorect = tocoorect + "<br>" + er;
                }
                var gr = itemsManager.GetGroupByName(group.groupName, company.companyId);

                return Content(HttpStatusCode.BadRequest, new { err = tocoorect, Group = gr });


            }


        }

        [Route("getItemsbyGroupId/{groupId}")]
        [HttpGet]
        public IHttpActionResult getItemsbyGroup(int groupId)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");
            }
            return Ok(itemsManager.getItemsbyGroup(groupId , company));
            //getItemsbyGroup
        }
    }
}
