﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Inventory.dl;
using Inventory.bl.Managers;
using Inventory.bl.DTO;
using Inventory.dl.Models;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/AccountOrders")]
    public class AccountOrdersController : BaseApiController
    {
     
        private AccountsManager AccountsManager;
        public AccountOrdersController()
        {
            this.AccountsManager = new AccountsManager(this.context);
        }


        // GET: api/AccountOrders
        [Route("GetAccountOrders/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetAccountOrders(int id)
        {
         

            return Ok(AccountsManager.GetAccountOrders(id , true).OrderByDescending(a => a.orderNo));
        }

        [Route("GetAccountOrdersDates")]
        [HttpPost]
        public IHttpActionResult Get(getOrders or)
        {
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
            var totalCount = AccountsManager.GetAccountOrders(or.id , or.isCompany).Where(a => a.orderDate >= sD && a.orderDate <= eD).Count();
            var totalPages = Math.Ceiling((double)totalCount / or.pageSize);

            var Orders = AccountsManager.GetAccountOrders(or.id , or.isCompany).Where(a => a.orderDate >= sD && a.orderDate <= eD);

            if(Orders.Count() == 0)
            {
                sD = DateTime.Now.AddDays(-29);
                eD = DateTime.Now;
                Orders = AccountsManager.GetAccountOrders(or.id , or.isCompany).Where(a => a.orderDate >= sD && a.orderDate <= eD);
            }

            var orders = Orders.OrderByDescending(a => a.orderNo).Skip((or.pageNumber - 1) * or.pageSize)
                                    .Take(or.pageSize)
                                    .ToList();

            if (!string.IsNullOrEmpty(or.search))
            {

                
              var   ordersse = Orders.Where(a => a.orderNo == AccountsManager.ConvertToId(or.search)).OrderByDescending(a => a.orderNo).Skip((or.pageNumber - 1) * or.pageSize)
                                    .Take(or.pageSize)
                                    .ToList();
                if(ordersse.Count > 0)
                {
                    orders = ordersse;
                }
            }




            var result = new
            {
                TotalCount = totalCount,
                totalPages = totalPages,
                Orders = orders
            };

            return Ok(result);
        }

        // GET: api/AccountOrders/5
        [ResponseType(typeof(AccountOrder))]
        public async Task<IHttpActionResult> GetAccountOrder(int id)
        {
            AccountOrder accountOrder = await db.AccountOrders.FindAsync(id);
            if (accountOrder == null)
            {
                return NotFound();
            }

            return Ok(accountOrder);
        }

        // PUT: api/AccountOrders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccountOrder(int id, AccountOrder accountOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accountOrder.AccountOrderId)
            {
                return BadRequest();
            }

            db.Entry(accountOrder).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccountOrders
        [ResponseType(typeof(OrderVm))]
        public async Task<IHttpActionResult> PostAccountOrder(OrderVm order)
        {
            var orderNo = 0;
            AccountOrder accOrder = new AccountOrder();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string uid = RequestContext.Principal.Identity.GetUserId();

            if (db.AccountOrders.Any(a => a.AccountOrderId == order.orderId))
            {
               accOrder = db.AccountOrders.Find(order.orderId);
               orderNo = order.orderId;
                accOrder.OrderNote = order.orderNote;
                accOrder.IsCompany = order.IsCompany;
                accOrder.CurrentWorkerId = uid;
                accOrder.BranchId = order.IsCompany == true ? null : order.branchId;
                if (order.IsCompany)
                {
                    accOrder.Branch = null;
                }
                db.Entry(accOrder).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                }
                catch
                {

                   
                }

                db.Entry(accOrder).GetDatabaseValues();

            }
            else
            {
                if (order.IsCompany)
                {
                    orderNo = db.AccountOrders.Where(a => a.CompanyId == order.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == order.CompanyId).Max(a => a.OrderNo) + 1 : 1;

                }
                else
                {
                    orderNo = db.AccountOrders.Where(a => a.BranchId == order.branchId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == order.branchId).Max(a => a.OrderNo) + 1 : 1;

                }

                accOrder = new AccountOrder()
                {
                    OrderDate = order.orderDate,
                    AccountOrderPropertiesId = 1,
                    OrderNo = orderNo,
                    BranchId = order.IsCompany == true ? null: order.branchId,
                    IsCompany = order.IsCompany,
                    CompanyId = order.CompanyId,
                    OrderNote = order.orderNote
                    

                };
                db.AccountOrders.Add(accOrder);

                try
                {
                    db.SaveChanges();
                }
                catch
                {

                   
                }

                db.Entry(accOrder).GetDatabaseValues();
            }


            List<AccountMovement> accountsToDel = db.AccountMovements.Where(a => a.AccountOrderId == accOrder.AccountOrderId).ToList();
            db.AccountMovements.RemoveRange(accountsToDel);
            decimal dAmounts = 0;
            decimal cAmounts = 0;

            //start check Amonts
            foreach (var Daccounts in order.debitorAccounts)
            {
                if (Daccounts.account.value != null && Daccounts.amount > 0)
                {
                    dAmounts += Daccounts.amount;
                }
            };

            foreach (var Caccounts in order.crditorAccounts)
            {
                if (Caccounts.account.value != null && Caccounts.amount > 0)
                {
                    cAmounts += Caccounts.amount;
                }
            }

            //end of Check amounts
            List<AccountMovement> accountsList = new List<AccountMovement>();

            if (cAmounts == dAmounts)
            {
               
                
                foreach (var Daccounts in order.debitorAccounts)
                {
                    if (Daccounts.account.value != null && Daccounts.amount > 0)
                    {
                        AccountMovement move = new AccountMovement()
                        {
                            AccountMovementDate = order.orderDate,
                            AccountOrderId = accOrder.AccountOrderId,
                            AccountId = Daccounts.account.id,
                            Debit = Daccounts.amount,
                            Crdit = 0,

                        };

                        accountsList.Add(move);
                    }
                };

                foreach (var Caccounts in order.crditorAccounts)
                {
                    if (Caccounts.account.value != null && Caccounts.amount > 0)
                    {
                        AccountMovement move = new AccountMovement()
                        {
                            AccountMovementDate = order.orderDate,
                            AccountOrderId = accOrder.AccountOrderId,
                            AccountId = Caccounts.account.id,
                            Crdit = Caccounts.amount,
                            Debit = 0

                        };

                        accountsList.Add(move);
                    }
                }
            };


            db.AccountMovements.AddRange(accountsList);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception)
            {
                db.AccountOrders.Remove(accOrder);
                throw;
            }   
           

            order.orderId = accOrder.AccountOrderId;

            return Ok(AccountsManager.GetAccountOrderById(accOrder.OrderNo ,accOrder.IsCompany == true ? (Int32)accOrder.CompanyId : (Int32)accOrder.BranchId , order.IsCompany));
        }

        // DELETE: api/AccountOrders/5
        [ResponseType(typeof(AccountOrder))]
        public async Task<IHttpActionResult> DeleteAccountOrder(int id)
        {
            AccountOrder accountOrder = await db.AccountOrders.FindAsync(id);
            if (accountOrder == null)
            {
                return NotFound();
            }

            db.AccountOrders.Remove(accountOrder);
            await db.SaveChangesAsync();

            return Ok(accountOrder);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountOrderExists(int id)
        {
            return db.AccountOrders.Count(e => e.AccountOrderId == id) > 0;
        }


    }
}