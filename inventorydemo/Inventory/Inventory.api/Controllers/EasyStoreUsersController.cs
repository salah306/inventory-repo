﻿using Inventory.bl.DTO;
using Inventory.bl.Managers;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.api.Controllers
{

    [RoutePrefix("api/easystoreusers")]
    public class EasyStoreUsersController : BaseApiController
    {

     
        private CompanyUserManager CompanyUserManager;
        public EasyStoreUsersController()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            this.CompanyUserManager = new CompanyUserManager(this.context , RequestContext.Principal.Identity.GetUserId());
        }


        [Route("getCompanyByName")]
        [HttpPost]
        public IHttpActionResult getCompanyByName(getCompanyByNameEasy getcompany)
        {
            var company = (from c in db.Companies
                           where c.CompanyName.Contains(getcompany.name)
                           && !c.ApplicationUsersComapnies.Any(a => a.ApplicationUserId == getcompany.uid)
                           select new GetCompanyeasy()
                           {
                               companyId = c.CompanyId,
                               companyName = c.CompanyName,
                               forUser = getcompany.uid
                           }).ToList();
            return Ok(company);
        }

        [Route("addUserToCompany")]
        [HttpPost]
        public IHttpActionResult addUserToCompany(GetCompanyeasy setappuserCompany)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.Companies.Find(setappuserCompany.companyId);
            if(company == null)
            {
                return BadRequest("شركة غير مسجلة");
            }
            var user = db.Users.Find(setappuserCompany.forUser);
            if(user == null)
            {
                return BadRequest("المستخدم غير مسجل");
            }
            if(user.ApplicationUsersComapnies.Any(a => a.CompanyId == company.CompanyId))
            {
                return BadRequest("المستخدم مسجل لدي الشركة بالفعل");
            }
            company.ApplicationUsersComapnies.ToList().Add(new ApplicationUserComapny
            {
                ApplicationUserId = setappuserCompany.forUser,
                CurrentWorkerId = uid
            });
            db.Entry(company).State = EntityState.Modified;
            db.SaveChanges();

            var usersett = db.ApplicationUsersComapnies.SingleOrDefault(a => a.ApplicationUserId == setappuserCompany.forUser && a.CompanyId == company.CompanyId);
            if(usersett == null)
            {
                return BadRequest("Unknown Error");
            }
            var ucom = new UserSettingseasy
            {
                settingsId = usersett.ApplicationUserComapnyId,
                companyId = usersett.CompanyId,
                companyName = usersett.Company.CompanyName,
                locked = usersett.Locked ?? false,
                properties = db.settingProperties.Select(r => new propertieseasy
                {
                    propertiesId = r.settingPropertiesId,
                    propertyName = r.propertyName
                }).ToList()

            };
            return Ok(ucom);
        }

        [Route("getAllUsers")]
        [HttpPost]
        public IHttpActionResult getAllUsers(setUsereasy setUser)
        {
            //getUserseasy

            var allUsers = db.Users.Select(u => new getUserseasy
            {
                userId = u.Id,
                userName = u.FirstName + " " + u.LastName,
                lockd = u.LockoutEnabled,
                userEmail = u.Email,
                phone = u.PhoneNumber
            }).ToList();

            if (!string.IsNullOrEmpty(setUser.Search))
            {
                allUsers = allUsers.Where(u => u.userEmail.Contains(setUser.Search) || u.userName.Contains(setUser.Search) || u.phone.Contains(setUser.Search)).ToList();
            }
            var totalCount = allUsers.Count();
            var totalPages = Math.Ceiling((double)totalCount / setUser.pageSize);
            var allusers = allUsers.Skip((setUser.pageNumber - 1) * setUser.pageSize)
                            .Take(setUser.pageSize)
                            .ToList();

            var reurnUsers = new
            {
                users = allusers,
                TotalCount = totalCount,
                totalPages = totalPages,
            };

            return Ok(reurnUsers);
        }

        [Route("getuers")]
        [HttpPost]
        public IHttpActionResult getUsers(setUsereasy setUser)
        {
            //getUserseasy
            string uid = RequestContext.Principal.Identity.GetUserId();
            var user = db.Users.Find(uid);

             if(user == null)
            {

            }
            var us = new getUsersMainInfo
            {
                userName = user.FirstName + " " + user.LastName,
                lockd = user.LockoutEnabled,
                userEmail = user.Email,
                phone = user.PhoneNumber

            };

            return Ok(us);
        }

        [Route("getusersand")]
        [HttpPost]
        public IHttpActionResult getUsersAnd(setUsereasy setUser)
        {
            //getUserseasy
            string uid = RequestContext.Principal.Identity.GetUserId();
            var user = db.Users.Find(uid);

            if (user == null)
            {
                return Unauthorized();
            }
           var us = new getUsersMainInfo
            {
                userName = user.FirstName + " " + user.LastName,
                lockd = user.LockoutEnabled,
                userEmail = user.Email,
                phone = user.PhoneNumber

            };
            var compaines = CompanyUserManager.GetBranchesGroup(true);
         
            var cb = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            branchGroupStVm cbvm = new branchGroupStVm();
            if (cb != null)
            {
                var isinCompany = compaines.Any(a => a.id == cb.id);
                if (isinCompany)
                {
                    cbvm = new branchGroupStVm
                    {
                        id = cb.id,
                        companyId = cb.companyId,
                        companyName = cb.name,
                        isCompany = cb.isCompany,
                        name = cb.name,
                        fullName = cb.fullName,
                        realName = cb.realName,
                        oId = cb.oId
                    };
                }
                else
                {
                    cbvm = new branchGroupStVm
                    {
                        id = "0"
                    };
                    db.branchGroupSt.Remove(cb);
                    db.SaveChanges();
                }
            }
            else
            {
                cbvm = new branchGroupStVm
                {
                    id = "0"
                };
            }

            var toReturn = new
            {
                user = us,
                companies = compaines,
                branchGroup = cbvm
            };
            return Ok(toReturn);
        }

        [Route("getUserSettings/{name}")]
        public IHttpActionResult getUserSettings(string name)
        {
            //UserSettingseasy
            var uid = db.Users.Find(name);
            var usersett = db.ApplicationUsersComapnies.Where(a => a.ApplicationUserId == name).Select(s => new UserSettingseasy
            {
                settingsId = s.ApplicationUserComapnyId,
                companyId = s.CompanyId,
                companyName = s.Company.CompanyName,
                locked =s.Locked ?? false,
                properties = db.settingProperties.Select(r => new propertieseasy
                                                    {
                                                      propertiesId =r.settingPropertiesId,
                                                      propertyName = r.propertyName
                                                    } ).ToList()

            }).ToList();


            return Ok(usersett);
        }

        [Route("getUserRole")]
        [HttpPost]
        public IHttpActionResult getUserRole(getuserRole role)
        {
            //List<roleseasy>
            var appuserCompany = db.ApplicationUsersComapnies.SingleOrDefault(a => a.CompanyId == role.CompanyId && a.ApplicationUserId == role.UserId);
            var properties = db.settingProperties.SingleOrDefault(a => a.settingPropertiesId == role.PropertyId);
            if(properties.name == "Company")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId  && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    typeRefrence = "Company",
                    typeRefrenceId = appuserCompany.Company.CompanyId,
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType =="Company" && r.RefrenceId == appuserCompany.Company.CompanyId),
                        rolepropertyName = s.roleName,
                        roleId = s.settingRoleId

                    }).ToList()
                } }.ToList();
                return Ok(roles);
            }
            if (properties.name == "Branch")
            {
                var roles = db.Branchs.Where(a => a.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.BranchName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == b.BranchName)),
                    typeRefrence = "Branch",
                    typeRefrenceId = b.BranchId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "Branch" && r.RefrenceId == b.BranchId),
                        rolepropertyName = s.roleName,
                        roleId = s.settingRoleId

                    }).ToList()
                }).ToList();

                return Ok(roles);
            }
            if (properties.name == "AccountsSetup")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName ==properties.propertyName),
                         rolepropertyName = s.roleName,
                         roleId = s.settingRoleId

                    }).ToList()
                } }.ToList();

                return Ok(roles);

            }

            if (properties.name == "BalanceSheetSetup")
            {
                var roles = db.BalanceSheets.Where(a => a.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.BalanceSheetName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == b.BalanceSheetName)),
                    typeRefrence = "BalanceSheetSetup",
                    typeRefrenceId = b.BalanceSheetId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceId == b.BalanceSheetId && r.RefrenceType == "BalanceSheetSetup"),
                        rolepropertyName = s.roleName,
                         roleId = s.settingRoleId
                    }).ToList()
                }).ToList();
                return Ok(roles);

            }
            if (properties.name == "BalanceSheetTypesSetup")
            {
                var roles = db.BalanceSheetTypes.Where(a => a.BalanceSheet.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.BalanceSheetTypeName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == b.BalanceSheetTypeName)),
                    typeRefrence = "BalanceSheetTypeSetup",
                    typeRefrenceId = b.BalanceSheetTypeId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "BalanceSheetTypeSetup" && r.RefrenceId == b.BalanceSheetTypeId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);

            }
            if (properties.name == "AccountCategoriesSetup")
            {
                var roles = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountCategoryName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == b.AccountCategoryName)),
                    typeRefrence = "AccountCategorySetup",
                    typeRefrenceId = b.AccountCategoryId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "AccountCategorySetup" && r.RefrenceId == b.AccountCategoryId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);

            }
            if (properties.name == "FstatementSetup")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName =properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == properties.propertyName),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();

                return Ok(roles);

            }

            if (properties.name == "OrdersSetup")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName ==  properties.propertyName),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();

                return Ok(roles);

            }
            if (properties.name == "ItemsSetup")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName ==  properties.propertyName),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();

                return Ok(roles);

            }

            if (properties.name == "GeneralJournal")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == "GeneralJournal")),
                    typeRefrence = "GeneralJournal",
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "GeneralJournal" && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }
          

            if (properties.name == "SubGl")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == "SubGl")),
                    typeRefrence = "SubGl",
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "SubGl" && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            if (properties.name == "Gl")
            {
                var roles = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountCategoryName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountCategoryId && r.RefrenceType == "Gl")),
                    typeRefrence = "Gl",
                    typeRefrenceId = b.AccountCategoryId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "Gl" && r.RefrenceId == b.AccountCategoryId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }
            if (properties.name == "TrialBalance")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName =properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == "ميزان المراجعة"),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();

                return Ok(roles);
            }
            if (properties.name == "fStatements")
            {
                var roles = db.FinancialLists.Where(a => a.CompanyId == appuserCompany.CompanyId).ToList().Select(b => new roleseasy
                {
                    roleName = b.FinancialListName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.FinancialListId && r.RefrenceType == "FinancialLists")),
                    typeRefrence = "FinancialLists",
                    typeRefrenceId = b.FinancialListId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == "FinancialLists" && r.RefrenceId == b.FinancialListId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            if (properties.name == "Orders")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = "مبيعات",
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.roleName == "مبيعات")),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == "مبيعات"),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                },
                  new  roleseasy()  {
                    roleName = "رد مبيعات",
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.roleName == "رد مبيعات")),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == "رد مبيعات"),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                },
                   new  roleseasy()  {
                    roleName = "مشتريات",
                     active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.roleName == "مشتريات")),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == "مشتريات"),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                },
                new  roleseasy()  {
                    roleName = "رد مشتريات",
                     active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.roleName == "رد مشتريات")),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName == "رد مشتريات"),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }

                }.ToList();

                return Ok(roles);
            }

            if (properties.name == "InventoryCards")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName ==  properties.propertyName),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();
                return Ok(roles);
            }

            if (properties.name == "Inventories")
            {
                var roles = new List<roleseasy>()
                {
                 new  roleseasy()  {
                    roleName = properties.propertyName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active && r.roleName == properties.propertyName)),
                    roleproperties = properties.settingRole.Select(s => new rolepropertieseasy
                    {
                        rolepropertiesId = s.settingPropertiesId,
                         isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.roleName ==  properties.propertyName),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                } }.ToList();
                return Ok(roles);
            }

            if (properties.name == "cash")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }


            if (properties.name == "bank")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }
            if (properties.name == "cust")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            if (properties.name == "pur")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            if (properties.name == "notepay")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            if (properties.name == "notereceive")
            {
                var roles = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == appuserCompany.CompanyId && a.AccountCategory.AccountsDataTypes.aliasName == properties.name).ToList().Select(b => new roleseasy
                {
                    roleName = b.AccountName,
                    active = db.settingPropertiesforUser.Any(a => a.settingPropertiesId == properties.settingPropertiesId && a.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && a.roles.Any(r => r.active == true && r.RefrenceId == b.AccountId && r.RefrenceType == properties.name)),
                    typeRefrence = properties.name,
                    typeRefrenceId = b.AccountId,
                    roleproperties = properties.settingRole.ToList().Select(s => new rolepropertieseasy()
                    {
                        rolepropertiesId = s.settingPropertiesId,
                        isTrue = db.settingRolesForUser.Any(r => r.active == true && r.settingRoleId == s.settingRoleId && r.settingPropertiesforUser.ApplicationUserComapnyId == appuserCompany.ApplicationUserComapnyId && r.settingPropertiesforUser.settingPropertiesId == s.settingPropertiesId && r.RefrenceType == properties.name && r.RefrenceId == b.AccountId),
                        roleId = s.settingRoleId,
                        rolepropertyName = s.roleName

                    }).ToList()
                }).ToList();
                return Ok(roles);
            }

            return Ok();
        }

        [Route("setUserRole")]
        [HttpPost]
        public IHttpActionResult setUserRole(setUserRole role)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var appUserCompany = db.ApplicationUsersComapnies.SingleOrDefault(a => a.ApplicationUserId == role.settingInfo.UserId && a.CompanyId == role.settingInfo.CompanyId);
            var settingProperty = db.settingProperties.Find(role.settingInfo.PropertyId);
            var isSettingExiset = db.settingPropertiesforUser.Any(a => a.ApplicationUserComapnyId == appUserCompany.ApplicationUserComapnyId && a.settingPropertiesId == settingProperty.settingPropertiesId);
            if (isSettingExiset)
            {
                var roletoRemove = db.settingPropertiesforUser.SingleOrDefault(a => a.ApplicationUserComapnyId == appUserCompany.ApplicationUserComapnyId && a.settingPropertiesId == settingProperty.settingPropertiesId);
                if(!roletoRemove.roles.Any())
                {
                    db.settingRolesForUser.RemoveRange(roletoRemove.roles);
                }
               
                db.settingPropertiesforUser.Remove(roletoRemove);
            }

            List<settingRolesForUser> setroles = new List<settingRolesForUser>();
            foreach (var roleTo in role.roleToAdd)
            {
                if (roleTo.active)
                {
                    foreach (var r in  roleTo.roleproperties)
                    {
                        if (r.isTrue)
                        {
                            var setroleforUser = new settingRolesForUser()
                            {
                                active = r.isTrue,
                                roleName = roleTo.roleName,
                                RefrenceType = roleTo.typeRefrence,
                                RefrenceId = roleTo.typeRefrenceId,
                                CurrentWorkerId = uid,
                                settingRoleId = r.roleId
                            };
                            setroles.Add(setroleforUser);
                        }
                      
                      
                    }

                   
                }
            }
            var setpropForUser = new settingPropertiesforUser()
            {
                settingPropertiesId = settingProperty.settingPropertiesId,
                CurrentWorkerId = uid,
                ApplicationUserComapnyId = appUserCompany.ApplicationUserComapnyId,
               
            };
            if(setroles.Count > 0)
            {
                if(setpropForUser.roles == null)
                {
                    setpropForUser.roles = new List<settingRolesForUser>();
                }
                setpropForUser.roles = setroles;
            }
            if(role.roleToAdd.Any(a => a.active == true && a.roleproperties.Any(r => r.isTrue)))
            {
                db.settingPropertiesforUser.Add(setpropForUser);
            }
           
            db.SaveChanges();
            return Ok();
        }

        [HttpPost]
        [Route("inviteUser")]
        public IHttpActionResult InviteUser(CreateUserInvite createUserModel)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = createUserModel.email,
                Email = createUserModel.email,
                FirstName = createUserModel.firstName,
                LastName = "",
                Level = 3,
                JoinDate = DateTime.Now.Date,
                PhoneNumber = createUserModel.phoneNumber,
                EmailConfirmed = true

            };

            string password = "Invite@" + CompanyUserManager.GenerateRandomNo();

            IdentityResult addUserResult =  this.AppUserManager.Create(user, password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }

            string code =  this.AppUserManager.GeneratePasswordResetToken(user.Id);

            //var callbackUrl = new Uri(Url.Link("ConfirmEmailRoute", new { userId = user.Id, code = code }));

            this.AppUserManager.SendEmail(user.Id,
                                                    "تاكيد العضوية",
                                                    "المستخدم" + "<br/>" + user.Email + "<br/>" + "كلمة المرور" + "<br/>" + password);

            //this.AppUserManager.SendSms(user.Id, "شكرا للاشتراك في SheetScript" + "<br/>" + user.Email + "<br/>" + "كلمة المرور" + "<br/>" + password);
            
            //Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Ok(user.Email);

        }
    }
}
