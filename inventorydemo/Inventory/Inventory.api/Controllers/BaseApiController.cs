﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Http;
using System.Web.Http;
using Inventory.bl.Managers;
using Inventory.dl;
using Microsoft.Owin;
using Inventory.api.Models;
using Inventory.bl.Services;

namespace Inventory.api.Controllers
{
    public class BaseApiController : ApiController
    {
        private ModelFactory _modelFactory;
        private IOwinContext _context = null;
        private ApplicationDbContext _db = null;
        private ApplicationUserManager _AppUserManager = null;
        private ApplicationRoleManager _AppRoleManager = null;
        private EmailTemplateManger _EmailTemplateManger = null;
        public BaseApiController()
        {

            _context = Request.GetOwinContext();
          
        }
        
        protected IOwinContext context
        {
            get
            {
                return _context ?? Request.GetOwinContext();
            }
        }

        protected ApplicationUserManager AppUserManager
        {
            get
            {
                return _AppUserManager ?? _context.GetUserManager<ApplicationUserManager>();
            }
        }
        protected ApplicationRoleManager AppRoleManager
        {
            get
            {
                return _AppRoleManager ?? _context.GetUserManager<ApplicationRoleManager>();
            }
        }
        protected ApplicationDbContext db
        {
            get
            {
                return _db ?? _context.Get<ApplicationDbContext>();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
                _AppUserManager.Dispose();
                _AppRoleManager.Dispose();
            }
            base.Dispose(disposing);
        }
        protected EmailTemplateManger EmailTemplateManger
        {
            get
            {
                return _EmailTemplateManger ?? new EmailTemplateManger();
            }
              
        }
        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.AppUserManager);
                }
                return _modelFactory;
            }
        }

       

        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

     
    }
}