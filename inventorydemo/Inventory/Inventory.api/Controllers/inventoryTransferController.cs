﻿using Inventory.bl.DTO;
using Inventory.bl.Managers;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/inventoryTransfer")]
    public class inventoryTransferController :  BaseApiController
    {
        private inventoryTransferManager inventoryTransferManager;
        public inventoryTransferController()
        {
            this.inventoryTransferManager = new inventoryTransferManager(this.context);
        }

        [Route("getInventory")]
        [HttpPost]
        public IHttpActionResult getInventory(InventoryTransferVm getInventory)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var findInv = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.AccountCategory.AccountsDataTypes.aliasName == "inventory").ToList();
            if(findInv == null)
            {
                return BadRequest("لا يوجد مخازن متاحة للعمليات في هذه الشركة");

            }
            var returninv = findInv.Select(a => new InventoryTransfer()
            {
                inventoryId = a.AccountId,
                inventoryName = a.AccountName,
                code = a.Code
            }).ToList();
            return Ok(returninv);
        }

        [Route("getInventoryTo")]
        [HttpPost]
        public IHttpActionResult getInventoryTo(getInventoryTransfer getInventory)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
           

            var items = inventoryTransferManager.getitemAvailableByinventoryId(getInventory.inventoryId, company);
            if (items == null)
            {
                return BadRequest("لا يوجد اصناف متاحة لهذة العملية");
            }

            var transferInventory = inventoryTransferManager.getInventoryMatchTransfers(company, getInventory.inventoryId);

            if (transferInventory == null)
            {
                return BadRequest("لا يوجد مخازن متاحة لعملية او بضاعة متاحة");

            }
            var toReturn = new 
            {
                items = items,
                InventoryTransferTo = transferInventory
            };
            return Ok(toReturn);

        }

        [Route("saveTransferOrder")]
        [HttpPost]
        public IHttpActionResult saveTransferOrder(InventoryTransferOrderVM order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

            #region valdate Order

            var getinfo =inventoryTransferManager.getTransferInfo(order.transferFrom.inventoryId, company, true);
            if(getinfo == null)
            {
                return BadRequest("يوجد اخطاء في البيان الذي تريد تسجيلة");
            }
            if(!getinfo.InventoryTransferTo.Any())
            {
                return BadRequest("لا توجد مخازن متوافقة مع طلبك");
            }
            #endregion

            if (order.IsNewOrder)
            {
                #region isNewOrder
                var orderNo = db.InventoryTransferEntity.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.MainAccountId == order.transferFrom.inventoryId).Count() != 0 ? db.InventoryTransferEntity.Where(a => a.CompanyId == company.companyId && a.BranchId == (order.IsCompany ? (int?)null : company.oId) && a.MainAccountId == order.transferFrom.inventoryId).Count() + 1 : 1;
                var newOrder = new InventoryTransferEntity()
                {
                    OrderDate = order.OrderDate,
                    OrderDateSecond = order.OrderDate,
                    OrderNo = orderNo,
                    IsCompany = order.IsCompany,
                    CompanyId = company.companyId,
                    BranchId = (order.IsCompany ? (int?)null : order.CompanyId),
                    DoneFirst =true,
                    DoneSceond = false,
                    MainAccountId = order.transferFrom.inventoryId,
                    TitleAccountId = order.transferTo.inventoryId,
                    CurrentWorkerId = uid,
                    Cancel = false
                    
                };
                foreach (var i in order.transferItems)
                {
                    if (string.IsNullOrEmpty(i.itemName) || string.IsNullOrEmpty(i.itemCode))
                    {
                        return BadRequest("يجب ادخال الصنف");

                    }
                   var findItem = getinfo.items.SingleOrDefault(a => a.itemName == i.itemName && a.itemCode == i.itemCode && a.itemId == i.itemId);
                    if(findItem == null)
                    {
                        return BadRequest("الصنف غير متاح للتحويل");
                    }
                    if (i.qty == 0)
                    {
                        return BadRequest("الكمية يجب ان تكون اكبر من صفر");
                    }
                    if(i.qty > findItem.Qty)
                    {
                     
                        return BadRequest("كمية غير متاحة من");
                    }
                    
                    
                }
                var invtransit = db.Accounts.SingleOrDefault(a => a.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit" && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId);

                var items = order.transferItems.Select(i => new InventoryItems() {
                    SubAccountFromId = i.itemId,
                    itemCode = i.itemCode,
                    itemName = i.itemName,
                    qty = i.qty,
                    price = i.price,
                    costPrice = getinfo.items.Where(n => n.itemId == i.itemId).Select(c => c.costPrice).SingleOrDefault(),
                    note = i.note,
                    CurrentWorkerId = uid,
                    ItemGroupId = db.SubAccounts.Where(r => r.SubAccountId == i.itemId).Select(g => g.ItemInfo.ItemGroupId).SingleOrDefault(),
                    SubAccountToId = db.SubAccounts.Where(s => s.AccountId == order.transferTo.inventoryId && s.SubAccountName == i.itemName).Select(su => su.SubAccountId).SingleOrDefault(),
                    SubAccountTransitId = invtransit.SubAccounts.Where(s => s.SubAccountName == i.itemName).Select(su => su.SubAccountId).SingleOrDefault(),
                    
                }).ToList();
                
                if(newOrder.Items == null)
                {
                    
                    newOrder.Items = new List<InventoryItems>();
                }
                newOrder.Items = items;
                db.InventoryTransferEntity.Add(newOrder);
                DateTime datenow =order.OrderDate;
                var subAccountMoveListFrom = new List<SubAccountMovement>();
                var subAccountMoveListTranst = new List<SubAccountMovement>();
                #region subaccountMove
                var getinfoTo = inventoryTransferManager.getitemAvailableByNotinventoryId(invtransit.AccountId, company, true);
                foreach (var item in items)
                {
                    var selectedItem = getinfo.items.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode && n.itemId == item.SubAccountFromId);

                    var subAccountsFrom = new SubAccountMovement()
                    {
                        SubAccountId = item.SubAccountFromId,
                        CurrentWorkerId = uid,
                        SubAccountMovementDate = datenow ,
                        OrderNote = "تحويل",
                        QuantityIn = 0,
                        QuantityOut = item.qty,
                        ItemPriceOut = selectedItem.costPrice,
                        ItemTotalPriceOut = selectedItem.costPrice * item.qty,
                        ItemStockQuantity = selectedItem.Qty - item.qty,
                        ItemTotalStockPrice = (selectedItem.Qty - item.qty) * selectedItem.costPrice,
                        ItemStockPrice = selectedItem.Qty - item.qty != 0  ?((selectedItem.Qty - item.qty) * selectedItem.costPrice) / (selectedItem.Qty - item.qty) : 0,
                        OrderId = orderNo
                    };
                    subAccountMoveListFrom.Add(subAccountsFrom);
                    var selectedItemTo = getinfoTo.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode && n.itemId == item.SubAccountTransitId);

                    var subAccountTranst = new SubAccountMovement()
                    {
                        SubAccountId = item.SubAccountTransitId,
                        CurrentWorkerId = uid,
                        SubAccountMovementDate = datenow,
                        OrderNote = "بضاعة محولة في الطريق",
                        QuantityIn = item.qty,
                        QuantityOut = 0,
                        ItemPricein = selectedItem.costPrice,
                        ItemTotalPriceIn = selectedItem.costPrice * item.qty,
                        ItemStockQuantity = selectedItemTo.Qty + item.qty,
                        ItemTotalStockPrice = selectedItemTo.totalStockprice + (selectedItem.costPrice * item.qty),
                        ItemStockPrice = (selectedItemTo.totalStockprice + (selectedItem.costPrice * item.qty)) / (selectedItemTo.Qty + item.qty),
                        OrderId = orderNo
                    };
                    subAccountMoveListTranst.Add(subAccountTranst);
                }
                subAccountMoveListFrom.AddRange(subAccountMoveListTranst);
                #region subAccountOrder
                var suborderfrom = new SubAccountOrder()
                {
                    OrderDate = datenow,
                    CurrentWorkerId = uid,
                    OrderNote = "تحويل صادر",
                    CustomSubAccountsOrderId = orderNo,
                    isDone = true,
                    SubAccountMovements = subAccountMoveListFrom
                };
              
                db.SubAccountOrders.Add(suborderfrom);
                
                #endregion
                #endregion

                var accountMoveListFrom = new List<AccountMovement>();

                #region accountMove
                var groupAmount = subAccountMoveListTranst.Select(a =>a.QuantityIn * a.ItemPricein).Sum();
                var accmoveFrom = new AccountMovement()
                {
                    AccountId = order.transferFrom.inventoryId,
                    Crdit = groupAmount,
                    Debit = 0,
                    CurrentWorkerId = uid,
                    AccountMovementDate = datenow,
                    AccountMovementNote = "تحويل",
                    
                };
                var accmoveTrnsit = new AccountMovement()
                {
                    AccountId = invtransit.AccountId,
                    Crdit = 0,
                    Debit = groupAmount,
                    CurrentWorkerId = uid,
                    AccountMovementDate = datenow,
                    AccountMovementNote = "تحويل",

                };
                accountMoveListFrom.Add(accmoveFrom);
                accountMoveListFrom.Add(accmoveTrnsit);
                var accorderNo = 1;
                if (company.isCompany)
                {
                    accorderNo = db.AccountOrders.Where(a => a.CompanyId == company.companyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == company.companyId).Max(a => a.OrderNo) + 1 : 1;
                }
                else
                {
                    accorderNo = db.AccountOrders.Where(a => a.BranchId == company.oId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == company.oId).Max(a => a.OrderNo) + 1 : 1;
                }
                var accnote = string.Format("بضاعة محولة في الطريق - من {0} - {1} الي {2} - {3} - رقم التحويل {4} - بقيمة {5}" , order.transferFrom.inventoryName , order.transferFrom.code , order.transferTo.inventoryName , order.transferTo.code , orderNo , groupAmount);
                Int32 refId = Convert.ToInt32((order.transferFrom.code + orderNo));
                var newAccountOrder = new AccountOrder()
                {
                    OrderDate = datenow,
                    CompanyId = company.isCompany ? company.companyId : (int?)null,
                    BranchId = company.isCompany ? (int?)null : company.oId,
                    IsCompany =  company.isCompany,
                    HasDone = true,
                    OrderIdRefrence = refId,
                    OrderTypeRefrence = "تحويل",
                    AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                    OrderNo = accorderNo,
                    OrderNote = accnote,
                    CurrentWorkerId = uid,
                    
                };
                if(newAccountOrder.AccountMovements == null)
                {
                    newAccountOrder.AccountMovements = new List<AccountMovement>();
                }
                newAccountOrder.AccountMovements = accountMoveListFrom;
                db.AccountOrders.Add(newAccountOrder);
                #endregion
                db.SaveChanges();
                #endregion
                var returned = inventoryTransferManager.getTransferbyId(new InventoryTransferById() { inventoryId = order.transferFrom.inventoryId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = orderNo } , company);
                var itemsupdated = inventoryTransferManager.getitemAvailableByinventoryId(order.transferFrom.inventoryId, company);
                return Ok(new {order = returned , items = itemsupdated });

            }
            else
            {
                #region Not IsNewOrder Canceltion
                var cOrder = db.InventoryTransferEntity.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.OrderNo == order.OrderNo);
                if (cOrder == null)
                {
                    return BadRequest("اذن التحويل غير مسجل او غير متاح لهذة العملية");
                }
                if (cOrder.DoneSceond)
                {
                    return BadRequest("لقد تم استلام التحويل وتاكيدة لا يمكن التعديل او الالغاء");
                }
                if (cOrder.Cancel)
                {
                    return BadRequest("لقد تم الالفاء من قبل");
                }
                if (order.cancel && !cOrder.Cancel && !cOrder.DoneSceond)
                {
                    

                    #region transferOrder
                        cOrder.Cancel = true;
                        cOrder.CurrentWorkerCancelId = uid;
                        cOrder.CancelDate = order.OrderDate;
                        cOrder.DoneSceond = true;
                        db.Entry(cOrder).State = EntityState.Modified;
                     
                    #endregion

                    #region AccountOrder
                 
                    Int32 refId = Convert.ToInt32((cOrder.MainAccount.Code + cOrder.OrderNo));
                    var accCancelOrder = db.AccountOrders.SingleOrDefault(a => a.OrderIdRefrence == refId
                                                                    && a.OrderTypeRefrence == "تحويل"
                                                                    && a.IsCompany == company.isCompany
                                                                    && a.CompanyId == (company.isCompany ? company.oId : (int?)null)
                                                                    && a.BranchId == (company.isCompany ? (int?)null : company.oId));
                    //var invtransit = accCancelOrder.AccountMovements.SelectMany(a => a.Debit > 0 && a.Crdit == 0 && a.AccountId != finordertrans.transferTo.inventoryId && a.Accounts.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit");
                    
                    var subAccountMoveListTo = new List<SubAccountMovement>();
                    var subAccountMoveListTranst = new List<SubAccountMovement>();
                    #region subaccountMove
                    var trnsitId = cOrder.Items.FirstOrDefault().SubAccountTransit.AccountId;
                    //var getinfoTo = this.AppCompanyUserManager.getTransferInfoNotinventory(new getInventoryTransfer { inventoryId = trnsitId, CompanyId = company.oId, inventoryName = order.transferFrom.inventoryName, isComapny = order.IsCompany }, company, true);
                    var getinfoTo = inventoryTransferManager.getitemAvailableByNotinventoryId(trnsitId, company, true);

                    foreach (var item in cOrder.Items)
                    {
                        var selectedItem = getinfo.items.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode && n.itemId == item.SubAccountFromId);
                        var subAccountsTo = new SubAccountMovement()
                        {
                            SubAccountId = item.SubAccountFromId,
                            CurrentWorkerId = uid,
                            SubAccountMovementDate = order.OrderDate,
                            OrderNote = "الغاء تحويل",
                            QuantityIn = item.qty,
                            QuantityOut = 0,
                            ItemPricein = item.costPrice,
                            ItemTotalPriceIn = item.costPrice * item.qty,
                            ItemStockQuantity = selectedItem.Qty + item.qty,
                            ItemTotalStockPrice = selectedItem.totalStockprice + (item.costPrice * item.qty),
                            ItemStockPrice = (selectedItem.totalStockprice + (item.costPrice * item.qty)) / (selectedItem.Qty + item.qty),
                            OrderId = order.OrderNo
                        };
                        
                        
                        var selectedItemTo = getinfoTo.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode && n.itemId == item.SubAccountTransitId);

                        var subAccountTranst = new SubAccountMovement()
                        {
                            SubAccountId = item.SubAccountTransitId,
                            CurrentWorkerId = uid,
                            SubAccountMovementDate = order.OrderDate,
                            OrderNote = "الغاء تحويل",
                            QuantityIn = 0,
                            QuantityOut = item.qty,
                            ItemPriceOut = selectedItem.costPrice,
                            ItemTotalPriceOut = selectedItem.costPrice * item.qty,
                            ItemStockQuantity = selectedItemTo.Qty - item.qty,
                            ItemTotalStockPrice = selectedItemTo.totalStockprice - (selectedItem.costPrice * item.qty),
                            ItemStockPrice = selectedItemTo.totalStockprice - (selectedItem.costPrice * item.qty) != 0 ? (selectedItemTo.totalStockprice - (selectedItem.costPrice * item.qty)) / (selectedItem.Qty - item.qty) : 0,
                            OrderId = order.OrderNo
                        };
                        subAccountMoveListTranst.Add(subAccountTranst);
                        subAccountMoveListTranst.Add(subAccountsTo);
                    }

                    #region subAccountOrder
                    var suborderfrom = new SubAccountOrder()
                    {
                        OrderDate = order.OrderDate,
                        CurrentWorkerId = uid,
                        OrderNote = "الغاء تحويل",
                        CustomSubAccountsOrderId = order.OrderNo,
                        isDone = true,
                        SubAccountMovements = subAccountMoveListTranst
                    };

                    db.SubAccountOrders.Add(suborderfrom);
                    #endregion
                    #endregion

                    var accountMoveListFrom = new List<AccountMovement>();
                    foreach (var move in accCancelOrder.AccountMovements)
                    {
                        var ac = new AccountMovement()
                        {
                            AccountId = move.AccountId,
                            Crdit = move.Crdit == 0 ? move.Debit : 0,
                            Debit = move.Debit == 0 ? move.Crdit : 0,
                            CurrentWorkerId = uid,
                            AccountMovementDate = order.OrderDate,
                            AccountMovementNote = "الغاء تحويل",
                        };
                        accountMoveListFrom.Add(ac);
                    }
                   
                   
                    var accorderNo = 1;
                    if (company.isCompany)
                    {
                        accorderNo = db.AccountOrders.Where(a => a.CompanyId == company.companyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == company.companyId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    else
                    {
                        accorderNo = db.AccountOrders.Where(a => a.BranchId == company.oId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == company.oId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    var vamount = subAccountMoveListTranst.Select(a => a.ItemTotalPriceOut).Sum();
                    var accnote = string.Format("قيد الغاء التحويل رقم {0} - من {1} - {2} - بقيمة {3} - رقم قيد التحويل {4}", cOrder.OrderNo, cOrder.MainAccount.AccountName, cOrder.MainAccount.Code, vamount , accCancelOrder.OrderNo);
                   
                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = order.OrderDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "الغاء تحويل",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,

                    };
                    if (newAccountOrder.AccountMovements == null)
                    {
                        newAccountOrder.AccountMovements = new List<AccountMovement>();
                    }
                    newAccountOrder.AccountMovements = accountMoveListFrom;
                    db.AccountOrders.Add(newAccountOrder);
                    #endregion

                    db.SaveChanges();
                    var returned = inventoryTransferManager.getTransferbyId(new InventoryTransferById() { inventoryId = cOrder.MainAccount.AccountId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = order.OrderNo }, company);
                    var itemsupdated = inventoryTransferManager.getitemAvailableByinventoryId(cOrder.MainAccount.AccountId, company);
                    return Ok(new { order = returned, items = itemsupdated });

                }
                #endregion

            }


            return BadRequest("لا يوجد عمليات متاحة لطلبك");
        }


        [Route("GetTransferOrderById")]
        [HttpPost]
        public IHttpActionResult TransferOrderBuId(InventoryTransferById order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var returned = inventoryTransferManager.getTransferbyId(new InventoryTransferById() { inventoryId = order.inventoryId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = order.OrderNo }, company);
            if(returned == null)
            {
                return BadRequest("لا يوجد تحويل مسجل بهذا الرقم");
            }
            return Ok(returned);

        }

        [Route("TransferOrderinById")]
        [HttpPost]
        public IHttpActionResult TransferOrderinById(InventoryTransferById order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var returned = inventoryTransferManager.getTransferinbyId(new InventoryTransferById() { inventoryId = order.inventoryId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = order.OrderNo }, company);
            if (returned == null)
            {
                return BadRequest("لا يوجد تحويل مسجل بهذا الرقم");
            }
            return Ok(returned);

        }

        [Route("transferOrderOutforInById")]
        [HttpPost]
        public IHttpActionResult TransferOrderOutforInById(InventoryTransferById order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var returned = inventoryTransferManager.getTransferforinbyId(new InventoryTransferById() { inventoryId = order.inventoryId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = order.OrderNo }, company);
            if (returned == null)
            {
                return BadRequest("لا يوجد تحويل مسجل بهذا الرقم");
            }
            return Ok(returned);

        }

        [Route("saveTransferOrderin")]
        [HttpPost]
        public IHttpActionResult saveTransferOrderin(SaveInventoryTransferin order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

                #region Confirm
                var cOrder = db.InventoryTransferEntity.SingleOrDefault(a => a.CompanyId == company.companyId && a.OrderNo == order.OrderNo && a.TitleAccountId == order.transferFromId);
                if (cOrder == null)
                {
                    return BadRequest("اذن التحويل غير مسجل او غير متاح لهذة العملية");
                }
                if (cOrder.DoneSceond)
                {
                    return BadRequest("لقد تم استلام التحويل وتاكيدة لا يمكن التعديل او الالغاء");
                }
                if (cOrder.Cancel)
                {
                    return BadRequest("لقد تم الالفاء من قبل");
                }
                if (!cOrder.DoneSceond)
                {

                var cOrderNo = db.InventoryTransferEntity.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.TitleAccountId == order.transferFromId).Select(b => b.OrderNoIn).DefaultIfEmpty(0).Max() + 1;

                #region transferOrder
                cOrder.DoneSceond = true;
                cOrder.OrderNoIn = cOrderNo;
                    cOrder.CurrentWorkerSecondId = uid;
                    cOrder.OrderDateSecond = order.OrderDate;
                // cOrder.Items.Where(a => a.SubAccountFromId == order.items.Single(a))
                order.items.ForEach(a =>
                {
                    cOrder.Items.Where(b => b.SubAccountFromId == a.itemId).Single().noteSecond = a.noteSecond;

                });
                db.Entry(cOrder).State = EntityState.Modified;

                    #endregion

                    #region AccountOrder

                    Int32 refId = Convert.ToInt32((cOrder.MainAccount.Code + cOrder.OrderNo));
                    var acctransferOrder = db.AccountOrders.SingleOrDefault(a => a.OrderIdRefrence == refId
                                                                    && a.OrderTypeRefrence == "تحويل"
                                                                    && a.IsCompany == cOrder.IsCompany
                                                                    && a.CompanyId == (cOrder.IsCompany ? cOrder.CompanyId : (int?)null)
                                                                    && a.BranchId == (cOrder.IsCompany ? (int?)null : cOrder.BranchId));
                    //var invtransit = accCancelOrder.AccountMovements.SelectMany(a => a.Debit > 0 && a.Crdit == 0 && a.AccountId != finordertrans.transferTo.inventoryId && a.Accounts.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit");

                    var subAccountMoveListTo = new List<SubAccountMovement>();
                    var subAccountMoveListTranst = new List<SubAccountMovement>();
                    #region subaccountMove
                    var trnsitId = cOrder.Items.FirstOrDefault().SubAccountTransit.AccountId;
                    //var getinfoTo = this.AppCompanyUserManager.getTransferInfoNotinventory(new getInventoryTransfer { inventoryId = trnsitId, CompanyId = company.oId, inventoryName = order.transferFrom.inventoryName, isComapny = order.IsCompany }, company, true);
                    var getinfoTo = inventoryTransferManager.getitemAvailableByNotinventoryId(trnsitId, company, true);
                   var getinfo = inventoryTransferManager.getTransferInfo(cOrder.TitleAccountId, company, true);

                foreach (var item in cOrder.Items)
                    {
                      
                        var selectedItem = getinfo.items.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode && n.itemId == item.SubAccountToId);
                        var subAccountsTo = new SubAccountMovement()
                        {
                            SubAccountId = item.SubAccountToId,
                            CurrentWorkerId = uid,
                            SubAccountMovementDate = order.OrderDate,
                            OrderNote = "تحويل وارد",
                            QuantityIn = item.qty,
                            QuantityOut = 0,
                            ItemPricein = item.costPrice,
                            ItemTotalPriceIn = item.costPrice * item.qty,
                            ItemStockQuantity = selectedItem.Qty + item.qty,
                            ItemTotalStockPrice = selectedItem.totalStockprice + (item.costPrice * item.qty),
                            ItemStockPrice = (selectedItem.totalStockprice + (item.costPrice * item.qty)) / (selectedItem.Qty + item.qty),
                            OrderId = order.OrderNo
                        };


                        var selectedItemTo = getinfoTo.SingleOrDefault(n => n.itemName == item.itemName && n.itemCode == item.itemCode  && n.itemId == item.SubAccountTransitId);

                        var subAccountTranst = new SubAccountMovement()
                        {
                            SubAccountId = item.SubAccountTransitId,
                            CurrentWorkerId = uid,
                            SubAccountMovementDate = order.OrderDate,
                            OrderNote = "تحويل وارد",
                            QuantityIn = 0,
                            QuantityOut = item.qty,
                            ItemPriceOut = selectedItem.costPrice,
                            ItemTotalPriceOut = selectedItem.costPrice * item.qty,
                            ItemStockQuantity = selectedItemTo.Qty - item.qty,
                            ItemTotalStockPrice = selectedItemTo.totalStockprice - (selectedItem.costPrice * item.qty),
                            ItemStockPrice = (selectedItemTo.totalStockprice - (selectedItem.costPrice * item.qty)) / (selectedItem.Qty - item.qty),
                            OrderId = order.OrderNo
                        };
                        subAccountMoveListTranst.Add(subAccountTranst);
                        subAccountMoveListTranst.Add(subAccountsTo);
                    }

                    #region subAccountOrder
                    var suborderfrom = new SubAccountOrder()
                    {
                        OrderDate = order.OrderDate,
                        CurrentWorkerId = uid,
                        OrderNote = "تحويل وارد",
                        CustomSubAccountsOrderId = order.OrderNo,
                        isDone = true,
                        SubAccountMovements = subAccountMoveListTranst
                    };

                    db.SubAccountOrders.Add(suborderfrom);
                #endregion
                #endregion
                   var amount = acctransferOrder.AccountMovements.Sum(a => a.Debit);
                    var accountMoveListFrom = new List<AccountMovement>();
                    var acOut = new AccountMovement()
                    {
                        AccountId = cOrder.TitleAccountId,
                        Crdit = amount,
                        Debit =  0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = order.OrderDate,
                        AccountMovementNote = "تحويل وارد",
                    };
                    accountMoveListFrom.Add(acOut);
                    var acin = new AccountMovement()
                    {
                        AccountId = trnsitId,
                        Crdit = 0,
                        Debit = amount,
                        CurrentWorkerId = uid,
                        AccountMovementDate = order.OrderDate,
                        AccountMovementNote = "تحويل وارد",
                    };
                accountMoveListFrom.Add(acin);


                var accorderNo = 1;
                    if (company.isCompany)
                    {
                        accorderNo = db.AccountOrders.Where(a => a.CompanyId == company.companyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == company.companyId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    else
                    {
                        accorderNo = db.AccountOrders.Where(a => a.BranchId == company.oId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == company.oId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    var vamount = subAccountMoveListTranst.Select(a => a.ItemTotalPriceOut).Sum();
                    var accnote = string.Format(" تحويل وارد رقم {0} - من {1} - {2} - بقيمة {3} - رقم قيد التحويل السابق {4}", cOrder.OrderNoIn, cOrder.MainAccount.AccountName, cOrder.MainAccount.Code, vamount, cOrder.OrderNo);

                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = order.OrderDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "تحويل وارد",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,

                    };
                    if (newAccountOrder.AccountMovements == null)
                    {
                        newAccountOrder.AccountMovements = new List<AccountMovement>();
                    }
                    newAccountOrder.AccountMovements = accountMoveListFrom;
                    db.AccountOrders.Add(newAccountOrder);
                    #endregion

                    db.SaveChanges();
                    var returned = inventoryTransferManager.getTransferinbyId(new InventoryTransferById() { inventoryId = cOrder.TitleAccount.AccountId, CompanyId = company.oId, isComapny = company.isCompany, OrderNo = order.OrderNo }, company);
                    return Ok(returned);

                }
                #endregion

            


            return BadRequest("لا يوجد عمليات متاحة لطلبك");
        }


        [Route("waitingOrderToin")]
        [HttpPost]
        public IHttpActionResult waitOrderToin()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var returned = db.InventoryTransferEntity.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.DoneFirst && !a.Cancel && !a.DoneSceond)
                .Select(o => new
                {
                    orderNo = o.OrderNo,
                    inventoryfrom = o.MainAccount.AccountName + " - " + o.MainAccount.Code,
                    inventoryId = o.MainAccountId,
                    inventoryTo = o.TitleAccount.AccountName + " - " + o.TitleAccount.Code,
                    inventoryToId = o.TitleAccountId,
                    date = o.OrderDate,
                    user = o.CurrentWorker != null ? o.CurrentWorker.FirstName +" " + o.CurrentWorker.LastName : "",
                     doneSecond = o.DoneSceond
                }).ToList();
            if (returned == null)
            {
                return BadRequest("التحويلات غير متاحة الان");
            }
            return Ok(returned);

        }
        [Route("getwaitinginCount")]
        [HttpPost]
        public IHttpActionResult getwaitingCount()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var returned = db.InventoryTransferEntity.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.DoneFirst && !a.Cancel && !a.DoneSceond).Count();

            return Ok(returned);

        }
    }
}
