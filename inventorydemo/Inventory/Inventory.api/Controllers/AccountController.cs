﻿using System;
using System.Linq;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Inventory.dl.Models;
using Inventory.bl.DTO;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : BaseApiController
    {
     


        [Route("users")]
        public IHttpActionResult GetUsers()
        {
            return Ok(this.AppUserManager.Users.ToList().Select(u => this.TheModelFactory.Create(u)));
        }

        [Route("user/{id:guid}", Name = "GetUserById")]
        public async Task<IHttpActionResult> GetUser(string Id)
        {
            var user = await this.AppUserManager.FindByIdAsync(Id);

            if (user != null)
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            return NotFound();

        }

        [Route("user/{username}")]
        public async Task<IHttpActionResult> GetUserByName(string username)
        {
            var user = await this.AppUserManager.FindByNameAsync(username);

            if (user != null)
            {
                return Ok(this.TheModelFactory.Create(user));
            }

            return NotFound();

        }

        public async Task<IHttpActionResult> CreateUser(CreateUserBindingModel createUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser()
            {
                UserName = createUserModel.username,
                Email = createUserModel.email,
            };

            IdentityResult addUserResult = await this.AppUserManager.CreateAsync(user, createUserModel.password);

            if (!addUserResult.Succeeded)
            {
                return GetErrorResult(addUserResult);
            }
            string code = await this.AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

            var msg = this.EmailTemplateManger.actionTemplate(new EmailActionTempModelDTO()
            {
                Tilte = "Please confirm your email address by clicking the link below.",
                bodyMsg = "We may need to send you critical information about our service and it is important that we have an accurate email address.",
                btnlinkValue = "Confirm Email Address",
                callbackUrl = new Uri(Url.Link("ConfirmEmail", new { userId = user.Id, code = code }))
            });


            await this.AppUserManager.SendEmailAsync(user.Id,
                                                    "Confirm your account", msg);

            Uri locationHeader = new Uri(Url.Link("GetUserById", new { id = user.Id }));

            return Created(locationHeader, TheModelFactory.Create(user));
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("ConfirmEmail", Name = "ConfirmEmail")]
        public async Task<IHttpActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return BadRequest();
            }

            try
            {
                var result = await this.AppUserManager.ConfirmEmailAsync(userId, code);

                if (!result.Succeeded)
                {
                    return GetErrorResult(result);
                }
                string url = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                //Need Implmention in front End ,(page to show succesfully Confirmed) //@Reham Please Take Note
                string Pageurl = "/ServiceTracker/#/login";
                url += Pageurl;

                Uri uri = new Uri(url);

                return Redirect(uri);


            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ReGenerateEmailConfirmation")]
        public async Task<IHttpActionResult> ReGenerateEmailConfirmation(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this.AppUserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    return BadRequest("User Not Found");
                }
                string code = await this.AppUserManager.GenerateEmailConfirmationTokenAsync(user.Id);

                var msg = this.EmailTemplateManger.actionTemplate(new EmailActionTempModelDTO()
                {
                    Tilte = "Please confirm your email address by clicking the link below.",
                    bodyMsg = "We may need to send you critical information about our service and it is important that we have an accurate email address.",
                    btnlinkValue = "Confirm Email Address",
                    callbackUrl = new Uri(Url.Link("ConfirmEmail", new { userId = user.Id, code = code }))
                });


                await this.AppUserManager.SendEmailAsync(user.Id,
                                                        "Confirm your account", msg);

                return Ok("Please Check your Email");
            }


            return BadRequest(ModelState);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await this.AppUserManager.FindByEmailAsync(model.Email);
                // If user has to activate his email to confirm his account, the use code listing below
                if (user == null)
                {
                    return BadRequest("User Not Found");
                }
                if (!(await this.AppUserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    ModelState.AddModelError("Confirm Email", "Please Confirm your email");
                    return BadRequest(ModelState);
                }



                string code = await this.AppUserManager.GeneratePasswordResetTokenAsync(user.Id);

                string url = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                //Need Implmention in front End ,(to route user to rest password) //@Reham Please Take Note
                string Pageurl = "/ServiceTracker/#/forgetPassword";
                url += Pageurl;
                url += string.Format("?code={0}", code);
                Uri uri = new Uri(url);

                var msg = this.EmailTemplateManger.actionTemplate(new EmailActionTempModelDTO()
                {
                    Tilte = "To initiate the password reset process for your Account, click the link below:",
                    bodyMsg = "If you've received this mail in error, it's likely that another user entered your email address by mistake while trying to reset a password. If you didn't initiate the request, you don't need to take any further action and can safely disregard this email..",
                    btnlinkValue = "Reset Password",
                    callbackUrl = uri
                });


                //var callbackUrl = new Uri(Url.Link("ResetPassword", new { userId = user.Id, code = code }));
                await this.AppUserManager.SendEmailAsync(user.Id,
                                                        "Reset Password",
                                                        msg);
                return Ok("Please Check your Email");
            }


            return BadRequest(ModelState);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("ResetPassword")]
        public async Task<IHttpActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = await this.AppUserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return BadRequest("User Not Found");
            }
            var result = await this.AppUserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            //string url = Request.RequestUri.GetLeftPart(UriPartial.Authority);
            //string Pageurl = "/emailConfirmed.html";
            //url +=  Pageurl;

            //Uri uri = new Uri(url);
            var uInfo = new { userEmail = user.Email, userName = user.UserName };
            return Ok(uInfo);

        }

        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await this.AppUserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

    }
}
