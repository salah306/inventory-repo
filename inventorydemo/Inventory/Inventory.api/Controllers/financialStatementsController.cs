﻿using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.api.Controllers
{
    [RoutePrefix("api/financialStatementsconfig")]
    public class financialStatementsController : BaseApiController
    {
      

        [Route("GetAllFinancialList")]
        [HttpPost]
        // [ResponseType(typeof(BranchTree))]
        public IHttpActionResult GetAllFinancialList()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
           

            List<GetFinancialList> ftree = new List<GetFinancialList>();

            var cac = (from c in db.CustomAccounts where c.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == company.companyId select new FListes { Id = c.CustomAccountId + "Ac", Show = false, Name = c.Account.AccountName, Level = 5, ParentId = c.CustomAccountCategoryId + "Acc", LevelName = "فرعي", ParentName = c.CustomAccountCategory.AccountCategoryName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var cacc = (from c in db.CustomAccountCategories select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == company.companyId).Select(c => new FListes { Id = c.CustomAccountCategoryId + "Acc", Show = false, childern = c.CustomAccount.Count(), Name = c.AccountCategoryName, Level = 4, AddTitle = "اضافة حسابات فرعية", Addlink = "ربط مع حساب مساعد", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "مساعد", ParentId = c.CustomBalanceSheetTypeId + "Bst", ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bst = ((from c in db.CustomBalanceSheetTypes select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheet.FinancialList.CompanyId == company.companyId).Select(c => new FListes { Id = c.CustomBalanceSheetTypeId + "Bst", Show = false, childern = c.CustomAccountCategories.Count(), Name = c.BalanceSheetTypeName, Level = 3, AddTitle = "اضافة حساب مساعد", Addlink = "ربط مع حساب عام", LevelName = "عام", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, ParentId = c.CustomBalanceSheetId + "Bs", ParentName = c.CustomBalanceSheet.BalanceSheetName })).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bs = (from c in db.CustomBalanceSheets select c).OrderBy(r => r.CreatedDate).Where(c => c.FinancialList.CompanyId == company.companyId).Select(c => new FListes { Id = c.CustomBalanceSheetId + "Bs", Name = c.BalanceSheetName, Show = true, accountType = c.AccountType.AccountTypeName, Level = 2, childern = c.CustomBalanceSheetTypes.Count(), AddTitle = "اضافة حساب عام", Addlink = "ربط مع حساب رئيسي", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "رئيسي", ParentId = c.FinancialListId + "F", ParentName = c.FinancialList.FinancialListName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var fl = (from c in db.FinancialLists where c.CompanyId == company.companyId orderby c.CreatedDate select new FListes { Id = c.FinancialListId + "F", forLevel = c.FinancialListId + "F", Name = c.FinancialListName, Level = 1, Show = true, ParentId = c.BranchId.ToString(), childern = c.CustomBalanceSheets.Count(), ParentName = c.Branch.BranchName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();

            List<FListes> listes = new List<FListes>();

            foreach (var mainList in fl)
            {
                foreach (var f in mainList)
                {
                    listes.Add(f);
                    foreach (var balancesheet in bs)
                    {
                        foreach (var customBS in balancesheet)
                        {
                            if (customBS.ParentId == f.Id)
                            {
                                customBS.forLevel = f.Id;
                                listes.Insert(listes.IndexOf(f), customBS);
                                foreach (var Bstype in bst)
                                {
                                    foreach (var CustomBst in Bstype)
                                    {
                                        if (CustomBst.ParentId == customBS.Id)
                                        {
                                            CustomBst.forLevel = f.Id;
                                            listes.Insert(listes.IndexOf(customBS), CustomBst);
                                            foreach (var CustomAccountc in cacc)
                                            {
                                                foreach (var CustomAcc in CustomAccountc)
                                                {
                                                    if (CustomAcc.ParentId == CustomBst.Id)
                                                    {
                                                        CustomAcc.forLevel = f.Id;
                                                        listes.Insert(listes.IndexOf(CustomBst), CustomAcc);
                                                        foreach (var Customacco in cac)
                                                        {
                                                            foreach (var CustomAc in Customacco)
                                                            {
                                                                if (CustomAc.ParentId == CustomAcc.Id)
                                                                {
                                                                    CustomAc.forLevel = f.Id;
                                                                    listes.Insert(listes.IndexOf(CustomAcc), CustomAc);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }



                        }
                    }
                }
            }
            var g = listes.GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
            List<Alllists> listes5 = new List<Alllists>();

            foreach (var l in g)
            {

                listes5.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, forWidget = false, Listes = l });
            }

            foreach (var l in listes5)
            {
                l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
            }

            return Ok(listes5);
        }
    }
}
