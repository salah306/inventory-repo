﻿using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace Inventory.api.Controllers
{
    [Authorize]
    [RoutePrefix("api/AppStorge")]

    public class AppStorgeController : BaseApiController
    {
        [Route("branchGroupSt")]
        [HttpPost]
        public IHttpActionResult saveStorge(branchGroupStVm st)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            new System.Threading.Tasks.Task(() =>
            {
                var checkmulti = db.branchGroupSt.Where(a => a.CurrentWorkerId == uid);
                if(checkmulti.Count() > 1)
                {
                    db.branchGroupSt.RemoveRange(checkmulti);
                    db.SaveChanges();
                }

                var check = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

                if (check == null)
                {


                    branchGroupSt br = new branchGroupSt()
                    {
                        id = st.id,
                        companyId = st.companyId,
                        companyName = st.companyName,
                        CurrentWorkerId = uid,
                        fullName = st.fullName,
                        name = st.name,
                        isCompany = st.isCompany,
                        realName = st.realName,
                        oId = st.oId
                    };
                    db.branchGroupSt.Add(br);
                    db.SaveChanges();

                  
                }
                else
                {
                    check.id = st.id;
                    check.companyId = st.companyId;
                    check.companyName = st.companyName;
                    check.fullName = st.fullName;
                    check.name = st.name;
                    check.isCompany = st.isCompany;
                    check.realName = st.realName;
                    check.oId = st.oId;
                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }).Start();
            return Ok();
        }

        [Route("billMainAccountSales")]
        [HttpPost]
        public IHttpActionResult billMainAccountSales(billMainAccountSalesStVm st)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            new System.Threading.Tasks.Task(() =>
            {

                var check = db.billMainAccountSalesSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
                if (check == null)
                {


                    billMainAccountSalesSt br = new billMainAccountSalesSt()
                    {
                        accountId = st.accountId,
                        accountName = st.accountName,
                        CurrentWorkerId = uid,
                        value = st.value
                        
                    };
                    db.billMainAccountSalesSt.Add(br);
                    db.SaveChanges();


                }
                else
                {
                    check.accountId = st.accountId;
                    check.accountName = st.accountName;
                    check.value = st.value;
                   
                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }).Start();
            return Ok();
        }
        [Route("billMainAccountPurSt")]
        [HttpPost]
        public IHttpActionResult billMainAccountPurSt(billMainAccountPurStVm st)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            new System.Threading.Tasks.Task(() =>
            {

                var check = db.billMainAccountPurSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
                if (check == null)
                {


                    billMainAccountPurSt br = new billMainAccountPurSt()
                    {
                        accountId = st.accountId,
                        accountName = st.accountName,
                        CurrentWorkerId = uid,
                        value = st.value

                    };
                    db.billMainAccountPurSt.Add(br);
                    db.SaveChanges();


                }
                else
                {
                    check.accountId = st.accountId;
                    check.accountName = st.accountName;
                    check.value = st.value;

                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }).Start();
            return Ok();
        }
        [Route("selectedOrderGroupNameSt")]
        [HttpPost]
        public IHttpActionResult selectedOrderGroupNameSt(selectedOrderGroupNameStVm st)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            new System.Threading.Tasks.Task(() =>
            {

                var check = db.selectedOrderGroupNameSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
                if (check == null)
                {


                    selectedOrderGroupNameSt br = new selectedOrderGroupNameSt()
                    {
                        orderGroupId = st.orderGroupId,
                        CurrentWorkerId = uid,
                        orderGroupName = st.orderGroupName

                    };
                    db.selectedOrderGroupNameSt.Add(br);
                    db.SaveChanges();


                }
                else
                {
                    check.orderGroupId = st.orderGroupId;
                    check.orderGroupName = st.orderGroupName;

                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }).Start();
            return Ok();
        }
        [Route("inventorySt")]
        [HttpPost]
        public IHttpActionResult inventoryStVm(inventoryStVm st)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            new System.Threading.Tasks.Task(() =>
            {

                var check = db.inventorySt.SingleOrDefault(a => a.CurrentWorkerId == uid);
                if (check == null)
                {


                    inventorySt br = new inventorySt()
                    {
                        accountId = st.accountId,
                        CurrentWorkerId = uid,
                        accountName = st.accountName

                    };
                    db.inventorySt.Add(br);
                    db.SaveChanges();


                }
                else
                {
                    check.accountId = st.accountId;
                    check.accountName = st.accountName;

                    db.Entry(check).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }).Start();
            return Ok();
        }

      
    }
}
