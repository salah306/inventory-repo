﻿using Inventory.bl.DTO;
using Inventory.bl.Managers;
using Inventory.dl;
using Inventory.dl.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;


namespace Inventory.api.Controllers
{
    public class BalanceSheetTypesController : BaseApiController
    {
        private BalanceSheetsManager BalanceSheetsManager;
        public BalanceSheetTypesController()
        {
            this.BalanceSheetsManager = new BalanceSheetsManager(this.context);
        }
     
        // GET: api/BalanceSheetTypes
        public IQueryable<BalanceSheetTypesVm> GetBalanceSheetTypes()
        {
            return BalanceSheetsManager.GetAllBalanceSheetTypes();
        }

        // GET: api/BalanceSheetTypes/5
        [ResponseType(typeof(BalanceSheetType))]
        public async Task<IHttpActionResult> GetBalanceSheetType(int id)
        {
            BalanceSheetType balanceSheetType = await db.BalanceSheetTypes.FindAsync(id);
            if (balanceSheetType == null)
            {
                return NotFound();
            }

            return Ok(balanceSheetType);
        }

        // PUT: api/BalanceSheetTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBalanceSheetType(int id, BalanceSheetType balanceSheetType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != balanceSheetType.BalanceSheetTypeId)
            {
                return BadRequest();
            }

            db.Entry(balanceSheetType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BalanceSheetTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BalanceSheetTypes
        [ResponseType(typeof(BalanceSheetType))]
        public async Task<IHttpActionResult> PostBalanceSheetType(BalanceSheetType balanceSheetType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BalanceSheetTypes.Add(balanceSheetType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = balanceSheetType.BalanceSheetTypeId }, balanceSheetType);
        }

        // DELETE: api/BalanceSheetTypes/5
        [ResponseType(typeof(BalanceSheetType))]
        public async Task<IHttpActionResult> DeleteBalanceSheetType(int id)
        {
            BalanceSheetType balanceSheetType = await db.BalanceSheetTypes.FindAsync(id);
            if (balanceSheetType == null)
            {
                return NotFound();
            }

            db.BalanceSheetTypes.Remove(balanceSheetType);
            await db.SaveChangesAsync();

            return Ok(balanceSheetType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BalanceSheetTypeExists(int id)
        {
            return db.BalanceSheetTypes.Count(e => e.BalanceSheetTypeId == id) > 0;
        }
    }
}