﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.DTO
{
    public class BalanceSheetTypesVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public string BalanceSheetsId { get; set; }
        public Int32 PublicId { get; set; }
        public virtual List<AccountCategoriesVm> AccountCategories { get; set; }
        public virtual List<UserRole> UserRoles { get; set; }



    }
}