﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.DTO
{
    public class GetCompanyeasy
    {
        public Int32 companyId { get; set; }
        public string companyName { get; set; }
        public string forUser { get; set; }
    }

    public class setUsereasy
    {

        public string Search { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
      
    }
    public class getCompanyByNameEasy
    {
        public string name { get; set; }
        public string uid { get; set; }
    }

    public class getuserRole
    {
        public Int32 CompanyId { get; set; }
        public string UserId { get; set; }
        public Int32 PropertyId { get; set; }

    }

    public class setUserRole
    {
        public getuserRole settingInfo { get; set; }
        public List<roleseasy> roleToAdd { get; set; }
    }

    public class getUserseasy
    {

        public string userId { get; set; }
        public string userName { get; set; }
        public string userEmail { get; set; }
        public bool lockd { get; set; }
        public string phone { get; set; }
    }

    public class getUsersMainInfo
    {

        public string userId { get; set; }
        public string userName { get; set; }
        public string userEmail { get; set; }
        public bool lockd { get; set; }
        public string phone { get; set; }
    }

    public class UserSettingseasy
    {
        public Int32 settingsId { get; set; }
        public Int32 companyId { get; set; }
        public string companyName { get; set; }
        public bool locked { get; set; }
        public List<propertieseasy> properties { get; set; }
    }

    public class propertieseasy
    {
        public Int32 propertiesId { get; set; }
        public string propertyName { get; set; }
        public List<roleseasy> roles { get; set; }
    }

    public class roleseasy
    {
        public Int32 rolesId { get; set; }
        public string roleName { get; set; }
        public bool active { get; set; }
        public string typeRefrence { get; set; }
        public Int32? typeRefrenceId { get; set; }
        public List<rolepropertieseasy> roleproperties { get; set; }

    }

    public class rolepropertieseasy
    {
        public Int32 rolepropertiesId { get; set; }
        public string rolepropertyName { get; set; }
        public bool isTrue { get; set; }
        public Int32 roleId { get; set; }

    }
}