﻿using Inventory.bl.Validators;
using Inventory.dl;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Inventory.bl.DTO
{
    #region orderOVm
    public class TransferToOVm
    {
        public int inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }
    public class SubjectAccountOVm
    {
        public int SubjectAccountId { get; set; }
        public string SubjectAccountName { get; set; }
        public string code { get; set; }
    }
    public class AccountOVm
    {
        public string name { get; set; }
        public int id { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
    }

    public class ItemsRequestOVm
    {
        [Required(ErrorMessage = "يجب تحديد الصنف")]
        public int itemId { get; set; }
        [Required(ErrorMessage = "اسم الصنف مطلوب لاتمام عملية الحفظ")]
        [Display(Name = "اسم الصنف")]
        public string itemName { get; set; }
        [Required(ErrorMessage = "كود الصنف مطلوب لاتمام عملية الحفظ")]
        [Display(Name = "الكود")]
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string realted { get; set; }
        public List<AccountOVm> accounts { get; set; }
        public string itemUnitType { get; set; }
        public string itemUnitName { get; set; }
        public decimal? maxQ { get; set; }
    }

    public class PayOVm
    {
        public int payId { get; set; }
        [Required(ErrorMessage = "يجب تحديد طريقة الدفع")]
        [Display(Name = "طريقة الدفع")]
        public string payName { get; set; }
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public string code { get; set; }
        public decimal amount { get; set; }
        [Required(ErrorMessage = "يجب تحديد تاريخ الاستحقاق ")]
        [Display(Name = "التاريخ")]
        public string dueDate { get; set; }
        public string note { get; set; }
        public string type { get; set; }
        public Int32? defaultChildAccountId { get; set; }
        public List<payvmAccounts> accounts { get; set; }
    }
    public class payvmAccounts
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
    }
    public class TotalAccountOVm
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
    }

    public class TermOVm
    {
        public int termId { get; set; }

        [Display(Name = "الشروط والاحكام")]
        public string termName { get; set; }
    }

    public class OrderOVm : IValidatableObject
    {
        public Int32 OrderId { get; set; }
        public Int32 orderrequestId { get; set; }
        public bool requestOrderAttched { get; set; }
        public Int32 orderrequestNo { get; set; }
        public int orderNo { get; set; }
        public string orderName { get; set; }
        public Int32 orderConfigId { get; set; }
        public string orderType { get; set; }
        public string oType { get; set; }

        public string orderDate { get; set; }
        public string orderDateSecond { get; set; }
        public string orderDateThird { get; set; }
        public string recivedDate { get; set; }
        public string userName { get; set; }
        public string userNameSecond { get; set; }
        public string userNameThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        public TransferToOVm transferTo { get; set; }
        public SubjectAccountOVm SubjectAccount { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال صنف واحد علي الاقل")]
        [Display(Name = "الاصناف")]
        public List<ItemsRequestOVm> itemsRequest { get; set; }

        [Display(Name = "طرق الدفع")]
        public List<PayOVm> pay { get; set; }
        public List<TotalAccountOVm> totalAccount { get; set; }
        public List<TermOVm> terms { get; set; }
        public int companyId { get; set; }
        public bool isCompany { get; set; }
        public string cancelDate { get; set; }
        public string userNameCancel { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            #region checkTotal
            var grandTotal = 0.0m;
            if (SubjectAccount == null || SubjectAccount.SubjectAccountId == 0)
            {
                string typ = oType == "pur" ? "المورد" : "العميل";
                string ms = string.Format("يجب تحديد {0}", typ);
                yield return new ValidationResult(ms, new[] { "الحساب الرئيسي" });

            }

            if (transferTo == null || transferTo.inventoryId == 0)
            {
                string typ = "المخزن";
                string ms = string.Format("يجب تحديد {0}", typ);
                yield return new ValidationResult(ms, new[] { "المخزن" });

            }
            if (Convert.ToDateTime(orderDate) < Convert.ToDateTime(recivedDate) && isNewOrder)
            {
                yield return new ValidationResult("يجب ان يكون تاريخ الاستلام اكبر من او يساوي تاريخ الاصدار.", new[] { "التواريخ" });

            }
            foreach (var item in itemsRequest)
            {
                if (item.qty <= 0)
                {
                    yield return new ValidationResult("يجب ان تكون الكمية اكبر من صفر.", new[] { item.itemName + " : " + item.qty });
                }
                if (item.price < 0)
                {
                    yield return new ValidationResult("سعر الصنف لا يجب ان يقل عن صفر.", new[] { item.itemName + " : " + item.price });
                }
                var itemTotal = item.price * item.qty;
                var totalamounts = 0.0m;
                if (item.accounts != null && item.accounts.Count > 0)
                {
                    foreach (var acc in item.accounts)
                    {
                        if (acc.amount < 0)
                        {
                            yield return new ValidationResult("لا يسمح باستخدام قيمة سالبة.", new[] { item.itemName + " , " + acc.name });
                        }
                        if (acc.amountType == "%")
                        {
                            var transFerToamount = 0.0m;
                            if (acc.type == "crdit")
                            {
                                transFerToamount = (itemTotal + totalamounts) / 100 * acc.amount;
                                totalamounts -= transFerToamount;
                            }
                            if (acc.type == "debit")
                            {
                                transFerToamount = (itemTotal + totalamounts) / 100 * acc.amount;
                                totalamounts += transFerToamount;
                            }
                        }
                        else
                        {
                            if (acc.type == "crdit")
                            {


                                totalamounts -= acc.amount;
                            }
                            if (acc.type == "debit")
                            {
                                totalamounts += acc.amount;
                            }
                        }
                    }
                }

                grandTotal += (itemTotal + totalamounts);
            }
            var totaltable = 0.0m;
            foreach (var acc in totalAccount)
            {

                if (acc.amount < 0)
                {
                    yield return new ValidationResult("لا يسمح باستخدام قيم سالبة.", new[] { acc.name + " : " + acc.amount });
                }
                if (acc.amountType == "%")
                {
                    var transFerToamount = 0.0m;
                    if (acc.type == "crdit")
                    {
                        transFerToamount = (grandTotal + acc.amount) / 100 * acc.amount;
                        totaltable -= transFerToamount;
                    }
                    if (acc.type == "debit")
                    {
                        transFerToamount = (grandTotal + totaltable) / 100 * acc.amount;
                        totaltable += transFerToamount;
                    }
                }
                else
                {
                    if (acc.type == "crdit")
                    {


                        totaltable -= acc.amount;
                    }
                    if (acc.type == "debit")
                    {
                        totaltable += acc.amount;
                    }
                }

            }
            grandTotal += totaltable;
            if (pay.Count() > 0)
            {
                foreach (var p in pay)
                {
                    if (Convert.ToDateTime(orderDate) < Convert.ToDateTime(p.dueDate) && isNewOrder)
                    {
                        yield return new ValidationResult("يجب ان يكون التاريخ في طرق الدفع اكبر من او يساوي تاريخ الاصدار.", new[] { "التواريخ" });

                    }
                    if (p.accountId == 0)
                    {
                        yield return new ValidationResult("يرجي اختيار طريقة دفع من الطرق المتاحة.", new[] { "طرق الدفع" });

                    }
                    if (string.IsNullOrEmpty(p.dueDate))
                    {
                        yield return new ValidationResult("يجب ادخال تاريخ الاستحقاق المفترض لانشاء طلب للحساب المستهدف.", new[] { "طرق الدفع" });

                    }
                    if (p.amount <= 0)
                    {
                        yield return new ValidationResult("لا يسمح باستخدام قيمة اقل من 1.", new[] { "طرق الدفع" });

                    }
                    if (p.accountId == SubjectAccount.SubjectAccountId)
                    {
                        yield return new ValidationResult("حسابات طرق الدفع يجب ان تكون مختلفه عن الحساب الرئيسي للامر.", new[] { "طرق الدفع" });
                    }
                }
            }


            #endregion
            if (orderType == "purReturn" || orderType == "sales")
            {
                ApplicationDbContext db = new ApplicationDbContext();
                foreach (var item in itemsRequest)
                {
                    var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.itemId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.itemId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                    if (lastStocktotalQty - item.qty < 0)
                    {
                        yield return new ValidationResult(string.Format("كمية غير متاحة للصرف من الصنف {0}", item.itemName), new[] { "صنف غير متاح" });
                    }
                }

            }
        }
    }

    public class ordertypeVM
    {
        public string orderType { get; set; }
        public string type { get; set; }
    }
    #endregion
}
