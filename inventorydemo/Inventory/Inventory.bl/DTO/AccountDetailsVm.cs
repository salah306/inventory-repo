﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.DTO
{
    public class AccountDetailsVm
    {
        public string id { get; set; }
        public string name { get; set; }
        public IQueryable<AccountOrderPropertiesVm> accountOrderProperties { get; set; }
        public IQueryable<AccountPropertyVM> accountpropertyValues { get; set; }
        public IQueryable<AccountMovementVm> accountMovement { get; set; }

        public Int32 publicId { get; set; }
    }
}