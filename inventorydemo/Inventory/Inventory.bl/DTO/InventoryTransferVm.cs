﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.DTO
{
    public class InventoryTransferVm
    {
        public bool isComapny { get; set; }
        public Int32 CompanyId { get; set; }
    }
    public class getInventoryTransfer
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public bool isComapny { get; set; }
        public Int32 CompanyId { get; set; }
    }
    public class InventoryTransfer
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }

    }

    public class InventoryTransferById
    {
        public Int32 inventoryId { get; set; }
        public Int32 OrderNo { get; set; }
        public bool isComapny { get; set; }
        public Int32 CompanyId { get; set; }

    }

    public class InventoryToQty
    {
        public IEnumerable<InventoryTransfer> InventoryTransferTo { get; set; }
        public IEnumerable<itemAvailable> items { get; set; }
       
    }

    public class itemAvailable
    {
        public Int32 itemId { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public decimal Qty { get; set; }
        public string GroupName { get; set; }
        public decimal costPrice { get; set; }
        public decimal totalStockprice { get; set; }
        public decimal total { get; set; }
        public decimal price { get; set; }

    }
}