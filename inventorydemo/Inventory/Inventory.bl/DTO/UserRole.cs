﻿
using System.Collections.Generic;

namespace Inventory.bl.DTO
{
    public class UserRole
    {
      
        public string Email { get; set; }
        public List<Rolelevels> Rolelevels { get; set; }
        public bool IsAuthorized(string name)
        {

            return this.Rolelevels.Find(a => a.Name == name) == null ? false : this.Rolelevels.Find(a => a.Name == name).IsAuthorized; ;
        }

    }


    public class Rolelevels
    {
       
        public string RoleName { get; set; }
        public bool IsAuthorized { get; set; }
        public string Name { get; set; }

    }

   


}