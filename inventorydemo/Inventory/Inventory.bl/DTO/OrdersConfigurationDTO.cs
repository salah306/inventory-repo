﻿using Inventory.bl.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.DTO
{
    #region PurchaseOrdersConfigurationVm
    public class PurchaseOrdersConfigurationVm
    {
        public Int32 PurId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم النوع للاستمرار")]
        [Index(IsUnique = true)]
        public string PurOrderName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر الشراء")]
        public string PurName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر رد المشتريات")]
        public string PurReturnName { get; set; }
        public bool PurOrderIsNew { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]

        public List<PurchasePayMethodVm> payMethod { get; set; }
        public Int32 defaultPayName { get; set; }
        public List<PurchaseTermVm> terms { get; set; }
        public List<PurchaseTableAccountVm> tableAccounts { get; set; }
        public List<PurchaseTotalAccountVm> totalAccounts { get; set; }
    }
    public class PurchasePayMethodVm
    {
        public Int32 payMethodId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم طريقة الدفع")]
        public string payName { get; set; }
        [Required]
        public string accountName { get; set; }
        [Index(IsUnique = true)]
        public int accountId { get; set; }
        public bool attchedRequest { get; set; }
        public Int32? defaultChildAccountId { get; set; }
    }

    public class PurchaseTermVm
    {
        public Int32 termId { get; set; }
        [Required(ErrorMessage = "يجب ادخال الشروط والاحكام")]
        public string name { get; set; }
    }

    public class PurchaseTableAccountVm
    {
        public Int32 tableAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public Int32 accountId { get; set; }
        [Required]
        public string accountType { get; set; }
    }

    public class PurchaseTotalAccountVm
    {
        public Int32 totalAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public Int32 accountId { get; set; }
        public string accountType { get; set; }
    }
    #endregion
    #region SalesOrdersConfigurationVm
    public class SalesOrdersConfigurationVm
    {
        public Int32 salesId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم النوع للاستمرار")]
        [Index(IsUnique = true)]
        public string salesOrderName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر البيع")]
        public string salesName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر رد المبيعات")]
        public string salesReturnName { get; set; }
        public bool salesOrderIsNew { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]

        public List<SalesPayMethodVm> payMethod { get; set; }

        public Int32 defaultPayName { get; set; }
        public List<SalesTermVm> terms { get; set; }
        public List<SalesTableAccountVm> tableAccounts { get; set; }
        public List<SalesTotalAccountVm> totalAccounts { get; set; }
    }
    public class SalesPayMethodVm
    {
        public Int32 payMethodId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم طريقة الدفع")]
        public string payName { get; set; }
        [Required]
        public string accountName { get; set; }
        [Index(IsUnique = true)]
        public int accountId { get; set; }
        public bool attchedRequest { get; set; }
        public Int32? defaultChildAccountId { get; set; }
    }

    public class SalesTermVm
    {
        public Int32 termId { get; set; }
        [Required(ErrorMessage = "يجب ادخال الشروط والاحكام")]
        public string name { get; set; }
    }

    public class SalesTableAccountVm
    {
        public Int32 tableAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public Int32 accountId { get; set; }
        [Required]
        public string accountType { get; set; }
    }

    public class SalesTotalAccountVm
    {
        public Int32 totalAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public Int32 accountId { get; set; }
        public string accountType { get; set; }
    }
    #endregion

    public class inventoriesConfigurtionVm
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public Int32 salesCostAccountId { get; set; }
        public string salesCostAccountName { get; set; }
        public Int32 salesAccountId { get; set; }
        public string salesAccountName { get; set; }
        public Int32 salesReturnId { get; set; }
        public string salesReturnName { get; set; }

    }


    #region globl order config class
    public class SetOrdersConfigurations
    {
        public Int32 orderId { get; set; }
        public string type { get; set; }
    }
    public class SetOrdersConfigurationsO
    {
        public Int32 orderId { get; set; }
        public string type { get; set; }
        public string orderType { get; set; }
    }
    #endregion
}
