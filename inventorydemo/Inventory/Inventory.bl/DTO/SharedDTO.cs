﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.DTO
{
    #region pur&sales
    public class InventoryRequestOrderVm
    {
        public Int32 orderId { get; set; }
        public int orderNo { get; set; }
        public int ONo { get; set; }
        public Int32 OrderOId { get; set; }

        public DateTime orderDate { get; set; }
        public DateTime recivedDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public string userName { get; set; }
        public string userNameSecond { get; set; }
        public string userNameThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }

        public purOrderRequestTo TransferTo { get; set; }
        public IEnumerable<ItemsRequestVm> itemsRequest { get; set; }

        public Int32 CompanyId { get; set; }
        public DateTime cancelDate { get; set; }
        public string userNameCancel { get; set; }
        public string OrderType { get; set; }
    }

    public class PurchaseOrderVm
    {
        public Int32 PurchaseOrderId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime recivedDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public string userName { get; set; }

        public string userNameSecond { get; set; }

        public string userNameThird { get; set; }

        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }

        public purOrderRequestTo TransferTo { get; set; }
        public List<ItemsRequestVm> itemsRequest { get; set; }

        public Int32 CompanyId { get; set; }
        public DateTime cancelDate { get; set; }
        public string userNameCancel { get; set; }

    }



    public class ItemsRequestVm
    {
        public Int32 ItemsRequestId { get; set; }
        public Int32 itemId { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        public string itemUnitName { get; set; }
        public string itemUnitType { get; set; }
        public string itemGroupName { get; set; }
        public decimal maxQ { get; set; }

    }

    public class getPurRequest
    {
        public Int32 orderNo { get; set; }
    }

    public class getorderO
    {
        public Int32 orderNo { get; set; }
        public string orderType { get; set; }
        public string type { get; set; }
    }


    public class getInventoryRequestes
    {
        public string orderType { get; set; }
        public Int32 inventoryId { get; set; }
        public string type { get; set; }
        public Int32 OrderNo { get; set; }
        public Int32 orderId { get; set; }
    }
    public class getwaitinPurRequest
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public string orderby { get; set; }
        public Int32 orderNo { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool all { get; set; }
        public string OrderType { get; set; }
        public string OType { get; set; }

    }

    public class purOrderRequestTo
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }
    #endregion
    public class orderBillNames
    {
        public string billName { get; set; }
        public Int32 typeId { get; set; }
        public String typeName { get; set; }
    }

    public class PurchasesVm
    {
        public Int32 id { get; set; }
        public Int32 purId { get; set; }
        public string purName { get; set; }
        public ordersTypepurchaes ordersType { get; set; }
        public List<payMethod> payMethod { get; set; }
        public List<OrderAccountsforSubAccountVm> orderAccountsforSubAccounts { get; set; }
        public List<OrderOtherTotalAccountVm> orderOtherTotalAccounts { get; set; }
        public List<OrderPropertiesVm> orderProperties { get; set; }
    }

    public class SalesVm
    {
        public Int32 id { get; set; }
        public Int32 salesId { get; set; }
        public string salesName { get; set; }
        public ordersTypesale ordersType { get; set; }
        public List<payMethod> payMethod { get; set; }
        public List<OrderMainAccountVm> orderMainAccounts { get; set; }
        public List<OrderAccountsforSubAccountVm> orderAccountsforSubAccounts { get; set; }
        public List<OrderOtherTotalAccountVm> orderOtherTotalAccounts { get; set; }
    }
    public class OrderPropertiesBillVm
    {
        public Int32 OrderPropertiesId { get; set; }
        public string OrderPropertiesName { get; set; }
        public string OrderPropertiesValue { get; set; }
        public bool IsRequired { get; set; }
        public Int32 OrderPropertyTypeId { get; set; }
        public string OrderPropertyTypeName { get; set; }
    }
    public class orderInventoryVm
    {
        public Int32 itemId { get; set; }
        public string itemDesc { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public List<OrderAccountsforSubAccountBill> OrderAccountsforSubAccountBill { get; set; }

    }

    public class OrderAccountsforSubAccountBill
    {

        public Int32 OrderAccountsforSubAccountId { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public decimal value { get; set; }
        public bool DebitWhenAdd { get; set; }
        public string typeWhenAdd { get; set; }
    }

    public class reciveOrderToBill
    {
        public Int32 orderGroupId { get; set; }
        public Int32 InventoryId { get; set; }
        public string Type { get; set; }
        public Int32 CompanyId { get; set; }
        public bool isCompny { get; set; }
    }



    public class getBill
    {
        public Int32 billId { get; set; }
        public Int32 CompanyId { get; set; }
        public bool isCompny { get; set; }
        public string type { get; set; }
        public string billTypeName { get; set; }
        public Int32 orderGroupId { get; set; }
    }

    public class orderGroupTobill
    {
        public List<BillItems> BillItems { get; set; }
        public billMainAccount billMainAccounts { get; set; }
        public List<billOtherAccounts> billOtherAccounts { get; set; }
        public List<billitemsQty> billitemsQty { get; set; }
        public List<billPaymethods> billPaymethods { get; set; }
        public List<tilteAccounts> TitleAccounts { get; set; }
        public List<orderBillNames> billNames { get; set; }



    }

    public class SavebillVm
    {
        public Int32 billId { get; set; }
        public string type { get; set; }
        public orderBillNames billName { get; set; }
        public Int32 billNo { get; set; }
        public DateTime billDate { get; set; }
        public billMainAccount billMainAccount { get; set; }
        public tilteAccounts billTitleaccount { get; set; }
        public List<List<itemPropertyValuesbill>> billItems { get; set; }
        public List<billOtherAccounts> billOtherTotalAcounts { get; set; }
        public List<billPaymethods> billPaymethods { get; set; }
        public BillTotalAccount billTotalAccount { get; set; }
        public BillType billType { get; set; }
        public Int32 OrderGroupId { get; set; }
        public bool IsCompany { get; set; }
        public Int32 ComapnyId { get; set; }
        public string worker { get; set; }
    }

    public class BillType
    {
        public Int32 typeId { get; set; }
        public String typeName { get; set; }
    }

    public class BillTotalAccount
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public decimal value { get; set; }
        public billPaymethodsforSave billPaymethods { get; set; }

    }

    public class billPaymethodsforSave
    {
        public Int32 payId { get; set; }
        public string payName { get; set; }
        public List<billPaymethodProperties> billPaymethodProperties { get; set; }

    }

    public class tilteAccounts
    {
        public Int32 AccountId { get; set; }
        public bool showInfo { get; set; }
        public string AccountName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }

    public class billitemsQty
    {
        public Int32 itemPropertyValueId { get; set; }
        public string itemPropertyValueName { get; set; }
        public string Code { get; set; }
        public string price { get; set; }
        public decimal QtyInStock { get; set; }
        public string GroupName { get; set; }
        public decimal costPrice { get; set; }
        public string UnitName { get; set; }
        public string UntitType { get; set; }
    }

    public class billOtherAccounts
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public decimal value { get; set; }
        public string type { get; set; }
    }

    public class billMainAccount
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public string value { get; set; }
    }

    public class BillItems
    {
        public Int32 itemPropertyId { get; set; }
        public string itemPropertyName { get; set; }
        public string type { get; set; }
        public List<itemPropertyValuesbill> itemPropertyValues { get; set; }

    }

    public class itemPropertyValuesbill
    {
        //"itemPropertyValueId": 1,
        //                "itemPropertyValueName": "مروحة",
        //                "itemPropertyId": 1,
        //                "value": ""
        public Int32 itemPropertyValueId { get; set; }
        public string itemPropertyValueName { get; set; }
        public Int32 itemPropertyId { get; set; }
        public string value { get; set; }
        public string type { get; set; }
    }

    public class billPaymethods
    {
        public Int32 payId { get; set; }
        public string payName { get; set; }
        public List<billMainAccount> accounts { get; set; }
        public List<billPaymethodProperties> billPaymethodProperties { get; set; }

    }

    public class billPaymethodProperties
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public string value { get; set; }
        public string typeName { get; set; }
        public bool toPrint { get; set; }
        public bool isRequied { get; set; }

    }

    public class orderGroupNamesVm
    {
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
    }

    public class orderGroupNameVm
    {
        public Int32 Id { get; set; }
        public string name { get; set; }
    }
    public class SaveOrderGroupVm
    {
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public Int32 CompanyBranchId { get; set; }
        public bool IsCompany { get; set; }
    }
    public class orderSalePurGet
    {
        public Int32 id { get; set; }
        public string type { get; set; }
    }
    public class SaveOrderNameVm
    {
        public Int32 OrderGroupId { get; set; }
        public Int32 OrderNameId { get; set; }
        public string OrderName { get; set; }
        public bool isDebit { get; set; }
        public string ordertype { get; set; }
    }

    public class AccountsforOrderGroupVm
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
        public bool? selectedaccount { get; set; }
        public Int32? OrderGroupId { get; set; }
        public bool? isDebit { get; set; }
    }

    public class AccountsforOrderGroupTotalVm
    {
        public Int32 AccountCategoryId { get; set; }
        public string AccountCategoryName { get; set; }
        public string AccountType { get; set; }
        public bool? selectedaccount { get; set; }
        public Int32? OrderGroupId { get; set; }
    }
    public class SaveOrderPropertyVm
    {
        public Int32 OrderGroupId { get; set; }
        public Int32 OrderPropId { get; set; }
        public string OrderPropName { get; set; }
        public string OrderpropTypeName { get; set; }
        public bool isRequird { get; set; }
    }

    public class SaveOrderMainAccountVm
    {
        public Int32 OrderGroupId { get; set; }
        public Int32 accountId { get; set; }
        public bool value { get; set; }
    }

    public class OrderGroupVm
    {
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public bool IsCompany { get; set; }
        public Int32 CompanyBranchId { get; set; }
        public SalesVm sales { get; set; }
        public PurchasesVm purchases { get; set; }

    }

    public class OrderGroupforBillVm
    {
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public bool IsCompany { get; set; }
        public Int32 CompanyBranchId { get; set; }
        public List<OrderGroupTypeForBill> type { get; set; }
        public List<orderInventotyAccounts> orderMainAccounts { get; set; }

    }

    public class orderInventotyAccounts
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public decimal value { get; set; }
    }
    public class OrderGroupTypeForBill
    {
        public Int32 typeId { get; set; }
        public string TypeName { get; set; }
    }



    public class ordersTypesale
    {
        public Int32 id { get; set; }
        public string sale { get; set; }
        public string returnd { get; set; }
    }
    public class SetSalesOrderAccountsMainAccounts
    {
        public List<OrderMainAccountVm> orderMainAccount { get; set; }
        public List<accountsforSetSalesOrderAccountsMainAccounts> Account { get; set; }
    }

    public class accountsforSetSalesOrderAccountsMainAccounts
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
    }
    public class ordersTypepurchaes
    {
        public Int32 id { get; set; }
        public string buy { get; set; }
        public string returnd { get; set; }
    }



    public class PayMethodPropertiesVm
    {
        public Int32 OrderPropertiesPayId { get; set; }
        public string OrderPropertiesPayName { get; set; }
        public Int32 OrderGroupPayMethodId { get; set; }
        public bool IsRequired { get; set; }
        public bool ToPrint { get; set; }
        public Int32 OrderPropertyTypeId { get; set; }
        public string OrderPropertyTypeName { get; set; }

    }

    public class OrderOtherTotalAccountVm
    {
        public Int32 OrderOtherTotalAccountId { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public Int32 salepurId { get; set; }
        public string typename { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }

        public string typeWhenAdd { get; set; }
        public bool? selectedaccount { get; set; }

    }


    public class OrderPropertiesVm
    {
        public Int32 OrderPropertiesId { get; set; }
        public string OrderPropertiesName { get; set; }
        public Int32 salepurId { get; set; }
        public string typename { get; set; }
        public bool IsRequired { get; set; }
        public Int32 OrderPropertyTypeId { get; set; }
        public string OrderPropertyTypeName { get; set; }
    }

    public class OrderAccountsforSubAccountVm
    {

        public Int32 OrderAccountsforSubAccountId { get; set; }
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public Int32 salepurId { get; set; }
        public string typename { get; set; }
        public bool DebitWhenAdd { get; set; }
        public bool IsNewOrder { get; set; }
        public bool CostEffect { get; set; }

        public bool DebitforPlus { get; set; }
        public string typeWhenAdd { get; set; }
        public bool? selectedaccount { get; set; }
    }
    public class OrderMainAccountVm
    {
        public Int32 OrderMainAccountId { get; set; }


        public Int32 SalesId { get; set; }
        public accountsforSetSalesOrderAccountsMainAccounts inventoryAccount { get; set; }

        public accountsforSetSalesOrderAccountsMainAccounts salesCostAccount { get; set; }
        public accountsforSetSalesOrderAccountsMainAccounts salesAccount { get; set; }
        public accountsforSetSalesOrderAccountsMainAccounts salesreturnAccount { get; set; }


    }
    public class billAccountVm
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }

    }

    public class OrdersNameVm
    {
        public Int32 OrdersNameId { get; set; }
        public string OrderName { get; set; }
        public Int32 OrderGroupId { get; set; }
        public string OrderGroupName { get; set; }
        public bool IsDebit { get; set; }
        public string Ordertype { get; set; }
    }


    public class bill
    {
        public Int32 billId { get; set; }
        public string billName { get; set; }
        public DateTime billDate { get; set; }
        public billAccountVm Mainaccount { get; set; }
        public List<OrderPropertiesBillVm> OrderPropertiesBill { get; set; }
        public List<orderInventoryVm> orderInventory { get; set; }
    }

    public class payMethod
    {
        public Int32 payMethodId { get; set; }
        public string payName { get; set; }
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public List<PayMethodPropertiesVm> PayMethodProperties { get; set; }
    }

    public class getTotalAccounts
    {
        public string accountAliesName { get; set; }
        public List<billAccountVm> accounts { get; set; }
    }
}
