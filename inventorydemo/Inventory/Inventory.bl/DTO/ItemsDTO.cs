﻿using Inventory.bl.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.DTO
{
    #region itemVM
    public class TypeDTO
    {
        public int typeId { get; set; }
        public string typeName { get; set; }
        public bool isNew { get; set; }

    }

    public class UnitDTO
    {
        public int unitId { get; set; }
        [Required(ErrorMessage = "يحب تحديد وحدة للفئة")]
        public string unitName { get; set; }
        public bool isNew { get; set; }
        public bool isToEdit { get; set; }

        public TypeDTO type { get; set; }

    }

    public class InventoryyDTO
    {
        public int inventoryId { get; set; }
        public string inventoryName { get; set; }
        public bool isNew { get; set; }

    }

    public class PropertyDTO
    {
        public int propertyNameId { get; set; }
        [Required(ErrorMessage = "يجب تحديد اسم الخاصية")]
        public string propertyName { get; set; }
        public TypeDTO type { get; set; }
        public bool isNew { get; set; }
        public PropertyValueDTO propertyValues { get; set; }

    }

    public class itemsGroupDTO
    {
        public int groupId { get; set; }
        [Required(ErrorMessage = "يجب تحديد اسم للفئة")]
        public string groupName { get; set; }
        public bool isNew { get; set; }
        public bool isToEdit { get; set; }

        public UnitDTO unit { get; set; }
        public IEnumerable<InventoryyDTO> inventoryies { get; set; }
        [IsAllPropertyDTOUnique(ErrorMessage = "لا يمكن تكرار خصائص الفئة")]
        public IEnumerable<PropertyDTO> properties { get; set; }
        public IEnumerable<GroupItemsDTO> items { get; set; }
    }

    public class GroupItemsDTO
    {
        public Int32 itemId { get; set; }
        public string itemCode { get; set; }

        public string itemName { get; set; }

        public decimal ItemPrice { get; set; }

        public string reorderPoint { get; set; }
        public int groupId { get; set; }
        public IEnumerable<PropertyDTO> properties { get; set; }
        public bool isNew { get; set; }
        public bool isToEdit { get; set; }
    }

    public class PropertyValueDTO
    {
        public Int32 PropertyValueId { get; set; }
        public Int32 propertyId { get; set; }
        public string propertyName { get; set; }

        public string PropertyValueName { get; set; }
        public bool isNew { get; set; }
        public bool isToEdit { get; set; }

    }
    #endregion
}
