﻿using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Linq;

namespace Inventory.bl.Managers
{
    public class AccountPropertyManager : ManagersHelper
    {
        public AccountPropertyManager(IOwinContext context)
           : base(context)
        {

        }


        public OrderRequestMoveVm getOrderRequestMoveById(getOrderRequest or)
        {
            
            var accType = "";

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "securitiesrecive":
                    accType = "اوراق قبض";
                    break;
                case "securitiespay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }
            if (string.IsNullOrEmpty(accType))
            {
                return null;
            }
            Int32 companyId = 0;

            if (or.isCompany)
            {
                companyId = or.CompanyId;

            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }

            var findR = db.OrderMoveRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.OrderNo == or.requestNo && a.typeName == or.typeName);
            if (findR == null)
            {
                return null;
            }
            var r = db.orderRequest.Find(findR.orderRequestId);
            OrderRequestMoveVm om = new OrderRequestMoveVm()
            {
                OrderNo = findR.OrderNo,
                mainAccount = new accountforRequestVm() { accountId = findR.mainAccountId, accountName = findR.mainAccount.AccountName, Code = findR.mainAccount.Code },
                tilteAccount = new accountforRequestVm() { accountId = findR.tilteAccountId, accountName = findR.tilteAccount.AccountName, Code = findR.tilteAccount.Code },
                typeName = findR.typeName,
                accountType = or.accountType,
                amount = findR.amount,
                OrderDate = findR.OrderDate,
                isDone = findR.isDone,
                note = findR.note,
                isToCancel = findR.isToCancel,
                orderMoveRequestId = 0,
                attchedRequest = findR.attchedRequest,
                isNewRequest = false,
                requestNo = findR.attchedRequest ? findR.orderRequestId.GetValueOrDefault() : 0,
                userName = db.Users.Find(findR.CurrentWorkerId) == null ? "" : db.Users.Where(u => u.Id == findR.CurrentWorkerId).Select(a => a.FirstName + " " + a.LastName).SingleOrDefault(),
                RequestTypeName = findR.attchedRequest ? findR.orderRequest.AccountsDataTypes.TypeName : "",
                orderRequest = new OrderRequesVm()
                {

                    requestNo = r.requestNo,
                    accountType = r.AccountsDataTypes.TypeName,
                    requestDate = r.requestDate,
                    isDone = r.isDone,
                    isQty = r.isQty,
                    mainAccount = new accountforRequestVm() { accountId = r.mainAccountId, accountName = r.mainAccount.AccountName, Code = r.mainAccount.Code },
                    tilteAccount = new accountforRequestVm() { accountId = r.mainAccountId, accountName = r.mainAccount.AccountName, Code = r.mainAccount.Code },
                    isNewRequest = false,
                    isToCancel = r.isToCancel,
                    orderRequestId = r.OrderRequestId,
                    typeName = r.typeName,
                    dueDate = findR.OrderDate,
                    userName = db.Users.Find(r.CurrentWorkerId) == null ? "" : db.Users.Where(u => u.Id == r.CurrentWorkerId).Select(a => a.FirstName + " " + a.LastName).SingleOrDefault(),
                    items = r.items.Select(i => new OrderRequestitemsVm
                    {
                        note = i.note,
                        price = i.price,
                        qty = i.qty,
                        amount = !r.isQty ? i.price : (i.price * i.qty),
                        refrenceType = i.refrenceType,
                        refrenceTypeId = i.refrenceTypeId.GetValueOrDefault(),
                        OrderRequestitemsId = i.OrderRequestItemsId
                    }).ToList()
                }
            };
            return om;
        }


    }
}