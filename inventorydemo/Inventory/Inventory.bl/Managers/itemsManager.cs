﻿using Inventory.bl.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Data.Entity;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using Inventory.dl;
using Inventory.dl.Models;
using Inventory.bl.Utility;
using System.Data.Entity.Validation;
using Microsoft.Owin;

namespace Inventory.bl.Managers
{

    public class itemsManager : ManagersHelper
    {
        private string uid;
        public itemsManager(IOwinContext context , string userId)
           : base(context)
        {
            this.uid = userId;
        }

        public GeneralReturnResult getUnit( branchGroupSt company)
        {

           
            try
            {
                var Result = new GeneralReturnResult();
                var types = db.TypeforItemGroups.Where(a => a.show).Select(u => new TypeDTO
                {
                    typeId = u.TypeforItemGroupId,
                    typeName = u.TypeName
                });
                var units = db.Units.Where(a => a.CompanyId == company.companyId).Select(u => new UnitDTO
                {
                    unitId = u.UnitId,
                    unitName = u.UnitName,
                    isNew = false,
                    type = new TypeDTO
                    {
                        typeId = u.UnitType.UnitTypeId,
                        typeName = u.UnitType.UnitTypeName
                    }
                });
                var UnitTypes = db.UnitTypes.Select(t => new TypeDTO()
                {
                    typeId = t.UnitTypeId,
                    typeName = t.UnitTypeName

                });

                var invetory = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId)
                    .Select(i => new InventoryyDTO
                    {
                        inventoryId = i.AccountId,
                        inventoryName = i.AccountName + " - " + i.Code

                    });
                var nullobj = new
                {
                    typeDTO = new TypeDTO { isNew = true, typeId = 0, typeName = null },
                    UnitDTO = new UnitDTO { isNew = true, unitId = 0, unitName = null, type = UnitTypes.FirstOrDefault() },
                    itemsGroupDTO = new itemsGroupDTO { isNew = true, groupId = 0, groupName = null, inventoryies = null, properties = new List<PropertyDTO> { new PropertyDTO() { propertyNameId = 0, propertyName = null, isNew = true, type = types.FirstOrDefault() } }, unit = units.FirstOrDefault() },
                    invetoryDTO = new InventoryyDTO { inventoryId = 0, inventoryName = null, isNew = true }
                };
                var getInfo = getItemInfo(new getitemInfoVM() { GroupId = 0, InventoryId = 0, orderType = true, pageNumber = 1, pageSize = 10, orderby = "qty" }, company);
                var groups = getAllGroupsByCompany(company.companyId);
                var toReturn = new { inventories = invetory, types = types, newObjctes = nullobj, units = units, unitTypes = UnitTypes, itemGroups = groups, items = getInfo.items, count = getInfo.count, totalPages = getInfo.totalPages, pageSize = getInfo.pageSize };
                Result.AddMainObject(toReturn);

                return Result;
            }
            catch (DbEntityValidationException ex)
            {
                var ErrorResult = new GeneralReturnResult();
                var err = ex.EntityValidationErrors.SelectMany(m => m.ValidationErrors)
                              .Select(e => e.ErrorMessage)
                              .ToList();
                ErrorResult.addError("لا يمكن اتمام العملية لعدم اكتمال البيانات التالية :");
                ErrorResult.DbEntityValidationExceptionEX(ex);
                return ErrorResult;
              
            }


        }
        public iteminfoandCount GroupSQLItem2(branchGroupSt company, Expression<Func<SubAccount, bool>> query, Func<IGrouping<string, SubAccount>, object> orderBy, int skip, int take, bool isorderBydesc = false)
        {
            var date = DateTime.Now;
            var firstDayOfYear = new DateTime(DateTime.Now.Year, 1, 1);
            var days = (date - firstDayOfYear).Days;


            var totalcount = db.SubAccounts.Where(query).Where(mor => mor.SubAccountMovements.Any()).GroupBy(grp => grp.ItemInfo.code).Count();

            if (isorderBydesc)
            {
                var ittoreturn3 = db.SubAccounts.Where(query).Where(mor => mor.SubAccountMovements.Any())
                    .GroupBy(grp => grp.ItemInfo.code)
                    .OrderByDescending(orderBy).Skip(skip).Take(take).ToList()
                    .Select(item => new itemInfoToReturn
                    {

                        itemName = item.FirstOrDefault().SubAccountName,
                        itemCode = item.Key,
                        qty = item.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity),
                        groupName = item.FirstOrDefault().ItemInfo.ItemGroup.ItemGroupName,
                        groupId = item.FirstOrDefault().ItemInfo.ItemGroupId,
                        unitId = item.FirstOrDefault().ItemInfo.UnitId,
                        unitTypeId = item.FirstOrDefault().ItemInfo.Unit.UnitTypeId,
                        unitName = item.FirstOrDefault().ItemInfo.Unit.UnitName,
                        unitType = item.FirstOrDefault().ItemInfo.Unit.UnitType.UnitTypeName,
                        price = item.Sum(iitem => iitem.ItemInfo.price * (iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)),
                        reorderPoint = item.FirstOrDefault().ItemInfo.reorderPoint,
                        expireinDayes = item.FirstOrDefault().ItemInfo.ExpireinDayes,
                        unitPrice = item.FirstOrDefault().ItemInfo.price,
                        LastMoveDate = item.Max(iitem => iitem.SubAccountMovements.Max(d => d.SubAccountMovementDate)),
                        COGS = item.Sum(iitem => iitem.SubAccountMovements
                                                  .Where(c => c.SubAccountMovementDate >= firstDayOfYear && (c.SubAccountOrder.OrderNote.Contains("اذن استلام") || c.SubAccountOrder.OrderNote.Contains("اذن صرف") || c.SubAccountOrder.OrderNote.Contains("مبيعات") || c.SubAccountOrder.OrderNote.Contains("مشتريات")))
                                                  .Sum(co => (co.ItemPricein * co.QuantityIn) - (co.ItemPriceOut * co.QuantityOut))),
                        AvgInventory = item.Sum(iitem => (

                                iitem.SubAccountMovements.OrderBy(mod => mod.SubAccountMovementDate).Where(da => da.SubAccountMovementDate >= firstDayOfYear && da.SubAccountOrder.OrderNote.Contains("اذن استلام") || da.SubAccountOrder.OrderNote.Contains("اذن صرف") || da.SubAccountOrder.OrderNote.Contains("مبيعات") || da.SubAccountOrder.OrderNote.Contains("مشتريات")).Select(md => md.ItemTotalStockPrice).DefaultIfEmpty(0.0m).FirstOrDefault()
                                +
                                iitem.SubAccountMovements.OrderByDescending(mod => mod.SubAccountMovementDate).Where(da => da.SubAccountMovementDate <= date && da.SubAccountOrder.OrderNote.Contains("اذن استلام") || da.SubAccountOrder.OrderNote.Contains("اذن صرف") || da.SubAccountOrder.OrderNote.Contains("مبيعات") || da.SubAccountOrder.OrderNote.Contains("مشتريات")).Select(md => md.ItemTotalStockPrice).DefaultIfEmpty(0.0m).FirstOrDefault()) / 2),
                        totalDays = days,
                        totalcostPrice = item.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice),

                        Inventory = item.GroupBy(invGrp => invGrp.AccountId)
                                                .Select(t => new itemInventoryDetailsVM()
                                                {
                                                    inventoryId = t.Key,
                                                    inventoryName = t.FirstOrDefault().Account.AccountName + " - " + t.FirstOrDefault().Account.Code,
                                                    costPrice = t.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice),
                                                    price = t.Sum(iitem => iitem.ItemInfo.price * (iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)),
                                                    qty = t.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)
                                                }).ToList()
                    }).ToList();
                return new iteminfoandCount { totalcount = totalcount, items = ittoreturn3 };

            }
            var ittoreturn2 = db.SubAccounts.Where(query).Where(mor => mor.SubAccountMovements.Any()).GroupBy(grp => grp.ItemInfo.code).
                OrderByDescending(orderBy).Skip(skip).Take(take).ToList()
                 .Select(item => new itemInfoToReturn
                 {
                     itemName = item.FirstOrDefault().SubAccountName,
                     itemCode = item.Key,
                     qty = item.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity),
                     groupName = item.FirstOrDefault().ItemInfo.ItemGroup.ItemGroupName,
                     groupId = item.FirstOrDefault().ItemInfo.ItemGroupId,
                     unitId = item.FirstOrDefault().ItemInfo.UnitId,
                     unitTypeId = item.FirstOrDefault().ItemInfo.Unit.UnitTypeId,
                     unitName = item.FirstOrDefault().ItemInfo.Unit.UnitName,
                     unitType = item.FirstOrDefault().ItemInfo.Unit.UnitType.UnitTypeName,
                     price = item.Sum(iitem => iitem.ItemInfo.price * (iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)),
                     reorderPoint = item.FirstOrDefault().ItemInfo.reorderPoint,
                     expireinDayes = item.FirstOrDefault().ItemInfo.ExpireinDayes,
                     unitPrice = item.FirstOrDefault().ItemInfo.price,
                     LastMoveDate = item.Max(iitem => iitem.SubAccountMovements.Max(d => d.SubAccountMovementDate)),
                     COGS = item.Sum(iitem => iitem.SubAccountMovements
                                               .Where(c => c.SubAccountMovementDate >= firstDayOfYear && (c.SubAccountOrder.OrderNote.Contains("اذن استلام") || c.SubAccountOrder.OrderNote.Contains("اذن صرف") || c.SubAccountOrder.OrderNote.Contains("مبيعات") || c.SubAccountOrder.OrderNote.Contains("مشتريات")))
                                               .Sum(co => (co.ItemPricein * co.QuantityIn) - (co.ItemPriceOut * co.QuantityOut))),
                     AvgInventory = item.Sum(iitem => (

                             iitem.SubAccountMovements.OrderBy(mod => mod.SubAccountMovementDate).Where(da => da.SubAccountMovementDate >= firstDayOfYear && da.SubAccountOrder.OrderNote.Contains("اذن استلام") || da.SubAccountOrder.OrderNote.Contains("اذن صرف") || da.SubAccountOrder.OrderNote.Contains("مبيعات") || da.SubAccountOrder.OrderNote.Contains("مشتريات")).Select(md => md.ItemTotalStockPrice).DefaultIfEmpty(0.0m).FirstOrDefault()
                             +
                             iitem.SubAccountMovements.OrderByDescending(mod => mod.SubAccountMovementDate).Where(da => da.SubAccountMovementDate <= date && da.SubAccountOrder.OrderNote.Contains("اذن استلام") || da.SubAccountOrder.OrderNote.Contains("اذن صرف") || da.SubAccountOrder.OrderNote.Contains("مبيعات") || da.SubAccountOrder.OrderNote.Contains("مشتريات")).Select(md => md.ItemTotalStockPrice).DefaultIfEmpty(0.0m).FirstOrDefault()) / 2),
                     totalDays = days,
                     totalcostPrice = item.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice),

                     Inventory = item.GroupBy(invGrp => invGrp.AccountId)
                                            .Select(t => new itemInventoryDetailsVM()
                                            {
                                                inventoryId = t.Key,
                                                inventoryName = t.FirstOrDefault().Account.AccountName + " - " + t.FirstOrDefault().Account.Code,
                                                costPrice = t.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice),
                                                price = t.Sum(iitem => iitem.ItemInfo.price * (iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)),
                                                qty = t.Sum(iitem => iitem.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity)
                                            }).ToList()
                 }).ToList();
            return new iteminfoandCount { totalcount = totalcount, items = ittoreturn2 };



        }
        //public List<itemInfoToReturn>  GroupItem(IQueryable<itemInfoToReturn> items)
        //{
        //    return items.ToList();
        //}

        //public List<itemInfoToReturn> GroupItem(List<SubAccount> subAccounts)
        //{
        //   return subAccounts.Select(s => getiteminfoData(s)).GroupBy(grp => grp.itemCode)
        //                           .Select(i => new itemInfoToReturn()
        //                           {
        //                               itemCode = i.Key,
        //                               costPrice = Math.Round(i.Select(c => c.costPrice).DefaultIfEmpty(0.0m).Average(), 2),
        //                               expireinDayes = i.FirstOrDefault().expireinDayes,
        //                               groupId = i.FirstOrDefault().groupId,
        //                               groupName = i.FirstOrDefault().groupName,
        //                               itemName = i.FirstOrDefault().itemName,
        //                               unitPrice = i.FirstOrDefault().unitPrice,
        //                               qty = i.Select(a => a.qty).DefaultIfEmpty(0.0m).Sum(),
        //                               reorderPoint = i.FirstOrDefault().reorderPoint,
        //                               unitId = i.FirstOrDefault().unitId,
        //                               unitName = i.FirstOrDefault().unitName,
        //                               unitType = i.FirstOrDefault().unitType,
        //                               unitTypeId = i.FirstOrDefault().unitTypeId,
        //                               price = i.Select(a => a.price).DefaultIfEmpty(0.0m).Sum(),
        //                               totalcostPrice = i.Select(a => a.totalcostPrice).DefaultIfEmpty(0.0m).Sum(),
        //                               turnOverRate = i.Select(a => a.AvgInventory).DefaultIfEmpty(0).Sum() > 0 ? i.Select(a => a.COGS).DefaultIfEmpty(0.0m).Sum() / i.Select(a => a.AvgInventory).DefaultIfEmpty(0).Sum() : 0,
        //                               avgStock = i.Select(iv => iv.avgStock).DefaultIfEmpty(0.0m).Average(),
        //                               Inventory = i.Where(q => q.qty > 0).GroupBy(invGrp => invGrp.InventoryId)
        //                               .Select(t => new itemInventoryDetailsVM()
        //                               {
        //                                   inventoryId = t.Key,
        //                                   inventoryName = t.FirstOrDefault().InventoryName,
        //                                   costPrice = t.Select(a => a.totalcostPrice).DefaultIfEmpty(0.0m).Sum(),
        //                                   price = t.Select(a => a.price).DefaultIfEmpty(0).Sum(),
        //                                   qty = t.Select(a => a.qty).DefaultIfEmpty(0).Sum()
        //                               }).AsQueryable(),

        //                           }).ToList();
        //}

        public itemInfoToReturnwithCount getItemInfo(getitemInfoVM itemInfoVM, branchGroupSt company)
        {
            try
            {
                var totalCount = 0;
                Expression<Func<SubAccount, bool>> query;
                if (itemInfoVM.InventoryId == 0)
                {
                    if (!string.IsNullOrEmpty(itemInfoVM.search))
                    {
                        if (itemInfoVM.GroupId == 0) query = a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && (a.ItemInfo.code.Contains(itemInfoVM.search) || a.SubAccountName.Contains(itemInfoVM.search));
                        else query = a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.ItemInfo.ItemGroupId == itemInfoVM.GroupId && (a.ItemInfo.code.Contains(itemInfoVM.search) || a.SubAccountName.Contains(itemInfoVM.search));

                    }
                    else
                    {
                        if (itemInfoVM.GroupId == 0) query = a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId;
                        else query = a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.ItemInfo.ItemGroupId == itemInfoVM.GroupId;
                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(itemInfoVM.search))
                    {
                        if (itemInfoVM.GroupId == 0) query = a => a.AccountId == itemInfoVM.InventoryId && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && (a.ItemInfo.code.Contains(itemInfoVM.search) || a.SubAccountName.Contains(itemInfoVM.search));
                        else query = a => a.AccountId == itemInfoVM.InventoryId && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.ItemInfo.ItemGroupId == itemInfoVM.GroupId && (a.ItemInfo.code.Contains(itemInfoVM.search) || a.SubAccountName.Contains(itemInfoVM.search));

                    }
                    else
                    {
                        if (itemInfoVM.GroupId == 0) query = a => a.AccountId == itemInfoVM.InventoryId && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId;
                        else query = a => a.AccountId == itemInfoVM.InventoryId && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.ItemInfo.ItemGroupId == itemInfoVM.GroupId;
                    }
                }


                List<itemInfoToReturn> itemsInfo = new List<itemInfoToReturn>();
                if (QueryHelper.PropertyExists<itemInfoToReturn>(itemInfoVM.orderby))
                {



                    switch (itemInfoVM.orderby)
                    {
                        case "itemCode":
                            if (itemInfoVM.orderType)
                            {
                                var info = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.code, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info.totalcount;
                                itemsInfo = info.items;
                            }
                            else
                            {
                                var info1 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.code, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info1.totalcount;
                                itemsInfo = info1.items;

                            }

                            break;

                        case "itemName":

                            if (itemInfoVM.orderType)
                            {
                                var info2 = GroupSQLItem2(company, query, or => or.FirstOrDefault().SubAccountName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info2.totalcount;
                                itemsInfo = info2.items;
                            }
                            else
                            {
                                var info3 = GroupSQLItem2(company, query, or => or.FirstOrDefault().SubAccountName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info3.totalcount;
                                itemsInfo = info3.items;

                            }
                            break;
                        case "qty":


                            if (itemInfoVM.orderType)
                            {
                                var info4 = GroupSQLItem2(company, query, or => or.Sum(r => r.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity), (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info4.totalcount;
                                itemsInfo = info4.items;
                            }
                            else
                            {
                                var info5 = GroupSQLItem2(company, query, or => or.Sum(r => r.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity), (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info5.totalcount;
                                itemsInfo = info5.items;

                            }
                            break;
                        case "unitName":


                            if (itemInfoVM.orderType)
                            {
                                var info6 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.Unit.UnitName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info6.totalcount;
                                itemsInfo = info6.items;
                            }
                            else
                            {
                                var info7 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.Unit.UnitName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info7.totalcount;
                                itemsInfo = info7.items;

                            }
                            break;
                        case "unitPrice":


                            if (itemInfoVM.orderType)
                            {
                                var info8 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.price, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info8.totalcount;
                                itemsInfo = info8.items;
                            }
                            else
                            {
                                var info9 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.price, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info9.totalcount;
                                itemsInfo = info9.items;

                            }
                            break;

                        case "groupName":


                            if (itemInfoVM.orderType)
                            {
                                var info12 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.ItemGroup.ItemGroupName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, true);
                                totalCount = info12.totalcount;
                                itemsInfo = info12.items;
                            }
                            else
                            {
                                var info13 = GroupSQLItem2(company, query, or => or.FirstOrDefault().ItemInfo.ItemGroup.ItemGroupName, (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                                totalCount = info13.totalcount;
                                itemsInfo = info13.items;

                            }
                            break;
                        default:
                            var info0 = GroupSQLItem2(company, query, or => or.Sum(r => r.SubAccountMovements.OrderByDescending(di => di.SubAccountMovementDate).FirstOrDefault().ItemStockQuantity), (itemInfoVM.pageNumber - 1) * itemInfoVM.pageSize, itemInfoVM.pageSize, false);
                            totalCount = info0.totalcount;
                            itemsInfo = info0.items;
                            break;


                    }
                    int pages = ((totalCount - 1) / itemInfoVM.pageSize) + 1;


                    return new itemInfoToReturnwithCount() { items = itemsInfo, count = totalCount, totalPages = pages, pageSize = itemInfoVM.pageSize };

                }
                return null;

            }
            catch
            {

                throw;
            }

        }

        public IEnumerable<itemsGroupDTO> getAllGroupsByCompany(int companyId)
        {
            var groups = db.ItemGroups.Where(i => i.CompanyId == companyId).Select(a => new itemsGroupDTO
            {
                groupId = a.ItemGroupId,
                groupName = a.ItemGroupName,
                isNew = false,
                unit = new UnitDTO
                {
                    unitId = a.UnitId,
                    unitName = a.Unit.UnitName,
                    isNew = false,
                    type = new TypeDTO()
                    {
                        typeId = a.Unit.UnitType.UnitTypeId,
                        typeName = a.Unit.UnitType.UnitTypeName
                    }
                },

                inventoryies = a.Accounts.Where(ty => ty.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory").Select(i => new InventoryyDTO
                {
                    inventoryId = i.AccountId,
                    inventoryName = i.Account.AccountName + " - " + i.Account.Code

                }),
                properties = a.Properties.Where(t => t.Show).Select(p => new PropertyDTO
                {
                    propertyNameId = p.PropertiesforItemGroupId,
                    propertyName = p.PropertiesforItemGroupName,
                    type = new TypeDTO
                    {
                        typeId = p.TypeforItemGroupId,
                        typeName = p.Type.TypeName
                    }

                })

            });

            return groups;
        }
        public IEnumerable<GroupItemsDTO> getItemsbyGroup(int groupId, branchGroupSt company)
        {

            var items = db.TempItems.Where(a => a.ItemGroupId == groupId && a.CompanyId == company.companyId).Select(a => new GroupItemsDTO()
            {
                itemId = a.TempItemId,
                groupId = groupId,
                isNew = false,
                isToEdit = false,
                itemCode = a.code,
                itemName = a.name,
                ItemPrice = a.price,
                reorderPoint = a.reorderPoint,
                properties = a.ItemGroup.Properties.Where(sh => sh.Show).
                Select(prop => new PropertyDTO()
                {
                    isNew = false,
                    propertyNameId = prop.PropertiesforItemGroupId,
                    propertyName = prop.PropertiesforItemGroupName,
                    type = new TypeDTO() { typeId = prop.TypeforItemGroupId, typeName = prop.Type.TypeName, isNew = false },
                    propertyValues = a.Properties.Where(pp => pp.PropertyValuesforItemGroup.PropertiesforItemGroupId == prop.PropertiesforItemGroupId).
                    Select(pv => pv.PropertyValuesforItemGroup).
                    Select(pvl => new PropertyValueDTO()
                    {
                        PropertyValueId = pvl.PropertyValuesforItemGroupId,
                        propertyId = prop.PropertiesforItemGroupId,
                        PropertyValueName = pvl.PropertyValuesforItemGroupName,
                        propertyName = prop.PropertiesforItemGroupName,
                        isNew = pvl.isNew,
                        isToEdit = false,
                    }).FirstOrDefault()

                })
            });
            return items;
        }
        public itemsGroupDTO saveGroup(itemsGroupDTO group, branchGroupSt company)
        {

            if (company == null)
            {
                throw new Exception("الشركة او الفرع غير مسجلين او لا تملك الصلاحيات الكافية للاستخدامهم");

            }


            //var unit = group.unit.unitId == 0 ? new Unit() { CompanyId = company.companyId, UnitName = group.unit.unitName, UnitTypeId = group.unit.type.typeId } : un;

            if (group.isNew)
            {


                string realted = Guid.NewGuid().ToString();
                do
                {
                    if (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted))
                    {
                        realted = Guid.NewGuid().ToString();
                    }

                } while (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted));
                var addprop = new List<PropertiesforItemGroup>()
                        {
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "الصنف", orderNum = 1 , TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "صنف").TypeforItemGroupId , Show = false },
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "الكود" , orderNum = 2 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "كود").TypeforItemGroupId , Show = false},
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "السعر" , orderNum = 3 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "سعر").TypeforItemGroupId , Show = false },
                             new PropertiesforItemGroup {PropertiesforItemGroupName = "حد الطلب" , orderNum = 4 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "حد الطلب").TypeforItemGroupId , Show = false,},
                        };
                var addprop2 = group.properties.Where(s => !string.IsNullOrEmpty(s.propertyName)).Select(p => new PropertiesforItemGroup()
                {
                    PropertiesforItemGroupName = p.propertyName,
                    TypeforItemGroupId = p.type.typeId,
                    Show = true,
                    orderNum = 5,


                }).ToList();
                if (addprop2.Any()) addprop.AddRange(addprop2);

                var newgroup = new ItemGroup
                {
                    ItemGroupName = group.groupName,
                    CompanyId = company.companyId,
                    CurrentWorkerId = uid,
                    Properties = addprop,
                    Accounts = group.inventoryies.Select(a => new AccountsforItemGroup()
                    {
                        AccountId = a.inventoryId,
                    }).ToList()

                };
                var unit = db.Units.SingleOrDefault(u => u.UnitId == group.unit.unitId && u.CompanyId == company.companyId);

                if (group.unit.unitId != 0 && unit != null)
                {
                    unit.UnitName = group.unit.unitName;
                    unit.UnitTypeId = group.unit.type.typeId;
                    db.Entry(unit).State = System.Data.Entity.EntityState.Modified;

                    newgroup.UnitId = unit.UnitId;

                }
                else
                {
                    newgroup.Unit = new Unit() { CompanyId = company.companyId, UnitName = group.unit.unitName, UnitTypeId = group.unit.type.typeId };
                }
                db.ItemGroups.Add(newgroup);
                //     if(group.unit.unitId != 0) db.Entry(newgroup.Unit).State = EntityState.Modified;
                db.SaveChanges();

                var accs = new HashSet<int>(newgroup.Accounts.Select(n => n.AccountId));
                var newgroupAccoounts = db.Accounts.Where(a => accs.Any(ac => ac == a.AccountId)).ToList();


                #region addTransitAccounts
                var trnsitAccountCat = db.AccountCategories.Where(a => a.AccountsDataTypes.aliasName == "goods-in-transit" && a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId).FirstOrDefault();
                var bsheet = db.AccountCategories.Where(a => a.AccountsDataTypes.aliasName == "inventory" && a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId).FirstOrDefault().BalanceSheetType;
                if (trnsitAccountCat == null)
                {

                    trnsitAccountCat = new AccountCategory()
                    {
                        AccountCategoryName = "بضاعة بالطريق",
                        BalanceSheetTypeId = bsheet.BalanceSheetTypeId,
                        AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.aliasName == "goods-in-transit").Id,
                        AccountTypeId = bsheet.BalanceSheet.AccountTypeId,
                        CurrentWorkerId = uid,
                        Code = bsheet.Code + bsheet.AccountCategories.Count() + 1,
                        Accounts = newgroupAccoounts.Select((na, index) => new Account()
                        {
                            AccountName = string.Format("بضاعة بالطريق - {0}", na.Branch == null ? na.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : na.Branch.BranchName),
                            BranchId = na.BranchId,
                            Activated = true,
                            Code = (bsheet.Code + (bsheet.AccountCategories.Count() + 1).ToString()) + (index + 1).ToString(),
                            AccountsforItemGroups = new List<AccountsforItemGroup>()
                                    {
                                        new AccountsforItemGroup()
                                        {
                                        ItemGroupId = newgroup.ItemGroupId
                                        }
                                    }
                        }).ToList()
                    };


                    db.AccountCategories.Add(trnsitAccountCat);
                    db.SaveChanges();

                }
                else
                {
                    //var acctrans = trnsitAccountCat.Accounts.Where(a => a.BranchId == (company.isCompany ? (int?)null : company.oId)).FirstOrDefault();
                    //acctrans.AccountsforItemGroups.Add(new AccountsforItemGroup()
                    //{
                    //    ItemGroupId = newgroup.ItemGroupId
                    //});
                    Int32 co = 0;
                    foreach (var a in newgroupAccoounts)
                    {

                        var acctrans2 = trnsitAccountCat.Accounts.Where(b => b.BranchId == a.BranchId).FirstOrDefault();
                        if (acctrans2 != null)
                        {
                            acctrans2.AccountsforItemGroups.ToList().Add(new AccountsforItemGroup()
                            {
                                ItemGroupId = newgroup.ItemGroupId
                            });
                            db.Entry(acctrans2).State = EntityState.Modified;

                        }
                        else
                        {
                            co++;
                            acctrans2 = new Account()
                            {
                                AccountName = string.Format("بضاعة بالطريق - {0}", a.Branch == null ? a.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : a.Branch.BranchName),
                                BranchId = a.BranchId,
                                Activated = true,
                                AccountCategoryId = a.AccountCategoryId,
                                Code = a.AccountCategory.Code.ToString() + co.ToString(),
                                AccountsforItemGroups = new List<AccountsforItemGroup>()
                                    {
                                        new AccountsforItemGroup()
                                        {
                                           ItemGroupId = newgroup.ItemGroupId
                                        }
                                    }
                            };
                            db.Accounts.Add(acctrans2);
                        }
                    };


                    //db.Entry(acctrans).State = EntityState.Modified;
                    db.SaveChanges();
                };

                #endregion


                return GetGroupById(newgroup.ItemGroupId, company.companyId);
            }
            else
            {

                var findGroup = db.ItemGroups.SingleOrDefault(g => g.ItemGroupId == group.groupId && g.CompanyId == company.companyId);
                var accToRemove = findGroup.Accounts.Where(a => a.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory" && !group.inventoryies.Any(i => i.inventoryId == a.AccountId)).Select(c => c.Account).ToList();

                if (accToRemove.Count() > 0)
                {
                    var SubaccountsforItemGroup = db.SubAccounts.Where(a => a.ItemInfo.ItemGroupId == findGroup.ItemGroupId && accToRemove.Any(ar => ar.AccountId == a.AccountId));

                    var accountsforItemGroup = findGroup.Accounts.Where(a => accToRemove.Any(ar => ar.AccountId == a.AccountId));

                    if (SubaccountsforItemGroup.Any() && !SubaccountsforItemGroup.Any(a => a.SubAccountMovements.Any()))
                    {
                        db.SubAccounts.RemoveRange(SubaccountsforItemGroup);
                        db.AccountsforItemGroups.RemoveRange(accountsforItemGroup);
                    }
                    db.SaveChanges();
                }

                var accToadd = group.inventoryies.Where(a => !findGroup.Accounts.Any(i => i.AccountId == a.inventoryId)).ToList();
               
                if (accToadd.Count() > 0)
                {

                    var accountsForItemGroup = accToadd.Select(a => new AccountsforItemGroup()
                    {

                        AccountId = a.inventoryId,
                        ItemGroupId = findGroup.ItemGroupId
                    }).ToList();
                    db.AccountsforItemGroups.AddRange(accountsForItemGroup);
                    Int32 codeIncrement = 0;
                    accountsForItemGroup.ForEach(acc => 
                    {
                        var accCode = db.Accounts.Find(acc.AccountId);
                        var groupSubAccounts = findGroup.Items.Select(a => new SubAccount()
                        {
                            AccountId = acc.AccountId,
                            SubAccountName = a.name,
                            CurrentWorkerId = uid,
                            ItemInfoId = a.TempItemId,
                            Code = accCode.Code +( accCode.SubAccounts.Count() + 1).ToString(),
                        });
                        db.SubAccounts.AddRange(groupSubAccounts);

                        var trnsitAccount = db.Accounts.Where(b => b.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit" && b.BranchId == accCode.BranchId && b.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId).FirstOrDefault();
                        if (trnsitAccount != null)
                        {
                            var groupTranstSubAccounts = findGroup.Items.Select(a => new SubAccount()
                            {
                                AccountId = trnsitAccount.AccountId,
                                SubAccountName = a.name,
                                CurrentWorkerId = uid,
                                ItemInfoId = a.TempItemId,
                                Code = trnsitAccount.Code + (trnsitAccount.SubAccounts.Count() + 1).ToString(),
                            });
                            db.SubAccounts.AddRange(groupTranstSubAccounts);
                        }
                        else
                        {
                            var trnsitAccountn = db.AccountCategories.Where(b => b.AccountsDataTypes.aliasName == "goods-in-transit" && b.BalanceSheetType.BalanceSheet.CompanyId == company.companyId).FirstOrDefault();
                            codeIncrement++;
                            var nAccount = new Account()
                            {
                                AccountCategoryId = trnsitAccountn.AccountCategoryId,
                                AccountName = string.Format("بضاعة بالطريق - {0}", accCode.Branch == null ? accCode.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : accCode.Branch.BranchName),
                                BranchId = accCode.BranchId,
                                Activated = true,
                                Code = (trnsitAccountn.Code) + (trnsitAccountn.Accounts.Count() + codeIncrement).ToString(),
                                AccountsforItemGroups = new List<AccountsforItemGroup>()
                                        {
                                            new AccountsforItemGroup()
                                            {
                                                ItemGroupId = findGroup.ItemGroupId
                                            }
                                        },
                                SubAccounts = findGroup.Items.Select((a, index) => new SubAccount()
                                {
                                    SubAccountName = a.name,
                                    CurrentWorkerId = uid,
                                    ItemInfoId = a.TempItemId,
                                    Code = ((trnsitAccountn.Code) + (trnsitAccountn.Accounts.Count() + codeIncrement).ToString()) + (index + 1).ToString(),
                                }).ToList()
                               
                            };
                            db.Accounts.Add(nAccount);
                        }

                    });
                    
                }

                var unit = db.Units.SingleOrDefault(u => u.UnitId == group.unit.unitId && u.CompanyId == company.companyId);
                if (group.unit.unitId != 0 && unit != null)
                {

                    unit.UnitName = group.unit.unitName;
                    unit.UnitTypeId = group.unit.type.typeId;
                    db.Entry(unit).State = System.Data.Entity.EntityState.Modified;

                    findGroup.UnitId = unit.UnitId;
                }
                else
                {
                    findGroup.Unit = new Unit() { CompanyId = company.companyId, UnitName = group.unit.unitName, UnitTypeId = group.unit.type.typeId };
                }
                findGroup.ItemGroupName = group.groupName;

                findGroup.CurrentWorkerId = uid;

                db.Entry(findGroup).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                #region updateproperties
                var propIds = new HashSet<int>(group.properties.Select(a => a.propertyNameId));
                var props = new HashSet<PropertiesforItemGroupVM>(group.properties.Select(a => new PropertiesforItemGroupVM { propertyId = a.propertyNameId, propertyName = a.propertyName }));



                var propertiesToRemove = findGroup.Properties.Where(a => a.Show && !a.PropertyValuesforItemGroup.Any() && !propIds.Any(p => p == a.PropertiesforItemGroupId && p != 0)).ToList();

                if (propertiesToRemove.Any())
                {
                    db.PropertiesforItemGroups.RemoveRange(propertiesToRemove);
                }
                var propertiesToUpdate = findGroup.Properties.Where(a => a.Show && props.Any(p => p.propertyId == a.PropertiesforItemGroupId)).ToList();
                group.properties.ToList().ForEach(a =>
                {
                    var updateprop = propertiesToUpdate.SingleOrDefault(b => b.PropertiesforItemGroupId == a.propertyNameId);
                    if (updateprop != null)
                    {
                        updateprop.PropertiesforItemGroupName = a.propertyName;
                        updateprop.TypeforItemGroupId = a.type.typeId;
                        db.Entry(updateprop).State = EntityState.Modified;
                    }

                });
                var propertiesToAdd = group.properties.Where(a => a.propertyNameId == 0 && !findGroup.Properties.Any(p => p.PropertiesforItemGroupName == a.propertyName)).ToList();
                if (propertiesToAdd.Any())
                {
                    var addprop = propertiesToAdd.Select(np => new PropertiesforItemGroup()
                    {
                        ItemGroupId = findGroup.ItemGroupId,
                        PropertiesforItemGroupName = np.propertyName,
                        TypeforItemGroupId = np.type.typeId,
                        Show = true,
                        orderNum = 6,
                    });

                    db.PropertiesforItemGroups.AddRange(addprop);
                }

                db.SaveChanges();


                #endregion

                return GetGroupById(group.groupId, company.companyId);



            }

        }
        public int save(GroupItemsDTO it , branchGroupSt company)
        {
            return 0;
            var ItemGroup = db.ItemGroups.FirstOrDefault(i => i.CompanyId == company.companyId && i.ItemGroupId == it.groupId);
            if (it.isNew && it.groupId == 0)
            {
                var nItem = new TempItem()
                {

                    code = it.itemCode,
                    CompanyId = company.companyId,
                    ItemGroupId = ItemGroup.ItemGroupId,
                    name = it.itemName,
                    SubAccounts = ItemGroup.Accounts.
                    Select(inv => inv.Account).
                    Select(s => new SubAccount()
                    {
                        AccountId = s.AccountId,
                        Activated = true,
                        Code = s.Code + (s.SubAccounts.Count() + 1).ToString(),
                        CurrentWorkerId = uid,
                        SubAccountName = it.itemName,

                    }).ToList(),
                    price = it.ItemPrice,
                    reorderPoint = it.reorderPoint,
                    Properties = it.properties.Select(p => new TempItempropertyValues()
                    {
                        PropertyValuesforItemGroup = new PropertyValuesforItemGroup()
                        {
                            PropertyValuesforItemGroupName = p.propertyValues.propertyName,
                            PropertiesforItemGroupId = p.propertyNameId
                        },
                        PropertyValuesforItemGroupId = p.propertyNameId,

                    }).ToList(),
                    ExpireinDayes = it.properties.Where(a => a.type.typeName == "مدة صلاحية").Select(b => b.propertyValues.PropertyValueName).FirstOrDefault()

                };
            }

        }
     
        public itemsGroupDTO GetGroupById(Int32 itemsGroupId, int companyId)
        {
            try
            {
                var toreturn = db.ItemGroups.SingleOrDefault(n => n.ItemGroupId == itemsGroupId && n.CompanyId == companyId);

                if (toreturn == null)
                {
                    return null;
                }
                var groupToreturn = new itemsGroupDTO
                {
                    groupId = toreturn.ItemGroupId,
                    groupName = toreturn.ItemGroupName,
                    isNew = false,
                    isToEdit = false,
                    unit = new UnitDTO
                    {
                        unitId = toreturn.UnitId,
                        unitName = toreturn.Unit.UnitName,
                        isNew = false,
                        isToEdit = false,
                        type = new TypeDTO()
                        {
                            typeId = toreturn.Unit.UnitTypeId,
                            typeName = toreturn.Unit.UnitType != null ? toreturn.Unit.UnitType.UnitTypeName : db.UnitTypes.Where(a => a.UnitTypeId == toreturn.Unit.UnitTypeId).SingleOrDefault().UnitTypeName
                        }
                    },

                    inventoryies = toreturn.Accounts.Where(ty => ty.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory").Select(i => new InventoryyDTO
                    {
                        inventoryId = i.AccountId,
                        inventoryName = i.Account.AccountName + " - " + i.Account.Code

                    }),
                    properties = toreturn.Properties.Where(t => t.Show).Select(p => new PropertyDTO
                    {
                        propertyNameId = p.PropertiesforItemGroupId,
                        propertyName = p.PropertiesforItemGroupName,
                        type = new TypeDTO
                        {
                            typeId = p.TypeforItemGroupId,
                            typeName = p.Type != null ? p.Type.TypeName : db.TypeforItemGroups.Where(a => a.TypeforItemGroupId == p.TypeforItemGroupId).SingleOrDefault().TypeName
                        }

                    })

                };
                return groupToreturn;
            }
            catch 
            {

               return null;
            }
          
        }

        public itemsGroupDTO GetGroupByName(string itemsGroupName ,int companyId)
        {
            try
            {
                var toreturn = db.ItemGroups.SingleOrDefault(n => n.ItemGroupName == itemsGroupName && n.CompanyId == companyId);
                if (toreturn == null)
                {
                    return null;
                }
                var groupToreturn = new itemsGroupDTO
                {
                    groupId = toreturn.ItemGroupId,
                    groupName = toreturn.ItemGroupName,
                    isNew = false,
                    isToEdit = false,
                    unit = new UnitDTO
                    {
                        unitId = toreturn.UnitId,
                        unitName = toreturn.Unit.UnitName,
                        isNew = false,
                        isToEdit = false,
                        type = new TypeDTO()
                        {
                            typeId = toreturn.Unit.UnitTypeId,
                            typeName = toreturn.Unit.UnitType != null ? toreturn.Unit.UnitType.UnitTypeName : db.UnitTypes.Where(a => a.UnitTypeId == toreturn.Unit.UnitTypeId).SingleOrDefault().UnitTypeName
                        }
                    },

                    inventoryies = toreturn.Accounts.Where(ty => ty.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory").Select(i => new InventoryyDTO
                    {
                        inventoryId = i.AccountId,
                        inventoryName = i.Account.AccountName + " - " + i.Account.Code

                    }),
                    properties = toreturn.Properties.Where(t => t.Show).Select(p => new PropertyDTO
                    {
                        propertyNameId = p.PropertiesforItemGroupId,
                        propertyName = p.PropertiesforItemGroupName,
                        type = new TypeDTO
                        {
                            typeId = p.TypeforItemGroupId,
                            typeName = p.Type != null ? p.Type.TypeName : db.TypeforItemGroups.Where(a => a.TypeforItemGroupId == p.TypeforItemGroupId).SingleOrDefault().TypeName
                        }

                    })

                };
                return groupToreturn;
            }
            catch
            {

                return null;
            }
          
        }


    }


    
}