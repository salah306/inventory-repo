﻿using Inventory.bl.DTO;
using Inventory.bl.Utility;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Managers
{
  

    public class ComapnyManager : ManagersHelper 
    {
        private string uid;
        public ComapnyManager(IOwinContext context , string userId)
           : base(context)
        {
            this.uid = userId;
        }
        public GeneralReturnResult AddCompany(string companyName)
        {
            var Result = new GeneralReturnResult();
            var userMaxCompany = db.Users.Find(uid).MaxCompany;
            var checkCompanycount = db.Companies.Count(a => a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if (checkCompanycount >= userMaxCompany)
            {
                Result.addError("لقد تجاوزت الحد الاقصي من الشركات المسموح لك باضافتها");
                return Result;
            };
            var cName = "شركة" + this.GetUniqueKey(4);
            if (string.IsNullOrEmpty(companyName))
            {
                do
                {
                    cName = "شركة" + this.GetUniqueKey(4);
                } while (db.Companies.Any(c => c.CompanyName == cName && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)));
            }
            else
            {
                var checkcompany = db.Companies.Any(a => a.CompanyName == companyName);
                if (checkcompany)
                {
                    Result.addError("يوجد شركة مسجلة بنفس الاسم");
                    return Result;
                }
                do
                {
                    cName = "شركة" + this.GetUniqueKey(4);
                } while (db.Companies.Any(c => c.CompanyName == cName && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)));

            }
           
            

            var nowdate = DateTime.Now.Date;
            var enddate = nowdate.AddYears(1);
            var company = new Company()
            {
                CompanyName = cName,
                CurrentWorkerId = uid,
                MaxBranch = 0,
                startDate = nowdate,
                endDate = enddate,
                BalanceSheets = SetChart(uid),
                FinancialLists = SetFinancialLists(uid),
                CompanyProperties = new List<CompanyProperties>()
                {
                    new CompanyProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "عنوان").AccountsCatogeryTypesAcccountsId,
                        propertyName = "العنوان",
                        CurrentWorkerId = uid,
                        CompanyPropertyValues = new List<CompanyPropertyValues>()
                        {
                            new CompanyPropertyValues() {CurrentWorkerId = uid , propertyValue = "عنوان الشركة هنا"  }
                        }
                    },

                    new CompanyProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "هاتف ارضي").AccountsCatogeryTypesAcccountsId,
                        propertyName = "هاتف",
                        CurrentWorkerId = uid,
                        CompanyPropertyValues = new List<CompanyPropertyValues>()
                        {
                            new CompanyPropertyValues() {CurrentWorkerId = uid , propertyValue = "002010000000000"  }
                        }

                    }

                },

               ApplicationUsersComapnies = new List<ApplicationUserComapny>()
               {
                   new ApplicationUserComapny()
                   {
                       ApplicationUserId = uid,
                   }
               }
            };
           

            db.Companies.Add(company);
            db.SaveChanges();

            CompaniesVm co = new CompaniesVm()
            {
                id = company.CompanyName,
                PublicId = company.CompanyId,
                name = company.CompanyName,
               
            };

            Result.AddMainObject(co);
            return Result;
        }
        public GeneralReturnResult AddBranch(int companyId, string branchName)
        {
            var Result = new GeneralReturnResult();
   
            var maxBranch = db.Companies.SingleOrDefault(a => a.CompanyId == companyId && a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)).MaxBranch;

            var checkBranchCount = db.Branchs.Where(a => a.CompanyId == companyId && a.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)).Count();
            if (checkBranchCount >= maxBranch)
            {
                Result.addError("لقد تجاوزت الحد الاقصي من الفروع المسموح لك باضافتها");
                return Result;
            };

            var cName = "فرع" + this.GetUniqueKey(4);
            do
            {
                cName = "فرع" + this.GetUniqueKey(4);
            } while (db.Branchs.Any(c => c.BranchName == cName && c.CompanyId == companyId && c.Company.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid)));

            var nowdate = DateTime.Now.Date;
            var enddate = nowdate.AddYears(1);
            var company = new Branch()
            {
                BranchName = cName,
                CurrentWorkerId = uid,
                CompanyId = companyId,

                BranchProperties = new List<BranchProperties>()
                {
                    new BranchProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "عنوان").AccountsCatogeryTypesAcccountsId,
                        propertyName = "العنوان",
                        CurrentWorkerId = uid,
                        BranchPropertyValues = new List<BranchPropertyValues>()
                        {
                            new BranchPropertyValues() {CurrentWorkerId = uid , propertyValue = "عنوان الفرع هنا"  }
                        }

                    },

                    new BranchProperties()
                    {
                        AccountsCatogeryTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(t => t.TypeName == "هاتف ارضي").AccountsCatogeryTypesAcccountsId,
                        propertyName = "هاتف",
                        CurrentWorkerId = uid,
                        BranchPropertyValues = new List<BranchPropertyValues>()
                        {
                            new BranchPropertyValues() {CurrentWorkerId = uid , propertyValue = "002010000000000"  }
                        }

                    }

                },


            };


            db.Branchs.Add(company);
            db.SaveChanges();

            BranchesVm co = new BranchesVm()
            {
                id = company.BranchName,
                PublicId = company.BranchId,
                name = company.BranchName
            };
            Result.AddMainObject(co);
            return Result;
        }
        public IEnumerable<settingRolesForUser> GetUserRole(bool byRefrenceType, string typeName, string name)
        {
      
            var getrole = db.settingRolesForUser.Where(s => s.settingPropertiesforUser.settingProperties.name.ToUpper() == name.ToUpper()
                                                              && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == typeName);


            return getrole;
        }

        public IEnumerable<CompaniesVm> GetAllCompanies(string userId)
        {

            var getRole = GetUserRole(true, "Company", "Company");
            var getRoleBranch = GetUserRole(true, "Branch", "Branch");

            var getCompany = db.Companies.Where(a => getRole.Any(r => r.RefrenceId == a.CompanyId))
                .Select(i => new CompaniesVm
                {
                    id = i.CompanyName,
                    name = i.CompanyName,
                    CurrentWorkerId = i.CurrentWorker.Email,
                    PublicId = i.CompanyId,
                    Branches = i.Branchs.Where(b => b.CompanyId == i.CompanyId).ToList().Where(a => getRoleBranch.Any(r => r.RefrenceId == a.BranchId))
                    .Select(b => new BranchesVm
                    {
                        id = b.BranchName,
                        name = b.BranchName,
                        PublicId = b.BranchId,
                        CompaniesId = i.CompanyId,

                    })
                });

            return getCompany;

        }


        public GeneralReturnResult updateCompanyName(int comapnyId, string name)
        {
            var Result = new GeneralReturnResult();

            var getRole = GetUserRole(true, "Company", "Company");
            var cCom = db.Companies.SingleOrDefault(a => a.CompanyId == comapnyId && getRole.Any(c => c.RefrenceId == comapnyId));
            if (cCom == null)
            {
                Result.addError("الشركة غير مسجلة او غير متاحة لهذة العملية");
                return Result;
            }
            var checkForName = db.Companies.Any(a => a.CompanyName == name && a.CompanyId != cCom.CompanyId);
            if (checkForName)
            {
                Result.addError("توجد شركة مسجلة بنفس الاسم");
                return Result;
            }

            cCom.CompanyName =name;
            cCom.CurrentWorkerId = uid;
            db.Entry(cCom).State = EntityState.Modified;
            db.SaveChanges();
            Result.success = "تم تعديل الاسم";
            return Result;
        }

        public GeneralReturnResult UpdateBranchName(int branchId, string name)
        {
            var Result = new GeneralReturnResult();

            var getRole = GetUserRole(true, "Branch", "Branch");
            var cCom = db.Branchs.SingleOrDefault(a => a.BranchId == branchId && getRole.Any(c => c.RefrenceId == a.BranchId));
            if (cCom == null)
            {
                Result.addError("الفرع غير مسجل او غير متاح لهذة العملية");
                return Result;
            }
            var checkForName = db.Branchs.Any(a => a.BranchName == name && a.CompanyId != cCom.CompanyId);
            if (checkForName)
            {
                Result.addError("يوجد فرع مسجل بنفس الاسم");
                return Result;
            }

            cCom.BranchName = name;
            cCom.CurrentWorkerId = uid;
            db.Entry(cCom).State = EntityState.Modified;
            db.SaveChanges();
            Result.success = "تم تعديل الاسم";
            return Result;
        }

        #region To use when creating New Company
        public IEnumerable<BalanceSheet> SetChart(string uid)
        {
            var typeIdD = db.AccountTypes.FirstOrDefault(e => e.AccountTypeName == "مدين").AccountTypeId; //1

            var typeIdC = db.AccountTypes.FirstOrDefault(e => e.AccountTypeName == "دائن").AccountTypeId; //2

            var other = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "other").Id;
            var inventory = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "inventory").Id;
            var goodstransit = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "goods-in-transit").Id;
            var cust = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "cust").Id;
            var sub = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "pur").Id;
            var notereceive = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "notereceive").Id;
            var notepay = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "notepay").Id;
            var sales = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "sales").Id;
            var cash = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "cash").Id;
            var bank = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "bank").Id;
            var costsales = db.AccountsDataTypes.FirstOrDefault(a => a.aliasName == "cost-sales").Id;

            var assets = db.BsMainType.FirstOrDefault(a => a.NickName == "assets").BsMainTypeId;
            var liabilities = db.BsMainType.FirstOrDefault(a => a.NickName == "liabilities").BsMainTypeId;
            var expenses = db.BsMainType.FirstOrDefault(a => a.NickName == "expenses").BsMainTypeId;
            var revenues = db.BsMainType.FirstOrDefault(a => a.NickName == "revenues").BsMainTypeId;

            var bsList = new List<BalanceSheet>()
            {
                new BalanceSheet(){
                    BalanceSheetName = "اصول",
                    BsMainTypeId = assets,
                    AccountTypeId = typeIdD,
                    Code = "1",
                    CurrentWorkerId = uid,
                    BalanceSheetTypes = new List<BalanceSheetType>()
                    {
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "اصول ثابتة" , Code = "11" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "الأراضي",Code = "111" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مباني وإنشاءات ومرافق وطرق",Code = "112" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "آلات وتجهيزات",Code = "113" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "وسائل نقل وانتقال",Code = "114" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "عدد وأدوات وقوالب",Code = "115" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أثاث ومعدات مكاتب",Code = "116" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "نفقات إيرادية مؤجلة",Code = "117" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اصول غير ملموسة",Code = "118" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }

                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "مشاريع تحت التنفيذ" , Code = "12" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "الأراضي",Code = "121" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مباني وإنشاءات ومرافق وطرق",Code = "122" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "آلات وتجهيزات",Code = "123" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "وسائل نقل وانتقال",Code = "124" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "عدد وأدوات وقوالب",Code = "125" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أثاث ومعدات مكاتب",Code = "126" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                            }
                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "المخزون" , Code = "13" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory()
                                {
                                    AccountCategoryName = "مخازن البضائع",Code = "131" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = inventory,

                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "المخزن الرئيسي" , Activated = true , Code = "1311" , CurrentWorkerId = uid}
                                    }
                                },
                                new AccountCategory() {AccountCategoryName = "بضائع بالطريق",Code = "132" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = goodstransit},
                                new AccountCategory() {AccountCategoryName = "مخازن مواد وموجودات مختلفة",Code = "133" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = inventory},
                                new AccountCategory() {AccountCategoryName = "اعتمادات لشراء المواد والبضائع",Code = "134" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "اقراض طويل الآجل" , Code = "14" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "قراض محلي طويل الآجل",Code = "141" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اقراض خارجي طويل الآجل",Code = "142" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "مدينون" , Code = "15" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory()
                                {
                                    AccountCategoryName = "عملاء",Code = "151" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = cust,
                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "نقدي/فوري" , Activated = true , Code = "1511" , CurrentWorkerId = uid}
                                    }
                                },
                                new AccountCategory() {AccountCategoryName = "اوراق قبض",Code = "152" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = notereceive},
                                new AccountCategory() {AccountCategoryName = "تأمينات وسلف",Code = "153" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "حسابات مدينة مختلفة" , Code = "16" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "مدينون مختلفون",Code = "161" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ايرادات جارية وتخصيصية مستحقة",Code = "162" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مصروفات جارية مدفوعه مقدماً",Code = "163" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},


                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "أموال جاهزة" , Code = "17" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory()
                                {
                                    AccountCategoryName = "الصندوق",Code = "171" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = cash,
                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "الصندوق الرئيسي" , Activated = true , Code = "1711" , CurrentWorkerId = uid }
                                    }
                                },
                                new AccountCategory() {AccountCategoryName = "البنك",Code = "172" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = bank},
                            }
                        },

                    }

                },
                new BalanceSheet()
                {
                    BalanceSheetName = "خصوم"  ,
                    BsMainTypeId = liabilities,
                    AccountTypeId = typeIdC,
                    Code = "2",
                    CurrentWorkerId = uid,
                    BalanceSheetTypes = new List<BalanceSheetType>()
                    {
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "رأس المال" , Code = "21" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory()
                                {
                                    AccountCategoryName = "رأس المال المدفوع",Code = "211" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other,
                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "رأسمال الشريك (1)" , Activated = true , Code = "2111" , CurrentWorkerId = uid }
                                    }
                                },
                                new AccountCategory()
                                {
                                    AccountCategoryName = "الأرباح والخسائر",Code = "212" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other,
                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "أرباح وخسائر مرحلة" , Activated = true , Code = "2121" , CurrentWorkerId = uid },
                                        new Account(){AccountName = "أرباح وخسائر الفترة" , Activated = true , Code = "2122" , CurrentWorkerId = uid }
                                    }
                                },

                            }

                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "احتياطيات" , Code = "22" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "احتياطى قانوني",Code = "221" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "احتياطى اختيارى",Code = "222" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "فائض مرحل",Code = "223" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other}
                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "مجمع الإهلاكات" , Code = "23" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "اهلاكات المباني والانشاءات",Code = "231" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اهلاكات الآلات والمعدات",Code = "232" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اهلاك وسائل النقل",Code = "233" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اهلاك العدد والأدوات والقوالب",Code = "234" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اهلاك الأثاث ومعدات المكاتب",Code = "235" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "المخصصات" , Code = "24" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "مخصص ضرائب ورسوم متنازع عليها",Code = "241" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مخصص الديون المشكوك في تحصيلها",Code = "242" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "قروض طويلة الأجل" , Code = "25" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "قروض طويلة محلية",Code = "251" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "قروض خارجية",Code = "252" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},


                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "دائنون" , Code = "26" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory()
                                {
                                    AccountCategoryName = "موردون",Code = "261" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = sub,
                                    Accounts = new List<Account>()
                                    {
                                        new Account(){AccountName = "نقدي/فوري" , Activated = true , Code = "2611" , CurrentWorkerId = uid }
                                    }
                                },
                                new AccountCategory() {AccountCategoryName = "أوراق دفع",Code = "262" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = notepay},
                                new AccountCategory() {AccountCategoryName = "دائنون متنوعون",Code = "263" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "حسابات دائنة مختلفة" , Code = "27" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "مصروفات جارية وتخصيصية مستحقة",Code = "271" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ايرادات جارية مقبوضة مقدما",Code = "272" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},

                            }
                        },

                    }
                },
                new BalanceSheet()
                {
                    BalanceSheetName = "مصروفات" ,
                    BsMainTypeId = expenses,
                    AccountTypeId = typeIdD,
                    Code = "3",
                    CurrentWorkerId = uid,
                    BalanceSheetTypes = new List<BalanceSheetType>()
                    {
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "الأجور" , Code = "31" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "أجور نقدية",Code = "311" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مزايا عينية",Code = "312" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "المستلزمات السلعية" , Code = "32" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "وقود وزيوت وقوي محركة للتشغيل",Code = "321" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "قطع غيار وأدوات صغيرة",Code = "322" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مواد تعبئة وتغليف",Code = "323" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أدوات كتابية ومطبوعات",Code = "324" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType(){
                            BalanceSheetTypeName = "مستلزمات خدمية وتوريدات خارجية" , Code = "33" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "مصروفات الصيانة الخارجية",Code = "331" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مصروفات تشغيل لدي الغير",Code = "332" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "دعاية واعلان وعلاقات عامة",Code = "333" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "نقل وانتقال",Code = "334" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "الإنارة والمياه",Code = "335" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "بريد وبرق وهاتف وتلكس",Code = "336" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "دعاية واعلان وعلاقات عامة",Code = "337" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مصروفات خدمية متنوعة",Code = "338" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "مصروفات البرامج والمواقع",Code = "339" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "صافي المشتريات" , Code = "34" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "إجمالي مشتريات بغرض البيع",Code = "341" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId =other},
                                new AccountCategory() {AccountCategoryName = "مردودات المشتريات",Code = "342" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId =other},
                                new AccountCategory() {AccountCategoryName = "مسموحات المشتريات",Code = "343" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId =other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "المصروفات التحويلية الجارية" , Code = "35" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "الضرائب والرسوم السلعية",Code = "351" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أعباء الأهتلاك",Code = "352" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "إيجارات فعليه",Code = "353" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "فوائد محلية",Code = "354" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "فوائد خارجية",Code = "355" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "خسائر الفترة الحالية",Code = "356" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "المصروفات الجارية التخصيصية" , Code = "36" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "تبرعات",Code = "361" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "اعانات للغير",Code = "362" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "تعويضات للغير وغرامات",Code = "363" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "خسائر رأسمالية",Code = "364" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ديون معدومة",Code = "365" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ضريبة دخل الأرباح",Code = "366" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = other},

                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "تكلفة المبيعات" , Code = "37" , AccountTypeId = typeIdD , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "تكلغة مبيعات بضائع",Code = "371" , CurrentWorkerId = uid ,AccountTypeId = typeIdD, AccountsDataTypesId = costsales},

                            }
                        },

                    }
                },
                new BalanceSheet()
                {
                    BalanceSheetName = "ايرادات",
                    BsMainTypeId = revenues,
                    AccountTypeId = typeIdC,
                    Code = "4",
                    CurrentWorkerId = uid,
                    BalanceSheetTypes = new List<BalanceSheetType>()
                    {
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "ايرادات تشغيل للغير" , Code = "41" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                           AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "صافي ايراد تشغيل للغير",Code = "411" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},


                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "خدمات مباعة" , Code = "42" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "صافي خدمات مباعة",Code = "421" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},


                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "ايرادات بضائع بغرض البيع" , Code = "43" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "صافي مبيعات بضائع بغرض البيع",Code = "431" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                            }
                        },
                        new BalanceSheetType()
                        {
                            BalanceSheetTypeName = "ايرادات تحويلية" , Code = "44" , AccountTypeId = typeIdC , CurrentWorkerId = uid,
                            AccountCategories = new List<AccountCategory>()
                            {
                                new AccountCategory() {AccountCategoryName = "ايراد أوراق مالية",Code = "441" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "فوائد دائنة",Code = "442" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ايجارات دائنة",Code = "443" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أرباح رأسمالية",Code = "444" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ايرادات السنوات السابقة",Code = "445" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "تعويضات وغرامات على الغير",Code = "446" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "ايرادات متنوعة",Code = "447" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},
                                new AccountCategory() {AccountCategoryName = "أرباح السنة الحالية",Code = "448" , CurrentWorkerId = uid ,AccountTypeId = typeIdC, AccountsDataTypesId = other},

                            }
                        },

                    }
                }
            };

            return bsList;

        }

        public IEnumerable<FinancialList> SetFinancialLists(string uid)
        {
            var assets = SetChart(uid).SingleOrDefault(a => a.BsMainType.NickName == "assets");
            var liabilities = SetChart(uid).SingleOrDefault(a => a.BsMainType.NickName == "liabilities");
            var expenses = SetChart(uid).SingleOrDefault(a => a.BsMainType.NickName == "expenses");
            var revenues = SetChart(uid).SingleOrDefault(a => a.BsMainType.NickName == "revenues");

            var list1 = new FinancialList()
            {
                FinancialListName = "قائمة المركز المالي",
                CustomBalanceSheets = new List<CustomBalanceSheet>()
                {
                    new CustomBalanceSheet()
                    {
                        BalanceSheetName = "صـافي الاصـول",
                        AccountTypeId = assets.AccountTypeId,
                        BalanceSheetCode = "1",
                        CurrentWorkerId = uid,
                        CustomBalanceSheetTypes = assets.BalanceSheetTypes.Select((bst , bstIndex)=> new CustomBalanceSheetType()
                        {
                            BalanceSheetTypeName = bst.BalanceSheetTypeName,
                            BalanceSheetTypeCode = 1 + (bstIndex + 1).ToString(),
                            AccountTypeId = bst.AccountTypeId,
                            CurrentWorkerId = uid,
                            CustomAccountCategories = bst.AccountCategories.Select((acc, accIndex) => new CustomAccountCategory
                            {
                                AccountCategoryName = acc.AccountCategoryName,
                                AccountTypeId = acc.AccountTypeId,
                                CurrentWorkerId = uid,
                                AccountCategoryCode = (1 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString(),
                                CustomAccount = acc.Accounts.Select((ac , acIndex) => new CustomAccount
                                {
                                    AccountName = ac.AccountName,
                                    Code =Convert.ToInt32(((1 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString()).ToString() + (acIndex + 1).ToString()),
                                    CurrentWorkerId = uid,
                                    AccountTypeId = acc.AccountTypeId,
                                    AccountId = ac.AccountId
                                })
                            })

                        })

                    },
                    new CustomBalanceSheet()
                    {
                        BalanceSheetName = "صـافي الخـصوم",
                        AccountTypeId = liabilities.AccountTypeId,
                        BalanceSheetCode = "2",
                        CurrentWorkerId = uid,
                        CustomBalanceSheetTypes = liabilities.BalanceSheetTypes.Select((bst , bstIndex)=> new CustomBalanceSheetType()
                        {
                            BalanceSheetTypeName = bst.BalanceSheetTypeName,
                            BalanceSheetTypeCode = 2 + (bstIndex + 1).ToString(),
                            AccountTypeId = bst.AccountTypeId,
                            CurrentWorkerId = uid,
                            CustomAccountCategories = bst.AccountCategories.Select((acc, accIndex) => new CustomAccountCategory
                            {
                                AccountCategoryName = acc.AccountCategoryName,
                                AccountTypeId = acc.AccountTypeId,
                                CurrentWorkerId = uid,
                                AccountCategoryCode = (2 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString(),
                                CustomAccount = acc.Accounts.Select((ac , acIndex) => new CustomAccount
                                {
                                    AccountName = ac.AccountName,
                                    Code =Convert.ToInt32(((2 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString()).ToString() + (acIndex + 1).ToString()),
                                    CurrentWorkerId = uid,
                                    AccountTypeId = acc.AccountTypeId,
                                    AccountId = ac.AccountId
                                })
                            })

                        })

                    }
                }
            };
            var list2 = new FinancialList()
            {
                FinancialListName = "قائمة الدخل",
                CustomBalanceSheets = new List<CustomBalanceSheet>()
                {
                    new CustomBalanceSheet()
                    {
                        BalanceSheetName = "صـافي الإيرادات",
                        AccountTypeId = revenues.AccountTypeId,
                        BalanceSheetCode = "3",
                        CurrentWorkerId = uid,
                        CustomBalanceSheetTypes = revenues.BalanceSheetTypes.Select((bst , bstIndex)=> new CustomBalanceSheetType()
                        {
                            BalanceSheetTypeName = bst.BalanceSheetTypeName,
                            BalanceSheetTypeCode = 3 + (bstIndex + 1).ToString(),
                            AccountTypeId = bst.AccountTypeId,
                            CurrentWorkerId = uid,
                            CustomAccountCategories = bst.AccountCategories.Select((acc, accIndex) => new CustomAccountCategory
                            {
                                AccountCategoryName = acc.AccountCategoryName,
                                AccountTypeId = acc.AccountTypeId,
                                CurrentWorkerId = uid,
                                AccountCategoryCode = (3 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString(),
                                CustomAccount = acc.Accounts.Select((ac , acIndex) => new CustomAccount
                                {
                                    AccountName = ac.AccountName,
                                    Code =Convert.ToInt32(((3 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString()).ToString() + (acIndex + 1).ToString()),
                                    CurrentWorkerId = uid,
                                    AccountTypeId = acc.AccountTypeId,
                                    AccountId = ac.AccountId
                                })
                            })

                        })

                    },
                    new CustomBalanceSheet()
                    {
                        BalanceSheetName = "صـافي المصروفات",
                        AccountTypeId = expenses.AccountTypeId,
                        BalanceSheetCode = "3",
                        CurrentWorkerId = uid,
                        CustomBalanceSheetTypes = expenses.BalanceSheetTypes.Select((bst , bstIndex)=> new CustomBalanceSheetType()
                        {
                            BalanceSheetTypeName = bst.BalanceSheetTypeName,
                            BalanceSheetTypeCode = 3 + (bstIndex + 1).ToString(),
                            AccountTypeId = bst.AccountTypeId,
                            CurrentWorkerId = uid,
                            CustomAccountCategories = bst.AccountCategories.Select((acc, accIndex) => new CustomAccountCategory
                            {
                                AccountCategoryName = acc.AccountCategoryName,
                                AccountTypeId = acc.AccountTypeId,
                                CurrentWorkerId = uid,
                                AccountCategoryCode = (3 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString(),
                                CustomAccount = acc.Accounts.Select((ac , acIndex) => new CustomAccount
                                {
                                    AccountName = ac.AccountName,
                                    Code =Convert.ToInt32(((3 + (bstIndex + 1).ToString()).ToString() + (accIndex + 1).ToString()).ToString() + (acIndex + 1).ToString()),
                                    CurrentWorkerId = uid,
                                    AccountTypeId = acc.AccountTypeId,
                                    AccountId = ac.AccountId
                                })
                            })

                        })

                    }
                }
            };

            List<FinancialList> all = new List<FinancialList>();
            all.Add(list1);
            all.Add(list2);

            return all;
        }
        #endregion
      
    }
}
