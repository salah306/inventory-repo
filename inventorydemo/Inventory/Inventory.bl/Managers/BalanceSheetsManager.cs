﻿
using Inventory.bl.DTO;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.Managers
{
    public class BalanceSheetsManager :ManagersHelper
    {


        public BalanceSheetsManager(IOwinContext context)
           : base(context)
        {

        }

        public IQueryable<BranchTree> GetAllBalanceSheets()
        {
            var bc = from b in db.Companies
                     select new BranchTree()
                     {
                         id = b.CompanyId.ToString(),
                         text = b.CompanyName,
                         children = (from i in db.BalanceSheets
                                     where i.CompanyId == b.CompanyId
                                     select new BalanceSheetTree()
                                     {
                                         id = i.BalanceSheetId.ToString(),
                                         text = i.BalanceSheetName,
                                         children = (from bt in db.BalanceSheetTypes
                                                     where bt.BalanceSheetId == i.BalanceSheetId
                                                     select new BalanceSheetTypeTree()
                                                     {
                                                         id = bt.BalanceSheetTypeId.ToString(),
                                                         text = bt.BalanceSheetTypeName,
                                                         children = (from ac in db.AccountCategories
                                                                     where ac.BalanceSheetTypeId == bt.BalanceSheetTypeId
                                                                     select new AccountCategoryTree()
                                                                     {
                                                                         id = ac.AccountCategoryName,
                                                                         text = ac.AccountCategoryName

                                                                     })

                                                     })

                                     })
                     };


            return bc;
        }

        public IQueryable<BalanceSheetTypesVm> GetAllBalanceSheetTypes()
        {

            var balanceSheetTypes = from i in db.BalanceSheetTypes
                                    select new BalanceSheetTypesVm()
                                    {
                                        id = i.BalanceSheetTypeName,
                                        name = i.BalanceSheetTypeName,
                                        BalanceSheetsId = i.BalanceSheet.BalanceSheetName,
                                        PublicId = i.BalanceSheetTypeId

                                    };

            return balanceSheetTypes;
        }

        public IQueryable<AccountCategoryPropertiesVM> GetAccountCatogeryProperties(Int32 accountCatogeryId)
        {
            
            var dt = (from d in db.AccountCategoryProperties
                      select new AccountCategoryPropertiesVM()
                      {
                          Id = d.AccountCategoryPropertiesId,
                          Name = d.AccountCategoryPropertiesName,
                         
                      }
                      );
            return dt;
        }

        public IQueryable<AccountsCatogeryDataTypes> GetAllDataTypes(SetAccountsCatogeryDataTypes cat)
        {
        
            var dataty = (from c in db.AccountsDataTypes
                          select new AccountsDataTypesVm
                          {
                              Id = c.Id,
                              TypeName = c.TypeName,
                              IsTrue = db.AccountCategories.Any(a => a.AccountCategoryId == cat.accountId && a.AccountsDataTypesId == c.Id)
                          });

            Int32 btypId = Convert.ToInt32(ConvertToId(cat.BalanceSheetTypeId));
            var dt = (from d in db.AccountCategories
                      where d.AccountCategoryId == cat.accountId
                      select new AccountsCatogeryDataTypes()
                      {
                          Id = d.AccountCategoryId,
                          CatogeryName = d.AccountCategoryName,
                          AccountsDataTypes = dataty.ToList()

                      });
            return dt;
        }
        public IQueryable<AccountCategoryPropertiesValuesVM> GetAccountCatogeryPropertiesValue(Int32 accountId)
        {
          
            var accid = db.Accounts.Find(accountId).AccountName;
            var dt = (from d in db.AccountCategoryProperties
                      select new AccountCategoryPropertiesValuesVM()
                      {
                          Id = d.AccountCategoryPropertiesId,
                          PropertName = d.AccountCategoryPropertiesName,
                          AccountId = accountId,
                          AccountName = accid,
                          Values = (from v in db.AccountCategoryPropertiesValues
                                    where v.AccountCategoryPropertiesId == d.AccountCategoryPropertiesId
                                    && v.AccountId == accountId
                                    select new AccPropertiesValuesVM()
                                    {
                                        Id = v.AccountCategoryPropertiesValueId,
                                        value = v.AccountCategoryPropertiesValueName
                                    }).ToList()
                      });
            var cc = dt;
            return dt;
        }

        public IEnumerable<AccountsTableFlatten> bsTree(Int32 companyId)
        {
            //IQueryable<AccountsTable> ListAccountsTable;
            Int32 indexp = 0;
            var tree =
            db.BalanceSheets.Where(b => b.CompanyId == companyId).ToList().OrderBy(cd => cd.CreatedDate).Select((bs, bsIndex) => new AccountsTableFlatten()
            {
                Id = bs.BalanceSheetId + "Bs",
                AccountLevel = "رئيسي",
                AccountName = bs.BalanceSheetName,
                TitleForAdd = "اضافة حـ عام",
                Code = bs.Code,
                ParentId = bs.CompanyId.ToString(),
                AccountType = bs.AccountType.AccountTypeName,
                indexNO = indexp++,
                Childern = bs.BalanceSheetTypes.OrderBy(cd => cd.CreatedDate).Select((bst, bstIndex) => new AccountsTableFlatten()
                {
                    Id = bst.BalanceSheetTypeId + "Bst",
                    AccountLevel = "عام",
                    AccountName = bst.BalanceSheetTypeName,
                    TitleForAdd = "اضافة حـ مساعد",
                    ParentId = bs.BalanceSheetId + "Bs",
                    AccountType = bst.AccountType.AccountTypeName,
                    Code = bst.Code,
                    indexNO = indexp + 2,
                    Childern = bst.AccountCategories.OrderBy(cd => cd.CreatedDate).Select((acc, accIndex) => new AccountsTableFlatten()
                    {
                        Id = acc.AccountCategoryId + "Acc",
                        AccountLevel = "مساعد",
                        AccountName = acc.AccountCategoryName,
                        AccountType = acc.AccountType.AccountTypeName,
                        TitleForAdd = "اضافة حـ فرعي",
                        Code = acc.Code,
                        ParentId = bst.BalanceSheetTypeId + "Bst",
                        AccountsDataTypesId = (Int32)acc.AccountsDataTypesId,
                        AccountsDataTypesName = acc.AccountsDataTypes.TypeName,
                        indexNO = indexp + 3,
                        Childern = acc.Accounts.OrderBy(cd => cd.CreatedDate).Select((ac, acIndex) => new AccountsTableFlatten()
                        {
                            Id = ac.AccountId + "Ac",
                            AccountLevel = "فرعي",
                            TitleForAdd = "اضافة حـ جزئي",
                            AccountName = ac.AccountName,
                            Code = ac.Code,
                            ParentId = acc.AccountCategoryId + "Acc",
                            linkedAccountId = ac.Branch == null ? bs.CompanyId : ac.BranchId,
                            linkedAccountName = ac.Branch == null ? bs.Company.CompanyName : ac.Branch.BranchName,
                            isCompany = ac.Branch == null ? true : false,
                            Activated = ac.Activated,
                            indexNO = indexp +4,
                            Childern = ac.SubAccounts.OrderBy(cd => cd.CreatedDate).Select((sac, sacIndex) => new AccountsTableFlatten()
                            {
                                Id = sac.SubAccountId + "Sac",
                                AccountLevel = "جزئي",
                                AccountName = sac.SubAccountName,
                                Code = sac.Code,
                                ParentId = ac.AccountId + "Ac",
                                indexNO = indexp + 1, 
                            })
                        })
                    })
                })
            }).Flatten(myObject => myObject.Childern)
        .ToList();

            var trre3 = tree.ToList();


            var trre4 = tree.OrderBy(cod => cod.Code).ToList();
            var trrsse4 = tree.OrderBy(cod => cod.indexNO).ToList();
            var trr333 = tree.GroupBy(a => a.ParentId).SelectMany(a => a.Key);
            var trre6 = tree.OrderBy(cod => cod.Id).ToList();
            var trre7 = tree.OrderBy(cod => cod.ParentId).ToList();

            var trre5 = tree.OrderBy(cod => cod.ParentId == cod.ParentId).ToList();

            var trre66 = tree.OrderByDescending(cod => cod.ParentId).ToList();

            var trr3ss33 = tree.GroupBy(a => a.ParentId).SelectMany(a => a);


            return tree;
        }
        public IQueryable<BranchTree> GetAllFinancialListTree(Int32 FinancialListId)
        {

     
            //  var roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new ApplicationDbContext()));
            //  var admin = roleManager.FindByName("Admin");
            // var notInThisRole = db.ApplicationUsersComapnies.Where(m => m.ApplicationUser.Roles == admin).ToList();
            var balanceSheets = from b in db.FinancialLists
                                where b.FinancialListId == FinancialListId
                                select new BranchTree()
                                {
                                    id = b.FinancialListId.ToString(),
                                    text = b.FinancialListName,
                                    children = (from i in db.CustomBalanceSheets
                                                where i.FinancialListId == b.FinancialListId
                                                select new BalanceSheetTree()
                                                {
                                                    id = i.CustomBalanceSheetId.ToString() + "CBs",
                                                    text = i.BalanceSheetName,
                                                    children = (from bt in db.CustomBalanceSheetTypes
                                                                where bt.CustomBalanceSheetId == i.CustomBalanceSheetId
                                                                select new BalanceSheetTypeTree()
                                                                {
                                                                    id = bt.CustomBalanceSheetTypeId.ToString() + "CBst",
                                                                    text = bt.BalanceSheetTypeName,
                                                                    children = (from ac in db.CustomAccountCategories
                                                                                where ac.CustomBalanceSheetTypeId == bt.CustomBalanceSheetTypeId
                                                                                select new AccountCategoryTree()
                                                                                {
                                                                                    id = ac.CustomAccountCategoryId + "CAcc",
                                                                                    text = ac.AccountCategoryName,
                                                                                    children = (from at in db.CustomAccounts
                                                                                                where at.CustomAccountCategoryId == ac.CustomAccountCategoryId
                                                                                                select new AccountTree()
                                                                                                {
                                                                                                    id = at.CustomAccountId.ToString() + "CAc",
                                                                                                    text = at.Account.AccountName

                                                                                                }).ToList()

                                                                                }).ToList()

                                                                }).ToList()

                                                }).ToList()
                                };
            var gg = balanceSheets;

            return balanceSheets;
        }

        public List<Alllists> fListes(isCompanyBranch set)
        {
            Int32 id = 0;
            if (set.IsCompany)
            {
                Company financialList = db.Companies.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return null;
                }
            }
            else
            {
                Branch financialList = db.Branchs.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return null;
                }
            }


            List<GetFinancialList> ftree = new List<GetFinancialList>();

            var cac = (from c in db.CustomAccounts where c.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id select new FListes { Id = c.CustomAccountId + "Ac", Show = false, Name = c.Account.AccountName, Level = 5, ParentId = c.CustomAccountCategoryId + "Acc", LevelName = "فرعي", ParentName = c.CustomAccountCategory.AccountCategoryName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var cacc = (from c in db.CustomAccountCategories select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomAccountCategoryId + "Acc", Show = false, childern = c.CustomAccount.Count(), Name = c.AccountCategoryName, Level = 4, AddTitle = "اضافة حسابات فرعية", Addlink = "ربط مع حساب مساعد", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "مساعد", ParentId = c.CustomBalanceSheetTypeId + "Bst", ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bst = ((from c in db.CustomBalanceSheetTypes select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetTypeId + "Bst", Show = false, childern = c.CustomAccountCategories.Count(), Name = c.BalanceSheetTypeName, Level = 3, AddTitle = "اضافة حساب مساعد", Addlink = "ربط مع حساب عام", LevelName = "عام", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, ParentId = c.CustomBalanceSheetId + "Bs", ParentName = c.CustomBalanceSheet.BalanceSheetName })).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bs = (from c in db.CustomBalanceSheets select c).OrderBy(r => r.CreatedDate).Where(c => c.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetId + "Bs", Name = c.BalanceSheetName, Show = true, accountType = c.AccountType.AccountTypeName, Level = 2, childern = c.CustomBalanceSheetTypes.Count(), AddTitle = "اضافة حساب عام", Addlink = "ربط مع حساب رئيسي", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "رئيسي", ParentId = c.FinancialListId + "F", ParentName = c.FinancialList.FinancialListName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var fl = (from c in db.FinancialLists where c.CompanyId == id orderby c.CreatedDate select new FListes { Id = c.FinancialListId + "F", forLevel = c.FinancialListId + "F", Name = c.FinancialListName, Level = 1, Show = true, ParentId = c.BranchId.ToString(), childern = c.CustomBalanceSheets.Count(), ParentName = c.Branch.BranchName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();

            List<FListes> listes = new List<FListes>();

            foreach (var mainList in fl)
            {
                foreach (var f in mainList)
                {
                    listes.Add(f);
                    foreach (var balancesheet in bs)
                    {
                        foreach (var customBS in balancesheet)
                        {
                            if (customBS.ParentId == f.Id)
                            {
                                customBS.forLevel = f.Id;
                                listes.Insert(listes.IndexOf(f), customBS);
                                foreach (var Bstype in bst)
                                {
                                    foreach (var CustomBst in Bstype)
                                    {
                                        if (CustomBst.ParentId == customBS.Id)
                                        {
                                            CustomBst.forLevel = f.Id;
                                            listes.Insert(listes.IndexOf(customBS), CustomBst);
                                            foreach (var CustomAccountc in cacc)
                                            {
                                                foreach (var CustomAcc in CustomAccountc)
                                                {
                                                    if (CustomAcc.ParentId == CustomBst.Id)
                                                    {
                                                        CustomAcc.forLevel = f.Id;
                                                        listes.Insert(listes.IndexOf(CustomBst), CustomAcc);
                                                        foreach (var Customacco in cac)
                                                        {
                                                            foreach (var CustomAc in Customacco)
                                                            {
                                                                if (CustomAc.ParentId == CustomAcc.Id)
                                                                {
                                                                    CustomAc.forLevel = f.Id;
                                                                    listes.Insert(listes.IndexOf(CustomAcc), CustomAc);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }



                        }
                    }
                }
            }
            var g = listes.GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
            List<Alllists> listes5 = new List<Alllists>();

            foreach (var l in g)
            {

                listes5.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, forWidget = false, Listes = l });
            }

            foreach (var l in listes5)
            {
                l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
            }

            return listes5;
        }
        public string GetBstType(Int32 accountId)
        {
            var ac = db.BalanceSheetTypes.Find(accountId);
            if (ac != null)
            {
                if (ac.AccountType != null)
                {
                    return ac.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheet.AccountType != null)
                {
                    return ac.BalanceSheet.AccountType.AccountTypeName;
                }

                else
                {
                    return ac.BalanceSheet.AccountType.AccountTypeName;

                }
            }
            else
            {
                return "";
            }


        }
        public string GetAccountCategoryType(Int32 accountId)
        {
            var ac = db.AccountCategories.Find(accountId);
            if (ac != null)
            {
                if (ac.AccountType != null)
                {
                    return ac.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheetType.AccountType != null)
                {
                    return ac.BalanceSheetType.AccountType.AccountTypeName;
                }
                else if (ac.AccountType == null && ac.BalanceSheetType.AccountType == null && ac.BalanceSheetType.BalanceSheet.AccountType != null)
                {
                    return ac.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                }
                else
                {
                    return ac.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;

                }
            }
            else
            {
                return "";
            }


        }
    }
    public static class LinqExtensions
    {
        /// <summary>
        ///   This method extends the LINQ methods to flatten a collection of 
        ///   items that have a property of children of the same type.
        /// </summary>
        /// <typeparam name = "T">Item type.</typeparam>
        /// <param name = "source">Source collection.</param>
        /// <param name = "childPropertySelector">
        ///   Child property selector delegate of each item. 
        ///   IEnumerable'T' childPropertySelector(T itemBeingFlattened)
        /// </param>
        /// <returns>Returns a one level list of elements of type T.</returns>
        public static IEnumerable<T> Flatten<T>(
            this IEnumerable<T> source,
            Func<T, IEnumerable<T>> childPropertySelector)
        {
            return source
                .Flatten((itemBeingFlattened, objectsBeingFlattened) =>
                         childPropertySelector(itemBeingFlattened));
        }

        /// <summary>
        ///   This method extends the LINQ methods to flatten a collection of 
        ///   items that have a property of children of the same type.
        /// </summary>
        /// <typeparam name = "T">Item type.</typeparam>
        /// <param name = "source">Source collection.</param>
        /// <param name = "childPropertySelector">
        ///   Child property selector delegate of each item. 
        ///   IEnumerable'T' childPropertySelector
        ///   (T itemBeingFlattened, IEnumerable'T' objectsBeingFlattened)
        /// </param>
        /// <returns>Returns a one level list of elements of type T.</returns>
        public static IEnumerable<T> Flatten<T>(
            this IEnumerable<T> source,
            Func<T, IEnumerable<T>, IEnumerable<T>> childPropertySelector)
        {
            return source
                .Concat(source
                            .Where(item => childPropertySelector(item, source) != null)
                            .SelectMany(itemBeingFlattened =>
                                        childPropertySelector(itemBeingFlattened, source)
                                            .Flatten(childPropertySelector)));
        }
    }


    public static class treeflat
    {

        //public static IEnumerable<T> Traverse<T>(this T root)
        //{
        //    var stack = new Stack<T>();
        //    stack.Push(root);
        //    while (stack.Count > 0)
        //    {
        //        var current = stack.Pop();
        //        yield return current;
        //        foreach (var child in current.)
        //            stack.Push(child);
        //    }
        //}
        //public static IEnumerable<T> Flatten<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> getChildren)
        //{
        //    var stack = new Stack<T>();
        //    foreach (var item in items)
        //        stack.Push(item);

        //    while (stack.Count > 0)
        //    {
        //        var current = stack.Pop();
        //        yield return current;

        //        var children = getChildren(current);
        //        if (children == null) continue;

        //        foreach (var child in children)
        //            stack.Push(child);
        //    }
        //}

    }
    public class AccountsTableFlatten
    {
        public string Id { get; set; }
        public string AccountName { get; set; }
        public string AccountLevel { get; set; }
        public string AccountType { get; set; }
        public string ParentId { get; set; }
        public string TitleForAdd { get; set; }
        public string Code { get; set; }
        public string fullCode { get; set; }
        public Int32? linkedAccountId { get; set; }
        public string linkedAccountName { get; set; }
        public bool isCompany { get; set; }
        public Int32 AccountsDataTypesId { get; set; }
        public bool Activated { get; set; }
        public string AccountsDataTypesName { get; set; }
        public IEnumerable<AccountsTableFlatten> Childern { get; set; }
        public Int32 indexNO { get; set; }
    }
}