﻿using Inventory.bl.DTO;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Managers
{
    public class AccountsManager : ManagersHelper
    {
        public AccountsManager(IOwinContext context)
           : base(context)
        {

        }

        public GetSetOrder GetOrder(GetSetOrder getOrder)
        {



            // isdebit mean : اذن صرف
           
            var order = db.AccountOrders.Find(getOrder.orderId);
            var firstAccount = db.Accounts.Find(getOrder.accountId);
            if (getOrder.isDebit)
            {
                var accType = firstAccount.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                if (accType == "دائن")
                {
                    getOrder.isDebit = false;
                }
            }
            else if (!getOrder.isDebit)
            {
                var accType = firstAccount.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName;
                if (accType == "دائن")
                {
                    getOrder.isDebit = true;
                }
            }
            var grder = new GetSetOrder()
            {
                branchId = getOrder.branchId,
                accountId = getOrder.accountId,
                accountName = getOrder.isDebit ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ",
                isDebit = getOrder.isDebit
            };
            if (order != null)
            {

                var isde = db.AccountMovements.Any(a => a.AccountOrderId == getOrder.orderId && a.AccountId == getOrder.accountId && a.Debit > 0);
                var checkCounts = GetAccountOrders(getOrder.branchId, getOrder.IsCompany).Where(a => a.orderId == getOrder.orderId).FirstOrDefault();
                if (!(checkCounts.crditorAccounts.Count > 1 && checkCounts.debitorAccounts.Count > 1))
                {
                    switch (isde)
                    {
                        case true:
                            grder.secondAccount = (from m in db.AccountMovements
                                                   where
           m.AccountOrderId == getOrder.orderId && m.Crdit > 0
                                                   select
new OrderAccounts
{
   amount = m.Crdit,
   account = new AccountObject()
   {
       id = m.AccountId,
       AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
       value = m.Accounts.AccountName,
       status = true

   },
   id = m.AccountOrderId
}).ToList();

                            grder.accountId = getOrder.accountId;
                            grder.accountName = isde ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ";
                            grder.tilte = "تعديل قيد اضافة رقم " + order.OrderNo;
                            grder.orderNo = order.OrderNo;
                            grder.orderId = order.AccountOrderId;
                            grder.orderDate = order.OrderDate;
                            grder.orderNote = order.OrderNote;
                            grder.isDebit = isde;
                            break;
                        case false:
                            grder.secondAccount = (from m in db.AccountMovements
                                                   where
                     m.AccountOrderId == getOrder.orderId && m.Debit > 0
                                                   select
new OrderAccounts
{
amount = m.Debit,
account = new AccountObject()
{
id = m.AccountId,
AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
value = m.Accounts.AccountName,
status = true

},
id = m.AccountOrderId
}).ToList();
                            grder.orderNo = order.OrderNo;
                            grder.accountId = getOrder.accountId;
                            grder.accountName = isde ? "المدين : " + firstAccount.AccountName + " - القيمة : " : "الدائن : " + firstAccount.AccountName + " - القيمة : ";
                            grder.orderNote = order.OrderNote;
                            grder.tilte = "تعديل قيد صرف رقم " + order.OrderNo;
                            grder.orderId = order.AccountOrderId;
                            grder.orderDate = order.OrderDate;
                            grder.isDebit = isde;
                            break;
                    }
                }

            }

            else
            {
                switch (getOrder.isDebit)
                {
                    case true:
                        grder.secondAccount = new List<OrderAccounts>() {

                            new OrderAccounts()
                            {
                                id = null,
                                amount = 0,
                                account = new AccountObject()
                                {
                                    id = 0,
                                    AccountCatogery = "",
                                    value = ""

                                }
                            }

                        };
                        if (getOrder.IsCompany)
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1).ToString();

                        }
                        else
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1).ToString();

                        }
                        grder.accountId = getOrder.accountId;

                        break;
                    case false:
                        grder.secondAccount = new List<OrderAccounts>() {

                            new OrderAccounts()
                            {
                                id = null,
                                amount = 0,
                                account = new AccountObject()
                                {
                                    id = 0,
                                    AccountCatogery = "",
                                    value = ""

                                }
                            }

                        };
                        if (getOrder.IsCompany)
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.CompanyId == getOrder.branchId && a.IsCompany == true).Max(a => a.OrderNo) + 1).ToString();

                        }
                        else
                        {
                            grder.orderNo = db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1;
                            grder.tilte = "انشاء قيد رقم" + (db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Count() == 0 ? 1 : db.AccountOrders.Where(a => a.BranchId == getOrder.branchId && a.IsCompany == false).Max(a => a.OrderNo) + 1).ToString();

                        }
                        grder.accountId = getOrder.accountId;

                        break;
                }
            }

            grder.IsCompany = getOrder.IsCompany;
            return grder;

        }
        public List<OrderVm> GetAccountOrders(Int32? branchId, bool isCompany)
        {
           

            List<OrderVm> orderlist = new List<OrderVm>();
            if (isCompany)
            {
                var orders = (from o in db.AccountOrders
                              where o.CompanyId == branchId
                              && o.IsCompany == true
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  IsCompany = true,
                                  branchId = o.BranchId,
                                  CompanyId = o.CompanyId,
                                  orderNote = o.OrderNote,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }
                   ).ToList();

                orderlist = orders;
            }
            else
            {
                var orders = (from o in db.AccountOrders
                              where o.BranchId == branchId
                              && o.IsCompany == false
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  IsCompany = false,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  CompanyId = o.CompanyId,
                                  orderNote = o.OrderNote,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }
           ).ToList();
                orderlist = orders;

            }
            return orderlist;
        }

        public List<AccountsMoves> GetAccountmoves(Int32 accountId, DateTime? mind, DateTime? maxd = null)
        {
     
            var checkformove = db.AccountMovements.Where(ac => ac.AccountId == accountId).FirstOrDefault();
            var sumer = 0m;
            var sumerDate = 0m;
            var PsumerDate = 0m;
            var pdebit = 0m;
            var pcrdit = 0m;
            if (checkformove != null)
            {
                sumer = (decimal?)(db.AccountMovements.Where(ac => ac.AccountId == accountId).Sum(ac => (decimal?)ac.Debit ?? 0) - db.AccountMovements.Where(ac => ac.AccountId == accountId).Sum(ac => (decimal?)ac.Crdit ?? 0)) ?? 0;
                if (mind != null)
                {
                    sumerDate = (from ac in db.AccountMovements
                                 where (ac.AccountId == accountId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd)
                                 select ac.Debit).DefaultIfEmpty(0).Sum()
                                 -
                                 (from ac in db.AccountMovements
                                  where
   (ac.AccountId == accountId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd)
                                  select ac.Crdit).DefaultIfEmpty(0).Sum();

                    PsumerDate = (from ac in db.AccountMovements
                                  where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                                  select ac.Debit).DefaultIfEmpty(0).Sum()
                                  -
                                  (from ac in db.AccountMovements
                                   where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                                   select ac.Crdit).DefaultIfEmpty(0).Sum();
                    pdebit = (from ac in db.AccountMovements
                              where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                              select ac.Debit).DefaultIfEmpty(0).Sum();
                    pcrdit = (from ac in db.AccountMovements
                              where (ac.AccountId == accountId && ac.AccountMovementDate < mind)
                              select ac.Crdit).DefaultIfEmpty(0).Sum();

                }
            }
            var ord = db.AccountMovements.Find(accountId);


            var moves = (from o in db.AccountMovements
                         where o.AccountId == accountId
                         select new AccountsMoves
                         {
                             moveId = o.AccountMovementId,
                             orderNote = o.AccountOrder.OrderNote,
                             accountId = o.AccountId,
                             accountName = o.Accounts.AccountName,
                             debit = o.Debit,
                             crdit = o.Crdit,
                             Pdebit = pdebit,
                             Pcrdit = pcrdit,
                             refrenceId = o.AccountOrder.OrderIdRefrence,
                             typeRefrence = o.AccountOrder.OrderTypeRefrence,
                             date = o.AccountMovementDate,
                             orderId = o.AccountOrderId,
                             orderNo = o.AccountOrder.OrderNo,
                             note = o.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName,
                             CompanyId = o.AccountOrder.CompanyId,
                             branchId = o.AccountOrder.BranchId,
                             summery = sumer > 0 ? ("رصيد الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("رصيد الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? (" رصيد الحساب " + 0.ToString()) : (" رصيد الحساب " + 0.ToString()),
                             summeryDate = sumerDate > 0 ? ("رصيد الفترة الحالية " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("رصيد الفترة الحالية " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? (" رصيد الفترة الحالية " + 0.ToString()) : (" رصيد الفترة الحالية " + 0.ToString()),
                             PsummeryDate = PsumerDate > 0 ? ("رصيد بداية الفترة " + Math.Abs(PsumerDate).ToString() + " ( مدين )") : PsumerDate < 0 ? ("رصيد بداية الفترة " + Math.Abs(PsumerDate).ToString() + " ( دائن )") : PsumerDate == 0 ? (" رصيد بداية الفترة " + 0.ToString()) : (" رصيد بداية الفترة " + 0.ToString()),
                             IsCompany = o.AccountOrder.IsCompany,
                             iscomplex = true

                         }
                       ).ToList();

            Int32 numid = 0;
            foreach (var o in moves)
            {
                numid += 1;
                o.indexdId = numid;

                var checkcom = GetAccountOrders(o.IsCompany == false ? o.branchId : o.CompanyId, o.IsCompany).Where(a => a.orderId == o.orderId).FirstOrDefault();

                if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count > 1)
                {
                    o.iscomplex = true;
                }
                else if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count == 1)
                {
                    var checkfirstacc = checkcom.debitorAccounts.Where(a => a.account.id == o.accountId).FirstOrDefault();
                    if (checkfirstacc == null)
                    {
                        o.iscomplex = true;
                    }
                    else
                    {
                        o.iscomplex = false;
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count > 1)
                {
                    var checkfirstacc = checkcom.crditorAccounts.Where(a => a.account.id == o.accountId).FirstOrDefault();
                    if (checkfirstacc == null)
                    {
                        o.iscomplex = true;
                    }
                    else
                    {
                        o.iscomplex = false;
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count == 1)
                {

                    o.iscomplex = false;
                }


            }

            return moves;
        }

        public List<AccountCatogeryLedger> GetCustomAccountmovesCatogery(Int32 customaAccountCatogerryId, DateTime mind, DateTime maxd)
        {
          
            var checkformove = db.CustomAccounts.Where(ac => ac.CustomAccountCategoryId == customaAccountCatogerryId).FirstOrDefault();
            var sumer = 0m;
            var sumerDate = 0m;
            var psumerDate = 0m;
            if (checkformove != null)
            {
                sumer = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                sumerDate = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                psumerDate = (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.CustomAccountCategories from a in acc.CustomAccount from ac in a.Account.AccountsMovements where (acc.CustomAccountCategoryId == customaAccountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
            }


            var Ledger = new List<AccountCatogeryLedger>();


            var Vaildgroups = new List<AccountCategory>();
            // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

            Ledger = (from cat in db.CustomAccounts
                      where cat.CustomAccountCategoryId == customaAccountCatogerryId
                      select new AccountCatogeryLedger()
                      {
                          id = cat.AccountId,
                          name = cat.Account.AccountName,
                          debit = cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                          crdit = Math.Abs(cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.Account.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                          minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                          maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.Account.AccountCategoryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                          summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                          summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                          psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),
                          total = sumerDate,
                          branchId = (int)cat.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId

                      }).ToList();
            foreach (var l in Ledger)
            {
                var code = GetAccountCode(l.branchId, l.id + "Ac");
                l.Code = code;
            }

            return Ledger;
        }
        public List<AccountCatogeryLedger> GetAccountmovesCatogery(Int32 accountCatogerryId, DateTime mind, DateTime maxd, bool isCompany, bool allAccounts)
        {
            if (isCompany)
            {
                if (allAccounts)
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    var psumerDate = 0m;
                    if (checkformove != null)
                    {
                        sumer = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        psumerDate = (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    }


                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.Accounts
                              where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountId,
                                  name = cat.AccountName,
                                  debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                                  crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                                  psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),


                              }).ToList();


                    return Ledger;
                }
                else
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.Accounts.BranchId == null).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    var psumerDate = 0m;
                    if (checkformove != null)
                    {
                        sumer = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                        psumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId == null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    }


                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.Accounts
                              where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                              && cat.BranchId == null
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountId,
                                  name = cat.AccountName,
                                  debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                                  crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                                  psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),


                              }).ToList();


                    return Ledger;
                }

            }
            else
            {
                var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.Accounts.BranchId != null).FirstOrDefault();
                var sumer = 0m;
                var sumerDate = 0m;
                var psumerDate = 0m;
                if (checkformove != null)
                {
                    sumer = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId) select ac.Crdit).DefaultIfEmpty(0).Sum();
                    sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                    sumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd) select ac.Crdit).DefaultIfEmpty(0).Sum();

                    psumerDate = (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Debit).DefaultIfEmpty(0).Sum() - (from acc in db.AccountCategories from a in acc.Accounts where (a.BranchId != null) from ac in a.AccountsMovements where (acc.AccountCategoryId == accountCatogerryId && ac.AccountMovementDate < mind) select ac.Crdit).DefaultIfEmpty(0).Sum();
                }


                var Ledger = new List<AccountCatogeryLedger>();


                var Vaildgroups = new List<AccountCategory>();
                // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                Ledger = (from cat in db.Accounts
                          where cat.AccountCategory.AccountCategoryId == accountCatogerryId
                         && cat.BranchId != null
                          select new AccountCatogeryLedger()
                          {
                              id = cat.AccountId,
                              name = cat.AccountName,
                              debit = cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0),
                              crdit = Math.Abs(cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Count() == 0 ? 0 : ((cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) > 0 ? 0 : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) < 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Sum(a => a.Crdit)) == 0 ? (cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Debit) - cat.AccountsMovements.Where(a => a.AccountMovementDate >= mind && a.AccountMovementDate <= maxd).Sum(a => a.Crdit)) : 0)),
                              minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Min(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Min(ad => ad.AccountMovementDate) : mind,
                              maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId).Max(ad => ad.AccountMovementDate) == null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategoryId == accountCatogerryId && ac.AccountId == cat.AccountId).Max(ad => ad.AccountMovementDate) : maxd,
                              summery = (sumerDate + psumerDate) > 0 ? ("مجموع الارصدة " + Math.Abs(sumerDate + psumerDate).ToString() + " ( مدين )") : (sumerDate + psumerDate) < 0 ? ("مجموع الارصدة  " + Math.Abs(sumerDate + psumerDate).ToString() + " ( دائن )") : (sumerDate + psumerDate) == 0 ? (" مجموع الارصدة " + 0.ToString()) : " مجموع الارصدة " + 0.ToString(),
                              summeryDate = sumerDate > 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("مجموع ارصدة الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? " مجموع ارصدة الفترة " + 0.ToString() : " مجموع ارصدة الفترة " + 0.ToString(),
                              psummeryDate = psumerDate > 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( مدين )") : psumerDate < 0 ? ("مجموع الارصدة السابقة " + Math.Abs(psumerDate).ToString() + " ( دائن )") : psumerDate == 0 ? " مجموع الارصدة السابقة " + 0.ToString() : " مجموع الارصدة السابقة " + 0.ToString(),


                          }).ToList();


                return Ledger;
            }

        }
        public Int32 GetAccountCode(int branchiId, string accountIdForCode)
        {
            var Tbs = (from bs in db.BalanceSheets
                       where bs.CompanyId == branchiId
                       orderby bs.RowVersion
                       select new AccountsTable
                       {
                           Id = bs.BalanceSheetId + "Bs",
                           Code = db.BalanceSheets.Where(a => a.CompanyId == branchiId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                           ParentId = bs.CompanyId.ToString(),

                       }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            var Tbst = (from bst in db.BalanceSheetTypes
                        where bst.BalanceSheet.CompanyId == branchiId
                        orderby bst.RowVersion
                        select new AccountsTable
                        {
                            Id = bst.BalanceSheetTypeId + "Bst",
                            ParentId = bst.BalanceSheetId + "Bs",
                            Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            List<AccountsTable> Taccounts = new List<AccountsTable>();


            var TAcc = (from Acc in db.AccountCategories
                        where Acc.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                        orderby Acc.RowVersion
                        select new AccountsTable
                        {
                            Id = Acc.AccountCategoryId + "Acc",
                            Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                            ParentId = Acc.BalanceSheetTypeId + "Bst"
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




            var TAc = (from Ac in db.Accounts
                       where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                       orderby Ac.RowVersion
                       select new AccountsTable
                       {
                           Id = Ac.AccountId + "Ac",
                           Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                           ParentId = Ac.AccountCategoryId + "Acc"
                       }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            foreach (var acc in TAcc)
            {
                foreach (var ac in TAc)
                {

                    if (ac[ac.Count - 1].ParentId == acc[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                        acc.AddRange(ac);
                    }
                }
            }

            foreach (var bst in Tbst)
            {
                foreach (var acc in TAcc)
                {

                    if (acc[0].ParentId == bst[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        //acc.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.Code = int.Parse(acc[0].Code.ToString() + c.Code.ToString()));

                        bst.AddRange(acc);
                    }
                }
            }

            foreach (var bs in Tbs)
            {
                foreach (var bst in Tbst)
                {

                    if (bst[0].ParentId == bs[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);

                        bs.AddRange(bst);

                    }
                }
            }

            foreach (var i in Tbs)
            {
                foreach (var a in i)
                {
                    i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                }
            }
            Tbs.OrderBy(a => a[0].Code);
            var accountcode = 0;
            //foreach(var i in Tbs)
            //{
            //    accountcode = i.Where(a => a.Id == accountIdForCode).FirstOrDefault().Code;
            //}

            foreach (var t in Tbs)
            {
                foreach (var i in t)
                {
                    if (i.Id == accountIdForCode)
                    {
                        accountcode = i.Code;
                    }
                }
            }

            return accountcode;
        }

        public List<Alllists> getfinancialStatement(int CompanyId)
        {
          
            List<GetFinancialList> ftree = new List<GetFinancialList>();
            List<Alllists> listes4 = new List<Alllists>();

            var cacc = (from c in db.CustomAccountCategories
                        where c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == CompanyId
                        select new FListes
                        {
                            Id = c.CustomAccountCategoryId + "Acc",
                            Name = c.AccountCategoryName,
                            Level = 4,
                            ParentId = c.CustomBalanceSheetTypeId + "Bst",
                            ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName,
                            amount = 0,
                            Code = db.CustomAccountCategories.Where(a => a.CustomBalanceSheetTypeId == c.CustomBalanceSheetTypeId).OrderBy(a => a.RowVersion).Count(x => x.CustomAccountCategoryId < c.CustomAccountCategoryId) + 1,

                        }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

            var bst = (from c in db.CustomBalanceSheetTypes
                       where c.CustomBalanceSheet.FinancialList.CompanyId == CompanyId
                       select new FListes
                       {
                           Id = c.CustomBalanceSheetTypeId + "Bst",
                           Name = c.BalanceSheetTypeName,
                           Level = 3,
                           ParentId = c.CustomBalanceSheetId + "Bs",
                           ParentName = c.CustomBalanceSheet.BalanceSheetName,
                           amount = 0,
                           Code = db.CustomBalanceSheetTypes.Where(a => a.CustomBalanceSheetId == c.CustomBalanceSheetId).OrderBy(a => a.RowVersion).Count(x => x.CustomBalanceSheetId < c.CustomBalanceSheetId) + 1,

                       }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

            var bs = (from c in db.CustomBalanceSheets
                      where c.FinancialList.CompanyId == CompanyId
                      select new FListes
                      {
                          Id = c.CustomBalanceSheetId + "Bs",
                          Name = c.BalanceSheetName,
                          Level = 2,
                          ParentId = c.FinancialListId + "F",
                          ParentName = c.FinancialList.FinancialListName,
                          amount = 0,
                          Code = db.CustomBalanceSheets.Where(a => a.FinancialListId == c.FinancialListId).OrderBy(a => a.RowVersion).Count(x => x.CustomBalanceSheetId < c.CustomBalanceSheetId) + 1

                      }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);

            var fl = (from c in db.FinancialLists
                      where c.CompanyId == CompanyId
                      select new FListes
                      {
                          Id = c.FinancialListId + "F",
                          Name = c.FinancialListName,
                          Level = 1,
                          ParentId = c.CompanyId.ToString(),
                          ParentName = c.Company.CompanyName,
                          amount = 0,
                          Code = db.FinancialLists.Where(a => a.CompanyId == CompanyId).OrderBy(a => a.RowVersion).Count(x => x.FinancialListId < c.FinancialListId) + 1

                      }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();

            List<FListes> listes = new List<FListes>();
            foreach (var caccount in cacc)
            {

                listes.AddRange(caccount);
                foreach (var ccatogery in bst)
                {
                    foreach (var ccatogeryItem in ccatogery)
                    {
                        if (caccount[0].ParentId == ccatogeryItem.Id)
                        {
                            ccatogeryItem.forLevel = ccatogeryItem.ParentId;
                            // caccount.Add(ccatogery[0]);

                            foreach (var c in caccount)
                            {
                                c.forLevel = ccatogeryItem.ParentId;
                            }
                            listes.Add(ccatogeryItem);
                        }

                    }

                }
            }


            List<FListes> listes2 = new List<FListes>();
            var groupLists = (from l in listes select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();


            foreach (var g in groupLists)
            {
                var last = g[g.Count - 1];
                foreach (var b in bs)
                {
                    foreach (var Ib in b)
                    {
                        if (Ib.Id == last.ParentId)
                        {
                            g.Add(Ib);
                            foreach (var c in g)
                            {
                                c.forLevel = last.ParentId;
                            }
                        }
                    }
                }
                listes2.AddRange(g);
            }

            var groupLists2 = (from l in listes2 select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
            List<FListes> listes3 = new List<FListes>();

            foreach (var g in groupLists2)
            {
                var last = g[g.Count - 1];
                foreach (var b in fl)
                {
                    foreach (var Ib in b)
                    {
                        if (Ib.Id == last.ParentId)
                        {
                            g.Add(Ib);
                            foreach (var c in g)
                            {
                                c.forLevel = last.ParentId;
                            }
                        }
                    }
                }
                listes3.AddRange(g);
            }


            var groupLists4 = (from l in listes3 select l).GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();

            foreach (var l in groupLists4)
            {

                listes4.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, Listes = l });
            }

            foreach (var l in listes4)
            {
                l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
            }

            var No = 0;
            foreach (var l in listes4)
            {
                var accounts = l.Listes.Count();

                foreach (var a in l.Listes)
                {
                    if (a.Level == 4)
                    {
                        a.Code = 1 + No;
                        No++;
                    }
                    else
                    {
                        a.Code = 0;
                    }

                }


            }

            return listes4;
        }

        public decimal GetAccountamountType(Int32 accountId, bool isDebit)
        {

            var ac = db.Accounts.Find(accountId);
            if (ac != null)
            {
                if (ac.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين")
                {
                    if (isDebit)
                    {
                        return 1;
                    }
                    else
                    {
                        return -1;
                    }

                }
                else if (ac.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "دائن")
                {
                    if (isDebit)
                    {
                        return -1;
                    }
                    else
                    {
                        return 1;
                    }
                }

                return 1;

            }
            else
            {
                return 1;
            }
        }

        public decimal GetCustomAccountamountType(Int32 accountIdLevel2)
        {

            var ac = db.CustomBalanceSheets.Find(accountIdLevel2);
            if (ac != null)
            {
                if (ac.AccountType.AccountTypeName == "مدين")
                {
                    return -1;

                }
                else if (ac.AccountType.AccountTypeName == "دائن")
                {
                    return 1;
                }

                return -1;

            }
            else
            {
                return -1;
            }
        }

        public List<AccountCatogeryLedger> GetAllAccountmovesCatogery(Int32 companyId, DateTime mind, DateTime maxd, bool isCompany, bool allAccounts)
        {
            if (isCompany)
            {
                if (allAccounts)
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    if (checkformove != null)
                    {

                        sumer = db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId)
                                .Select(ac => ac.Debit).DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();
                        sumerDate = db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(a => a.Debit)
                                    .DefaultIfEmpty(0)
                                    .Sum()
                                    -
                                    db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                    .Sum();

                    }

                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.AccountCategories
                              where cat.BalanceSheetType.BalanceSheet.CompanyId == companyId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountCategoryId,
                                  name = cat.AccountCategoryName,
                                  debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                                  crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                                  sumDebit = 0,
                                  sumCrdit = 0,
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                                  AccountCatogeryId = cat.BalanceSheetTypeId,
                                  AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                              }).ToList();


                    return Ledger;
                }
                else
                {
                    var checkformove = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).FirstOrDefault();
                    var sumer = 0m;
                    var sumerDate = 0m;
                    if (checkformove != null)
                    {

                        sumer = db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null)
                                .Select(ac => ac.Debit).DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();
                        sumerDate = db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(a => a.Debit)
                                    .DefaultIfEmpty(0)
                                    .Sum()
                                    -
                                    db.AccountMovements
                                    .Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null &&
                                                 ac.AccountMovementDate >= mind &&
                                                 ac.AccountMovementDate <= maxd)
                                    .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                    .Sum();

                    }

                    var Ledger = new List<AccountCatogeryLedger>();


                    var Vaildgroups = new List<AccountCategory>();
                    // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();

                    Ledger = (from cat in db.AccountCategories
                              where cat.BalanceSheetType.BalanceSheet.CompanyId == companyId
                              select new AccountCatogeryLedger()
                              {
                                  id = cat.AccountCategoryId,
                                  name = cat.AccountCategoryName,
                                  debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                                  crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                                  sumDebit = 0,
                                  sumCrdit = 0,
                                  minDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null).Min(ad => ad.AccountMovementDate) : mind,
                                  maxDate = db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.BranchId == null).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == null).Max(ad => ad.AccountMovementDate) : maxd,
                                  summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                                  summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                                  AccountCatogeryId = cat.BalanceSheetTypeId,
                                  AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                              }).ToList();


                    return Ledger;
                }
            }
            else
            {
                var checkformove = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).FirstOrDefault();
                var sumer = 0m;
                var sumerDate = 0m;
                if (checkformove != null)
                {

                    sumer = db.AccountMovements
                            .Where(ac => ac.Accounts.BranchId == companyId)
                            .Select(ac => ac.Debit).DefaultIfEmpty(0)
                            .Sum()
                            -
                            db.AccountMovements
                            .Where(ac => ac.Accounts.BranchId == companyId)
                            .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                            .Sum();
                    sumerDate = db.AccountMovements
                                .Where(ac => ac.Accounts.BranchId == companyId &&
                                             ac.AccountMovementDate >= mind &&
                                             ac.AccountMovementDate <= maxd)
                                .Select(a => a.Debit)
                                .DefaultIfEmpty(0)
                                .Sum()
                                -
                                db.AccountMovements
                                .Where(ac => ac.Accounts.BranchId == companyId &&
                                             ac.AccountMovementDate >= mind &&
                                             ac.AccountMovementDate <= maxd)
                                .Select(ac => ac.Crdit).DefaultIfEmpty(0)
                                .Sum();

                }

                var Ledger = new List<AccountCatogeryLedger>();


                var Vaildgroups = new List<AccountCategory>();
                // Vaildgroups = db.Accounts.Where(a => a.AccountCategoryId == accountCatogerryId && a.Accounts.Count > 0).ToList();
                var branch = db.Branchs.Find(companyId);
                Ledger = (from cat in db.AccountCategories
                          where cat.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId

                          select new AccountCatogeryLedger()
                          {
                              id = cat.AccountCategoryId,
                              name = cat.AccountCategoryName,
                              debit = (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0,
                              crdit = Math.Abs((cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) > 0 ? 0 : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) < 0 ? (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) : (cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Debit)) - cat.Accounts.Sum(a => a.AccountsMovements.Where(ac => ac.Accounts.AccountCategoryId == cat.AccountCategoryId && ac.Accounts.BranchId == companyId && ac.AccountMovementDate >= mind && ac.AccountMovementDate <= maxd).Sum(c => c.Crdit))) == 0 ? 0 : 0),
                              sumDebit = 0,
                              sumCrdit = 0,
                              minDate = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).Min(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Min(ad => ad.AccountMovementDate) : mind,
                              maxDate = db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId).Max(ad => ad.AccountMovementDate) != null ? db.AccountMovements.Where(ac => ac.Accounts.BranchId == companyId && ac.Accounts.AccountCategory.AccountCategoryId == cat.AccountCategoryId).Max(ad => ad.AccountMovementDate) : maxd,
                              summery = sumer > 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( مدين )") : sumer < 0 ? ("الاجمالي عن مجمل الحساب " + Math.Abs(sumer).ToString() + " ( دائن )") : sumer == 0 ? 0.ToString() : 0.ToString(),
                              summeryDate = sumerDate > 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( مدين )") : sumerDate < 0 ? ("الاجمالي عن هذه الفترة " + Math.Abs(sumerDate).ToString() + " ( دائن )") : sumerDate == 0 ? 0.ToString() : 0.ToString(),
                              AccountCatogeryId = cat.BalanceSheetTypeId,
                              AccountCatogeryName = cat.BalanceSheetType.BalanceSheetTypeName

                          }).ToList();


                return Ledger;
            }

        }
        public AccountsMoves SetOrder(GetSetOrder setOrder, string currentWorkerId, DateTime? mind, DateTime? maxd = null)
        {
            var order = db.AccountOrders.Find(setOrder.orderId);
            if (order != null)
            {
                order.CurrentWorkerId = currentWorkerId;
                order.OrderNote = setOrder.orderNote;
                db.Entry(order).State = EntityState.Modified;
                var accToDel = db.AccountMovements.Where(a => a.AccountOrderId == order.AccountOrderId);
                db.AccountMovements.RemoveRange(accToDel);
                decimal totalAmount = 0;
                List<AccountMovement> secondAcc = new List<AccountMovement>();
                foreach (var o in setOrder.secondAccount)
                {
                    totalAmount = totalAmount + o.amount;
                    var move = new AccountMovement()
                    {
                        AccountId = o.account.id,
                        AccountMovementDate = order.OrderDate,
                        AccountOrderId = order.AccountOrderId,
                        Debit = setOrder.isDebit ? 0 : o.amount,
                        Crdit = setOrder.isDebit ? o.amount : 0,
                        CurrentWorkerId = currentWorkerId
                    };
                    secondAcc.Add(move);

                }

                var Firstacc = new AccountMovement()
                {
                    AccountId = setOrder.accountId,
                    AccountMovementDate = order.OrderDate,
                    AccountOrderId = order.AccountOrderId,
                    Debit = setOrder.isDebit ? totalAmount : 0,
                    Crdit = setOrder.isDebit ? 0 : totalAmount,
                    CurrentWorkerId = currentWorkerId
                };

                secondAcc.Add(Firstacc);
                db.AccountMovements.RemoveRange(accToDel);
                db.AccountMovements.AddRange(secondAcc);
                db.SaveChanges();

            }
            else
            {

                order = new AccountOrder
                {
                    OrderDate = DateTime.Now,
                    BranchId = setOrder.branchId,
                    OrderNote = setOrder.orderNote,
                    CurrentWorkerId = currentWorkerId,
                    OrderNo = db.AccountOrders.Where(a => a.BranchId == setOrder.branchId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == setOrder.branchId).Max(a => a.OrderNo) + 1 : 1,
                    AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId
                };

                db.AccountOrders.Add(order);
                db.SaveChanges();
                db.Entry(order).GetDatabaseValues();
                decimal totalAmount = 0;
                List<AccountMovement> secondAcc = new List<AccountMovement>();
                foreach (var o in setOrder.secondAccount)
                {
                    totalAmount = totalAmount + o.amount;
                    var move = new AccountMovement()
                    {
                        AccountId = o.account.id,
                        AccountMovementDate = order.OrderDate,
                        AccountOrderId = order.AccountOrderId,

                        Debit = setOrder.isDebit ? 0 : o.amount,
                        Crdit = setOrder.isDebit ? o.amount : 0,
                        CurrentWorkerId = currentWorkerId
                    };
                    secondAcc.Add(move);

                }

                var Firstacc = new AccountMovement()
                {
                    AccountId = setOrder.accountId,
                    AccountMovementDate = DateTime.Now,
                    AccountOrderId = order.AccountOrderId,
                    Debit = setOrder.isDebit ? totalAmount : 0,
                    Crdit = setOrder.isDebit ? 0 : totalAmount,
                    CurrentWorkerId = currentWorkerId
                };

                secondAcc.Add(Firstacc);
                db.AccountMovements.AddRange(secondAcc);
                db.SaveChanges();
            }

            var ord = GetAccountmoves(setOrder.accountId, mind, maxd).Where(a => a.orderId == order.AccountOrderId).FirstOrDefault();
            return ord;

        }
        public OrderVm GetAccountOrderById(int id, Int32 branchId, bool isCompany)
        {
            if (isCompany)
            {
                var orders = (from o in db.AccountOrders
                              where o.OrderNo == id && o.CompanyId == branchId
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  orderNote = o.OrderNote,
                                  IsCompany = false,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }).FirstOrDefault();
                return orders;
            }
            else
            {
                var orders = (from o in db.AccountOrders
                              where o.OrderNo == id && o.BranchId == branchId
                              select new OrderVm
                              {
                                  orderId = o.AccountOrderId,
                                  orderDate = o.OrderDate,
                                  orderNo = o.OrderNo,
                                  branchId = o.BranchId,
                                  orderNote = o.OrderNote,
                                  IsCompany = false,
                                  debitorAccounts = (from m in o.AccountMovements
                                                     where m.Debit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Debit,
                                                         id = m.AccountOrderId
                                                     }).ToList(),
                                  crditorAccounts = (from m in o.AccountMovements
                                                     where m.Crdit > 0
                                                     select new OrderAccounts
                                                     {
                                                         account = new AccountObject
                                                         {
                                                             id = m.Accounts.AccountId,
                                                             AccountCatogery = m.Accounts.AccountCategory.AccountCategoryName,
                                                             status = false,
                                                             value = m.Accounts.AccountName,
                                                             Key = (m.Accounts.AccountId.ToString() + m.Accounts.AccountName)
                                                         },
                                                         amount = m.Crdit,
                                                         id = m.AccountOrderId
                                                     }).ToList()
                              }).FirstOrDefault();
                return orders;
            }

        }

        public IQueryable<BranchesGroupVm> GetBranchesGroup(string uid, bool IsCompany)
        {
           
            var returndCompany = from s in db.settingRolesForUser
                                 where s.settingPropertiesforUser.settingProperties.name == "Company"
                                 && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Company"
                                 join c in db.Companies on s.RefrenceId equals c.CompanyId
                                 select new BranchesGroupVm()
                                 {
                                     id = c.CompanyId + "C",
                                     name = c.CompanyName,
                                     companyName = "شركات",
                                     fullName = c.CompanyName,
                                     isCompany = true,
                                     companyId = c.CompanyId,
                                     realName = c.CompanyName,
                                     oId = c.CompanyId,
                                 };

            var returndBranch = from s in db.settingRolesForUser
                                where s.settingPropertiesforUser.settingProperties.name == "Branch"
                                && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Branch"
                                join c in db.Branchs on s.RefrenceId equals c.BranchId
                                select new BranchesGroupVm()
                                {
                                    id = c.BranchId + "B",
                                    name = c.BranchName + " - " + c.Company.CompanyName,
                                    companyName = "فروع",
                                    fullName = c.BranchName + " - " + c.Company.CompanyName,
                                    isCompany = false,
                                    companyId = c.CompanyId,
                                    realName = c.BranchName,
                                    oId = c.BranchId
                                };
            returndCompany.ToList().AddRange(returndBranch);
            return returndCompany;

        }

        public IQueryable<AccountObject> SearchAllAccounts(Int32 branchId, bool IsCompany)
        {
            if (IsCompany)
            {
                var account = (from i in db.Accounts
                               where (i.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branchId)
                               && i.BranchId == null
                               && i.Activated == true
                               select new AccountObject()
                               {
                                   id = i.AccountId,
                                   value = i.AccountName,
                                   AccountCatogery = i.AccountCategory.AccountCategoryName,
                                   status = false


                               });
                return account;
            }
            else
            {
                var account = (from i in db.Accounts
                               where (i.BranchId == branchId)
                               && i.Activated == true
                               select new AccountObject()
                               {
                                   id = i.AccountId,
                                   value = i.AccountName,
                                   AccountCatogery = i.AccountCategory.AccountCategoryName,
                                   status = false


                               });
                return account;
            }


            //var filter = account.Where(x => !notAccount.Any(y => y == x.value));


        }

        public IQueryable<AccountObject> SearchAllFinancialLists(Int32 branchId)
        {
            var account = (from i in db.FinancialLists
                           where (i.CompanyId == branchId)
                           select new AccountObject()
                           {
                               id = i.FinancialListId,
                               value = i.FinancialListName,
                               status = false


                           });

            //var filter = account.Where(x => !notAccount.Any(y => y == x.value));

            return account;
        }

        public IQueryable<AccountObject> SearchAllAccountsCatogery(Int32 branchId, bool isCompany)
        {
            if (isCompany)
            {
                var account = (from i in db.AccountCategories
                               where (i.BalanceSheetType.BalanceSheet.CompanyId == branchId)
                               select new AccountObject()
                               {
                                   id = i.AccountCategoryId,
                                   value = i.AccountCategoryName,
                                   AccountCatogery = i.BalanceSheetType.BalanceSheetTypeName,
                                   status = false


                               });

                return account;
            }
            else
            {
                var branch = db.Branchs.Find(branchId);

                var account = (from i in db.AccountCategories
                               where (i.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId)
                               select new AccountObject()
                               {
                                   id = i.AccountCategoryId,
                                   value = i.AccountCategoryName,
                                   AccountCatogery = i.BalanceSheetType.BalanceSheetTypeName,
                                   status = false


                               });

                return account;
            }

        }
    }
}
