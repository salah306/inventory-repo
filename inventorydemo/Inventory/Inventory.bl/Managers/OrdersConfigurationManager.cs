﻿
using Inventory.bl.DTO;
using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.bl.Managers
{

    public class OrdersConfigurationManager : ManagersHelper
    {
        public OrdersConfigurationManager(IOwinContext context) 
            : base(context)
        {
        }

        public SalesOrdersConfigurationVm getSalesOrderconfgById(SetOrdersConfigurations o, branchGroupSt company)
        {

            if (o.type == "sales")
            {
              
                #region sales
                var order = db.SalesOrdersConfigurations.Where(a => a.SalesOrdersConfigurationId == o.orderId && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                    .ToList()
                    .Select(or => new SalesOrdersConfigurationVm()
                    {
                        salesId = or.SalesOrdersConfigurationId,
                        salesOrderName = or.SalesOrderName,
                        salesName = or.SalesName,
                        salesReturnName = or.SalesReturnName,
                        defaultPayName = or.DefaultPayAccountCategoryId.GetValueOrDefault(),
                        salesOrderIsNew = false,
                        payMethod = or.PayMethod.Select(paym => new SalesPayMethodVm()
                        {
                            payMethodId = paym.SalessOrdersConfigurationPayMethodId,
                            accountId = paym.AccountCategoryId,
                            accountName = paym.AccountCategory.AccountCategoryName + " - " + paym.AccountCategory.Code,
                            payName = paym.PayName,
                            defaultChildAccountId = paym.DefaultChildAccountId,
                            attchedRequest = true,

                        }).ToList(),
                        terms = or.Terms.Select(tr => new SalesTermVm()
                        {
                            termId = tr.SalessOrdersConfigurationTermId,
                            name = tr.Note
                        }).ToList(),
                        tableAccounts = or.TableAccounts.Select(ta => new SalesTableAccountVm()
                        {
                            tableAccountId = ta.SalessOrdersConfigurationTableAccountId,
                            accountId = ta.AccountId,
                            accountName = ta.Account.AccountName + " - " + ta.Account.Code,
                            accountType = ta.AccountType.AccountTypeName,
                            nickName = ta.AccountNickName

                        }).ToList(),
                        totalAccounts = or.TotalAccounts.Select(to => new SalesTotalAccountVm()
                        {
                            totalAccountId = to.SalessOrdersConfigurationTotalAccountId,
                            accountId = to.AccountId,
                            accountName = to.Account.AccountName + " - " + to.Account.Code,
                            accountType = to.AccountType.AccountTypeName,
                            nickName = to.AccountNickName
                        }).ToList()
                    }).SingleOrDefault();

                if (order == null)
                {
                    return null;

                }
                return order;
                #endregion

            }

            return null;
        }
        public PurchaseOrdersConfigurationVm getPurOrderconfgById(SetOrdersConfigurations o, branchGroupSt company)
        {

            if (o.type == "pur")
            {
                #region pur
                var order = db.PurchaseOrdersConfigurations.Where(a => a.PurchaseOrdersConfigurationId == o.orderId && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                     .ToList()
                    .Select(or => new PurchaseOrdersConfigurationVm()
                    {
                        PurId = or.PurchaseOrdersConfigurationId,
                        PurOrderName = or.PurchaseOrderName,
                        PurName = or.PurchaseName,
                        PurReturnName = or.PurchaseReturnName,
                        defaultPayName = or.DefaultPayAccountCategoryId.GetValueOrDefault(),
                        PurOrderIsNew = false,
                        payMethod = or.PayMethod.Select(paym => new PurchasePayMethodVm()
                        {
                            payMethodId = paym.PurchasesOrdersConfigurationPayMethodId,
                            accountId = paym.AccountCategoryId,
                            accountName = paym.AccountCategory.AccountCategoryName + " - " + paym.AccountCategory.Code,
                            payName = paym.PayName,
                            defaultChildAccountId = paym.DefaultChildAccountId,
                            attchedRequest = true,

                        }).ToList(),
                        terms = or.Terms.Select(tr => new PurchaseTermVm()
                        {
                            termId = tr.PurchasesOrdersConfigurationTermId,
                            name = tr.Note
                        }).ToList(),
                        tableAccounts = or.TableAccounts.Select(ta => new PurchaseTableAccountVm()
                        {
                            tableAccountId = ta.PurchasesOrdersConfigurationTableAccountId,
                            accountId = ta.AccountId,
                            accountName = ta.Account.AccountName + " - " + ta.Account.Code,
                            accountType = ta.AccountType.AccountTypeName,
                            nickName = ta.AccountNickName

                        }).ToList(),
                        totalAccounts = or.TotalAccounts.Select(to => new PurchaseTotalAccountVm()
                        {
                            totalAccountId = to.PurchasesOrdersConfigurationTotalAccountId,
                            accountId = to.AccountId,
                            accountName = to.Account.AccountName + " - " + to.Account.Code,
                            accountType = to.AccountType.AccountTypeName,
                            nickName = to.AccountNickName
                        }).ToList()
                    }).SingleOrDefault();

                if (order == null)
                {
                    return null;

                }
                return order;
                #endregion
            }
            return null;
        }
    }
}