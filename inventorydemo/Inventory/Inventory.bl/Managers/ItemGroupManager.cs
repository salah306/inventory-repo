﻿using Inventory.dl;
using Inventory.dl.Models;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Managers
{
    public class ItemGroupManager : ManagersHelper
    {
        
        public ItemGroupManager(IOwinContext context)
           : base(context)
        {
           
        }


        public Int32 GetAccountCode(int branchiId, string accountIdForCode)
        {
            var Tbs = (from bs in db.BalanceSheets
                       where bs.CompanyId == branchiId
                       orderby bs.RowVersion
                       select new AccountsTable
                       {
                           Id = bs.BalanceSheetId + "Bs",
                           Code = db.BalanceSheets.Where(a => a.CompanyId == branchiId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                           ParentId = bs.CompanyId.ToString(),

                       }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            var Tbst = (from bst in db.BalanceSheetTypes
                        where bst.BalanceSheet.CompanyId == branchiId
                        orderby bst.RowVersion
                        select new AccountsTable
                        {
                            Id = bst.BalanceSheetTypeId + "Bst",
                            ParentId = bst.BalanceSheetId + "Bs",
                            Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            List<AccountsTable> Taccounts = new List<AccountsTable>();


            var TAcc = (from Acc in db.AccountCategories
                        where Acc.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                        orderby Acc.RowVersion
                        select new AccountsTable
                        {
                            Id = Acc.AccountCategoryId + "Acc",
                            Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                            ParentId = Acc.BalanceSheetTypeId + "Bst"
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




            var TAc = (from Ac in db.Accounts
                       where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branchiId
                       orderby Ac.RowVersion
                       select new AccountsTable
                       {
                           Id = Ac.AccountId + "Ac",
                           Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                           ParentId = Ac.AccountCategoryId + "Acc"
                       }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            foreach (var acc in TAcc)
            {
                foreach (var ac in TAc)
                {

                    if (ac[ac.Count - 1].ParentId == acc[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                        acc.AddRange(ac);
                    }
                }
            }

            foreach (var bst in Tbst)
            {
                foreach (var acc in TAcc)
                {

                    if (acc[0].ParentId == bst[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);
                        //acc.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.Code = int.Parse(acc[0].Code.ToString() + c.Code.ToString()));

                        bst.AddRange(acc);
                    }
                }
            }

            foreach (var bs in Tbs)
            {
                foreach (var bst in Tbst)
                {

                    if (bst[0].ParentId == bs[0].Id)
                    {
                        //Taccounts.AddRange(bs);
                        //Taccounts.AddRange(bst);

                        bs.AddRange(bst);

                    }
                }
            }

            foreach (var i in Tbs)
            {
                foreach (var a in i)
                {
                    i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                }
            }
            Tbs.OrderBy(a => a[0].Code);
            var accountcode = 0;
            //foreach(var i in Tbs)
            //{
            //    accountcode = i.Where(a => a.Id == accountIdForCode).FirstOrDefault().Code;
            //}

            foreach (var t in Tbs)
            {
                foreach (var i in t)
                {
                    if (i.Id == accountIdForCode)
                    {
                        accountcode = i.Code;
                    }
                }
            }

            return accountcode;
        }

        public ItemGroupVM GetAllItems(Int32 itemGroupId)
        {

            var returnedItems = (from i in db.ItemGroups
                                 where i.ItemGroupId == itemGroupId
                                 select new ItemGroupVM
                                 {
                                     ItemGroupId = i.ItemGroupId,
                                     ItemGroupName = i.ItemGroupName,
                                     Properties = (from p in i.Properties
                                                   where p.ItemGroupId == i.ItemGroupId
                                                   orderby p.orderNum
                                                   select new PropertiesforItemGroupVM
                                                   {
                                                       propertyId = p.PropertiesforItemGroupId,
                                                       propertyName = p.PropertiesforItemGroupName,
                                                       Show = p.Show,
                                                       propertyValues = (from pv in p.PropertyValuesforItemGroup
                                                                         where pv.PropertiesforItemGroupId == p.PropertiesforItemGroupId
                                                                         select new PropertyValuesforItemGroupVM
                                                                         {
                                                                             propertyValueId = pv.PropertyValuesforItemGroupId,
                                                                             propertyId = p.PropertiesforItemGroupId,
                                                                             propertyValueName = pv.PropertyValuesforItemGroupName,
                                                                             value = pv.PropertyValuesforItemGroupValue,
                                                                             typeName = p.Type.TypeName
                                                                         }).ToList(),
                                                       Type = (new TypeforItemGroupVM { typeId = p.TypeforItemGroupId, typeName = p.Type.TypeName, show = p.Type.show, description = p.Type.description, propertyId = p.PropertiesforItemGroupId })

                                                   }).ToList(),
                                     Accounts = (from acc in i.Accounts
                                                 where acc.ItemGroupId == i.ItemGroupId
                                                 select new AccountsforItemGroupVM
                                                 {
                                                     AccountId = acc.AccountId,
                                                     accountName = acc.Account.AccountName,
                                                     code = "",
                                                     ItemGroupId = acc.ItemGroupId,

                                                 }).ToList(),
                                     CompanyId = i.CompanyId,
                                     Unit = (new UnitVM { UnitId = i.UnitId, UnitName = i.Unit.UnitName, UnitTypeId = i.Unit.UnitTypeId })

                                 }).SingleOrDefault();
            if (returnedItems != null)
            {
                foreach (var acc in returnedItems.Accounts)
                {
                    acc.code = GetAccountCode(returnedItems.CompanyId, acc.AccountId + "Ac").ToString();
                }
            }


            return returnedItems;
        }


    }
}
