﻿using Inventory.dl.Models;
using Microsoft.Owin.Security.DataHandler.Encoder;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Managers
{
    public static class ClientsStore
    {
        public static ConcurrentDictionary<string, Client> ClientsList = new ConcurrentDictionary<string, Client>();

        static ClientsStore()
        {
            // Should store clints values in db , to do "Mohamed Saad"
            ClientsList.TryAdd("f0fc04db181f4d53b1560851ebf3b23d",
                                new Client
                                {
                                    ClientId = "f0fc04db181f4d53b1560851ebf3b23d",
                                    Base64Secret = "2OsUy5WAjDDAxLB2QhyMGwWfFUMdObntkqaOetJ_HnA",
                                    Name = "Inventory.web"
                                });
        }

        public static Client AddClient(string name)
        {
            var clientId = Guid.NewGuid().ToString("N");
            //this key will be shared between the Authorization server and the Resource server(Clients) only
            var key = new byte[32];
            RNGCryptoServiceProvider.Create().GetBytes(key);
            var base64Secret = TextEncodings.Base64Url.Encode(key);

            Client newClient = new Client { ClientId = clientId, Base64Secret = base64Secret, Name = name };
            ClientsList.TryAdd(clientId, newClient);
            return newClient;
        }

        public static Client FindClient(string clientId)
        {
            Client Client = null;
            if (ClientsList.TryGetValue(clientId, out Client))
            {
                return Client;
            }
            return null;
        }
    }
}
