﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Utility
{
    public class GeneralReturnResult
    {
        public GeneralReturnResult()
        {
          Succeeded = true;
          errs = new List<string>();
        }


        private List<string> errs;
        private bool Succeeded { get; set; }
        public bool IsValid { get { return Succeeded; }  }
        public string success { get; set; }
        public object obj { get; set; }
        public IList<object> objs { get; set; }

        //private IList<Tuple<string, object>> Objects;
        public void addError(string err)
        {
            Succeeded = false;
            errs.Add(err);
        }
        public void addRangeErrors(List<string> errs)
        {
            errs.AddRange(errs);
        }
        public string[] GetErrs()
        {
            return errs.ToArray();
        }
        public string GetErrsAsCommaSeparated()
        {
            return errs.Aggregate((a, b) => a + ',' + b);
        }

        public string GetErrsAsHtmlNewLine()
        {
            return errs.Aggregate((a, b) => a + "<br>" + b);
        }

        public void AddMainObject(object obj)
        {
            //this.Objects.Add(Tuple.Create(obj.GetType().Name, obj));
        }
        public void AddRangeObjects(List<object> objs)
        {
            foreach (var obj in objs)
            {
                //this.Objects.Add(Tuple.Create(obj.GetType().Name, obj));
            }

        }

        public object GetMainObject()
        {
            return this.obj;
        }
        public IEnumerable<Tuple<string, object>> GetObjects()
        {
            return null;
        }

        public IEnumerable<object> GetObjectByType(object obj)
        {
            return null; //this.Objects.Where(a => a.Item1 == obj.GetType().Name).DefaultIfEmpty(null).Select(b => b.Item2);
        }

        public void DbEntityValidationExceptionEX(DbEntityValidationException ex)
        {
          
            var err = ex.EntityValidationErrors.SelectMany(m => m.ValidationErrors)
                          .Select(e => e.ErrorMessage)
                          .ToList();
            addError("لا يمكن اتمام العملية لعدم اكتمال البيانات التالية :");
            this.addRangeErrors(err);
        }
    }
}
