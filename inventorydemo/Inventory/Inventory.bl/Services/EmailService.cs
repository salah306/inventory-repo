﻿using Inventory.bl.DTO;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Services
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri("https://api.mailgun.net/v3"),
                Authenticator = new HttpBasicAuthenticator("api", "key-472341d43b68ad04fcc49d8cf83fc2bb")
            };
            var request = new RestRequest();
            request.AddParameter("domain", "egymart.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "Egy Mart <mohamed.saad@egymart.com>");
            request.AddParameter("to", message.Destination);
            request.AddParameter("subject", message.Subject);
            request.AddParameter("text", message.Body);
            request.AddParameter("html", message.Body);
            request.Method = Method.POST;
            var response = await client.ExecuteTaskAsync(request);
            int sc = (int)response.StatusCode;
            if (response.StatusCode != HttpStatusCode.OK)
            {

            }
        }

    }


    public static class ClientService
    {
        public static void sendToClient(ClientData message)
        {
            var client = new RestClient
            {
                BaseUrl = new Uri("/api/Clint"),

            };


            var request = new RestRequest("/resource/", Method.POST);
            var jsonTosend = JsonConvert.SerializeObject(message);
            request.AddParameter("application/json; charset=utf-8", jsonTosend, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;
            var response = client.Execute(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {

            }

        }
    }

    public class ClientData
    {
        public string url { get; set; }
    }

    public class EmailTemplateManger
    {
        public string actionTemplate(EmailActionTempModelDTO temp)
        {


            var callbackUrl = temp.callbackUrl;
            var msgTilte = temp.Tilte;
            var msgBody = temp.bodyMsg;
            var linkValue = temp.btnlinkValue;

            string msg = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\" style=\"font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">" +
            "<head>" +
            "<meta name=\"viewport\" content=\"width=device-width\" />" +
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />" +
            "<title>Actionable emails e.g. reset password</title>" +
            "<style type=\"text/css\">" +
            "img {" +
            "max-width: 100%;" +
            "}" +
            "body {" +
            "-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em;" +
            "}" +
            "body {" +
            "background-color: #f6f6f6;" +
            "}" +
            "@media only screen and (max-width: 640px) {" +
            "  body {" +
            "    padding: 0 !important;" +
            "  }" +
            "  h1 {" +
            "    font-weight: 800 !important; margin: 20px 0 5px !important;" +
            "  }" +
            "  h2 {" +
            "    font-weight: 800 !important; margin: 20px 0 5px !important;" +
            "  }" +
            "  h3 {" +
            "    font-weight: 800 !important; margin: 20px 0 5px !important;" +
            "  }" +
            "  h4 {" +
            "    font-weight: 800 !important; margin: 20px 0 5px !important;" +
            "  }" +
            "  h1 {" +
            "    font-size: 22px !important;" +
            "  }" +
            "  h2 {" +
            "    font-size: 18px !important;" +
            "  }" +
            "  h3 {" +
            "    font-size: 16px !important;" +
            "  }" +
            "  .container {" +
            "    padding: 0 !important; width: 100% !important;" +
            "  }" +
            "  .content {" +
            "    padding: 0 !important;" +
            "  }" +
            "  .content-wrap {" +
            "    padding: 10px !important;" +
            "  }" +
            "  .invoice {" +
            "    width: 100% !important;" +
            "  }" +
            "}" +
            "</style>" +
            "</head>" +
            "<body itemscope itemtype=\"http://schema.org/EmailMessage\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;\" bgcolor=\"#f6f6f6\">" +
            "<table class=\"body-wrap\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\" bgcolor=\"#f6f6f6\"><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>" +
            "		<td class=\"container\" width=\"600\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\" valign=\"top\">" +
            "			<div class=\"content\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\">" +
            "				<table class=\"main\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" itemprop=\"action\" itemscope itemtype=\"http://schema.org/ConfirmAction\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px solid #e9e9e9;\" bgcolor=\"#fff\"><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\" valign=\"top\">" +
            "							<meta itemprop=\"name\" content=\"Confirm Email\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\" /><table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">" +
            "										" + msgTilte + "" +
            "									</td>" +
            "								</tr><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">" +
            "										" + msgBody + "" +
            "									</td>" +
            "								</tr><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" itemprop=\"handler\" itemscope itemtype=\"http://schema.org/HttpActionHandler\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">" +
            "										<a href=\"" + callbackUrl + "\" class=\"btn-primary\" itemprop=\"url\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">" + linkValue + "</a>" +
            "									</td>" +
            "								</tr><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\" valign=\"top\">" +
            "										&mdash; EgyMart IT" +
            "									</td>" +
            "								</tr></table></td>" +
            "					</tr></table><div class=\"footer\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\">" +
            "					<table width=\"100%\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"aligncenter content-block\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;\" align=\"center\" valign=\"top\">Follow <a href=\"https://twitter.com/egymart\" style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; color: #999; text-decoration: underline; margin: 0;\">@egymart</a> on Twitter.</td>" +
            "						</tr></table></div></div>" +
            "		</td>" +
            "		<td style=\"font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\" valign=\"top\"></td>" +
            "	</tr></table></body>" +
            "</html>";
            return msg;
        }
    }
}
