﻿using Inventory.dl;
using Microsoft.AspNet.Identity;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.bl.Validators
{
   
    public class ValidationSubAccountbyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.SubAccounts.Where(a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.SubAccountId == accId && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }

    
}
