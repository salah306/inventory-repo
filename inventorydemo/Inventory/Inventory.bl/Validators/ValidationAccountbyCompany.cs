﻿using Inventory.dl;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.bl.Validators
{
    public class ValidationAccountbyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.AccountId == accId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }
}
