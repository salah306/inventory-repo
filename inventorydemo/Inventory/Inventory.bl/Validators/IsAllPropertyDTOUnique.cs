﻿using Inventory.bl.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.bl.Validators
{
    public class IsAllPropertyDTOUnique : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList<PropertyDTO>;
            var AllUnique = list.GroupBy(x => x.propertyName).All(g => g.Count() == 1);
            return AllUnique;
        }
    }
}
