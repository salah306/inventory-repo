﻿using Inventory.dl;
using Microsoft.AspNet.Identity;

using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.bl.Validators
{
    public class ValidationAccountCatogerybyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.AccountCategoryId == accId).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }
}
